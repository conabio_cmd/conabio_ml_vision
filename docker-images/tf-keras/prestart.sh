#!/usr/bin/env sh

DEFAULT_MODULE_PATH=/LUSTRE/users/ecoinf_admin/conabio_ml_vision
DEFAULT_GIT_BRANCH=dev

MODULE_PATH=${MODULE_PATH:-$DEFAULT_MODULE_PATH}
GIT_BRANCH=${GIT_BRANCH:-$DEFAULT_GIT_BRANCH}

if [ -d "$MODULE_PATH" ]; then
    echo "Module path: $MODULE_PATH"
    cd $MODULE_PATH
else
    apt-get update -y && apt-get install -y git
    MODULE_PATH=/lib/conabio_ml_vision
    echo "Module path does not exist, then using: $MODULE_PATH"
    cd /lib
    git clone -b ${GIT_BRANCH} --recurse-submodules https://bitbucket.org/conabio_cmd/conabio_ml_vision.git
    # cd $MODULE_PATH && git submodule update --remote
fi

pip install --upgrade pip && pip install -r $MODULE_PATH/conabio_ml/requirements.txt && pip install -r $MODULE_PATH/requirements.txt # && pip install -r docker-images/tf2/requirements.txt

export PYTHONPATH=$PYTHONPATH:${MODULE_PATH}:${MODULE_PATH}/conabio_ml
export ML=/LUSTRE/users/ecoinf_admin/conabio_ml_vision/
export LOGGING_LEVEL=INFO

if [ ! -z "${PIP_INSTALL}" ]; then
    pip install $PIP_INSTALL
fi
if [ ! -z "${APT_INSTALL}" ]; then
    apt-get update -y && apt-get install -y $APT_INSTALL
fi
if [ ! -z "${ADD_TO_PYTHONPATH}" ]; then
    export PYTHONPATH=$PYTHONPATH:$ADD_TO_PYTHONPATH
fi

exec "$@"
