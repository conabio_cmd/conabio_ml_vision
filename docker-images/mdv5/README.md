## Build image
```
docker build -t conabio_cd/conabio_ml_vision:mdv5 .
```

## Run container
```
docker run -it --gpus all --name VISION_MDV5 -v /home/ubuntu:/LUSTRE/users/ecoinf_admin conabio_cd/conabio_ml_vision:mdv5 bash
```

**En caso de levantar en el cluster**

Actualmente en el cluster no se puede usar las tarjetas gráficas por lo que 
es necesario desactivarlas en el contenedor
```
docker run -it --name VISION_MDV5 -v /LUSTRE/users/ecoinf_admin/:/LUSTRE/users/ecoinf_admin/ -p 3335:3335 -e CUDA_VISIBLE_DEVICES='' conabio_cd/conabio_ml_vision:mdv5 bash
```

En caso de si querer usar las tarjetas gráficas se puede lanzar el contenedor
como 

```
docker run -it --gpus all --name VISION_MDV5 -v /LUSTRE/users/ecoinf_admin/:/LUSTRE/users/ecoinf_admin/ -p 3335:3335 -e CUDA_VISIBLE_DEVICES='' conabio_cd/conabio_ml_vision:mdv5 bash
```

Después, actualizar variables e instalar paqueterias auxiliares
```
export PYTHONPATH=/lib/megadetector/cameratraps:/lib/megadetector/ai4eutils:/lib/megadetector/yolov5:/LUSTRE/users/ecoinf_admin/conabio_ml_vision:/LUSTRE/users/ecoinf_admin/conabio_ml_vision/conabio_ml
export ML=/LUSTRE/users/ecoinf_admin/conabio_ml_vision
apt-get update && apt-get install vim -y
source ${ML}/env_constants.sh
```


## Test `run_detector`
```
python3 detection/run_detector.py ../../md_v5a.0.0.pt --image_file c1dee237e51e11c3b4e506a9ef169eba_medium.jpg --threshold 0.2
```

## Test 2 `run_detector`
```bash
MODEL_PATH=/lib/megadetector/cameratraps/md_v5a.pt
TEST_IMG=/lib/megadetector/cameratraps/test_img.jpg
TEST_IMG_RES=/lib/megadetector/cameratraps/test_img_detections.jpg
wget -O ${TEST_IMG} 'https://monitoreo.conabio.gob.mx/snmb_pics/110139/2017_06/fotos_videos/thumbs_medium/4b6c12bf6bbedfb9e04197d73b42a5c9_medium.jpg'
wget -O ${MODEL_PATH} 'https://github.com/microsoft/CameraTraps/releases/download/v5.0/md_v5a.0.0.pt'
cd /lib/megadetector/cameratraps
python3 detection/run_detector.py ${MODEL_PATH} --image_file ${TEST_IMG} --threshold 0.2
cmp --silent ${TEST_IMG} ${TEST_IMG_RES} || echo "MDv5 is working!"
```
