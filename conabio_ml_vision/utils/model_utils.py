import os
import sys
import numpy as np
import time
from collections import defaultdict
from shutil import rmtree

from conabio_ml.utils.logger import get_logger
from conabio_ml_vision.datasets import ImagePredictionDataset

logger = get_logger(__name__)

def set_label_and_score_for_item_in_ensemble(item: str,
                                             models_to_dfs: dict,
                                             item_to_lbls_scrs: dict,
                                             model_weights: dict):
    """Assigns for `item` the score that corresponds to each label according to the predictions of
    the models in `models_to_dfs` and the weights in `model_weights`, and stores them in
    `item_to_lbls_scrs`.

    Parameters
    ----------
    item : str
        Item to be analysed. It must be contained in the column "item" of each value (`DataFrame`)
        of `models_to_dfs`
    models_to_dfs : dict
        Dictionary containing the results of the predictions made by the models,
        in the form: {`model_name`: `model_preds`, ...}
    item_to_lbls_scrs : dict
        Dictionary in which the results will be inserted,
        in the form: {`item`: [{`label_i`: `score_i`}, ...]}
    model_weights : dict
        Dictionary containing the weights of each model, which will multiply the probabilities of
        each of its predictions
    """
    label_to_scores = defaultdict(list)
    for model, df in models_to_dfs.items():
        for _, crop in df[df["item"] == item].iterrows():
            score = crop['score']
            score *= model_weights[model]
            label_to_scores[crop["label"]].append(score)
    label_to_mean = {}
    for label, scores in label_to_scores.items():
        label_to_mean[label] = np.sum(np.array(scores))
    for lbl, scor in dict(sorted(label_to_mean.items(), key=lambda x: x[1], reverse=True)).items():
        item_to_lbls_scrs[item] = {'label': lbl,
                                   'score': scor}
        break


def get_partitioned_file_path(num_tasks: int,
                              task_num: int,
                              file_path: str) -> str:
    """Function that obtains the path to be used to split the results of parallel processing,
    which can then be merged into the original `file_path` file.

    Parameters
    ----------
    num_tasks : int
        Number of tasks into which the result will be split
    task_num : int
        Number of the current task, which must be between `[1, num_tasks]`
    file_path : str
        Path of the original file to be split

    Returns
    -------
    str
        Path to the file that will be used to store the intermediate results of the `task_num` task
    """
    file_dirname = os.path.dirname(file_path)
    file_basename = os.path.basename(file_path)
    file_name_wo_ext, file_ext = os.path.splitext(file_basename)
    split_name = f"split_{file_name_wo_ext}"
    split_dir_path = os.path.join(file_dirname, split_name)
    os.makedirs(split_dir_path, exist_ok=True)
    split_filename = f'{file_name_wo_ext}_{task_num}_of_{num_tasks}{file_ext}'
    split_file_path = os.path.join(split_dir_path, split_filename)

    return split_file_path


def set_cuda_dev_environ(num_gpus_per_instance: int, task_num: int):
    """Assigns the `CUDA_VISIBLE_DEVICES` environment variable to perform task number `task_num`
    execution when parallel processing is performed and there are `num_gpus_per_instance` GPUs
    in each instance

    Parameters
    ----------
    num_gpus_per_instance : int
        Number of GPUs in each instance on which parallel processing will be performed
    task_num : int
        Number of the current task, which must be between `[1, num_tasks]`
    """
    if num_gpus_per_instance is None:
        os.environ['CUDA_VISIBLE_DEVICES'] = "0"
    elif num_gpus_per_instance is not None and num_gpus_per_instance > 0:
        gpu_device = (task_num-1) % num_gpus_per_instance
        os.environ['CUDA_VISIBLE_DEVICES'] = f"{gpu_device}"
    else:
        os.environ['CUDA_VISIBLE_DEVICES'] = "-1"


def merge_splits_or_exit(partitioned_res_ds: ImagePredictionDataset,
                         partitioned_res_file: str,
                         num_tasks: int,
                         task_num: int,
                         exit_if_not_last: bool = True,
                         delete_splits_after_finish: bool = True):
    """Function that logs the `partitioned_res_ds` result of the `task_num` task in parallel
    processing and, if it is the last to finish, joins all results and optionally deletes all
    intermediate result files created by the tasks involved in parallel processing

    Parameters
    ----------
    partitioned_res_ds : ImagePredictionDataset
        Predictions resulting from the `task_num` task in parallel processing
    partitioned_res_file : str
        File in which the results of the `task_num` task will be stored.
    num_tasks : int
        Number of tasks into which the result will be split
    task_num : int
        Number of the current task, which must be between `[1, num_tasks]`
    exit_if_not_last : bool
        Whether or not to exit the program if it is not the last of the `num_tasks` to finish,
        by default True
    delete_splits_after_finish : bool
        Whether or not to delete all intermediate result files created by the tasks involved in
        parallel processing, by default True

    Returns
    -------
    ImagePredictionDataset
        Image prediction dataset with the union of all intermediate results of `num_tasks` in
        parallel processing. This is performed using the `ImagePredictionDataset.from_csv` method.
    """
    partitioned_res_ds.to_csv(dest_path=partitioned_res_file)
    split_dir_path = os.path.dirname(partitioned_res_file)
    split_filename = os.path.basename(partitioned_res_file)
    log_file = os.path.join(split_dir_path, "files.log")
    # TODO: Add exclusion checking
    with open(log_file, "a") as log:
        log.write(f"{split_filename}\n")
    with open(log_file, 'r') as log:
        lines = log.read()
    n_finished_tasks = len([x for x in lines.split('\n') if x])
    if n_finished_tasks < num_tasks:
        if exit_if_not_last:
            logger.debug(f"Exiting program after creating split {task_num} of {num_tasks}.")
            sys.exit()
        return
    logger.debug(f"Creation of {num_tasks} splits complete.")
    logger.debug(f"Sleeping for 20 seconds before joining the {num_tasks} splits.")
    # To ensure the correct creation of the CSV data
    time.sleep(20)
    # TODO: Add cases for other types of files (e.g., JSON files)
    detections_dataset = ImagePredictionDataset.from_csv(source_path=split_dir_path)
    if delete_splits_after_finish:
        rmtree(split_dir_path, ignore_errors=True)
        logger.debug(f"Folder {split_dir_path} was deleted")

    return detections_dataset
