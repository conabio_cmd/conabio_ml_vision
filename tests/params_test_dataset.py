import os

RANDOM_STATE = 892812
results_path = os.path.join('results', 'test_datasets')
images_path = os.path.join(results_path, 'images_test1')
partitioned_images_path = os.path.join(results_path, 'partitioned_images_test1')
csv_path = os.path.join(results_path, 'dataset_test1.csv')
csv_img_level_path = os.path.join(results_path, 'dataset_img_level_test1.csv')
csv_partitions_path = os.path.join(results_path, 'dataset_partitions.csv')
to_folder_path = os.path.join(results_path, 'to_folder_images_test1')
crops_path = os.path.join(results_path, 'crops_test1')
cats_1 = ['mazama pandora', 'gallus gallus', 'tinamus major', 'felis silvestris']
exclude_cats_1 = ['bos taurus', 'Urocyon cinereoargenteus', 'Equus caballus']

STATE_COL = 'state'
MUNICIPALITY_COL = 'municipality'
