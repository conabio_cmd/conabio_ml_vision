from math import floor, ceil

from conabio_ml.utils.utils import is_array_like
from conabio_ml.utils.logger import get_logger

logger = get_logger(__name__)


class COORDINATES_FORMATS():
    """Valid coordinate formats, either [x, y, width, height], [x1, y1, x2, y2] and
    [y1, x1, y2, x2]
    """
    X_Y_WIDTH_HEIGHT = "x_y_width_height"
    X1_Y1_X2_Y2 = "x1_y1_x2_y2"
    Y1_X1_Y2_X2 = "y1_x1_y2_x2"
    VALUES = [X_Y_WIDTH_HEIGHT, X1_Y1_X2_Y2, Y1_X1_Y2_X2]


class COORDINATES_TYPES():
    """Valid coordinate types, either `absolute`, `relative` or `normalized` (the same as
    `relative`)
    """
    # Values given in pixels; does not depend on image size
    ABSOLUTE = "absolute"
    # Values given in a floating number in [0,1] and depends on the size of the image
    RELATIVE = "relative"
    # All values of the coordinates are zero or any of them are NaN
    INVALID = "invalid"
    # The same as relative
    NORMALIZED = RELATIVE
    VALUES = [ABSOLUTE, RELATIVE, INVALID, NORMALIZED]


class COORDINATES_DATA_TYPES():
    """Valid coordinate data types, either `string`, `tuple`, `array` or `list` (the same as
    `array`)
    """
    STRING = "string"
    TUPLE = "tuple"
    ARRAY = "array"
    LIST = ARRAY
    VALUES = [STRING, LIST, ARRAY]


def get_bbox_from_dataset_row(row,
                              output_format=COORDINATES_FORMATS.X_Y_WIDTH_HEIGHT,
                              output_coords_type=COORDINATES_TYPES.ABSOLUTE,
                              output_data_type=COORDINATES_DATA_TYPES.TUPLE,
                              image_width=None,
                              image_height=None):
    """Function to convert between coordinate formats, from a detection of a prediction dataset

    Parameters
    ----------
    row : pandas.Series
        Row containing an object detection record of a dataset
    output_format : COORDINATES_FORMATS, optional
        Determine the order and meaning of the elements of the output,
        by default COORDINATES_FORMATS.X_Y_WIDTH_HEIGHT
    output_coords_type : `COORDINATES_TYPES`, optional
        Determines if the value of the output coordinates will be given in absolute pixel values,
        or normalized in relation to the image size and therefore in the range [0,1].
        By default COORDINATES_TYPES.ABSOLUTE
    output_data_type : `COORDINATES_DATA_TYPES`, optional
        Determines the data type of the output, by default COORDINATES_DATA_TYPES.TUPLE
    image_width : int, optional
        Image width value, given in pixels, by default None
    image_height : int, optional
        Image height value, given in pixels, by default None

    Returns
    -------
    list, tuple or str
        Coordinates resulting from the conversion, given in the format and type of coordinates and
        in the type of data requested
    """
    if not 'bbox' in row:
        raise ValueError("In order to get bbox, a detection dataset is needed")
    return transform_coordinates(bbox=row['bbox'],
                                 input_format=COORDINATES_FORMATS.X_Y_WIDTH_HEIGHT,
                                 output_format=output_format,
                                 output_coords_type=output_coords_type,
                                 output_data_type=output_data_type,
                                 image_width=image_width,
                                 image_height=image_height)


def get_coordinates_type_from_coords(coord1, coord2, coord3, coord4):
    """Determines if the set of coordinates are absolute or relative, according to their value

    Parameters
    ----------
    coord1 : int or float
        Value of the first coordinate
    coord2 : int or float
        Value of the second coordinate
    coord3 : int or float
        Value of the third coordinate
    coord4 : int or float
        Value of the fourth coordinate

    Returns
    -------
    str
        Either `COORDINATES_TYPES.RELATIVE` ('relative') or
        `COORDINATES_TYPES.ABSOLUTE` ('absolute')
    """
    if all([i == 0. for i in [coord1, coord2, coord3, coord4]]):
        return COORDINATES_TYPES.INVALID
    if all([0. <= i <= 1. for i in [coord1, coord2, coord3, coord4]]):
        return COORDINATES_TYPES.RELATIVE
    return COORDINATES_TYPES.ABSOLUTE


def transform_coordinates(bbox,
                          input_format=COORDINATES_FORMATS.X_Y_WIDTH_HEIGHT,
                          output_format=COORDINATES_FORMATS.Y1_X1_Y2_X2,
                          output_coords_type=COORDINATES_TYPES.ABSOLUTE,
                          output_data_type=COORDINATES_DATA_TYPES.ARRAY,
                          image_width=None,
                          image_height=None):
    """Function to convert between coordinate formats, specifying the input and output formats,
    the output coordinate types and the output data type

    Parameters
    ----------
    bbox : str or array-like
        Coordinates to be converted. It must contain four elements that have the meaning according
        to `input_format`. If str, elements must be separated by commas
    input_format : `COORDINATES_FORMATS`, optional
        Determine the order and meaning of the elements of `bbox`, by default
        COORDINATES_FORMATS.X_Y_WIDTH_HEIGHT
    output_format : `COORDINATES_FORMATS`, optional
        Determine the order and meaning of the elements of the output, by default
        COORDINATES_FORMATS.Y1_X1_Y2_X2
    output_coords_type : `COORDINATES_TYPES`, optional
        Determines if the value of the output coordinates will be given in absolute pixel values,
        or normalized in relation to the image size and therefore in the range [0,1].
        By default COORDINATES_TYPES.ABSOLUTE
    output_data_type : `COORDINATES_DATA_TYPES`, optional
        Determines the data type of the output, by default COORDINATES_DATA_TYPES.ARRAY
    image_width : int, optional
        Image width value, given in pixels, by default None
    image_height : int, optional
        Image height value, given in pixels, by default None

    Returns
    -------
    list, tuple or str
        Coordinates resulting from the conversion, given in the format and type of coordinates and
        in the type of data requested
    """
    image_width = float(image_width)
    image_height = float(image_height)
    if type(bbox) == str:
        [coord1, coord2, coord3, coord4] = [float(x) for x in bbox.split(',')]
    elif is_array_like(bbox):
        [coord1, coord2, coord3, coord4] = [x for x in bbox]
    else:
        raise ValueError(f"'{type(bbox)}' is not a valid type for a bounding box.")

    if input_format == COORDINATES_FORMATS.X_Y_WIDTH_HEIGHT:
        [x, y, width, height] = [coord1, coord2, coord3, coord4]
        [x1, y1, x2, y2] = [x, y, x+width, y+height]
    elif input_format == COORDINATES_FORMATS.X1_Y1_X2_Y2:
        [x1, y1, x2, y2] = [coord1, coord2, coord3, coord4]
    elif input_format == COORDINATES_FORMATS.Y1_X1_Y2_X2:
        [y1, x1, y2, x2] = [coord1, coord2, coord3, coord4]
    else:
        raise ValueError("Invalid input coordinates format.")

    input_coords_type = get_coordinates_type_from_coords(x1, y1, x2, y2)
    if input_coords_type == COORDINATES_TYPES.INVALID:
        raise ValueError(f"Coordinates are given in and invalid type: {x1, y1, x2, y2}")
    if (input_coords_type == COORDINATES_TYPES.ABSOLUTE and
            output_coords_type == COORDINATES_TYPES.RELATIVE):
        if image_width is None or image_height is None:
            raise ValueError("You must supply the image_width and image_height of image")
        x1, y1, x2, y2 = (x1 / image_width,
                          y1 / image_height,
                          x2 / image_width,
                          y2 / image_height)
    elif (input_coords_type == COORDINATES_TYPES.RELATIVE and
            output_coords_type == COORDINATES_TYPES.ABSOLUTE):
        if image_width is None or image_height is None:
            raise ValueError("You must supply the image_width and image_height of image")
        x1, y1, x2, y2 = (x1 * image_width,
                          y1 * image_height,
                          x2 * image_width,
                          y2 * image_height)

    if output_coords_type == COORDINATES_TYPES.ABSOLUTE:
        x1, y1, x2, y2 = (int(floor(x1)),
                          int(floor(y1)),
                          int(ceil(x2)),
                          int(ceil(y2)))

    if output_format == COORDINATES_FORMATS.X_Y_WIDTH_HEIGHT:
        coord1, coord2, coord3, coord4 = x1, y1, x2-x1, y2-y1
    elif output_format == COORDINATES_FORMATS.Y1_X1_Y2_X2:
        coord1, coord2, coord3, coord4 = y1, x1, y2, x2
    elif output_format == COORDINATES_FORMATS.X1_Y1_X2_Y2:
        coord1, coord2, coord3, coord4 = x1, y1, x2, y2
    else:
        raise ValueError("Invalid input output coordinates format.")

    if output_data_type == COORDINATES_DATA_TYPES.ARRAY:
        return [coord1, coord2, coord3, coord4]
    elif output_data_type == COORDINATES_DATA_TYPES.TUPLE:
        return (coord1, coord2, coord3, coord4)
    elif output_data_type == COORDINATES_DATA_TYPES.STRING:
        return ",".join([str(x) for x in [coord1, coord2, coord3, coord4]])
    else:
        raise ValueError("Invalid output data type for coordinates transform")


def rescale_bboxes(bbox, actual_img_size, new_img_size):
    """Rescale bounding boxes when images are resized

    Parameters
    ----------
    bbox : str
        Actual value of the bounding box. It must be a str in the form 'x1,y1,width,height'
    actual_img_size : dict or 2-tuple of integers
        Size of the image before the resizing, in the form {'width': width, 'height': height} or
        (width, height)
    new_img_size : dict or 2-tuple of integers
        Size of the image after the resizing, in the form {'width': width, 'height': height} or
        (width, height)

    Returns
    -------
    str
        The bounding box resized according to the new image size, in the form 'x1,y1,width,height'
    """
    if bbox == "":
        return ""

    [x1, y1, width, height] = [float(x) for x in bbox.split(',')]

    if type(actual_img_size) is dict:
        act_width = actual_img_size['width']
        act_height = actual_img_size['height']
    else:
        act_width = actual_img_size[0]
        act_height = actual_img_size[1]

    if type(new_img_size) is dict:
        new_width = new_img_size['width']
        new_height = new_img_size['height']
    else:
        new_width = new_img_size[0]
        new_height = new_img_size[1]

    x1 = int(x1 * new_width/act_width)
    y1 = int(y1 * new_height/act_height)
    width = int(width * new_width/act_width)
    height = int(height * new_height/act_height)

    return ','.join([str(int(x)) for x in [x1, y1, width, height]])


def non_max_supression(boxA, boxB, iou_threshold=0.5):
    """Determine if one box is inside the other or if the intersection on the union (IoU) of both
    is < `iou_threshold`

    Parameters
    ----------
    boxA : list of float
        Represents the coordinates of a bounding box, given in the format [x1, y1, x2, y2]
    boxB : list of float
        Represents the coordinates of a bounding box, given in the format [x1, y1, x2, y2]
    iou_threshold : float, optional
       Minimum value of the intersection on the union (IoU) to know if the two bounding boxes
       overlap , by default 0.5

    Returns
    -------
    bool
        `False` in case one box is inside the other or the two overlap
    """
    # bbox: x1, y1, x2, y2
    if is_insideof(boxA, boxB) or is_insideof(boxB, boxA):
        return False
    if bb_intersection_over_union(boxA, boxB) >= iou_threshold:
        return False
    return True


def is_insideof(boxA, boxB):
    """Determine if one box is inside the other from the coordinates of both

    Parameters
    ----------
    boxA : list of float
        Represents the coordinates of a bounding box, given in the format [x1, y1, x2, y2]
    boxB : list of float
        Represents the coordinates of a bounding box, given in the format [x1, y1, x2, y2]

    Returns
    -------
    bool
        `True` in case one bounding box is inside the other
    """
    # bbox: x1, y1, x2, y2
    x1A, y1A, x2A, y2A = boxA[0], boxA[1], boxA[2], boxA[3]
    x1B, y1B, x2B, y2B = boxB[0], boxB[1], boxB[2], boxB[3]

    return (
        x1A >= x1B and
        x2A <= x2B and
        y1A >= y1B and
        y2A <= y2B)


def bboxes_overlap(boxA, boxB):
    """Determine if two boxes overlap from the coordinates of both

    Parameters
    ----------
    boxA : list of float
        Represents the coordinates of a bounding box, given in the format [x1, y1, x2, y2]
    boxB : list of float
        Represents the coordinates of a bounding box, given in the format [x1, y1, x2, y2]

    Returns
    -------
    bool
        `True` in case the two boxes overlap
    """
    # bbox: x1, y1, x2, y2
    x1A, y1A, x2A, y2A = boxA[0], boxA[1], boxA[2], boxA[3]
    x1B, y1B, x2B, y2B = boxB[0], boxB[1], boxB[2], boxB[3]

    # If one rectangle is on the left side of other
    if x1A > x2B or x1B > x2A:
        return False
    # If one rectangle is above other
    if y1A > y2B or y1B > y2A:
        return False

    return True


def bb_intersection_over_union(boxA, boxB):
    """Compute the intersection over union (IoU) of two bounding boxes from the coordinates
    of both

    Parameters
    ----------
    boxA : list of float
        Represents the coordinates of a bounding box, given in the format [x1, y1, x2, y2]
    boxB : list of float
        Represents the coordinates of a bounding box, given in the format [x1, y1, x2, y2]

    Returns
    -------
    float
        The value of the intersection over union IoU of the two bounding boxes, and 0. if the boxes
        do not overlap
    """
    # bbox: x1, y1, x2, y2
    if not boxA or not boxB:
        return 0.

    if not bboxes_overlap(boxA, boxB):
        return 0.

    x1_A, y1_A, x2_A, y2_A = boxA[0], boxA[1], boxA[2], boxA[3]
    x1_B, y1_B, x2_B, y2_B = boxB[0], boxB[1], boxB[2], boxB[3]

    xA = max(x1_A, x1_B)
    yA = max(y1_A, y1_B)
    xB = min(x2_A, x2_B)
    yB = min(y2_A, y2_B)

    # compute the area of intersection rectangle
    interArea = (xB - xA) * (yB - yA)

    # compute the area of both the prediction and ground-truth
    # rectangles
    boxAArea = (x2_A - x1_A) * (y2_A - y1_A)
    boxBArea = (x2_B - x1_B) * (y2_B - y1_B)

    # compute the intersection over union by taking the intersection
    # area and dividing it by the sum of prediction + ground-truth
    # areas - the interesection area
    iou = interArea / float(boxAArea + boxBArea - interArea)

    # return the intersection over union value
    return iou
