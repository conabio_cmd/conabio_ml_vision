#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import cv2
from multiprocessing import Manager
from typing import List

from conabio_ml_vision.utils.images_utils import movement_detection_in_seqs
from conabio_ml.utils.utils import parallel_exec
from conabio_ml.utils.logger import get_logger

logger = get_logger(__name__)


def video_to_frames(input_video_file: str,
                    output_folder: str,
                    freq_sampling: int = None,
                    frame_numbers: List[int] = None,
                    time_positions: List[float] = None,
                    videos_data: dict = None,
                    overwrite: bool = False):
    """Create the image files by taking `freq_sampling` frames every second from the video file
    `input_video_file` and stores them in `output_folder`, saving in the dictionary `videos_data`
    the information related to the conversion

    Parameters
    ----------
    input_video_file : str
        Path of the video to be used for conversion
    output_folder : str
        Path to the directory where the created frames will be stored.
        Inside the folder the frames will be saved in the form `frame0000i.jpg`, where `i` is the
        number of the frame inside the video, i.e. `[1, cv2.CAP_PROP_FRAME_COUNT]`
    freq_sampling : int, optional
        If provided, it is the number of frames per second to be taken from each video.
        This parameter is mutually exclusive with `time_positions` and `frame_numbers`.
        By default None
    frame_numbers : list of int, optional
        If provided, it is a list containing the 1-based positions of the frames to be taken from
        the video.
        This parameter is mutually exclusive with `time_positions` and `freq_sampling`.
        By default None
    time_positions : list of float, optional
        If provided, it is a list containing the time positions in seconds to be taken from the
        video.
        This parameter is mutually exclusive with `frame_numbers` and `freq_sampling`.
        By default None
    videos_data : dict, optional
        Dictionary that will contain the information resulting from the conversion, in the form:
        `{input_video_file: {'frames_filenames': [frame_filename_0, ...],
        'frames_num_video': [frame_num_video_0, ...], 'fps': fps, 'frame_count': n_frames}}`,
        by default None
    overwrite : bool, optional
        Whether or not to overwrite the frames in case they have been previously created,
        by default False

    Returns
    -------
    tuple of ([str], int, int)
        A tuple with the values (frame_filenames, `cv2.CAP_PROP_FPS`, `cv2.CAP_PROP_FRAME_COUNT`),
        where the first element is the list with the paths of the created images and the other two
        are video properties
    """
    assert os.path.isfile(input_video_file), 'File {} not found'.format(input_video_file)

    os.makedirs(output_folder, exist_ok=True)

    vidcap = cv2.VideoCapture(input_video_file)
    n_frames = int(vidcap.get(cv2.CAP_PROP_FRAME_COUNT))
    fps = vidcap.get(cv2.CAP_PROP_FPS)
    if freq_sampling is not None:
        if freq_sampling > fps:
            freq_sampling = fps
        every_n_frames = round(fps / freq_sampling)

    frame_filenames = []
    frame_nums_video = []

    if time_positions is not None:
        frame_numbers = {round(time_pos * fps) + 1: time_pos for time_pos in time_positions}

    for frame_number in range(0, n_frames):
        if freq_sampling is not None:
            if frame_number % every_n_frames != 0:
                continue
        else:
            if frame_number + 1 not in frame_numbers:
                continue

        frame_filename = 'frame{:05d}.jpg'.format(frame_number + 1)
        frame_filename = os.path.join(output_folder, frame_filename)
        frame_filenames.append(frame_filename)
        frame_nums_video.append(frame_number + 1)

        if not overwrite and os.path.isfile(frame_filename):
            continue

        if time_positions is not None:
            vidcap.set(cv2.CAP_PROP_POS_MSEC, frame_numbers[frame_number + 1] * 1000)
        else:
            vidcap.set(cv2.CAP_PROP_POS_FRAMES, frame_number)
        success, image = vidcap.read()
        if not success:
            assert image is None
            break

        try:
            cv2.imwrite(os.path.normpath(frame_filename), image)
            assert os.path.isfile(frame_filename), f'Output frame {frame_filename} unavailable'
        except KeyboardInterrupt:
            vidcap.release()
            raise
        except Exception as e:
            print('Error on frame {} of {}: {}'.format(frame_number+1, n_frames, str(e)))

    vidcap.release()

    if videos_data is not None:
        videos_data[input_video_file] = {
            'frames_filenames': frame_filenames,
            'frames_num_video': frame_nums_video,
            'fps': int(fps),
            'frame_count': n_frames
        }

    return frame_filenames, fps, n_frames


def frames_to_video(images: List[str],
                    freq_sampling: int,
                    output_file_name: str,
                    force=False):
    """Create a video from the set of frames specified in `images`, assuming that the video was
    previously sampled with a sampling frequency `freq_sampling`

    Parameters
    ----------
    images : List[str]
        List with the paths of the frames from which the video will be created
    freq_sampling : int
        Number of frames per second that were taken from the video when the video was split into
        frames
    output_file_name : str
        Path of the video file to be created
    codec_spec : str, optional
        Code of the codec to be used in the constructor `cv2.VideoWriter_fourcc`, by default "mp4v"
    """
    if len(images) == 0 or (os.path.isfile(output_file_name) and not force):
        return

    os.makedirs(os.path.dirname(output_file_name), exist_ok=True)

    frame = cv2.imread(images[0])
    height, width, _ = frame.shape

    # Define the codec and create VideoWriter object
    codec_spec = "mp4v"
    fourcc = cv2.VideoWriter_fourcc(*codec_spec)
    out = cv2.VideoWriter(output_file_name, fourcc, freq_sampling, (width, height))

    for image in images:
        frame = cv2.imread(image)
        out.write(frame)

    out.release()


def filter_false_positive_detections(frames_ds,
                                     dets_ds,
                                     mov_detection_method: str = 'Acc',
                                     window_size: int = 7,
                                     frame_num_field: str = 'video_frame_num',
                                     group_frames_field: str = 'video_id'
                                     ):
    """Filter the detections in `dets_ds` from the images in `frames_ds` that have been extracted
    from a set of videos or sequences, discarding those in which no motion was detected by applying
    motion detection using the method defined in `mov_detection_method`, grouping the frames by the
    `group_frames_field` field and sorting them by the `frame_num_field` field

    Parameters
    ----------
    frames_ds : ImageDataset
        ImageDataset containing the frames of the videos or sequences.
        It must contain the `group_frames_field` and `frame_num_field` fields
    dets_ds : ImagePredictionDataset
        Dataset with the detections made on `frames_ds` and of which it will be determined which
        ones are false positives applying motion detection
    mov_detection_method : str, optional
        Method applied to perform motion detection; it can be: "Acc" (Background Subtraction)
        or "MoG" (Mixture of Gaussian), by default 'Acc'
    window_size : int, optional
        Size of the window around which the movement detection will be performed on the detections
        of each frame. If None, the analysis will be performed on all the frames of the sequence,
        by default 7
    frame_num_field : str, optional
        Name of the column in `dets_ds` that contains the sequential value of the frame within
        the sequence, by default 'video_frame_num'
    group_frames_field : str, optional
        Name of the column in `frames_df` and `dets_ds` by which the sequences will be grouped

    Returns
    -------
    ImagePredictionDataset
        Same dataset as `dets_ds` but without the detections that were considered as false
        positives by the motion detection method
    """
    assert mov_detection_method in ("Acc", "MOG"), f'mov_detection_method must be in (Acc, MOG)'
    logger.debug(
        f'Filtering false positive detections using the method "{mov_detection_method}" and a '
        f'window size of {str(window_size)}')

    frames_df = frames_ds.as_dataframe(
        sort_by=[group_frames_field, frame_num_field], sort_asc=True)
    dets_df = dets_ds.as_dataframe(
        sort_by=[group_frames_field, frame_num_field], sort_asc=True)
    seq_id_to_valid_dets_ids = Manager().dict()

    parallel_exec(func=movement_detection_in_seqs,
                  elements=dets_df[group_frames_field].unique(),
                  images_df=frames_df,
                  dets_df=dets_df,
                  mov_detection_method=mov_detection_method,
                  seq_id_to_valid_dets_ids=seq_id_to_valid_dets_ids,
                  seq_id=lambda elem: elem,
                  seqs_field=group_frames_field,
                  frames_field=frame_num_field,
                  window_size=window_size)

    valid_dets_ids = []
    for dets_ids in seq_id_to_valid_dets_ids.values():
        valid_dets_ids.extend(dets_ids)

    return dets_ds.filter_by_column('id', values=valid_dets_ids, mode='include', inplace=False)
