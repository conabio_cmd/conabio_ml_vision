import os
from typing import TypeVar
from typing import Tuple

from conabio_ml.datasets.dataset import Dataset
from conabio_ml.preprocessing import PreProcessing
from conabio_ml.utils.utils import parallel_exec, get_chunk
from conabio_ml.utils.logger import get_logger
from conabio_ml_vision.preprocessing.process_images import process_image
from conabio_ml_vision.preprocessing.process_images import apply_clahe_wb_to_image
from conabio_ml_vision.utils.coords_utils import rescale_bboxes

logger = get_logger(__name__)


class ImageProcessing(PreProcessing):
    """Class to apply preprocessing operations to images before entering the
    training and evaluation stages.
    """
    ImageProcessingType = TypeVar('ImageProcessingType', bound='ImageProcessing')

    @classmethod
    def get_type(cls):
        """
        Returns the type of this current class

        Returns
        -------
        ImageProcessingType
        """
        return cls.ImageProcessingType

    @staticmethod
    def resize(dataset: Dataset.DatasetType,
               size: Tuple[int, int]) -> Dataset.DatasetType:
        """Resize images of a dataset

        Parameters
        ----------
        dataset : ImageDataset
            Dataset that contains images to be resized.
        size : tuple of ints
            The dimensions (width, height) to which all images found will be resized

        Returns
        -------
        dict
            Dictionary with item to new size, in the form:
                {`item`: {"width": int, "height": int}, ...}
        """
        logger.info(f"Resizing images in dataset to (width, height): {size}")

        df = dataset.as_dataframe()
        new_size = {'width': size[0], 'height': size[1]}
        new_sizes = {
            image: new_size
            for image in df['item'].unique()
        }

        parallel_exec(
            func=process_image,
            elements=df['item'].unique(),
            source_path=lambda item: item,
            dest_path=None,
            size=(size[0], size[1]))

        flds = dataset.MEDIA_FIELDS
        act_sizes = dataset.get_images_sizes()
        if dataset.is_detection_dataset():
            dataset.data['bbox'] = (
                df.apply(lambda x: rescale_bboxes(
                    x['bbox'],
                    {
                        'width': act_sizes.loc[x['item']][flds.WIDTH],
                        'height': act_sizes.loc[x['item']][flds.HEIGHT]
                    },
                    new_size),
                    axis=1
                )
            )

        dataset.set_images_sizes(new_sizes)

        return dataset

    @staticmethod
    def apply_clahe_wb(dataset: Dataset,
                       images_dir_dest: str = None,
                       discard_exist: bool = True,
                       num_tasks: int = None,
                       task_num: int = None) -> Dataset:
        """Appy the CLAHE and SimpleWB pre-processing operations to the images of `dataset`
        and saves the resulting images in the path `images_dir_dest`

        Parameters
        ----------
        dataset : ImageDataset
            Dataset that contains images to be resized.
        images_dir_dest : str, optional
            Path of the resulting image. If None, it will be the same as `image_origin`.
            Default is None
        discard_exist : bool, optional
            Whether to discard images that already exists in `image_dest` or not, by default True
        num_tasks : int, optional
            In case of parallel processing (e.g., on several instances), this is the total number
            of tasks into which the processing is divided.
            By default None
        task_num : int, optional
            In case of parallel processing, this is the number of the current task.
            It must be an integer in the range `[1, num_tasks]`. By default None
        """
        images = dataset.get_unique_items()
        if num_tasks is not None and task_num is not None:
            images = get_chunk(images, num_tasks, task_num)
        logger.info(f"Applying CLAHE & Simple WB to {len(images)} images")

        imgs_dir = dataset.get_images_dir()

        def get_image_dest(img):
            if images_dir_dest is None:
                return None
            return os.path.join(images_dir_dest, os.path.relpath(img, imgs_dir))

        parallel_exec(
            func=apply_clahe_wb_to_image,
            elements=images,
            image_origin=lambda img: img,
            image_dest=get_image_dest,
            discard_exist=discard_exist)
