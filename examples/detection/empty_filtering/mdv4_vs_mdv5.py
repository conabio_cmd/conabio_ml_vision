#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import argparse
from numpy import linspace

from conabio_ml_vision.datasets import ImagePredictionDataset
from conabio_ml_vision.preprocessing.preprocess import ImageProcessing
from conabio_ml_vision.utils.aux_utils import get_success_vs_errors_ds
from conabio_ml_vision.utils.aux_utils import eval_binary_from_detections
from conabio_ml_vision.utils.scripts import visualize_detections_in_ds
from conabio_ml_vision.utils.evaluator_utils import get_image_level_binary_pred_dataset
from conabio_ml_vision.utils.aux_utils import balance_binary_dataset

import params
import utils_exps

"""Script that evaluates and compares the results of the binary classification animal/empty or
person/empty of the Megadetector v4 and v5 models for the SNMB dataset.
"""


def binary_eval(eval_dir, pos_label, pos_label_perc, md_version, filter_species):
    """Function that performs the binary evaluation at image level of the Megadetector version
    `md_version` on the set of SNMB images, for the positive class `pos_label` vs. the negative
    class formed by the rest of the classes, and that will be filtered according to
    `filter_species` and sampled according to the `pos_label_perc` provision.

    Parameters
    ----------
    eval_dir : str
        Folder where the results of the evaluations will be stored
    pos_label : str
        The positive class for the evaluation. Must be a str in ('animal', 'person')
    pos_label_perc : str
        String witht percents of the positive class for the evaluation
    md_version : str
        Versions of the Megadetector that are going to be used, separated by commas (e.g., 'v4,v5')
    filter_species : str or None
        Species used in the evaluation. Must be in ('only-photographable', 'non-photographable').
        If None, all species are going to be used.
    """
    pred_method = 'any_pos_label_pred' if pos_label == 'person' else 'pos_label_preds_mode'

    dataset_bin_true = utils_exps.get_dataset_bin_true(pos_label, filter_species)

    for md_ver in md_version.split(','):
        dets_ds = utils_exps.get_md_ds(md_version=md_ver)

        for pos_perc in utils_exps.get_pos_percs(pos_label_perc):
            pos_perc_str = f'{int(pos_perc*100)}'
            eval_dir_exp = os.path.join(eval_dir, f'MD{md_ver}', f'{pos_perc_str}_perc')

            ds_true = dataset_bin_true.copy()
            balance_binary_dataset(ds_true, pos_perc, random_state=params.RAND_STATE)

            eval_binary_from_detections(
                dets_ds,
                ds_true,
                eval_dir=eval_dir_exp,
                pos_label=pos_label,
                pred_method=pred_method,
                init_thres=params.INIT_THRES,
                end_thres=params.END_THRES,
                num_steps=params.NUM_STEPS
            )


def compare_mdv4_mdv5(results_dir,
                      pos_label,
                      pos_label_perc,
                      filter_species,
                      mdv4_thres,
                      mdv5_thres):
    """Function that allows you to see those detections in which one model made a mistake for the
    label `pos_label`, while the other one did it correctly. The models being compared are the 'v4'
    and 'v5' versions of the Megadetector.

    Parameters
    ----------
    results_dir : str
        Folder where the results of the comparison will be stored
    pos_label : str
        The positive class for the evaluation. Must be a str in ('animal', 'person')
    pos_label_perc : str
        String witht percents of the positive class for the evaluation
    filter_species : str or None
        Species used in the evaluation. Must be in ('only-photographable', 'non-photographable').
        If None, all species are going to be used.
    mdv4_thres : float
        Fixed value of threshold to be used for the detections of MDv4
    mdv5_thres : float
        Fixed value of threshold to be used for the detections of MDv5
    """
    pred_method = 'any_pos_label_pred' if pos_label == 'person' else 'pos_label_preds_mode'

    min_score_dets = min(mdv4_thres, mdv5_thres)
    for pos_perc in utils_exps.get_pos_percs(pos_label_perc):
        pos_perc_str = f'{int(pos_perc*100)}'

        ds_true = utils_exps.get_dataset_bin_true(pos_label, filter_species, image_level_ds=False)
        balance_binary_dataset(ds_true, pos_perc, random_state=params.RAND_STATE)

        mdv4_dets_ds = utils_exps.get_md_ds('v4', thresh=mdv4_thres)
        mdv5_dets_ds = utils_exps.get_md_ds('v5', thresh=mdv5_thres)
        dets_ds = ImagePredictionDataset.from_datasets(mdv4_dets_ds, mdv5_dets_ds)

        mdv4_ds_bin_pred = get_image_level_binary_pred_dataset(
            ds_true, mdv4_dets_ds, pred_method=pred_method, pos_label=pos_label)
        mdv5_ds_bin_pred = get_image_level_binary_pred_dataset(
            ds_true, mdv5_dets_ds, pred_method=pred_method, pos_label=pos_label)
        # These datasets have the labels: {'TP', 'FP', 'TN' 'FN'}
        mdv4_img_ds = get_success_vs_errors_ds(ds_true, mdv4_ds_bin_pred, pos_label)
        mdv5_img_ds = get_success_vs_errors_ds(ds_true, mdv5_ds_bin_pred, pos_label)

        # First take the incorrect MDv4 predictions and compare them with the correct MDv5
        # predictions, and then the opposite
        tuples = [(mdv4_img_ds, mdv5_img_ds, 'MDv4_incorrect'),
                  (mdv5_img_ds, mdv4_img_ds, 'MDv5_incorrect')]
        for incorrect_ds, correct_ds, exp_str in tuples:
            incorrect_df = incorrect_ds.as_dataframe()
            correct_df = correct_ds.as_dataframe()
            res_dir_exp = os.path.join(results_dir, f'{pos_perc_str}_perc', exp_str)
            images_w_bboxes_folder = os.path.join(res_dir_exp, 'images_w_bboxes')

            incorrect_img_ids_FN = incorrect_df[incorrect_df.label == 'FN'].image_id
            incorrect_img_ids_FP = incorrect_df[incorrect_df.label == 'FP'].image_id
            correct_img_ids_TP = correct_df[correct_df.label == 'TP'].image_id
            correct_img_ids_TN = correct_df[correct_df.label == 'TN'].image_id

            # The ids of the images that incorrect_ds predicted incorrectly as empty and correct_ds
            # predicted correctly as non-empty + the ids of the images that incorrect_ds predicted
            # incorrectly as non-empty and correct_ds predicted correctly as empty
            imgs_ids = list(
                (set(incorrect_img_ids_FN) & set(correct_img_ids_TP)) |
                (set(incorrect_img_ids_FP) & set(correct_img_ids_TN)))
            incorrect_filtered_ds = incorrect_ds.filter_by_column(
                'image_id', values=imgs_ids, mode='include', inplace=False)

            dets_to_show_ds = dets_ds.filter_by_column(
                'image_id', imgs_ids, mode='include', inplace=False)
            dets_to_show_ds.set_images_dir(params.snmb_images_dir)
            # MDv4 detections have neither width nor height
            _ = dets_to_show_ds.get_images_sizes()

            incorrect_filtered_ds.set_images_dir(params.snmb_images_dir)
            incorrect_filtered_ds.to_folder(
                images_w_bboxes_folder, split_in_labels=True, set_images_dir=True)
            # Set the items of dets_to_show_ds with those of
            # incorrect_filtered_ds (partitioned in TP, FP, TN and FN folders)
            img_id_to_item = incorrect_filtered_ds.as_dataframe().set_index('image_id')['item']
            dets_to_show_ds.set_anns_info_field_by_expr(
                'item', lambda row: img_id_to_item.loc[row['image_id']], inplace=True)
            ImageProcessing.resize(dets_to_show_ds, (800, 600))

            visualize_detections_in_ds(
                images_ds=incorrect_filtered_ds,
                detections=dets_to_show_ds,
                images_dir=images_w_bboxes_folder,
                dest_dir=images_w_bboxes_folder,
                min_score_dets=min_score_dets,
                include_score=True,
                colors_by_label=True)


def main():
    parser = argparse.ArgumentParser()

    parser.add_argument("--binary_eval", default=False, action="store_true")
    parser.add_argument("--compare_mdv4_mdv5", default=False, action="store_true")

    parser.add_argument('--pos_label', default='animal', type=str, choices=['animal', 'person'])
    parser.add_argument('--pos_label_perc', default='30,50', type=str)
    parser.add_argument('--md_version', default='v4,v5', type=str, choices=['v4,v5', 'v4', 'v5'])
    parser.add_argument('--filter_species', default=None, type=str,
                        choices=["only-photographables", "non-photographable", None])
    parser.add_argument('--mdv4_thres', default=0.2, type=float)
    parser.add_argument('--mdv5_thres', default=0.15, type=float)

    args = parser.parse_args()

    results_path = os.path.join('results', 'mdv4_mdv5_comparatives')
    if args.pos_label == 'person':
        spcs_str = 'person'
    else:
        spcs_str = args.filter_species or "all_species"

    if args.binary_eval:
        eval_dir = os.path.join(results_path, "binary_eval", spcs_str)
        os.makedirs(eval_dir, exist_ok=True)
        binary_eval(
            eval_dir=eval_dir,
            pos_label=args.pos_label,
            pos_label_perc=args.pos_label_perc,
            md_version=args.md_version,
            filter_species=args.filter_species)

    if args.compare_mdv4_mdv5:
        results_dir = os.path.join(results_path, "compare_mdv4_mdv5", spcs_str)
        os.makedirs(results_dir, exist_ok=True)
        compare_mdv4_mdv5(
            results_dir=results_dir,
            pos_label=args.pos_label,
            pos_label_perc=args.pos_label_perc,
            filter_species=args.filter_species,
            mdv4_thres=args.mdv4_thres,
            mdv5_thres=args.mdv5_thres)


if __name__ == "__main__":
    main()
