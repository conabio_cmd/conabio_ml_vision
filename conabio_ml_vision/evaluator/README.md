# Evaluación de modelos de clasificación

## Clasificación Binaria

En la clasificación binaria, generalmente tenemos dos clases, a menudo llamadas Positiva y Negativa, y tratamos de predecir la clase para cada muestra. Por ejemplo, si tenemos un conjunto de imágenes etiquetado de tal forma que nos permita saber cuáles imágenes contienen animales y cuáles no, y estamos interesados en detectar las fotos que contienen animales, en este caso, nuestra clase Positiva es la de las imágenes de animales y la clase Negativa es la de las imágenes que no contienen animales. En otras palabras, si una imagen de muestra contiene un animal, es un Positivo, si no es así, es Negativo. Nuestro clasificador predice, para cada foto, si es Positiva (P) o Negativa (N): ¿hay un animal en la foto?

### Matriz de confusión

La matriz de confusión es una de las maneras más intuitivas y fáciles que se utilizan para encontrar la exactitud y precisión del modelo. Se utiliza para el problema de clasificación donde la salida puede ser de dos o más clases. La matriz de confusión en sí misma no es una medida de rendimiento como tal, pero casi todas las métricas de rendimiento se basan en la matriz de confusión y los números que contiene.

#### Términos asociados con la matriz de confusión

- **Verdaderos Positivos (TP)**: Los verdaderos positivos son los casos en que la clase real de la muestra de datos era 1 (Verdadero) y la predicha también es 1 (Verdadero).
- **Falsos Positivos (FP)**: Los falsos positivos son los casos en que la clase real de la muetra de datos era 0 (Falso) y la predicha es 1 (Verdadero). Falso es porque el modelo ha predicho incorrectamente y positivo porque la clase pronosticada fue positiva (1).
- **Falsos negativos (FN)**: los falsos negativos son los casos en que la clase real de la muestra de datos era 1 (Verdadero) y la predicha es 0 (Falso). Falso es porque el modelo ha predicho incorrectamente y negativo porque la clase predicha fue negativa (0).
- **Verdaderos negativos (TN)**: los verdaderos negativos son los casos en que la clase real de la muestra de datos era 0 (Falso) y la predicha también es 0 (Falso).

Para la clasificación binaria, una matriz de confusión tiene dos filas y dos columnas, y muestra en la primera columna cuántas muestras positivas se predijeron como positivas (primera fila) o negativas (segunda fila), y en la segunda columna cuántas muestras negativas se predijeron como positivas (primera fila) o negativas (segunda fila). Por lo tanto, tiene un total de 4 celdas. Cada vez que nuestro clasificador hace una predicción, una de las celdas de la tabla se incrementa en uno, siguiendo los cuatro criterios listados arriba, y en la posición que le corresponda según la siguiente figura:

![img](http://www.conabio.gob.mx/snmb_files/documentation/evaluator/images/images/confusion_matrix.png)

Al final del proceso, podemos ver exactamente cómo se desempeñó nuestro clasificador.

### Métricas

#### Precisión

La precision intenta responder la siguiente pregunta: **¿Qué proporción de predicciones positivas fue realmente correcta?**
La precisión se define de la siguiente manera:

![img](http://www.conabio.gob.mx/snmb_files/documentation/evaluator/images/images/precision_formula.png)

#### Recall
El recall intenta responder la siguiente pregunta: **¿Qué proporción de elementos positivos se identificó correctamente?**
Matemáticamente, el recall se define de la siguiente manera:

![img](http://www.conabio.gob.mx/snmb_files/documentation/evaluator/images/images/recall_formula.png)

Lo anterior se puede visualizar en el siguiente diagrama:

![img](http://www.conabio.gob.mx/snmb_files/documentation/evaluator/images/images/precision_recall_venn_diagram.png)

En general, preferimos clasificadores con buena precisión y buen recall. Sin embargo, existe una compensación entre la precisión y el recall: al ajustar un clasificador, mejorar la precisión a menudo resulta en penalizar el recall y viceversa. Debido a esto, según el tipo de problema se le podrá dar más relevancia a un aspecto u otro, dependiendo si es más importante tener más predicciones correctas o encontrar la mayoría de los elementos de la clase positiva. Por ejemplo, en un sistema de monitoreo de fauna silvestre, podría ser más importante encontrar la mayor cantidad de imágenes con animales, aunque muchas de las detecciones contengan imágenes sin animales.

#### Score-F1
A veces es deseable contar con una métrica que contenga la información de la precisión y el recall de tal manera que se puedan comparar dos modelos directamente. Una forma de obtener esta métrica es a través de la media aritmética de la precisión y el recall. Sin embargo, esta tiene el problema de que no representa bien el desempeño de modelos para datasets con una clase mucho mayor que la otra y que siempre genere clasificaciones de la clase mayor. Por el contrario, la media armónica da mayor peso a los números más pequeños, y penalizará más, por ejemplo, un modelo con una alta precisión pero un recall muy bajo.

El score-F1 se basa en la media armónica y se define de la siguiente manera:

![img](http://www.conabio.gob.mx/snmb_files/documentation/evaluator/images/images/f1_formula.png)

Y en general, el score-F se define para un β real positivo en el cual se le da β veces más importancia a la precisión que al recall:

![img](http://www.conabio.gob.mx/snmb_files/documentation/evaluator/images/images/fbeta_formula.png)

En este sentido, el score-F1 es un caso especial del score-F donde β tiene un valor de 1.

#### Accuracy
La exactitud (Accuracy) en los problemas de clasificación es el número de predicciones correctas realizadas por el modelo sobre todo tipo de predicciones realizadas.

![img](http://www.conabio.gob.mx/snmb_files/documentation/evaluator/images/images/accuracy_formula.png)

Esta métrica solo debe usarse para evaluar datasets que están más o menos balanceados, y nunca cuando exista una clase que tenga la mayoría de elementos.

## Clasificación multiclase
Los problemas de clasificación binaria se centran en una clase positiva que queremos detectar. Por el contrario, en un típico problema de clasificación multiclase, debemos clasificar cada muestra en una de N clases diferentes. Por ejemplo, podríamos querer clasificar una foto que contiene animales en alguna de varias categorías diferentes, que podrían ser parte de una jerarquía taxonómica.

Para este tipo de modelos se suele realizar el cálculo de las métricas descritas en la sección Clasificación binaria para cada una de las categorías, esto es, tomando una categoría como la clase positiva, y la suma de todas las demás como la clase negativa.

Por ejemplo, supongamos un modelo que trata de clasificar fotos que tienen animales de las siguientes clases: Mamíferos, Peces y Aves. Nuestro clasificador tiene que predecir qué animal se muestra en cada foto. Este es un problema de clasificación con N=3 clases.

Supongamos que la siguiente es la matriz de confusión después de clasificar 25 fotos:

![img](http://www.conabio.gob.mx/snmb_files/documentation/evaluator/images/images/confusion_matrix_multiclass_ex1.png)

De manera similar al caso binario, podemos definir precisión y recall para cada una de las clases. Por ejemplo, la precisión para la clase Mamífero es el número de fotos de mamíferos predichas correctamente (4) de todas las fotos de mamíferos predichas (4 + 3 + 6 = 13), lo que equivale a 4/13 = 30.8%. Entonces, solo alrededor de un tercio de las fotos que nuestro predictor clasifica como Mamífero son en realidad de mamíferos.
Por otro lado, el recall para la clase Mamífero es el número de fotos de mamíferos (4) pronosticadas correctamente del número de fotos de mamíferos reales (4 + 1 + 1 = 6), que es 4/6 = 66.7%. Esto significa que nuestro clasificador clasificó 2/3 de las fotos de mamíferos como Mamífero.

![img](http://www.conabio.gob.mx/snmb_files/documentation/evaluator/images/images/confusion_matrix_multiclass_ex2.png)![img](http://www.conabio.gob.mx/snmb_files/documentation/evaluator/images/images/confusion_matrix_multiclass_ex3.png)![img](http://www.conabio.gob.mx/snmb_files/documentation/evaluator/images/images/confusion_matrix_multiclass_ex4.png)

De manera similar, podemos calcular la precisión y el recall de las otras dos clases: Peces y Aves. Para Peces, los números son 66.7% y 20.0% respectivamente. Para Aves, el número tanto de precisión como de recall es del 66,7%.
Los valores de score-F1 para cada categoría son: Mamífero = 42.1%, Peces = 30.8% y Aves = 66.7%.
Todas estas métricas se resumen en la siguiente tabla:

![img](http://www.conabio.gob.mx/snmb_files/documentation/evaluator/images/images/table_multiclass_ex1.png)

### Macro-promedio

Ahora que tenemos los valores de las métricas para cada categoría, el siguiente paso es combinarlas para obtener las métricas para todo el clasificador. Hay varias formas de hacer esto y aquí analizaremos las dos más utilizadas. Empecemos con la más sencilla: calcular la media aritmética de la precisión y el recall y a partir de ellas el score-F1 global. Esta es llamada el Macro-promedio de la métrica, y se calcula:

![img](http://www.conabio.gob.mx/snmb_files/documentation/evaluator/images/images/macro_averaging_formula.png)

Una variante para calcular el **Macro-F1** que se suele preferir al evaluar datasets desbalanceados es con la media aritmética de los score-F1 individuales de todas las clases, ya que se ha demostrado<sup>[1]</sup> que el otro método es excesivamente "benevolente" hacia clasificadores muy sesgados y puede dar valores engañosamente altos cuando se están evaluando este tipo de datasets, y ya que se suele usar el Macro-F1 con la intención de asignar igual peso a la clase más frecuente y a la menos frecuente, se recomienda utilizar esta última definición, que es significativamente más robusta hacia la distribución de este tipo de error.

### Micro-promedio

Otra forma de realizar el promedio de las métricas es con el micro-promedio, para el cuál se deben tomar todas las muestras juntas para hacer el cálculo de la precisión y el recall, y después con estos valores hacer el cálculo del score-F:

![img](http://www.conabio.gob.mx/snmb_files/documentation/evaluator/images/images/micro_averaging_formula.png)

Para calcular la **micro-precisión** tenemos que sumar el total de TP y dividirlo entre el total de predicciones positivas. Para nuestro ejemplo, sumamos el total de elementos en la diagonal (color verde) para obtener TP=12. Después sumamos los falsos positivos, es decir, los elementos fuera de la diagonal, y obtenemos FP=13. Nuestra precisión es, por lo tanto, 12 / (12 + 13) = 48.0%.
Para calcular el **micro-recall** tenemos que sumar el total de falsos negativos (FN) para todas las clases, que al igual que para los FP, consiste en sumar todos los elementos fuera de la diagonal, con lo que se obtendría nuevamente FN=13 y el valor del micro-recall también es 12 / (12 + 13) = 48.0%.
Dado que el micro-recall es igual a la micro-precisión, entonces la media armónica, y por lo tanto el **micro-F1**, también será igual a este valor. Por tanto, en general se puede decir que:

![img](http://www.conabio.gob.mx/snmb_files/documentation/evaluator/images/images/micro_averaging_formula_equals.png)

# Evaluación de modelos de detección

## Definiciones

### Intersection Over Union (IOU)

Intersection Over Union (IOU) es una medida basada en el índice Jaccard que evalúa la superposición entre dos recuadros (bounding boxes). Requiere un recuadro verdadero (ground truth) ![](http://latex.codecogs.com/gif.latex?B_%7Bgt%7D) y un recuadro de una predicción ![](http://latex.codecogs.com/gif.latex?B_p). Al aplicar el IOU podemos saber si una detección es válida (Verdadero Positivo) o no (Falso Positivo).
El IOU viene dado por el área de solapamiento entre el recuadro de la predicción y el recuadro verdadero dividida por el área de unión entre ellos:

![img](http://www.conabio.gob.mx/snmb_files/documentation/evaluator/images/images/iou_formula.gif)

La imagen debajo muestra el IOU entre una recuadro verdadero (en verde) y el de una detección (en rojo).

![img](http://www.conabio.gob.mx/snmb_files/documentation/evaluator/images/images/iou_graph.png)

### Verdadero Positivo, Falso Positivo, Falso Negativo y Verdadero Negativo

Algunos conceptos básicos para las métricas:

* **Verdadero Positivo (TP)**: Una detección correcta. Detección con IOU ≥ _threshold_
* **Falso Positivo (FP)**: Una detección equivocada. Detección con IOU < _threshold_  
* **Falso Negativo (FN)**: Un recuadro verdadero no detectado  
* **Verdadero Negativo (TN)**: No aplica. Representaría la situación en que los recuadros *vacíos* son correctamente detectado como "no-objetos". En la tarea de detección de objetos hay muchos recuadros posibles que no deberían detectarse dentro de una imagen. Por lo tanto, TN serían todos los recuadros posibles que no se detectaron correctamente (tantos recuadros posibles dentro de una imagen). Es por eso que no es utilizado por las métricas.

_threshold_: (umbral) dependiendo de la métrica, generalmente se establece en 50%, 75% o 95%.

Debajo se muestran los siguientes casos: una predicción de un verdadero positivo a la izquierda, dos casos de predicciones de falsos positivos en el centro, y a la derecha una predicción de un falso negativo.

![img](http://www.conabio.gob.mx/snmb_files/documentation/evaluator/images/images/iou_examples_es.png)

### Precisión

La precisión es la capacidad de un modelo para identificar **únicamente** los objetos relevantes. Es el porcentaje de predicciones positivas correctas y viene dado por:

![img](http://www.conabio.gob.mx/snmb_files/documentation/evaluator/images/images/precision_formula.gif)

La precisión varía entre 0 y 1, una alta precisión implica que la mayoría de los objetos detectados coinciden con objetos verdaderos. P.e. Precisión = 0.8, cuando se detecta un objeto, el 80% del tiempo el detector acierta.

### Recall 

Recall es la capacidad de un modelo para encontrar **todos** los casos relevantes (todos los recuadros verdaderos). Es el porcentaje de Verdaderos Positivos (TP) detectados de entre todos los recuadros verdaderos y está dado por:

![img](http://www.conabio.gob.mx/snmb_files/documentation/evaluator/images/images/recall_formula.gif)

Del mismo modo, el recall varía entre 0 y 1, donde un valor de recall alto significa que se detectaron la mayoría de los objetos verdaderos. Por ejemplo, recall = 0.6, implica que el modelo detecta el 60% de los objetos correctamente.

### Interpretaciones

- Recall alto pero baja precisión implican que se han detectado todos los objetos verdaderos, pero la mayoría de las detecciones son incorrectas (muchos falsos positivos).
- Bajo recall pero alta precisión implica que todos los objetos predichos son correctas, pero se han perdido la mayoría de los objetos verdaderos (muchos falsos negativos).
- Alta precisión y alto recall, el detector ideal tiene la mayoría de los objetos verdaderos detectados correctamente.

Tenga en cuenta que podemos evaluar el rendimiento del modelo en su conjunto, así como evaluar su rendimiento por cada categoría, calculando métricas de evaluación específicas de la clase.

### Cómo funcionan las predicciones

- Cuando varios recuadros detectan el mismo objeto, el recuadro con el IoU más alto se considera TP, mientras que los recuadros restantes se consideran FP.
- Si el objeto está presente y el recuadro predicho tiene un umbral IoU < _threshold_ con el recuadro verdadero, la predicción se considera FP. Más importante aún, debido a que ningún recuadro lo detectó correctamente, el objeto de la clase recibe FN.
- Si el objeto no está en la imagen, pero el modelo detecta alguno, la predicción se considera FP.
- El recall y la precisión se calculan para cada clase aplicando las fórmulas mencionadas anteriormente, donde se acumulan las predicciones de TP, FP y FN.

Para ilustrar cómo se calculan el recall y la precisión, veamos un ejemplo de un modelo de detección de objetos. A continuación se muestran imágenes de objetos donde los de la izquierda representan los recuadros reales, y los de la derecha representan los recuadros predichos. Establecemos el umbral de IoU en 0.5.

Tenga en cuenta que las predicciones se calculan individualmente para cada clase.

![img](http://www.conabio.gob.mx/snmb_files/documentation/evaluator/images/images/example_recall_precision_for_person.png)

Calculemos el recall y la precisión para la categoría "Persona":

![img](http://www.conabio.gob.mx/snmb_files/documentation/evaluator/images/images/example_recall_precision_for_person_formula.png)

## Métricas

En los temas a continuación hay algunos comentarios sobre las métricas más populares utilizadas para la detección de objetos.

### Precisión - recall y el umbral de confianza

Al optimizar un modelo tanto para el recall como para la precisión, es poco probable que un detector de objetos produzca tanto el recall como la precisión máximos para una clase en todo momento, principalmente debido a una compensación entre las dos métricas. Esta compensación depende del umbral de confianza. Veamos cómo funciona esto:

El detector de objetos predice recuadros, cada uno asociado a un score de confianza. El score de confianza se utiliza para evaluar la probabilidad de la clase del objeto que aparece en el recuadro. En consecuencia, establecemos un umbral para convertir estas probabilidades de confianza en clasificaciones, donde las detecciones con un score de confianza por encima del umbral predeterminado se consideran verdaderos positivos (TP), mientras que las que están por debajo del umbral se consideran falsos positivos (FP).

Al elegir un umbral de confianza alto, el modelo se vuelve robusto a los ejemplos positivos (es decir, recuadros que contienen un objeto), por lo tanto, habrá menos predicciones positivas. Como resultado, los falsos negativos aumentan y los falsos positivos disminuyen, esto reduce el recall (el denominador aumenta en la fórmula de recall) y mejora la precisión (el denominador disminuye en la fórmula de precisión). Del mismo modo, bajar aún más el umbral hace que la precisión disminuya y el recall aumente.

Por lo tanto, el umbral de confianza es un parámetro ajustable donde, al ajustarlo, podemos definir las detecciones de TP a partir de las FP, controlando la precisión y el recall, determinando así el rendimiento del modelo.

### Curva de Precisión x Recall

La curva de Precision x Recall es una buena forma de evaluar el rendimiento de un detector de objetos, cambiando los niveles de score (probabilidad) al trazar una curva para cada categoría de objetos. Un detector de objetos de una categoría en particular se considera bueno si su precisión se mantiene alta a medida que aumenta el recall, lo que significa que al variar el umbral de score, la precisión y el recall seguirán siendo altos. Otra forma de identificar un buen detector de objetos es buscar un detector que pueda identificar solo objetos relevantes (0 falsos positivos = alta precisión), encontrando todos los objetos verdaderos (0 falsos negativos = alto recall).

Un mal detector de objetos necesita aumentar el número de objetos detectados (aumentando los falsos positivos = menor precisión) para recuperar todos los objetos verdaderos (alto recall). Es por eso que la curva Precisión x Recall generalmente comienza con valores altos de precisión, disminuyendo a medida que aumenta el recall. Se puede ver un ejemplo de la curva Precisión x Recall en el siguiente tema (Precisión promedio). Este tipo de curva es utilizada en conjunto con la precisión promedio en el desafío PASCAL VOC 2012.

### Precisión Promedio (AP)

Otra forma de comparar el rendimiento de los detectores de objetos es calcular el área bajo la curva (AUC) de la curva de Precisión x Recall. Como las curvas AP a menudo son curvas en zigzag que suben y bajan, comparar diferentes curvas (detectores diferentes) en el mismo diagrama generalmente no es una tarea fácil, porque las curvas tienden a cruzarse entre sí con mucha frecuencia. Es por eso que la precisión promedio (AP), una métrica numérica, también puede ayudarnos a comparar diferentes detectores. En la práctica, AP es la precisión promediada en todos los valores de recall entre 0 y 1.

A partir de 2010, el método de calcular la AP mediante el desafío PASCAL VOC ha cambiado. Actualmente, **la interpolación realizada por el desafío PASCAL VOC utiliza todos los puntos de datos, en lugar de interpolar solo 11 puntos igualmente espaciados como se indica en su [artículo](http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.157.5766&rep=rep1&type=pdf)**.

#### Interpolación de 11-puntos

La interpolación de 11 puntos intenta resumir la forma de la curva Precisión x Recall promediando la precisión en un conjunto de once niveles de recall igualmente espaciados [0, 0.1, 0.2, ... , 1]:

![img](http://www.conabio.gob.mx/snmb_files/documentation/evaluator/images/images/11-point_interpolation_formula1.gif)

con

![img](http://www.conabio.gob.mx/snmb_files/documentation/evaluator/images/images/11-point_interpolation_formula2.gif)

Donde ![](http://www.conabio.gob.mx/snmb_files/documentation/evaluator/images/images/11-point_interpolation_let1.gif) es la precisión medida al nivel de recall ![](http://www.conabio.gob.mx/snmb_files/documentation/evaluator/images/images/11-point_interpolation_let2.gif).

En lugar de utilizar la precisión observada en cada punto, el AP se obtiene interpolando la precisión solo en los 11 niveles ![](http://www.conabio.gob.mx/snmb_files/documentation/evaluator/images/images/11-point_interpolation_let3.gif) tomando la **máxima precisión cuyo valor de recall es mayor que ![](http://www.conabio.gob.mx/snmb_files/documentation/evaluator/images/images/11-point_interpolation_let3.gif)**.

#### Interpolación de todos los puntos

En lugar de interpolar solo en los 11 puntos igualmente espaciados, puede interpolar a través de todos los puntos de tal manera que:

![img](http://www.conabio.gob.mx/snmb_files/documentation/evaluator/images/images/all-point_interpolation_formula1.gif)
 
with

![img](http://www.conabio.gob.mx/snmb_files/documentation/evaluator/images/images/all-point_interpolation_formula2.gif)

Donde ![](http://www.conabio.gob.mx/snmb_files/documentation/evaluator/images/images/11-point_interpolation_let1.gif) es la precisión medida al nivel de recall ![](http://www.conabio.gob.mx/snmb_files/documentation/evaluator/images/images/11-point_interpolation_let2.gif).

En este caso, en lugar de utilizar la precisión observada en solo unos pocos puntos, el AP ahora se obtiene al interpolar la precisión en **cada nivel**, ![](http://www.conabio.gob.mx/snmb_files/documentation/evaluator/images/images/11-point_interpolation_let2.gif) tomando la **máxima precisión cuyo valor de recall es mayor que ![](http://www.conabio.gob.mx/snmb_files/documentation/evaluator/images/images/all-point_interpolation_let1.gif)**. De esta manera se calcula un estimado del área bajo la curva.

#### Proceso de cálculo de la curva Precisión x Recall

Primero se crear una lista `TP_FP` donde se van acumulando los Verdaderos positivos y Falsos positivos de todas las imágenes, de la siguiente manera:
- Para cada imagen `image`:
    - Para cada categoría `cat`:
        - Se obtienen los recuadros de las etiquetas reales (`groundtruth_boxes`), los recuadros de las detecciones (`detected_boxes`) y los scores de las detecciones (`detected_scores`) de la categoría `cat`.
        - Se aplica el algoritmo Non Max Supression (NMS) sobre `detected_boxes` para descartar las detecciones con recuadros que tengan un `iou >= nms_iou_threshold` con algún bounding box de otra detección con score mayor.
        - Se calcula `iou` para cada par de los `detected_boxes` resultantes y los `groundtruth_boxes`.
        - Para cada `detected_box` se busca el `groundtruth_box` con el que tenga el iou mayor y que cumpla que `iou >= iou_threshold` y que no haya sido marcado antes por otro `detected_box`. Si se cumple esto, la detección es acumulada como TP (se inserta un `True`) en `TP_FP[cat]` y el `groundtruth_box` es descartado para ser comparado con otras detecciones. Si no se cumple esto se acumula un FP (se inserta un `0`) en `TP_FP[cat]`. Al final se suma el total de `groundtruth_boxes` en `num_total_gt[cat]`, que servirá posteriormente para calcular el recall.

Después se calcula la precisión y recall sobre la lista de `TP_FP` de cada categoría:
- Para cada categoría `cat`:
    - Se ordena `TP_FP[cat]` de forma descendente a partir del score de las detecciones.
    - Se realiza una suma acumulada de True Positives y False Positives. P.e., si tenemos 10 detecciones y únicamente la segunda es un True Positive, tendríamos lo siguiente:
        `true_positives` => `[False, True, False, False, False, False, False, False, False, False]`
        `false_positives` => `[True, False, True, True, True, True, True, True, True, True]`
        `cum_true_positives` => `[0, 1, 1, 1, 1, 1, 1, 1, 1, 1]`
        `cum_false_positives` => `[ 1.,  1.,  2.,  3.,  4.,  5.,  6.,  7.,  8.,  9.]`
    - Se realiza el cálculo de la precisión y el recall con estos arreglos "acumulados" y el número de anotaciones de la categoría `cat` con las siguientes fórmulas:
        `precision = cum_true_positives / (cum_true_positives + cum_false_positives)`
        `recall = cum_true_positives / num_gt`
        Obteniendo:
            `precision` => `[0., 0.5, 0.33, 0.25, 0.2, 0.167, 0.14, 0.125, 0.11, 0.1]`
            `recall` => `[0., 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25]`



#### Ejemplo ilustrativo
Para aclarar las cosas, proporcionamos un ejemplo comparando ambas interpolaciones. Considere las detecciones a continuación:
  
![img](http://www.conabio.gob.mx/snmb_files/documentation/evaluator/images/images/example1_detections.png)
  
Hay 7 imágenes con 15 objetos verdaderos representados por los recuadros delimitadores verdes y 24 detecciones representados por los recuadros delimitadores rojos. Cada objeto detectado tiene un nivel de confianza y se identifica con una letra (A,B,...,Y).  

La siguiente tabla muestra los recuadros con sus correspondientes scores. La última columna identifica las detecciones como TP o FP. En este ejemplo, se considera un TP si IOU ![](http://www.conabio.gob.mx/snmb_files/documentation/evaluator/images/images/example1_let1.gif) 30%, de lo contrario es un FP. Al observar las imágenes de arriba podemos decir aproximadamente si las detecciones son TP o FP.

![img](http://www.conabio.gob.mx/snmb_files/documentation/evaluator/images/images/example1_table1.png)

En algunas imágenes hay más de una detección que se superpone a recuadro verdadero (Imágenes 2, 3, 4, 5, 6 y 7). Para esos casos, la detección con el mayor IOU se considera TP y los otros se consideran FP. Esta regla se aplica mediante la métrica PASCAL VOC 2012: "por ejemplo, 5 detecciones (TP) de un solo objeto se cuentan como 1 detección correcta y 4 detecciones falsas".

La curva de Precisión x Recall se traza calculando la precisión y los valores de recall de las detecciones acumuladas de TP o FP. Para esto, primero debemos ordenar las detecciones por sus scores, luego calculamos la precisión y recall para cada detección acumulada como se muestra en la tabla a continuación: 

![img](http://www.conabio.gob.mx/snmb_files/documentation/evaluator/images/images/example1_table2.png)

Al trazar los valores de precisión y recall tenemos lo siguiente *Curva de Precisión x Recall*:
 
![img](http://www.conabio.gob.mx/snmb_files/documentation/evaluator/images/images/example1_plot1.png)
 
Como se mencionó anteriormente, hay dos formas diferentes de medir la precisión promedio interpolada: **interpolación de 11 puntos** e **interpolación de todos los puntos**. A continuación hacemos una comparación entre ellos:

#### Cálculo de la interpolación de 11-puntos

La idea de la precisión promedio interpolada de 11-puntos es promediar las precisiones en un conjunto de 11 niveles de recall (0, 0.1, ..., 1). Los valores de precisión interpolados se obtienen tomando la precisión máxima cuyo valor de recall es mayor que su valor de recall actual de la siguiente manera:

![img](http://www.conabio.gob.mx/snmb_files/documentation/evaluator/images/images/example1_plot2.png)

Aplicandpo la interpolación de 11-puntos tenemos:

![](http://www.conabio.gob.mx/snmb_files/documentation/evaluator/images/images/example1_formula1.gif)  
![](http://www.conabio.gob.mx/snmb_files/documentation/evaluator/images/images/example1_formula2.gif)  
![](http://www.conabio.gob.mx/snmb_files/documentation/evaluator/images/images/example1_formula3.gif)


#### Calculando la interpolación en todos los puntos

Al interpolar todos los puntos, la precisión promedio (AP) se puede interpretar como un AUC aproximado de la curva de precisión x recall. La intención es reducir el impacto de las oscilaciones de la curva. Al aplicar las ecuaciones presentadas anteriormente, podemos obtener las áreas como se demostrará aquí. También podríamos tener visualmente los puntos de precisión interpolados al observar los recalls comenzando desde el más alto (0.4666) a 0 (mirando el gráfico de derecha a izquierda) y, a medida que disminuimos el recall, recopilamos los valores de precisión que son los más altos como se muestra en la imagen a continuación:
    
![img](http://www.conabio.gob.mx/snmb_files/documentation/evaluator/images/images/example1_plot3.png)
  
Mirando la gráfica de arriba, podemos dividir el AUC en 4 áreas (A1, A2, A3 y A4):
  
![img](http://www.conabio.gob.mx/snmb_files/documentation/evaluator/images/images/example1_plot4.png)

Calculando el área total, tenemos el AP:

![](http://www.conabio.gob.mx/snmb_files/documentation/evaluator/images/images/example1_formula4.gif)  
  
Con
![](http://www.conabio.gob.mx/snmb_files/documentation/evaluator/images/images/example1_formula5.gif)  
![](http://www.conabio.gob.mx/snmb_files/documentation/evaluator/images/images/example1_formula6.gif)  
![](http://www.conabio.gob.mx/snmb_files/documentation/evaluator/images/images/example1_formula7.gif)  
![](http://www.conabio.gob.mx/snmb_files/documentation/evaluator/images/images/example1_formula8.gif)  

![](http://www.conabio.gob.mx/snmb_files/documentation/evaluator/images/images/example1_formula9.gif)  
   ![](http://www.conabio.gob.mx/snmb_files/documentation/evaluator/images/images/example1_formula10.gif)  
![](http://www.conabio.gob.mx/snmb_files/documentation/evaluator/images/images/example1_formula11.gif)  

Los resultados entre los dos métodos de interpolación diferentes son un poco diferentes: 24.56% y 26.84% por la interpolación de cada punto y la interpolación de 11 puntos respectivamente.  

Nuestra implementación predeterminada es la misma que VOC PASCAL: **interpolación en todos los puntos**.

## Media de la precisión promedio (mAP)

Ahora que entendemos la Precisión promedio (AP), la media de la Precisión Promedio (mAP) para un conjunto de datos que contiene N categorías, es el promedia de la AP sobre las N clases.

![img](http://www.conabio.gob.mx/snmb_files/documentation/evaluator/images/images/mAP_formula.png)

El desafío PASCAL VOC usa la mAP como métrica con un umbral de IoU de 0.5, mientras que MS COCO promedia mAP sobre diferentes umbrales de IoU (0.5, 0.55, 0.6, 0.65, 0.7, 0.75, 0.8, 0.85, 0.9, 0.95) con un paso de 0.05, esta métrica se denota en los artículos como mAP@[.5,.95]. Por lo tanto, COCO no solo promedia la AP en todas las clases sino también en distintos umbrales de IoU.

Tenga en cuenta que mAP y AP a menudo se usan indistintamente, este es el caso de la competencia COCO. Según su sitio web oficial:

- 'AP y AR se promedian sobre múltiples valores de Intersección sobre Unión (IoU). Específicamente utilizamos 10 umbrales de IoU de .50:.05:.95. Esto rompe con la tradición, donde AP se calcula en un único valor de IoU de .50 (que corresponde a nuestra métrica AP<sup>IoU</sup>=.50. El promedio sobre IoUs pondera más a los detectores con una mejor localización.'

Al optimizar AP o AR (que se analizará a continuación) sobre múltiples valores de umbral de IoU, el modelo penaliza las localizaciones deficientes y optimiza para una buena localización. Simplemente porque la precisión de la localización se evalúa sobre la base de la IoU entre los recuadros reales y el recuadro predicho, esta es la estrategia óptima que se utilizará para aplicaciones de detección que requieren un nivel alto de localización, como la conducción autónoma o las imágenes médicas.

## Recall promedio (AR)

En lugar de calcular el recall en un IoU particular, calculamos el recall promedio (AR) en los umbrales de IoU de 0.5 a 1 y, por lo tanto, resumimos la distribución del recall en un rango de umbrales de IoU.

Solo promediamos el recall sobre los umbrales de IoU de [0.5, 1] porque el rendimiento de la detección se correlaciona con el recall en los umbrales superiores a 0.5, donde en 0.5 los recuadros localizan muy poco los objetos y en un IoU de 1 los objetos están localizados completamente.

Por lo tanto, si se desea optimizar un modelo para un alto recall y una localización precisa de los objetos, puede considerar el recall promedio como una métrica de evaluación.

El recall promedio describe el área duplicada bajo la curva Recall x IoU. La curva Recall x IoU muestra los resultados de recall para cada umbral de IoU donde IoU ∈ [0.5,1.0], con los umbrales de IoU en el eje-x y los niveles de recall en el eje-y.

Esto se describe de forma matemática de la siguiente manera:

![img](http://www.conabio.gob.mx/snmb_files/documentation/evaluator/images/images/AR_formula.png)

De manera similar a mAP, mAR es el promedio de AR sobre el número de clases del conjunto de datos.

## Conjuntos de métricas más usadas en los desafíos de detección de objetos

Existen varias competencias que definen su propio conjunto de métricas de evaluación de modelos de detección de objetos como estándar, y se han hecho tan populares que ahora se utizan comúnmente para la evaluación de este tipo de modelos.

### El desafío Pascal VOC

La métrica para evaluar la calidad general de los detectores de objetos será la media de la precisión promedio (mAP), calculada de acuerdo con el protocolo del PASCAL VOC Challenge 2010-2012.

La tarea de detección por clase será evaluada por la curva de Precisión x Recall. La principal medida cuantitativa utilizada será la precisión promedio (AP) calculada en un umbral de IoU único de 0.5.

El cálculo de la medida de precisión promedio (AP) se modificó en 2010 para mejorar la precisión y la capacidad de medir las diferencias entre los métodos con bajo AP.
Tenga en cuenta que antes de 2010, el AP se calcula muestreando la curva decreciente monotónicamente en un conjunto fijo de valores de recall uniformemente espaciados 0,0.1,0.2, ..., 1. Por el contrario, VOC 2010-2012 muestrea la curva en todos los valores únicos de recall.

Para utilizar esta métrica se debe agregar a la lista del parámetro 'metrics_set' el valor 'pascal_voc_detection_metrics'

#### PASCAL VOC Ponderado
De acuerdo con la métrica **PASCAL VOC 2010**, el rendimiento en cada una de las dos clases contribuiría igualmente al valor final del mAP, mientras que para la métrica **Weighted PASCAL VOC**, el valor final de mAP estará influenciado por la frecuencia de cada clase. 

Para utilizar esta métrica se debe agregar a la lista del parámetro 'metrics_set' el valor 'weighted_pascal_voc_detection_metrics'

#### Formato de salida
```
{
    'one_class': {
        'label 1': {
            'precisions': float numpy array, list of precisions.
            'recalls': float numpy array, list of recalls.
            'average_precision': float, average precision for each class.
        },
        'label 2': { ... },
        ...
    },
    'all_classes': {
        'mAP': float, mean average precision of all classes. Computes the mean average precision as the mean of the per-class average precisions.
        'total_positives': int, total number of ground truth positives.
        'total_TP': int, total number of True Positive detections.
        'total_FP': int, total number of False Negative detections.
    }
}
```

### El desafío de detección de objetos COCO

Evalúa la detección de objetos usando 12 métricas:

- mAP (referido indistintamente en la competencia por AP) es la métrica principal para la evaluación en la competencia, donde AP se promedia sobre los 10 umbrales de IoU y las 80 categorías del conjuntos de datos COCO. Esto se denota por AP@[.5:.95] o AP@[.50:.05:.95] incrementándose con .05. Por lo tanto, un AP más alto según el protocolo de evaluación COCO indica que los objetos detectados tienen una mejor localización. Dado que se penalizan las peores localizaciones y se optimiza para las mejores localizaciones, esta métrica es óptima para aplicaciones que requieren una alta precisión en la localización, como la conducción autónoma o las imágenes médicas.
- Además, COCO evalúa individualmente el AP en umbrales de 0.5 y 0.75 IoU, esto se denota por AP@.50 o AP<sup>IOU=0.50</sup> y AP@0.75 o AP<sup>IOU=0.75</sup> respectivamente.
- Dado que el conjunto de datos COCO contiene más objetos pequeños que grandes, el rendimiento se evalúa con el AP en distintos tamaños de objeto donde:
    - AP<sup>small</sup> para objetos pequeños con área menor a 32<sup>2</sup>.
    - AP<sup>medium</sup> para objetos medianos con área 32<sup>2</sup> < área < 96<sup>2</sup>.
    - Finalmente, AP<sup>large</sup> para objetos grandes con área mayor que 96<sup>2</sup>.
    El área del objeto se define por el número de píxeles en la máscara del objeto proporcionada para la tarea de segmentación de objetos en la competencia COCO.
- De manera similar, COCO evalúa los algoritmos en mAR (AR se refiere a mAR) en todas las escalas con AR<sup>small</sup>, AR<sup>medium</sup> y AR<sup>large</sup>. Si se quiere optimizar un modelo para tener un alto recall y una buena precisión en la localización, se debe considerar usar el AR como la métrica de evaluación.
- Además, COCO utiliza una métrica adicional que basa la evaluación del AR en el número de detecciones por imagen, específicamente AR<sup>max=1</sup> dada 1 detección por imagen, AR<sup>max=10</sup> para 10 detecciones por imagen y AR<sup>max=100</sup> para 100 detecciones por imagen.

Para utilizar esta métrica se debe agregar a la lista del parámetro 'metrics_set' el valor 'coco_detection_metrics'

#### Output format
```
'coco_detection_metrics': {
    'one_class': {
        'label 1': {
            'Precision/mAP ByCategory/class_n': float, mean average precision over classes averaged over IOU thresholds ranging from .5 to .95 with .05 increments.
            'Precision/mAP@.50IOU ByCategory/class_n': float, mean average precision at 50% IOU.
            'Precision/mAP@.75IOU ByCategory/class_n': float, mean average precision at 75% IOU.
            'Precision/mAP (small) ByCategory/class_n': float, mean average precision for small objects (area < 32^2 pixels).
            'Precision/mAP (medium) ByCategory/class_n': float, mean average precision for medium sized objects (32^2 pixels < area < 96^2 pixels).
            'Precision/mAP (large) ByCategory/class_n': float, mean average precision for large objects (96^2 pixels < area < 10000^2 pixels).
            'Recall/AR@1 ByCategory/class_n': float, average recall with 1 detection.
            'Recall/AR@10 ByCategory/class_n': float, average recall with 10 detections.
            'Recall/AR@100 ByCategory/class_n': float, average recall with 100 detections.
            'Recall/AR@100 (small) ByCategory/class_n': float, average recall for small objects with 100.
            'Recall/AR@100 (medium) ByCategory/class_n': float, average recall for medium objects with 100.
            'Recall/AR@100 (large) ByCategory/class_n': float, average recall for large objects with 100 detections.
        },
        'label 2': { ... },
        ...
    },
    'all_classes': {
        'Precision/mAP': float, mean average precision over classes averaged over IOU thresholds ranging from .5 to .95 with .05 increments.
        'Precision/mAP@.50IOU': float, mean average precision at 50% IOU.
        'Precision/mAP@.75IOU': float, mean average precision at 75% IOU.
        'Precision/mAP (small)': float, mean average precision for small objects (area < 32^2 pixels).
        'Precision/mAP (medium)': float, mean average precision for medium sized objects (32^2 pixels < area < 96^2 pixels).
        'Precision/mAP (large)': float, mean average precision for large objects (96^2 pixels < area < 10000^2 pixels).
        'Recall/AR@1': float, average recall with 1 detection.
        'Recall/AR@10': float, average recall with 10 detections.
        'Recall/AR@100': float, average recall with 100 detections.
        'Recall/AR@100 (small)': float, average recall for small objects with 100.
        'Recall/AR@100 (medium)': float, average recall for medium objects with 100.
        'Recall/AR@100 (large)': float, average recall for large objects with 100 detections.
    }
}
```

### El desafío de Google Open Images V2

Para evaluar el resultado de los modelos de detección de objetos, el desafío de Google Open Images V2 utiliza la Precisión Prmedio (AP) para cada clase y la media de la precisión promedio (mAP) sobre todas las clases en el conjunto de datos con un umbral de IoU de 0.5.
La dierencia con las métricas de PASCAL VOC 2010 es que se agrega un campo de agrupamiento de los recuadros verdaderos, que son tratados de forma distinta con el objetivo de decidir si las detecciones son verdaderos positivos, falsos positivos o son ignoradas.

Para utilizar esta métrica se debe agregar a la lista del parámetro 'metrics_set' el valor 'oid_V2_detection_metrics'

#### Formato de salida
```
{
    'one_class': {
        'label 1': {
            'precisions': float numpy array, list of precisions.
            'recalls': float numpy array, list of recalls.
            'average_precision': float, average precision for each class.
        },
        'label 2': { ... },
        ...
    },
    'all_classes': {
        'mAP': float, mean average precision of all classes. Computes the mean average precision as the mean of the per-class average precisions.
        'total_positives': int, total number of ground truth positives.
        'total_TP': int, total number of True Positive detections.
        'total_FP': int, total number of False Negative detections.
    }
}
```

# Referencias 

[1] Opitz, Juri, and Sebastian Burst. “Macro F1 and Macro F1.” arXiv preprint arXiv:1911.03347 (2019).