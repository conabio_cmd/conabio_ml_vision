from conabio_ml.trainer.predictor_config import PredictorConfig
from typing import TypeVar


class TFPredictorConfig(PredictorConfig):
    """Class that allows setting up a prediction environment for the 
    Tensorflow library.
    """
    TFPredictorConfigType = TypeVar('TFPredictorConfigType', bound='TFPredictorConfig')

    def __init__(self: TFPredictorConfigType,
                 batch_size,
                 master,
                 num_readers,
                 num_parallel_batches,
                 num_prefetch_batches,
                 read_block_length,
                 num_preprocessing_threads,
                 num_tasks,
                 task_num):
        self.batch_size = batch_size
        self.master = master
        self.num_readers = num_readers
        self.num_parallel_batches = num_parallel_batches
        self.num_prefetch_batches = num_prefetch_batches
        self.read_block_length = read_block_length
        self.num_preprocessing_threads = num_preprocessing_threads
        self.num_tasks = num_tasks
        self.task_num = task_num

    @classmethod
    def create(cls,
               batch_size: int = 32,
               master: str = '',
               num_readers: int = 8,
               num_parallel_batches: int = 8,
               num_prefetch_batches: int = 2,
               read_block_length: int = 32,
               num_preprocessing_threads: int = 4,
               num_tasks: int = None,
               task_num: int = None) -> TFPredictorConfigType:
        """Method that allows to create a configuration of a evaluation 
        environment for the Tensorflow Object Detection API.

        Parameters
        ----------
        batch_size : int, optional
            The number of samples in each batch. (default is 32)
        master : str, optional
            The address of the TensorFlow master to use. (default is '')
        num_readers : int, optional
            Number of file shards to read in parallel. (default is 8)
        num_parallel_batches : int, optional
            Number of batches to produce in parallel. If this is run on a 2x2 TPU set 
            this to 8. (default is 8)
        num_prefetch_batches : int, optional
            Number of batches to prefetch. Prefetch decouples input pipeline and 
            model so they can be pipelined resulting in higher throughput. Set this 
            to a small constant and increment linearly until the improvements become 
            marginal or you exceed your cpu memory budget. Setting this to -1, 
            automatically tunes this value for you. (default is 2)
        read_block_length : int, optional
            Number of records to read from each reader at once.
            (default is 32)
        num_preprocessing_threads : int, optional
            The number of threads used to create the batches. (default is 4)
        num_tasks : int, optional
            The total number of tasks to make the prediction. 
            If you configure this option, you must configure also the `task_num` option.
            Dataset will be partitioned in `num_tasks` chunks, 
            each one of size rows_per_chunk and this process only will process elements in
            [rows_per_chunk*(task_num-1) : rows_per_chunk*task_num - 1] of the dataset.
            (default is None)
        task_num : int or None, optional
            1-based number of the task to make the prediction. 
            If you configure this option, you must configure also the 
            `num_tasks` option.
            Dataset will be partitioned in `num_tasks` chunks, 
            each one of size rows_per_chunk and this process only will process elements in
            [rows_per_chunk*(task_num-1) : rows_per_chunk*task_num - 1] of the dataset.
            (default is None)

        Returns
        -------
        TFPredictorConfig
            An evaluation environment instance for the Tensorflow API.
        """
        return cls(batch_size,
                   master,
                   num_readers,
                   num_parallel_batches,
                   num_prefetch_batches,
                   read_block_length,
                   num_preprocessing_threads,
                   num_tasks,
                   task_num)


class BasicPredictorConfig():
    """Class that allows setting up a basic prediction environment
    """

    def __init__(self,
                 batch_size=1,
                 num_tasks=None,
                 task_num=None):
        self.batch_size = batch_size
        self.num_tasks = num_tasks
        self.task_num = task_num
        assert bool(num_tasks) == bool(task_num), (f"Either you must specify both "
                                                   f"num_tasks and task_num or neither.")
        assert num_tasks is None or (task_num <= num_tasks), (f"task_num must be less than "
                                                              f"or equal to num_tasks")
