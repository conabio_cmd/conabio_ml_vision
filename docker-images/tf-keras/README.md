## Build image
```
docker image build -t conabio_cd/conabio_ml_vision:tf_2 . --build-arg TF_VERSION=2.7.4-gpu-jupyter
```

* `TF_VERSION` arg should be a valid 
  [tensorflow's docker image tag](https://hub.docker.com/r/tensorflow/tensorflow/tags),
  for example, `1.15.5-gpu-py3-jupyter` or `2.7.4-gpu-jupyter`. If no argument
  is provided the image will be built using the `latest-gpu-jupyter` tag.

## Run container
```
docker run -it --gpus 2 --name API_VISION_TF_2 -p 3331:3331 -v /LUSTRE/Ecoinformatica/jlopez:/LUSTRE/Ecoinformatica/jlopez/ -v /LUSTRE/users/ecoinf_admin/:/LUSTRE/users/ecoinf_admin/ -e PIP_INSTALL="keras" -e APT_INSTALL="less vim ffmpeg libsm6 libxext6" conabio_cd/conabio_ml_vision:tf_2 bash

# Para nodo4
docker run -it --gpus 2 --name API_VISION_TF_2 -p 3331:3331 -v /LUSTRE/Ecoinformatica/jlopez:/LUSTRE/Ecoinformatica/jlopez/ -v /LUSTRE/users/ecoinf_admin/:/LUSTRE/users/ecoinf_admin/ -v /var/www/shared_files/ecoinformatica:/var/www/shared_files/ecoinformatica/ -e PIP_INSTALL="tqdm keras" -e APT_INSTALL="less vim ffmpeg libsm6 libxext6" conabio_cd/jlopezconabio_ml_vision:tf_2 bash
```
