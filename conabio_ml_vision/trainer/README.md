# Modelos de imágenes

El API conabio_ml define dos tipos de modelos para el caso de imágenes: **Clasificación** y **Detección de Objetos**. El módulo conabio_ml.trainer.images.models define las clases ClassificationModel y ObjectDetectionModel para cada uno de ellos.

## Modelos de Clasificación

El API conabio_ml utiliza la biblioteca de modelos de clasificación de imágenes [TensorFlow-Slim](https://github.com/tensorflow/models/tree/master/research/slim "TensorFlow-Slim"), que proporciona la implementación de una serie de modelos que podrán ser ajustados a partir de un dataset representado en el formato TFRecords.

El ajuste del modelo se podrá realizar desde cero o usando la técnica de Transfer Learning, a partir de los archivos de checkpoint generados por un entrenamiento previo. En el siguiente [enlace](https://github.com/tensorflow/models/tree/master/research/slim#pre-trained-models "enlace") se pueden descargar los archivos checkpoint para los modelos disponibles en Tensorflow-slim pre-entrenados con el conjunto de imágenes [Image-net](http://www.image-net.org/ "Image-net").

Para aplicar la técnica de Transfer learning la clase ClassificationModel proveé el método de clase load_saved_model, que crea una instancia de ClassificationModel que podrá ser utilizada en el proceso de entrenamiento o evaluación de un modelo. El método load_saved_model recibe el path donde se encuentran los archivos de checkpoint (*.ckpt) del modelo pre-entrenado, así como un diccionario con la configuración del modelo.

A continuación se ejemplifica la creación de una instancia del modelo de clasificación Inception V3 a partir de un archivo checkpoint y una configuración, que podrá ser utilizada en un proceso de entrenamiento y evaluación del modelo:

```python
from conabio_ml.trainer.images.model import ClassificationModel

inception_v3_model = ClassificationModel.load_saved_model(
    source_path='./files/inception_v3.ckpt',
    model_config={
        "inception_v3":{
            "weight_decay": 0.0005
        }
    }
)
```

Como se puede observar, el parámetro model_config es un diccionario que contiene la configuración del modelo y sigue el formato dado en la [Especificación para configuración de modelos de clasificación](https://conabio-ml.readthedocs.io/es/latest/user_guide/model/image_classification_model_config.html#image-classification-model-config-specification-label "Especificación para configuración de modelos de clasificación").

## Modelos de Detección de Objetos

El API conabio_ml utiliza el [API de Detección de Objetos de Tensorflow](https://github.com/tensorflow/models/tree/master/research/object_detection "API de Detección de Objetos de Tensorflow") (TFODAPI), que al igual que Tensorflow-Slim, proporciona la implementación modelos que podrán ser ajustados a partir de un dataset representado en el formato TFRecords.

También es posible ajustar un modelo desde cero o utilizar la técnica de Transfer Learning, a partir de los archivos de checkpoint generados por un entrenamiento previo. En el siguiente [enlace](https://github.com/tensorflow/models/tree/master/research/slim#pre-trained-models "enlace") se pueden descargar los archivos checkpoint para los modelos disponibles en el TFODAPI pre-entrenados sobre distintos conjuntos de imágenes.

Para aplicar la técnica de Transfer learning la clase ObjectDetectionModel proveé el método de clase load_saved_model, que crea una instancia de ObjectDetectionModel que podrá ser utilizada en el proceso de entrenamiento o evaluación de un modelo. El método load_saved_model recibe el path donde se encuentran los archivos de checkpoint (*.ckpt) del modelo pre-entrenado, así como un diccionario con la configuración del modelo.

A continuación se ejemplifica la creación de una instancia del modelo de detección de objetos Faster-RCNN a partir de un archivo checkpoint y una configuración, que podrá ser utilizada en un proceso de entrenamiento y evaluación del modelo:

```python
from conabio_ml.trainer.images.model import ObjectDetectionModel

detection_model = ObjectDetectionModel.load_saved_model(
    source_path='./files/model.ckp',
    model_config={
        "faster_rcnn": {
            "image_resizer": {
                "keep_aspect_ratio_resizer": {
                    "min_dimension": 600,
                    "max_dimension": 800
                }
            },
            "feature_extractor": {
                "type": 'faster_rcnn_resnet101',
                "first_stage_features_stride": 16
            },
            "first_stage_anchor_generator": {
                "grid_anchor_generator": {
                    "scales": [0.25, 0.5, 1.0, 2.0],
                    "aspect_ratios": [0.5, 1.0, 2.0],
                    "height_stride": 16,
                    "width_stride": 16
                }
            },
            "first_stage_box_predictor_conv_hyperparams": {
                "op": "CONV",
                "regularizer": {
                    "l2_regularizer": {
                        "weight": 0.0
                    }
                },
                "initializer": {
                    "truncated_normal_initializer": {
                        "stddev": 0.01
                    }
                }
            },
            "first_stage_nms_score_threshold": 0.0,
            "first_stage_nms_iou_threshold": 0.7,
            "first_stage_max_proposals": 300,
            "first_stage_localization_loss_weight": 2.0,
            "first_stage_objectness_loss_weight": 1.0,
            "initial_crop_size": 14,
            "maxpool_kernel_size": 2,
            "maxpool_stride": 2,
            "second_stage_box_predictor": {
                "mask_rcnn_box_predictor": {
                    "fc_hyperparams": {
                        "op": "FC",
                        "regularizer": {
                            "l2_regularizer": {
                                "weight": 0.0
                            }
                        },
                        "initializer": {
                            "variance_scaling_initializer": {
                                "factor": 1.0,
                                "uniform": True,
                                "mode": "FAN_AVG"
                            }
                        }
                    }
                }
            },
            "second_stage_post_processing": {
                "batch_non_max_suppression": {
                    "score_threshold": 0.0,
                    "iou_threshold": 0.6,
                    "max_detections_per_class": 100,
                    "max_total_detections": 300
                },
                "score_converter": "SOFTMAX"
            },
            "second_stage_localization_loss_weight": 2.0,
            "second_stage_classification_loss_weight": 1.0
        }
    }
)
```

El parámetro model_config es un diccionario que contiene la configuración del modelo y sigue el formato dado en la [Especificación para configuración de modelos de detección](https://conabio-ml.readthedocs.io/es/latest/user_guide/model/image_detection_model_config.html#image-detection-model-config-specification-label "Especificación para configuración de modelos de detección").


# Entrenadores de imágenes

El API conabio_ml define dos tipos de entrenadores para problemas de visión por computadora en imágenes, uno para cada tipo de modelo que se define: **Clasificación** y **Detección de Objetos**. El módulo `conabio_ml.trainer.images.trainer` define las clases `ClassificationTrainer` y `ObjectDetectionTrainer` para cada uno de ellos.

Cada una de estas clases tiene el método estático train que recibe los cuatro parámetros detallados en la sección Trainer para poder realizar el entrenamiento: dataset, modelo, configuración de ejecución, configuración de entrenamiento.

El tipo de modelo que recibe el método train debe coincidir con el tipo de entrenador que se va a utilizar. P. e., el método train de la clase `ObjectDetectionTrainer` debe recibir un modelo del tipo `ObjectDetectionModel`.

## Configuración de ejecución

El API conabio_ml define un configurador de ejecución de entrenamiento (`TrainerConfig`) por cada plataforma utilizada para realizar los entrenamientos. Para el caso de entrenamientos de modelos de visión por computadora en imágenes, se definen en el módulo `conabio_ml.trainer.images.trainer_config` las clases `TFSlimTrainConfig` y `TFObjectDetectionAPITrainConfig` para usar las plataformas [Tensorflow-Slim](https://github.com/tensorflow/models/tree/master/research/slim "Tensorflow-Slim") y el [API de Detección de Objetos de Tensorflow](https://github.com/tensorflow/models/tree/master/research/object_detection "API de Detección de Objetos de Tensorflow"), respectivamente. Ambas clases cuentan con el método estático create, que permite configurar el ambiente de ejecución de esa plataforma y crea una instancia que puede pasarse al método train del entrenador en el parámetro execution_config.

En el siguiente ejemplo se crea una instancia de configuración de ambiente para entrenar un modelo con biblioteca de modelos de clasificación de imágenes TensorFlow-Slim usando dos tarjetas gráficas (GPUs) en el ambiente local y que guarda el estado del entrenamiento cada 300 segundos:

```python
from conabio_ml.trainer.images.trainer_config import TFSlimTrainConfig

tf_slim_config = TFSlimTrainConfig.create(clone_on_cpu=False,
                                        num_clones=2,
                                        save_summaries_secs=300,
                                        save_interval_secs=300)
```

## Configuración de entrenamiento

La configuración del entrenamiento se pasa en forma de diccionario y define parámetros como la configuración de la función de optimización, learning rate, número de épocas, capas a re-entrenar, dropout, etc., y sigue la [Especificación para configuración de entrenamiento en modelos de clasificación y detección de objetos](https://bitbucket.org/conabio_cmd/conabio_ml/wiki/Especificaci%C3%B3n%20de%20Entrenamiento).

En el siguiente ejemplo se configura un entrenamiento de clasificación de imágenes para el modelo Inception V3 al que se re-entrenan las última cuatro capas por 20 épocas y se usa la función de optimización RMSprop con un learning rate exponencial:

```python
from conabio_ml.trainer.images.trainer import ClassificationTrainer

...
ClassificationTrainer.train(
    snmb_dataset,
    inception_v3_model,
    tf_slim_config,
    train_config={
        "train_dir": os.path.join(test_path, 'train'),
        "checkpoint_exclude_scopes": "InceptionV3/Logits,InceptionV3/AuxLogits,InceptionV3/Mixed_7c,InceptionV3/Mixed_7b",
        "trainable_scopes": "InceptionV3/Logits,InceptionV3/AuxLogits,InceptionV3/Mixed_7c,InceptionV3/Mixed_7b",
        "optimizer": {
            'rms_prop': {
                "learning_rate": {
                    'exponential': {
                        'initial_learning_rate': 0.01,
                        "decay_epochs": 2
                    }
                },
                "decay": 0.9,
                "epsilon": 1.0
            },
            "moving_average_decay": 0.9999
        },
        "num_epochs": 20
    }
)
```