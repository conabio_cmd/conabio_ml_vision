from collections import defaultdict
import math
from multiprocessing import Manager
import numpy as np
import os
import pandas as pd
import subprocess
import uuid

from conabio_ml_vision.utils.images_utils import get_image_size
from conabio_ml_vision.utils import coords_utils
from conabio_ml.utils.logger import get_logger
from conabio_ml.utils.utils import is_array_like
from conabio_ml.utils.utils import parallel_exec
from conabio_ml.utils.dataset_utils import get_cleaned_label


logger = get_logger(__name__)

LABELMAP_FILENAME = 'labels.txt'
DET_LABELMAP_FILENAME = "labelmap.pbtxt"
STATS_FILENAME = 'stats.txt'
STD_DATEFORMAT = '%Y-%m-%d %H:%M:%S'


def get_images_sizes(images):
    """Determine the sizes of `images`

    Parameters
    ----------
    images : list of str
        List of image paths

    Returns
    -------
    pd.DataFrame
        Dataframe containing 'width' and 'height' columns and the item as index
    """
    images_dict = Manager().dict()
    logger.debug("Getting images sizes from stored files...")

    parallel_exec(
        func=set_image_size,
        elements=images,
        image=lambda image: image,
        images_dict=images_dict)

    images_sizes = {
        image: {
            'width': size['width'],
            'height': size['height'],
        }
        for image, size in images_dict.items()
    }

    return pd.DataFrame(data=images_sizes.values(), index=images_sizes.keys())


def set_image_size(image, images_dict):
    """Determine the size of the passed image and store it in the `images_dict` dictionary in the
    form {`image`: {'width': `width`, 'height': `height`}}

    Parameters
    ----------
    image : str
        Path of an image
    images_dict : dict
        Dictionary where the image size will be stored
    """
    width, height = get_image_size(image)
    images_dict[image] = {
        'width': width,
        'height': height
    }


def write_pbtxt(labelmap, output_name):
    """Write the pbtxt file for the dataset

    Parameters
    ----------
    labelmap : dict
        Dictionary with ids to class names.
    output_name : str
        Path of the labelmap.pbtxt out file.
    """
    if os.path.isfile(output_name):
        os.remove(output_name)
    end = '\n'
    s = ' '
    for id, name in labelmap.items():
        out = ''
        out += 'item' + s + '{' + end
        out += s*2 + 'id:' + ' ' + str(id) + end
        out += s*2 + 'name:' + ' ' + '\'' + name + '\'' + end
        out += '}' + end*2
    with open(output_name, 'a') as f:
        f.write(out)
    return output_name


class ImageReader(object):
    """Class to decode and read JPEG images
    """

    def __init__(self):
        import tensorflow as tf

        # Initializes function that decodes RGB JPEG data.
        self._decode_jpeg_data = tf.placeholder(dtype=tf.string)
        self._decode_jpeg = tf.image.decode_jpeg(self._decode_jpeg_data, channels=3)

    def read_image_dims(self, sess, image_data):
        image = self.decode_jpeg(sess, image_data)
        return image.shape[0], image.shape[1]

    def decode_jpeg(self, sess, image_data):
        image = sess.run(self._decode_jpeg, feed_dict={self._decode_jpeg_data: image_data})
        assert len(image.shape) == 3
        assert image.shape[2] == 3
        return image


def get_bbox_from_json_row(row, include_bboxes_with_label_empty):
    """Get the bbox field of a record that comes from a JSON file of type COCO

    Parameters
    ----------
    row : dict
        Register of a JSON file of type COCO
    include_bboxes_with_label_empty : bool
        Whether to allow annotations with label 'empty' or not.

    Returns
    -------
    str or np.NaN
        String containing the values of the coordinates of the bbox field,
        in the same format as in `row`.
    """
    if (('bbox' in row and row['bbox'] is not np.NaN and row['label']) and
            (row['label'] != 'empty' or include_bboxes_with_label_empty)):
        return ','.join([str(x) for x in row['bbox']])
    return np.NaN

def get_bbox_from_dataset_row(row):
    """Get the bbox field of a record that comes from an ImageDataset

    Parameters
    ----------
    row : dict
        Register of an ImageDataset

    Returns
    -------
    list of float
        List with the representation of the bounding box of an individual record of a dataset in
        the format `[x, y, width, height]`
    """
    if 'bbox' in row and row['bbox'] != '':
        return [float(coord) for coord in row.split(',')]
    return np.NaN

def get_cats_from_source(categories, clean_names=True):
    """Obtains the categories from one of the following sources:
    - List of strings containing the categories.
    - Path to a CSV file that has no header and contains the categories in `column 0`.
    - Path to a text file containing the categories separated by line breaks.
    - String containing the categories separated by commas.

    Parameters
    ----------
    categories : array-like, str or None
        Source from which to obtain the categories. If None, None is returned
    clean_names : bool, optional
        Whether to clean or not the category names, converting to lower case and removing spaces at
        the beginning and at the end. By default True

    Returns
    -------
    list
        List of the categories

    Raises
    ------
    ValueError
        In case `categories` is invalid
    """
    if categories is None:
        return None
    elif is_array_like(categories):
        results = categories
    elif os.path.isfile(categories):
        if categories.lower().endswith('.csv'):
            df = pd.read_csv(categories, header=None, names=["cat"])
            results = [row["cat"] for _, row in df.iterrows()]
        else:   # text file
            with open(categories, mode="r") as f:
                results = [cat.replace("\n", "") for cat in f.readlines() if cat.replace("\n", "")]
    elif type(categories) is str:
        results = categories.split(",")
    else:
        raise ValueError(f"Invalid value for categories")

    return [get_cleaned_label(x) if clean_names else x for x in results]


def get_list_of_detections(data,
                           inv_labelmap,
                           out_coords_type=coords_utils.COORDINATES_TYPES.RELATIVE,
                           score_threshold=0.3,
                           form_id_with_filename_without_prefix=None,
                           images_sizes=None,
                           add_detection_id=False):
    """Gets a list with the dataset detections. Each item in the list is a dictionary as follows:

    ```
    {
        'detections': [detection],
        'id': str,   # Id of the image
        'max_detection_conf': float
    }
    detection{
        'bbox' : [x, y, width, height],
        'category': str,
        'conf': float,
        'id': str, optional
    }
    ```

    Parameters
    ----------
    data : pd.DataFrame
        DataFrame with the data of the predictions dataset. The DataFrame must have the column
        'image_id'.
    inv_labelmap : dict
        Dictionary with class names to ids, in the form {'class_name': 'id'}
    out_coords_type : COORDINATES_TYPES, optional
        Determines the type of coordinates of the output, either relative or absolute,
        by default coords_utils.COORDINATES_TYPES.RELATIVE
    score_threshold : float, optional
        Threshold for which detections will be considered or ignored, by default 0.3
    form_id_with_filename_without_prefix : str, optional
            Prefix that must be removed from the items to form the id of each element.
            If None, id will be the 'image_id' field of each item.
            By default None
    images_sizes : dict, optional
        Dictionary containing 'width' and 'height' of each image, by default None
    add_detection_id : bool, optional
        Whether to include the `id` in the `detections` field or not, by default False

    Returns
    -------
    list of dict
        List with the detections for each image in the dataset
    """
    results = Manager().list()
    parallel_exec(
        func=add_detection,
        elements=data["item"].unique(),
        item=lambda item: item,
        data=data,
        results=results,
        inv_labelmap=inv_labelmap,
        out_coords_type=out_coords_type,
        score_threshold=score_threshold,
        form_id_with_filename_without_prefix=form_id_with_filename_without_prefix,
        images_sizes=images_sizes,
        add_detection_id=add_detection_id)
    return [x for x in results]


def add_detection(item,
                  data,
                  results,
                  inv_labelmap,
                  out_coords_type,
                  score_threshold,
                  form_id_with_filename_without_prefix,
                  images_sizes,
                  add_detection_id=False):
    """Appends the detections of the `item` to the `results` list, performing a coordinate
    conversion if necessary

    Parameters
    ----------
    item : str
        Item for which detections will be added
    data : pd.DataFrame
        DataFrame containing the data and in which the search for detections will be carried out
    results : list
        List to which the element's detections are appended
    inv_labelmap : dict
        Reverse dictionary containing a mapping of the category names to their corresponding id
    out_coords_type : COORDINATES_TYPES
        Determines the type of coordinates of the output, either relative or absolute
    score_threshold : float
        Threshold for which detections will be considered or ignored, by default 0.3
    form_id_with_filename_without_prefix : str
        Prefix that must be removed from the items to form the id of each element.
        If None, id will be the 'image_id' field of each item.
        By default None
    images_sizes : dict
        Dictionary containing 'width' and 'height' of each image
    add_detection_id : bool, optional
        Whether to include the `id` in the `detections` field or not, by default False
    """
    rows_item = data.loc[(data["item"] == item) & (data["score"] >= score_threshold)]
    if len(rows_item) == 0:
        return
    detections = []
    for _, row in rows_item.iterrows():
        bbox = coords_utils.transform_coordinates(
            row['bbox'],
            input_format=coords_utils.COORDINATES_FORMATS.X_Y_WIDTH_HEIGHT,
            output_format=coords_utils.COORDINATES_FORMATS.X_Y_WIDTH_HEIGHT,
            output_coords_type=out_coords_type,
            output_data_type=coords_utils.COORDINATES_DATA_TYPES.LIST,
            image_width=images_sizes[item]['width'] if images_sizes is not None else None,
            image_height=images_sizes[item]['height'] if images_sizes is not None else None)
        det = {
            'category': str(inv_labelmap[row['label']]),
            'bbox': bbox,
            'conf': row['score']
        }
        if add_detection_id:
            det['id'] = row['id']
        detections.append(det)
    id = (
        os.path.relpath(rows_item.iloc[0]["item"], form_id_with_filename_without_prefix)
        if form_id_with_filename_without_prefix is not None
        else rows_item.iloc[0]["image_id"]
    )
    results.append({
        'detections': detections,
        'id': str(id),
        'max_detection_conf': rows_item.iloc[0]["score"],
        'file': os.path.basename(item)
    })


def download_with_azcopy(filenames,
                         azcopy_exec,
                         batch_size,
                         aux_files_folder,
                         output_imgs_dir,
                         sas_url):
    """Download images with the command-line utility Azcopy

    Parameters
    ----------
    filenames : list of str
        List of the filenames to download
    azcopy_exec : str
        Path of the Azcopy executable
    batch_size : int
        Number of images to be downloaded simultaneously in each request, through auxiliary files
    aux_files_folder : str
        Folder in which auxiliary files will be stored
    output_imgs_dir : str
        Path of the folder in which downloaded images will be stored
    sas_url : str
        SAS (Shared Access Signature) URL of the source collection (E.g., given by LILA BC datasets
        in http://lila.science/wp-content/uploads/2020/03/lila_sas_urls.txt)
    """
    num_aux_files = math.ceil(len(filenames) / batch_size)
    os.makedirs(aux_files_folder, exist_ok=True)
    os.makedirs(output_imgs_dir, exist_ok=True)

    for i in range(num_aux_files):
        az_filename = os.path.join(aux_files_folder, f'{str(uuid.uuid4())}.txt')
        with open(az_filename, 'w') as f:
            for filename in filenames[batch_size*i: batch_size*(i+1)]:
                f.write(filename.replace('\\', '/') + '\n')
        cmd = [
            azcopy_exec, "cp", sas_url, output_imgs_dir,
            "--list-of-files", az_filename,
            "--overwrite=false",
            "--log-level=ERROR",
            "--check-length=false",
            "--check-md5=NoCheck"
        ]
        _ = subprocess.call(cmd)

        os.remove(az_filename)


def sample_data_elements(data,
                         n,
                         random_state=None,
                         by_items=False,
                         groupby='label',
                         n_min=None,
                         fields_to_sort_for_random='id'):
    """Function that samples the elements of a DataFrame by grouping them by field(s) `groupby` and
    taking a random number determined by the value of `n`.

    Parameters
    ----------
    data : pd.DataFrame
        DataFrame containing the elements to be sampled
    n : str, int, float or dict
        If int, indicates the maximum number of samples to be taken from among the elements of each
        `groupby`.
        If float (0, 1), it refers to the percentage of elements to be taken from each `groupby`.
        If str, indicates the name of the method to be used in the sampling operation. The possible
        method names are:
        * fewer: will take as the sample number the smallest value of the elements grouped by label
        in the set.
        * mean: calculates the average of the element counts for each category and takes this value
        as the maximum number of elements for each category.

    random_state : int, optional
        Seed for random number generator, by default None
    by_items : bool, optional
        If True, the data sampling will be applied based on the items of the dataset, otherwise it
        will be applied based on the annotations of the dataset. By default False
    groupby : list of str, str or None, optional
        Field(s) of `data` by which the data will be grouped for sampling.
        If None, no grouping will be done and sampling will be performed for all `data`.
        By default 'label'
    n_min : int, optional
        Minimum number of elements that the elements of each `groupby` must have, otherwise they
        will be discarded. By default None
    fields_to_sort_for_random : str or list of str, optional
        Field(s) by which the elements of the dataset will be sorted to apply random sampling,
        by default 'id'
    Returns
    -------
    pd.DataFrame
        Instance of the modified DataFrame after sampling
    """
    if n is None:
        return data
    if by_items:
        data.drop_duplicates(subset='item', keep='first', inplace=True)

    def sample_num(x, n, rand_state):
        return x.sample(n=n, random_state=rand_state) if len(x) > n else x

    def sample_perc(x, frac, rand_state):
        return x.sample(frac=frac, random_state=rand_state)

    def sample_dict(x, n, rand_state):
        # TODO: generalize this
        if type(x.name) is tuple:
            n_val = n.get(x.name[0])
        else:
            n_val = n.get(x.name)
        if type(n_val) is float and n_val < 1.:
            return sample_perc(x, frac=n_val, rand_state=rand_state)
        elif type(n_val) in (int, float) and n_val >= 1.:
            return sample_num(x, n=n_val, rand_state=rand_state)
        return x

    # Rudimentary way to avoid grouping by any column
    if groupby is None or (groupby == 'label' and 'label' not in data):
        data['dummy_col'] = 'dummy_val'
        groupby = 'dummy_col'

    if random_state is not None:
        if not is_array_like(fields_to_sort_for_random):
            fields_to_sort_for_random = [fields_to_sort_for_random]
        assert_cond = all([x in data.columns for x in fields_to_sort_for_random])
        assert assert_cond, f"Not all the fields are in data: {fields_to_sort_for_random}"
        data = data.sort_values(fields_to_sort_for_random)

    if n_min is not None:
        data = (
            data
            .reset_index(drop=True)
            .groupby(groupby, as_index=False)
            .apply(lambda x: x if len(x) >= n_min else None)
            .reset_index(drop=True)
        )

    if type(n) is float and n < 1.:
        data = (
            data
            .reset_index(drop=True)
            .groupby(groupby, as_index=False)
            .apply(sample_perc, frac=n, rand_state=random_state)
            .reset_index(drop=True)
        )
    if type(n) is str:
        if n == 'fewer':
            counts_res = (
                data
                .groupby(groupby).apply(lambda x: len(x))
                .reset_index(name='counts')
                .sort_values(by="counts", axis=0, ascending=True, inplace=False)
                .iloc[0]["counts"]
            )
        if n == 'mean':
            counts_res = (
                math.ceil(data.groupby(groupby).apply(lambda x: len(x))
                          .reset_index(name='counts')
                          .counts.mean())
            )
        n = int(counts_res)
    if type(n) in (int, float) and n >= 1:
        data = (
            data
            .reset_index(drop=True)
            .groupby(groupby, as_index=False)
            .apply(sample_num, n=n, rand_state=random_state)
            .reset_index(drop=True)
        )
    elif type(n) is dict:
        data = (
            data
            .reset_index(drop=True)
            .groupby(groupby, as_index=False)
            .apply(sample_dict, n=n, rand_state=random_state)
            .reset_index(drop=True)
        )
    if groupby == 'label' and 'label' not in data:
        data.drop('dummy_col', axis=1, inplace=True)
    return data


def fix_partitioning_by_priorities(dataset, priority_order=['train', 'test', 'validation']):
    """Repairs the partitioning of the dataset based on the number of samples for each tag, moving
    elements to higher priority partitions from lower priority partitions

    Parameters
    ----------
    dataset : Dataset
        Dataset to be processed
    priority_order : list, optional
        List of partitions, given in the desired order of priority,
        by default ['train', 'test', 'validation']
    """
    partitions = dataset.get_partitions_names()
    parts = [part for part in priority_order if part in partitions]
    df = dataset.as_dataframe()
    data = df.label.value_counts().to_frame()
    for partition in parts:
        data[partition] = df[df.partition == partition].label.value_counts()
    data.fillna(0, inplace=True)
    # prior_1 goes from highest to lowest + 1 priorities
    for prior_1 in range(len(parts) - 1):
        prior_1_part = parts[prior_1]
        # prior_2 goes from lowest to prior_1 - 1 priorities
        for prior_2 in reversed(range(prior_1 + 1, len(parts))):
            prior_2_part = parts[prior_2]

            # Get labels without samples for prior_1_part and at least one sample in prior_2_part
            lbls = data[(data[prior_1_part] == 0) & (data[prior_2_part] > 0)].index.values
            idxs = set(df[(df.label.isin(lbls)) & (df.partition == prior_2_part)].index.values)
            prior_1_idx = set(dataset.params["partitions"].get(prior_1_part, []))
            prior_2_idx = set(dataset.params["partitions"].get(prior_2_part, []))
            # Move elements from prior_2_part to prior_1_part
            dataset._set_partition(prior_1_part, list(prior_1_idx | idxs))
            dataset._set_partition(prior_2_part, list(prior_2_idx - idxs))

            # When there are categories with more elements in prior_2_part than elements in
            # prior_1_part, then swap them
            counts_per_parts = (
                df[df.partition.isin([prior_1_part, prior_2_part])]
                .groupby(['label', 'partition'])['item']
                .count()
                .unstack()
                .fillna(0)
            )
            lbls = (
                counts_per_parts[counts_per_parts.apply(
                    lambda row: row[prior_2_part] > row[prior_1_part], axis=1)].index.values
            )
            if len(lbls) > 0:
                lbls_in_prior_1_idxs = set(
                    df[(df.label.isin(lbls)) & (df.partition == prior_1_part)].index.values)
                lbls_in_prior_2_idxs = set(
                    df[(df.label.isin(lbls)) & (df.partition == prior_2_part)].index.values)
                prior_1_idx = set(dataset.params["partitions"].get(prior_1_part, []))
                prior_2_idx = set(dataset.params["partitions"].get(prior_2_part, []))
                dataset._set_partition(
                    prior_1_part, list(prior_1_idx - lbls_in_prior_1_idxs | lbls_in_prior_2_idxs))
                dataset._set_partition(
                    prior_2_part, list(prior_2_idx - lbls_in_prior_2_idxs | lbls_in_prior_1_idxs))


def get_new_item_set_media_dir(row,
                               new_items_dict,
                               actual_media_dir,
                               new_media_dir,
                               append_partition_to_item_path,
                               append_label_to_item_path,
                               from_basename,
                               validate_filenames,
                               not_exist_ok):
    """Creates the path that the field 'item' of `row` will have once it is assigned the directory
    `new_media_dir`

    Parameters
    ----------
    row : dict
        Element in a dataset. Must contain the fields 'item', 'partition' and 'label'
    new_items_dict : dict
        Shared dictionary that helps to run this function in parallel, saving the result of the
        generated `item` instead of returning it.
    actual_media_dir : str
        The current media directory for the item.
        May have been assigned in a previous call to the `set_media_dir` method.
    new_media_dir : str
        The media directory which will be assigned to the item.
    append_partition_to_item_path : bool
        Whether to append the 'partition' of each row in the 'item' path or not,
    append_label_to_item_path : bool
        Whether to append the 'label' of each row in the 'item' path or not,
    from_basename : bool
        Whether the initial value of each 'item' is obtained from the basename of the 'item' or not
    validate_filenames : bool
        Whether or not to validate that the items exist. Set to False to speed up execution time
    not_exist_ok : bool
        Whether to include media that exist in `self.MEDIA_FIELD_DIR` and silently pass exceptions
        for those media that are not present

    Returns
    -------
    str
        The new path to be assigned to the 'item' field of the `row`
    """
    if from_basename:   # True only from _callback_to_folder
        new_item = os.path.basename(row['item'])
    elif actual_media_dir:
        new_item = os.path.relpath(row['item'], actual_media_dir)
    else:
        new_item = row['item']

    if append_partition_to_item_path and 'partition' in row:
        new_item = os.path.join(
            os.path.dirname(new_item), row['partition'], os.path.basename(new_item))
    if append_label_to_item_path:
        new_item = os.path.join(
            os.path.dirname(new_item), row['label'], os.path.basename(new_item))

    if validate_filenames:
        if ((new_media_dir is None and os.path.isfile(new_item))
                or (new_media_dir and new_item.startswith(new_media_dir))):
            item = new_item
        elif new_media_dir is not None and os.path.isfile(os.path.join(new_media_dir, new_item)):
            item = os.path.join(new_media_dir, new_item)
        elif not_exist_ok:
            item = np.NaN   # Sentinel value
        else:
            invalid_item = os.path.join(new_media_dir, new_item)
            raise ValueError(f"Item not found in file system: {invalid_item}")
    elif new_media_dir and not new_item.startswith(new_media_dir):
        item = os.path.join(new_media_dir, new_item)
    else:
        item = new_item

    if new_items_dict is not None:
        new_items_dict[row['item']] = item
    else:
        return item


def get_new_item_to_csv(row,
                        media_dir,
                        remove_partition_to_item_path,
                        remove_label_to_item_path):
    """Generates the 'item' to be written to the CSV file

    Parameters
    ----------
    row : dict
        Element in a dataset. Must contain the fields 'item' and 'partition'
    media_dir : str
        The path of the media directory that was assigned to the dataset
    remove_partition_to_item_path : bool
        Whether to remove the `partition` in the 'item' path or not
    remove_label_to_item_path : bool
        Whether to remove the `label` in the 'item' path or not

    Returns
    -------
    str
        The path to be written to the CSV file in the field 'item' for `row`
    """
    new_item = os.path.relpath(row['item'], media_dir)
    if remove_partition_to_item_path and 'partition' in row:
        if row['partition'] == new_item.split(os.sep)[0]:
            new_item = os.path.join(*new_item.split(os.sep)[1:])
    if remove_label_to_item_path and len(new_item.split(os.sep)) > 1:
        new_item = os.path.join(*new_item.split(os.sep)[1:])
    return new_item


def get_filename(image_dict, set_filename_with_id_and_extension=None):
    """Get the file name of the image based on its own image id.

    Parameters
    ----------
    image_dict : dict
        Dict that contains image information with the keys 'file_name' and 'id'
    set_filename_with_id_and_extension : str, optional
        Extension to be added to the id of each item to set the file name (default is None)

    Returns
    -------
    str
        File name of the image.
    """
    if set_filename_with_id_and_extension is not None:
        ext = set_filename_with_id_and_extension
        if ext.startswith('.'):
            ext = ext[1:]
        fname = f'{image_dict["id"]}.{ext}'
    elif 'file_name' in image_dict:
        fname = image_dict['file_name']
    else:
        fname = f'{image_dict["id"]}.jpg'
    return fname.replace('\\', '/')
