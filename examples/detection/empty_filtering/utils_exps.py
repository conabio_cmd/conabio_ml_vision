import pandas as pd
from collections import defaultdict

from conabio_ml_vision.datasets import ImageDataset
from conabio_ml_vision.datasets import ImagePredictionDataset
from conabio_ml_vision.utils.aux_utils import get_species_of_interest

import params


def get_pos_percs(pos_percs_str):
    pos_percs = [float(x) for x in pos_percs_str.split(',')]
    pos_percs = [x if x <= 1 else x / 100 for x in pos_percs]
    assert all([0 < x < 1 for x in pos_percs]), "Invalid values for pos_label_perc"

    return pos_percs


def get_dataset_bin_true(pos_label, filter_species, image_level_ds=True):
    photogr_spcs = get_species_of_interest(params.aves_ft_csv, params.mammals_ft_csv)
    photogr_spcs_and_empty = photogr_spcs + ['empty']
    photogr_spcs_and_persons = photogr_spcs + ['homo sapiens']

    dataset_true = ImageDataset.from_json(params.snmb_json)
    if image_level_ds:
        dataset_true = dataset_true.create_image_level_ds()

    if pos_label == 'animal' and filter_species == 'only-photographables':
        # Include photographable species (no persons) and empty
        dataset_true.filter_by_categories(
            categories=photogr_spcs_and_empty, mode='include', inplace=True)
    elif pos_label == 'animal' and filter_species == 'non-photographable':
        # Exclude photographable species and persons,
        # i.e. include non-photographable species only (no persons) and empty
        dataset_true.filter_by_categories(
            categories=photogr_spcs_and_persons, mode='exclude', inplace=True)
    elif pos_label == 'animal':
        # if pos_label == 'animal': All species (persons included) + empty
        # else pos_label == 'person: Persons vs (All species + empty)
        pass
    bin_map = params.ANIMAL_BIN_MAP if pos_label == 'animal' else params.PERSON_BIN_MAP
    dataset_true.map_categories(mapping_classes=bin_map, inplace=True)

    return dataset_true


def get_md_ds(md_version, thresh=None):
    animal_dets_ds = ImagePredictionDataset.from_csv(params.md_dets_csvs[md_version]['animal'])
    empty_dets_ds = ImagePredictionDataset.from_csv(params.md_dets_csvs[md_version]['empty'])
    dets_ds = ImagePredictionDataset.from_datasets(animal_dets_ds, empty_dets_ds)
    dets_ds.map_categories(mapping_classes=params.DETECTIONS_MAP, inplace=True)
    # animal_filter_mode = 'include' if pos_label == 'animal' else 'exclude'
    # dets_ds.filter_by_categories(categories='animal', mode=animal_filter_mode, inplace=True)
    if thresh is not None:
        dets_ds.filter_by_score(min_score=thresh, inplace=True)

    return dets_ds
