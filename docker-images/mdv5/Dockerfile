FROM nvidia/cuda:11.3.1-cudnn8-runtime-ubuntu20.04

ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update && apt-get install -y wget git python3-dev python3-pip ffmpeg libsm6 libxext6 && rm -rf /var/lib/apt/lists/*

# Megadetector installation
WORKDIR /lib/megadetector
RUN git clone https://github.com/Microsoft/cameratraps /lib/megadetector/cameratraps
RUN git clone https://github.com/Microsoft/ai4eutils /lib/megadetector/ai4eutils
# For MDv5
RUN git clone https://github.com/ultralytics/yolov5/ /lib/megadetector/yolov5
RUN cd /lib/megadetector/yolov5 && git checkout c23a441c9df7ca9b1f275e8c8719c949269160d1

RUN pip install --upgrade pip && pip install jupyter "jupyterlab>=2.0.0,<3.0.0" --upgrade

# conabio_ml_vision installation
RUN git clone https://bitbucket.org/conabio_cmd/conabio_ml_vision.git /lib/conabio_ml_vision
RUN pip install -r /lib/conabio_ml_vision/requirements.txt
RUN git clone https://bitbucket.org/conabio_cmd/conabio_ml.git /lib/conabio_ml_vision/conabio_ml
RUN pip install -r /lib/conabio_ml_vision/conabio_ml/requirements.txt

# MDv5 dependencies
RUN pip install ipykernel
RUN pip install jsonpickle
RUN pip install humanfriendly
RUN pip install PyYAML>=5.3.1
RUN pip install torch==1.10.1
RUN pip install torchvision==0.11.2

# Create alias for python3 -> python
RUN ln -s /usr/bin/python3 /usr/bin/python

ENV PYTHONPATH=$PYTHONPATH:/lib/conabio_ml_vision:/lib/conabio_ml_vision/conabio_ml
ENV PYTHONPATH=$PYTHONPATH:/lib/megadetector/cameratraps:/lib/megadetector/ai4eutils:/lib/megadetector/yolov5
ENV MEGADETECTOR_EXEC=/lib/megadetector/cameratraps/detection/run_detector_batch.py
ENV PYTHON_EXEC=python

CMD [ "echo" , "Environment for Megadetector v5 installed!"]
