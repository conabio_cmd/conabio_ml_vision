import os
from typing import TypeVar
from itertools import cycle
from multiprocessing import Manager
from collections import defaultdict

import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.metrics import precision_score, recall_score, f1_score, confusion_matrix

from conabio_ml.datasets.dataset import Partitions
from conabio_ml.evaluator.evaluator import Evaluator, Metrics
from conabio_ml.evaluator.generic.evaluator import Evaluator as GenEval
from conabio_ml.assets import AssetTypes
from conabio_ml_vision.datasets import ImagePredictionDataset, ImageDataset
from conabio_ml_vision.preprocessing.process_images import aspect_preserving_resize
from conabio_ml.utils.utils import get_and_validate_args
from conabio_ml.utils import report_params
from conabio_ml.utils.report_params import report_transl as transl
from conabio_ml.utils.report_params import languages
from conabio_ml.utils.logger import get_logger
from conabio_ml.utils.utils import parallel_exec
from conabio_ml_vision.utils.evaluator_utils import get_min_diffs

logger = get_logger(__name__)


class ImageClassificationMetrics(Metrics):
    """Class containing the results of the evaluation of a classification model in images datasets
    """

    def __init__(self, results, dataset_true, dataset_pred, eval_config):
        """Create a Metrics instance for an image dataset classification model

        Parameters
        ----------
        results : dict
            Dictionary that contains the results of an evaluation of a classification model in
            image datasets with the following structure:
            {
                `metric_set`: {
                    'one_class': {
                        `metric_name`: `value`, ...
                    },
                    'per_class': {
                        `metric_name`: `value`, ...
                    }
                }
            }

            Where `metric_set` must be one of ("BINARY", "MULTICLASS")
        dataset_true : ImageDataset
            Image dataset of true labels
        dataset_pred : ImagePredictionDataset
            Image prediction dataset that coming from a Model's predict method
        eval_config : dict
            Dictionary that contains the configuration of the evaluation process. This must contain
            the elements defined in the `args_eval_def` property of the class
        """
        super().__init__(results)
        self.dataset_true = dataset_true
        self.dataset_pred = dataset_pred
        self.eval_config = eval_config

    ImageClassificationMetricsType = TypeVar('ImageClassificationMetricsType',
                                             bound='ImageClassificationMetrics')

    def reporter(self: ImageClassificationMetricsType,
                 dest_path: str,
                 process_args: dict,
                 **kwargs):
        """Method that generates the report elements of evaluated metrics. It contains:
            - results: Path of the evaluation results JSON file.
            - result_plots: Resulting plots of the evaluation.
            - result_plots_trunc: Resulting plots of the evaluation with `MAX_REGISTERS_RESUME`
            number of records that will be shown in the per-class plots.
            - classification_examples: Best and worst examples of classification, and those that
            are on the boundary between two classes.
            - results_tables: Tables of the evaluation results
            - eval_config: Options to define the evaluation params of the model

        Parameters
        ----------
        dest_path : str
            Path of the folder to store the resulting files
        process_args : dict
            The args of the process that wraps this class
        **kwargs :
            Extra named arguments that may contains:
                * lang : language of the report, by default languages.EN

        Returns
        -------
        dict
            Params to be reported in the Pipeline
        """
        lang = kwargs.get("lang", languages.EN)
        results_path = os.path.join(dest_path, "results.json")
        self.store_eval_metrics(dest_path=results_path)
        data_to_report = {
            "results": {
                "asset": results_path,
                "type": AssetTypes.DATA
            },
            "result_plots": {
                "asset": self.result_plots(dest_path, lang=lang, report=True),
                "type": AssetTypes.PLOT
            },
            "result_plots_trunc": {
                "asset": self.result_plots(dest_path, report_params.MAX_REGISTERS_RESUME,
                                           lang, report=True),
                "type": AssetTypes.PLOT
            },
            "classification_examples": {
                "asset": self.classification_examples(5, dest_path),
                "type": AssetTypes.FILE
            },
            "results_tables": {
                "asset": self.get_results_tables(),
                "type": AssetTypes.DATA
            },
            "eval_config": {
                "asset": self.eval_config,
                "type": AssetTypes.DATA
            }
        }
        return super().reporter(dest_path, process_args, data_to_report)

    def get_results_tables(self):
        """Arrange the evaluation results in a format that makes it easy to create the tables that
        will be displayed in a report

        Returns
        -------
        dict
            Dictionary of the form
            {`metric_set`: 'one_class': {'metric': [`metrics`], 'value': [`metric_values`]},
                           'per_class': {'category': [`categories`],
                                         `metric_name`: [`metric_values`]}}
        """
        # metric_set: 'BINARY', 'MULTICLASS'
        for metric_set, metrics_info in self.results.items():
            data_one_class = defaultdict(lambda: defaultdict(list))
            data_per_class = defaultdict(lambda: defaultdict(list))
            # metric_type: one_class, per_class
            for metric_type, metric_values in metrics_info.items():
                if metric_type == 'one_class':
                    for metric_name, metric_value in metric_values.items():
                        data_one_class[metric_set]["metric"].append(metric_name)
                        data_one_class[metric_set]["value"].append(metric_value)
                else:
                    for category, metrics_cat in metric_values.items():
                        data_per_class[metric_set]["category"].append(category.capitalize())
                        for metric_name, metric_value in metrics_cat.items():
                            data_per_class[metric_set][metric_name].append(metric_value)
            # Order per-class data
            metrics_names = list(
                set([x for x in data_per_class[metric_set].keys()]) - set(["category"]))
            df_per_class = pd.DataFrame(data=data_per_class[metric_set])
            df_per_class.sort_values(by=metrics_names, axis=0, ascending=False, inplace=True)
            data_per_class[metric_set] = df_per_class.to_dict(orient="list")

        return {
            metric_set: {
                "one_class": dict(data_one_class[metric_set]),
                "per_class": dict(data_per_class[metric_set])
            } for metric_set in self.results.keys()
        }

    def classification_examples(self, n_examples, dest_path, top_n=5):
        """Calculate the `n_examples` best and worst evaluated examples, as well as those that are
        on the boundary between two classes, and creates a scaled copy of images that will be
        stored at `dest_path`

        Parameters
        ----------
        n_examples : int
            The number of examples that are taken from each of the types (best, worst, boundary)
        dest_path : str
            Path where the examples images will be stored
        top_n : int, optional
            The highest number of detections to get from each example, by default 5

        Returns
        -------
        dict
            Dictionary containing the classification examples in the form
            {`example_type`: [{'item': `item`, 'top_n': [{'label': `label`, 'score': `score`}]}]}
        """
        results = {
            "best": [],
            "worst": [],
            "boundary": []
        }
        # Compute 'best' and 'worst' examples
        df_true = self.dataset_true.as_dataframe()
        preds_hghst_scrs = self.dataset_pred.as_dataframe(sort_by="score", sort_asc=False)
        preds_hghst_scrs.drop_duplicates(subset='item', keep='first', inplace=True)
        for _, pred_row in preds_hghst_scrs.iterrows():
            if len(results["best"]) == n_examples and len(results["worst"]) == n_examples:
                break
            true_rows = df_true.loc[df_true["item"] == pred_row["item"]]
            if any([t_row["label"] == pred_row["label"] for _, t_row in true_rows.iterrows()]):
                if len(results["best"]) < n_examples:
                    results["best"].append(pred_row["item"])
            else:
                if len(results["worst"]) < n_examples:
                    results["worst"].append(pred_row["item"])
        # Compute 'boundary' examples
        df_pred = self.dataset_pred.as_dataframe()
        # I obtain the 2 highest detections of each item
        preds_2_hghst_scrs = self.dataset_pred.as_dataframe(sort_by="score", sort_asc=False)
        preds_2_hghst_scrs = preds_2_hghst_scrs.groupby('item').head(2)
        preds_2_hghst_scrs.sort_values(by=["item", "score"], axis=0, ascending=False, inplace=True)
        if len(df_pred["item"].unique()) == int(len(preds_2_hghst_scrs) / 2):
            item_diff = {
                "item": [],
                "diff": []
            }
            # I compute the difference in scores of each items's 2 highest detections
            for i in range(int(len(preds_2_hghst_scrs) / 2)):
                item_diff["item"].append(preds_2_hghst_scrs.iloc[i*2]["item"])
                item_diff["diff"].append(preds_2_hghst_scrs.iloc[i*2]["score"] -
                                         preds_2_hghst_scrs.iloc[i*2+1]["score"])
            preds_smllr_dffs = pd.DataFrame(item_diff)
            # Sort the resulting differences by 'diff' in order to get the items with the smallest
            # differences at the top
            preds_smllr_dffs.sort_values(by=["diff"], axis=0, ascending=True, inplace=True)
            for i in range(n_examples):
                results["boundary"].append(preds_smllr_dffs.iloc[i]["item"])
        else:
            logger.warning(
                f"In order to set boundary examples there must be at least 2 predictions per item")

        res = defaultdict(list)
        items_lst, new_items_lst = [], []
        for ex_type, examples in results.items():
            for item in examples:
                fname = os.path.basename(item)
                new_item = os.path.join(dest_path, fname)
                items_lst.append(item)
                new_items_lst.append(new_item)
                n_items = df_pred.loc[df_pred["item"] == item].sort_values(by=["score"],
                                                                           axis=0,
                                                                           ascending=False)[:top_n]
                res[ex_type].append({
                    "item": new_item,
                    "top_n": [{"label": x["label"], "score":x["score"]}
                              for _, x in n_items.iterrows()]
                })
        aspect_preserving_resize(items_lst, new_items_lst, report_params.EXAMPLES_LARGEST_SIDE)

        return dict(res)

    def result_plots(self,
                     dest_path: str = "",
                     truncate_categories: int = None,
                     lang=languages.EN,
                     report=False,
                     **kwargs):
        """Plot the results of the evaluation in different graphs depending on its type

        Parameters
        ----------
        dest_path : str, optional
            Base path where the plots will be saved, by default ""
        truncate_categories : int, optional
            Maximum number of records that will be shown in the per-class plots.
            The `truncate_categories` records with the highest value will be displayed.
            In case there are fewer records than `truncate_categories`, `None` will be returned.
            By default None
        lang : languages, optional
            Language of the report, by default languages.EN
        report : bool, optional
            Whether the plot will be saved as an image file or displayed in a window,
            by default False

        Returns
        -------
        dict
            Dictionary with the resulting assets in the form
            {`metric_set`: {'per_class': `metric_name`: `asset`,
                            'one_class': `metric_name`: `asset`}
            }
        """
        prefix_filename = kwargs.get("prefix_filename", '')
        sort_plots_metrics_by = kwargs.get("sort_plots_metrics_by", 'value')

        partition = self.eval_config.get("partition", None)
        df_true = self.dataset_true.get_rows(partition, sort_by="item")
        df_true = df_true.drop_duplicates(subset='item', keep='first', inplace=False)
        df_true = df_true.label.value_counts().rename_axis('label').to_frame('counts')
        labels_counts_true = df_true.to_dict(orient='index')

        df_pred = self.dataset_pred.as_dataframe(only_highest_score=True, sort_by="item")
        df_pred = df_pred.label.value_counts().rename_axis('label').to_frame('counts')
        labels_counts_pred = df_pred.to_dict(orient='index')

        result_assets = {}
        # metric_set: 'BINARY', 'MULTICLASS'
        for metric_set, metrics in self.results.items():
            data_one_class = defaultdict(list)
            data_per_class = defaultdict(lambda: defaultdict(list))
            data_confusion_matrix = None
            data_labels = None
            result_assets[metric_set] = {"per_class": {}, "one_class": {}}
            # metric_type: one_class, per_class
            for metric_type, metric_values in metrics.items():
                if metric_type == 'one_class':
                    # metric_name: precision, recall, f1_score, confusion_matrix
                    for metric_name, metric_value in metric_values.items():
                        if metric_name == 'confusion_matrix':
                            data_confusion_matrix = np.array(metric_value)
                        elif metric_name == 'labels':
                            data_labels = metric_value
                        else:
                            data_one_class["metric"].append(
                                transl["metric_plt"]["metric_name"][metric_name][lang])
                            data_one_class["value"].append(metric_value)
                else:
                    for cat, metrics_cat in metric_values.items():
                        # metric_name: precision, recall, f1_score
                        for metric_name, metric_value in metrics_cat.items():
                            data_per_class[metric_name]["category"].append(cat.capitalize())
                            data_per_class[metric_name]["value"].append(metric_value)

                            true = labels_counts_true.get(cat, {}).get('counts', 0)
                            pred = labels_counts_pred.get(cat, {}).get('counts', 0)
                            data_per_class[metric_name]["predicted"].append(pred)
                            data_per_class[metric_name]["true"].append(true)
                            if metric_name == 'precision':
                                tp = round(metric_value * pred)
                                fp = round(pred * (1 - metric_value))
                                data_per_class[metric_name]["TP"].append(tp)
                                data_per_class[metric_name]["FP"].append(fp)
                            if metric_name == 'recall':
                                tp = round(metric_value * true)
                                fn = round(true * (1 - metric_value))
                                data_per_class[metric_name]["TP"].append(tp)
                                data_per_class[metric_name]["FN"].append(fn)
            # Per-class results
            for metric_name, metrics_val in data_per_class.items():
                extra_cols = []

                extra_cols = ['predicted', 'true']
                if metric_name == 'precision':
                    extra_cols += ["TP", "FP"]
                if metric_name == 'recall':
                    extra_cols += ["TP", "FN"]

                df = pd.DataFrame(data=metrics_val, columns=["category", "value"] + extra_cols)
                if sort_plots_metrics_by == 'category':
                    df.sort_values(by=["category"], axis=0, ascending=True, inplace=True)
                else:
                    df.sort_values(by=["value"], axis=0, ascending=False, inplace=True)
                if truncate_categories is not None:
                    if len(df) <= truncate_categories:
                        return None
                    df = df.head(truncate_categories)
                if report:
                    metric_id = f"{prefix_filename}{metric_set}-per_class-{metric_name}"
                    filename = os.path.join(dest_path, metric_id)
                    filename = f"{filename}-trunc" if truncate_categories is not None else filename
                else:
                    filename = None
                paths = self.plot_numeric_metric(metric_results=df,
                                                 x_axis="category",
                                                 y_axis="value",
                                                 x_label=transl["metric_plt"]["category"][lang],
                                                 y_label=metric_name,
                                                 filename=filename,
                                                 **kwargs)
                if report:
                    result_assets[metric_set]["per_class"][metric_name] = paths["plot"]
            # One-class results
            if report:
                metric_id = f"{prefix_filename}{metric_set}-one_class"
                filename = os.path.join(dest_path, metric_id)
                filename = f"{filename}-trunc" if truncate_categories is not None else filename
            else:
                filename = None
            df = pd.DataFrame(data=data_one_class, columns=["metric", "value"])
            paths = self.plot_numeric_metric(metric_results=df,
                                             x_axis="metric",
                                             y_axis="value",
                                             x_label=transl["metric_plt"]["metric"][lang],
                                             y_label=transl["metric_plt"]["value"][lang],
                                             filename=filename,
                                             **kwargs)
            if report:
                result_assets[metric_set]["one_class"]["general_metrics"] = paths["plot"]
            if data_confusion_matrix is not None:
                if report:
                    metric_id = f"{prefix_filename}{metric_set}-one_class-confusion_matrix"
                    filename = os.path.join(dest_path, metric_id)
                    filename = f"{filename}-trunc" if truncate_categories is not None else filename
                else:
                    filename = None
                paths = self.plot_confusion_matrix(data_confusion_matrix, data_labels,
                                                   filename=filename)
                if report:
                    result_assets[metric_set]["one_class"]["confusion_matrix"] = paths["plot"]
        if report:
            return result_assets

    def plot_numeric_metric(self,
                            metric_results,
                            x_axis, y_axis,
                            x_label, y_label,
                            filename=None,
                            **kwargs):
        """Plot metric results as bar graphs, either saving it to an image file or displaying it
        in a window

        Parameters
        ----------
        metric_results : pd.DataFrame
            Data containing the results of the metrics with columns `x_axis` and `y_axis`
        x_axis : str
            Column name for x-axis
        y_axis : str
            Column name for y-axis
        x_label : str
            Label for x-axis
        y_label : str
            Label for y-axis
        filename : str, optional
            Full path without file extension where the bar plot is saved as an image.
            If None, the bar plot will be displayed in a window instead. By default None

        Returns
        -------
        dict
            Dictionary of the form (plot, asset)
        """
        title = kwargs.get('title', None)
        try:
            if filename is not None:
                plot_filename = f"{filename}.png"
                asset_filename = f"{filename}.csv"
                metric_results.to_csv(asset_filename, index=False)

            figsize = (10, 10)
            if len(metric_results) > report_params.MAX_REGISTERS_RESUME:
                x_axis, y_axis = y_axis, x_axis
                x_label, y_label = y_label, x_label
                figsize = (17, 30)
                rotation = 0
            elif len(metric_results) > 5:
                rotation = 30
            else:
                rotation = 0
            sns.set(style="whitegrid")
            _, ax = plt.subplots(1, 1, figsize=figsize)
            g = sns.barplot(x=x_axis, y=y_axis, data=metric_results, ax=ax)
            if y_axis == 'value':
                ax.set_ylim(0, 1)
            else:
                ax.set_xlim(0, 1)
            if rotation > 0 and x_axis == "category":
                g.set_xticklabels(g.get_xticklabels(), rotation=rotation)
            plt.xlabel(x_label)
            plt.ylabel(y_label)
            if title is not None:
                plt.title(title)
            if filename is not None:
                plt.savefig(plot_filename, dpi=100)
                plt.close()
                metric_asset = {"plot": plot_filename,
                                "asset": asset_filename}
                return metric_asset
            else:
                plt.show()
        except Exception as ex:
            logger.exception(f"{ex}")
            raise(ex)

    def plot_confusion_matrix(self,
                              data,
                              labels,
                              filename=None,
                              **kwargs):
        """Plot the confusion matrix of `data`, either saving it to an image file or displaying it
        in a window

        Parameters
        ----------
        data : ndarray
            Array containing the data to display
        labels : list
            List of category names
        filename : str, optional
            Full path without file extension where the confusion matrix is saved as an image.
            If None, the confusion matrix will be displayed in a window instead. By default None

        Returns
        -------
        dict
            Dictionary of the form (plot, asset)
        """
        if filename is not None:
            plot_filename = f"{filename}.png"
        fig, ax = plt.subplots(1, 1, figsize=(40, 30))
        labels = labels or 'auto'
        vmax = 1 if data.max() <= 1 else data.max()
        ax = sns.heatmap(data, cmap='coolwarm', yticklabels=labels, xticklabels=labels,
                         annot=False, vmin=0, vmax=vmax)

        if filename is not None:
            plt.savefig(plot_filename, dpi=100)
            plt.close()
            metric_asset = {"plot": plot_filename,
                            "asset": None}
            return metric_asset
        else:
            plt.show()

    class Sets():
        """
        Allowed types of metrics sets
        """
        BINARY = "BINARY"
        MULTICLASS = "MULTICLASS"
        MULTILABEL = "MULTILABEL"
        NAMES = [BINARY, MULTICLASS, MULTILABEL]


class ImageClassificationEvaluator(Evaluator):
    """
    Class to calculate the classification metrics of a model on a set of images

    Attributes
    ----------
    dataset_pred : ImagePredictionDataset
        Dataset of predictions that coming from Model.predict method
    dataset_true : ImageDataset
        Dataset of true labels
    eval_config : dict
        Options to define the evaluation params of the model
    """
    ImageClassificationEvaluatorType = TypeVar('ImageClassificationEvaluatorType',
                                               bound='ImageClassificationEvaluator')

    args_eval_def = {
        'metrics_set': {
            'type': dict,
            'default': {
                ImageClassificationMetrics.Sets.MULTICLASS: {}
            }
        },
        'partition': {
            'type': [None, str],
            'default': Partitions.TEST
        },
        'eval_dir': {
            'type': str,
            'default': './eval'
        },
        'labels': {
            'type': list,
            'default': None
        },
        'export_path': {
            'type': str,
            'default': './export'
        }
    }

    args_metrics_def = {
        ImageClassificationMetrics.Sets.BINARY: {
            'pos_label': {
                'type': [str, int],
                'default': None
            },
            'sample_weight': {
                'type': [list, tuple],
                'default': None
            }
        },
        ImageClassificationMetrics.Sets.MULTICLASS: {
            'labels': {
                'type': list,
                'default': None
            },
            'average': {
                'type': str,
                'default': 'macro'
            },
            'sample_weight': {
                'type': [list, tuple],
                'default': None
            }
        }
    }

    classification_metrics_map = {
        'precision': precision_score,
        'recall': recall_score,
        'f1_score': f1_score,
    }

    evaluation_def = {
        "precision": {
            'labels': {
                'type': list,
                'optional': False
            },
            'pos_label': {
                'type': str,
                'optional': True
            },
            'average': {
                'type': [None, str],
                'optional': True,
                'default': 'macro'
            },
            'sample_weight': {
                'type': [list, tuple],
                'default': None
            },
            'zero_division': {
                'type': [float, int, str],
                'optional': True,
                'default': 'warn'
            }
        },
        'recall': {
            'labels': {
                'type': list,
                'optional': False
            },
            'pos_label': {
                'type': str,
                'optional': True
            },
            'average': {
                'type': [None, str],
                'optional': True,
                'default': 'macro'
            },
            'sample_weight': {
                'type': [list, tuple],
                'default': None
            },
            'zero_division': {
                'type': [float, int, str],
                'optional': True,
                'default': 'warn'
            }
        },
        'f1_score': {
            'labels': {
                'type': list,
                'optional': False
            },
            'pos_label': {
                'type': str,
                'optional': True
            },
            'average': {
                'type': [None, str],
                'optional': True,
                'default': 'macro'
            },
            'sample_weight': {
                'type': [list, tuple],
                'default': None
            },
            'zero_division': {
                'type': [float, int, str],
                'optional': True,
                'default': 'warn'
            }
        },
        'confusion_matrix': {
            'labels': {
                'type': list,
                'optional': False
            },
            'sample_weight': {
                'type': [list, tuple],
                'default': None
            },
            'normalize': {
                'type': [str, None],
                'default': 'true'
            }
        },
        'accuracy': {
            'labels': {
                'type': list,
                'optional': False
            },
            'pos_label': {
                'type': str,
                'optional': True
            },
            'average': {
                'type': [None, str],
                'optional': True,
                'default': 'macro'
            },
            'sample_weight': {
                'type': [list, tuple],
                'default': None
            }
        }
    }

    def __init__(self,
                 dataset_true,
                 dataset_pred,
                 eval_config):
        self.dataset_pred = dataset_pred
        self.dataset_true = dataset_true
        self.eval_config = get_and_validate_args(eval_config, self.args_eval_def)

    def evaluation(self, y_true, y_pred, metrics_opts,
                   eval_type=ImageClassificationMetrics.Sets.MULTICLASS):
        """Performs multi-class of binary evaluation with true and predicted images

        Parameters
        ----------
        y_true : list of str
            List of true labels
        y_pred : list of str
            List of predicted labels
        metrics_opts : dict
            Dictionary with the configuration options of the evaluation.
            Options will be validated against `evaluation_def`
        eval_type : str, optional
            Type of the evaluation to perform,
            by default ImageClassificationMetrics.Sets.MULTICLASS

        Returns
        -------
        dict
            Dictionary containing the results of evaluation, in the form:
            {'per_class': {`category`: {`metric_name`: `metric_value`}}
             'one_class': {`metric_name`: `metric_value`}
            }
        """
        res_multi = {
            'per_class': defaultdict(dict),
            'one_class': {}
        }
        matrix_opts = get_and_validate_args(metrics_opts,
                                            self.evaluation_def["confusion_matrix"],
                                            ignore_invalid=True)

        if eval_type == ImageClassificationMetrics.Sets.BINARY:
            if len(metrics_opts["labels"]) != 2:
                raise ValueError("For BINARY classification labels must be two.")

        res_multi['one_class'] = {
            'confusion_matrix': confusion_matrix(y_true, y_pred, **matrix_opts).tolist(),
            "labels": [x.capitalize() for x in matrix_opts["labels"]]
        }
        for metric_name, metric in self.classification_metrics_map.items():
            metric_opts = get_and_validate_args(metrics_opts,
                                                self.evaluation_def[metric_name],
                                                ignore_invalid=True)
            if eval_type == ImageClassificationMetrics.Sets.BINARY:
                metric_opts["average"] = 'binary'
            res_multi['one_class'][metric_name] = metric(y_true, y_pred, **metric_opts)

            if "average" not in metric_opts:
                continue
            metric_opts["average"] = None
            if 'pos_label' in metric_opts:
                del metric_opts['pos_label']
            per_class_result = metric(y_true, y_pred, **metric_opts)
            for i, label in enumerate(metric_opts["labels"]):
                res_multi['per_class'][label][metric_name] = per_class_result[i]

        return res_multi

    @classmethod
    def eval(cls,
             dataset_true: ImageDataset.DatasetType,
             dataset_pred: ImagePredictionDataset.DatasetType,
             eval_config: dict = {}) -> Metrics.MetricsType:
        """Perform the evaluation of a model from the prediction dataset resulting
        from the inference on a set of images, the original dataset, and an evaluation
        configuration

        Parameters
        ----------
        dataset_true : ImageDataset.DatasetType
            Dataset of true labels
        dataset_pred : ImagePredictionDataset.DatasetType
            Dataset of predictions that coming from Model.predict method
        eval_config : dict, optional
            Options to define the evaluation params of the model, by default {}

        Returns
        -------
        ImageClassificationMetrics
            Result with evaluation metrics

        Raises
        ------
        ValueError
            In case both the prediction and true items not be the same
        """
        instance = cls(dataset_true, dataset_pred, eval_config)

        if "partition" in instance.eval_config:
            partition = instance.eval_config["partition"]
        else:
            partition = None

        df_true = instance.dataset_true.get_rows(partition, sort_by="item")
        df_true = df_true.drop_duplicates(subset='item', keep='first', inplace=False)
        df_pred = instance.dataset_pred.as_dataframe(only_highest_score=True, sort_by="item")

        y_true = [x for x in df_true["label"]]
        y_pred = [x for x in df_pred["label"]]

        if not set(df_true["item"]) == set(df_pred["item"]):
            raise ValueError("Elements in y_true and y_pred are not the same")

        results = {}
        for metric_set_name, config in instance.eval_config['metrics_set'].items():
            config["labels"] = (instance.eval_config.get("labels", None) or
                                list(set(df_true["label"]).union(set(df_pred["label"]))))
            if metric_set_name in (ImageClassificationMetrics.Sets.MULTICLASS,
                                   ImageClassificationMetrics.Sets.BINARY):
                res = instance.evaluation(y_true=y_true,
                                          y_pred=y_pred,
                                          metrics_opts=config,
                                          eval_type=metric_set_name)
            elif metric_set_name == ImageClassificationMetrics.Sets.MULTILABEL:
                eval_config["metrics_set"][metric_set_name]["pre_eval_func"] = \
                    lambda ds, metric_opts: ds.as_dataframe(
                        only_highest_score=True, sort_by="item")
                metrics_res = GenEval.eval(dataset_true, dataset_pred, eval_config)
                res = metrics_res.results[ImageClassificationMetrics.Sets.MULTILABEL]
            results[metric_set_name] = res
        return ImageClassificationMetrics(results, dataset_true, dataset_pred, eval_config)


class ImageObjectDetectionMetrics(Metrics):

    def __init__(self, results, dataset_true, dataset_pred, eval_config):
        super().__init__(results)
        self.dataset_true = dataset_true
        self.dataset_pred = dataset_pred
        self.eval_config = eval_config

    ImageObjectDetectionMetricsType = TypeVar('ImageObjectDetectionMetricsType',
                                              bound='ImageObjectDetectionMetrics')

    def reporter(self: ImageObjectDetectionMetricsType,
                 dest_path: str,
                 process_args: dict,
                 **kwargs):
        """Method that generates the report elements of evaluated metrics. It contains:
            - results: Path of the evaluation results JSON file.
            - result_plots: Resulting plots of the evaluation.
            - result_plots_trunc: Resulting plots of the evaluation with `MAX_REGISTERS_RESUME`
            number of records that will be shown in the per-class plots.
            - classification_examples: Best and worst examples of classification, and those that
            are on the boundary between two classes.
            - results_tables: Tables of the evaluation results
            - eval_config: Options to define the evaluation params of the model

        Parameters
        ----------
        dest_path : str
            Path of the folder to store the resulting files
        process_args : dict
            The args of the process that wraps this class
        **kwargs :
            Extra named arguments that may contains:
                * lang : language of the report, by default languages.EN

        Returns
        -------
        dict
            Params to be reported in the Pipeline
        """
        lang = kwargs.get("lang", languages.EN)
        results_path = os.path.join(dest_path, "results.json")
        self.store_eval_metrics(dest_path=results_path)
        data_to_report = {
            "results": {
                "asset": results_path,
                "type": AssetTypes.DATA
            },
            "result_plots": {
                "asset": self.result_plots(dest_path=dest_path, lang=lang),
                "type": AssetTypes.PLOT
            },
            "result_plots_trunc": {
                "asset": self.result_plots(dest_path, report_params.MAX_REGISTERS_RESUME, lang),
                "type": AssetTypes.PLOT
            },
            "classification_examples": {
                "asset": self.classification_examples(n_examples=5, dest_path=dest_path),
                "type": AssetTypes.FILE
            },
            "results_tables": {
                "asset": self.get_results_tables(),
                "type": AssetTypes.DATA
            },
            "eval_config": {
                "asset": self.eval_config,
                "type": AssetTypes.DATA
            }
        }
        return super().reporter(dest_path, process_args, data_to_report)

    def get_results_tables(self):
        """Arrange the evaluation results in a format that makes it easy to create the tables that
        will be displayed in a report

        Returns
        -------
        dict
            Dictionary of the form
            {`metric_set`: 'one_class': {'metric': [`metrics`], 'value': [`metric_values`]},
                           'per_class': {'category': [`categories`],
                                         `metric_name`: [`metric_values`]}}
        """
        # metric_set = 'BINARY', 'MULTICLASS'
        for metric_set, metrics_info in self.results.items():
            data_one_class = defaultdict(lambda: defaultdict(list))
            data_per_class = defaultdict(lambda: defaultdict(list))
            # metric_type = one_class, per_class
            for metric_type, metric_values in metrics_info.items():
                if metric_type == 'one_class':
                    for metric_name, metric_value in metric_values.items():
                        if metric_name != "is_class_correctly_detected_in_images":
                            data_one_class[metric_set]["metric"].append(metric_name)
                            data_one_class[metric_set]["value"].append(metric_value)
                else:
                    for category, metrics_cat in metric_values.items():
                        data_per_class[metric_set]["category"].append(category.capitalize())
                        for metric_name, metric_value in metrics_cat.items():
                            if metric_name not in ("precisions", "recalls",
                                                   "is_class_correctly_detected_in_images"):
                                data_per_class[metric_set][metric_name].append(metric_value)
            # Order per-class data
            metrics_names = list(set([x for x in data_per_class[metric_set].keys()])
                                 - set(["category"]))
            df_per_class = pd.DataFrame(data=data_per_class[metric_set])
            df_per_class.sort_values(by=metrics_names, axis=0, ascending=False, inplace=True)
            data_per_class[metric_set] = df_per_class.to_dict(orient="list")

        return {
            metric_set: {
                "one_class": dict(data_one_class[metric_set]),
                "per_class": dict(data_per_class[metric_set])
            } for metric_set in self.results.keys()
        }

    def classification_examples(self, n_examples, dest_path, score_threshold=0.3):
        """Calculate the `n_examples` best and worst evaluated examples, as well as those that are
        on the boundary between two classes, and creates the images showing the predicted
        bounding boxes that will be stored at `dest_path`

        Parameters
        ----------
        n_examples : int
            The number of examples that are taken from each of the types (best, worst, boundary)
        dest_path : str
            Path where the examples images will be stored
        score_threshold : float, optional
            The minimum value of the score predictions that will be considered, by default 0.3

        Returns
        -------
        dict
            Dictionary containing the classification examples in the form
            {`example_type`: [{'item': `item`}]}
        """
        results = {
            "best": [],
            "worst": [],
            "boundary": []
        }
        # Compute 'best' and 'worst' examples
        df_true = self.dataset_true.as_dataframe()
        df_pred = self.dataset_pred.as_dataframe(sort_by="score", sort_asc=False)
        for _, pred_row in df_pred.iterrows():
            if (len(results["best"]) == n_examples and len(results["worst"]) == n_examples):
                break
            true_rows = df_true.loc[df_true["item"] == pred_row["item"]]
            if any([t_row["label"] == pred_row["label"] for _, t_row in true_rows.iterrows()]):
                if len(results["best"]) < n_examples and pred_row["item"] not in results["best"]:
                    results["best"].append(pred_row["item"])
            else:
                if len(results["worst"]) < n_examples and pred_row["item"] not in results["worst"]:
                    results["worst"].append(pred_row["item"])
        # Compute 'boundary' examples
        manager = Manager()
        smallest_diffs = manager.dict()
        df_pred = self.dataset_pred.as_dataframe()
        df_pred = df_pred.loc[df_pred["score"] > score_threshold]
        parallel_exec(
            func=get_min_diffs,
            elements=df_pred["item"].unique(),
            item=lambda item: item,
            df_pred=df_pred,
            smallest_diffs=smallest_diffs)
        xx = sorted(
            [{"item": k, "diff": v} for k, v in smallest_diffs.items()], key=lambda i: i['diff'])
        for i in range(n_examples):
            results["boundary"].append(xx[i]["item"])
        # Visualize detections on examples
        items = [x for yy in results.values() for x in yy]
        ds_true = ImageDataset(df_true.loc[df_true["item"].isin(items)], info={})
        ds_pred = ImagePredictionDataset(df_pred.loc[df_pred["item"].isin(items)], info={})
        # TODO: Implementar estar funcionalidad con las funciones que dibujan bboxes en las imagenes
        raise Exception("Implement functionality to show examples")
        res = defaultdict(list)
        for example_type, items in results.items():
            for item in items:
                fname = os.path.basename(item)
                new_item = os.path.join(dest_path, fname)
                res[example_type].append({"item": new_item})

        return dict(res)

    def result_plots(self,
                     dest_path: str = "",
                     truncate_categories: int = None,
                     lang=languages.EN,
                     report=False,
                     **kwargs):
        """Plot the results of the evaluation in different graphs depending on its type

        Parameters
        ----------
        dest_path : str, optional
            Base path where the plots will be saved, by default ""
        truncate_categories : int, optional
            Maximum number of records that will be shown in the per-class plots.
            The `truncate_categories` records with the highest value will be displayed.
            In case there are fewer records than `truncate_categories`, `None` will be returned.
            By default None
        lang : languages, optional
            Language of the report, by default languages.EN
        report : bool, optional
            Whether the plot will be saved as an image file or displayed in a window,
            by default False

        Returns
        -------
        dict
            Dictionary with the resulting assets in the form
            {`metric_set`: {'per_class': `metric_name`: `asset`,
                            'one_class': `metric_name`: `asset`}
            }
        """
        result_assets = {}
        # metric_set: 'PASCAL_VOC', 'CLASSIFICATION_LIKE'
        for metric_set, metrics in self.results.items():
            data_one_class = defaultdict(list)
            data_per_class = defaultdict(lambda: defaultdict(list))
            result_assets[metric_set] = {"per_class": {}, "one_class": {}}
            # metric_type: one_class, per_class
            for metric_type, metric_values in metrics.items():
                if metric_type == 'one_class':
                    metric_name = 'mAP'
                    data_one_class["metric"].append(metric_name)
                    data_one_class["value"].append(metric_values[metric_name])
                else:
                    for category, metrics_cat in metric_values.items():
                        metric_name = 'average_precision'
                        data_per_class[metric_name]["category"].append(category.capitalize())
                        data_per_class[metric_name]["value"].append(metrics_cat[metric_name])
            # Per-class results
            # Average precision
            df = pd.DataFrame(data=data_per_class["average_precision"],
                              columns=["category", "value"])
            df.sort_values(by=["value"], axis=0, ascending=False, inplace=True)
            if truncate_categories is not None:
                if len(df) <= truncate_categories:
                    return None
                df = df.head(truncate_categories)
            if report:
                metric_id = f"{metric_set}-per_class-average_precision"
                filename = os.path.join(dest_path, metric_id)
                filename = f"{filename}-trunc" if truncate_categories is not None else filename
            else:
                filename = None
            paths = self.__plot_bars(
                results_df=df,
                x_axis="category",
                y_axis="value",
                x_label=transl["metric_plt"]["category"][lang],
                y_label=transl["metric_plt"]["metric_name"]["average_precision"][lang],
                filename=filename)
            result_assets[metric_set]["per_class"]["average_precision"] = paths["plot"]
            # Precision recall curves
            if report:
                metric_id = f"{metric_set}-per_class-precision_recall_curves"
                filename = os.path.join(dest_path, metric_id)
                filename = f"{filename}-trunc" if truncate_categories is not None else filename
            else:
                filename = None
            paths = self.__plot_graphs(results_dict=self.results, filename=filename, **kwargs)
            result_assets[metric_set]["per_class"]["precision_recall_curves"] = paths["plot"]
            # One-class results (mAP)
            df = pd.DataFrame(data=data_one_class, columns=["metric", "value"])
            if report:
                metric_id = f"{metric_set}-one_class-mAP"
                filename = os.path.join(dest_path, metric_id)
                filename = f"{filename}-trunc" if truncate_categories is not None else filename
            else:
                filename = None
            paths = self.__plot_bars(
                results_df=df,
                x_axis="metric",
                y_axis="value",
                x_label=transl["metric_plt"]["metric"][lang],
                y_label=transl["metric_plt"]["metric_name"]["mAP"][lang],
                filename=filename,
                **kwargs)
            result_assets[metric_set]["one_class"]["general_metrics"] = paths["plot"]

        return result_assets

    def __plot_bars(self,
                    results_df,
                    x_axis, y_axis,
                    x_label, y_label,
                    filename=None,
                    **kwargs):
        """Plot the results of an evaluation in a bar plot, either saving it to an image file or
        displaying it in a window

        Parameters
        ----------
        results_df : pd.DataFrame
            Data containing the results of the metrics with columns `x_axis` and `y_axis`
        x_axis : str
            Column name for x-axis
        y_axis : str
            Column name for y-axis
        x_label : str
            Label for x-axis
        y_label : str
            Label for y-axis
        filename : str, optional
            Full path without file extension where the bar plot is saved as an image.
            If None, the bar plot will be displayed in a window instead. By default None

        Returns
        -------
        dict
            Dictionary of the form (plot, asset)
        """
        try:
            if filename is not None:
                asset_filename = f"{filename}.csv"
                plot_filename = f"{filename}.png"
                results_df.to_csv(asset_filename, index=False)

            if len(results_df) > report_params.MAX_REGISTERS_RESUME:
                x_axis, y_axis = y_axis, x_axis
                x_label, y_label = y_label, x_label
                figsize = (17, 30)
                rotation = 0
            else:
                figsize = (10, 10)
                rotation = 30

            sns.set(style="whitegrid")
            f, ax = plt.subplots(1, 1, figsize=figsize)
            g = sns.barplot(x=x_axis, y=y_axis, data=results_df, ax=ax)
            if rotation > 0:
                g.set_xticklabels(g.get_xticklabels(), rotation=rotation)
            plt.xlabel(x_label)
            plt.ylabel(y_label)
            if filename is not None:
                plt.savefig(plot_filename, dpi=100)

                metric_asset = {"plot": plot_filename,
                                "asset": asset_filename}
                return metric_asset
            else:
                plt.show()
        except Exception as ex:
            logger.exception(f"{ex}")
            raise(ex)

    def __plot_graphs(self,
                      results_dict,
                      filename=None,
                      **kwargs):
        """Plot precision-recall curves, either saving it to an image file or displaying it
        in a window

        Parameters
        ----------
        results_dict : dict
            Dictionary with the results of an evaluation
        filename : str, optional
            Full path without file extension where the bar plot is saved as an image.
            If None, the bar plot will be displayed in a window instead. By default None

        Returns
        -------
        dict
            Dictionary of the form (plot, asset)
        """
        try:
            if filename is not None:
                dest_path = os.path.dirname(filename)
                asset_filename = os.path.join(dest_path, f"results.json")
                plot_filename = f"{filename}.png"
            else:
                plot_filename = None
            self.plot_precision_recall_curves(results=results_dict,
                                              filename_to_save=plot_filename)
            if filename is not None:
                return {
                    "plot": plot_filename,
                    "asset": asset_filename
                }
        except Exception as ex:
            logger.exception(f"{ex}")
            raise(ex)

    def plot_precision_recall_curves(self,
                                     results: dict,
                                     title="Precision/Recall curves",
                                     figsize=(7, 8),
                                     filename_to_save=None) -> None:
        """Plot precision-recall curves, either saving it to an image file or displaying it
        in a window

        Parameters
        ----------
        results : dict
            Dictionary with the results of an evaluation
        title : str, optional
            Title of the plot, by default "Precision/Recall curves"
        figsize : tuple, optional
            Tuple of (width, height) in inches, by default (7, 8)
        filename_to_save : str, optional
            Full path of the resulting image file, by default None
        """
        colors = cycle(['darkorange', 'turquoise', 'navy', 'cornflowerblue', 'teal'])
        fig = plt.figure()
        plt.figure(figsize=figsize)
        plt.title(title)
        lines = []
        labels = []
        if 'per_class' not in results:
            results_name = [name for name, results in results.items()][0]
            results = results[results_name]
        for cat, color in zip(results["per_class"].keys(), colors):
            l, = plt.plot(np.array(results["per_class"][cat]["recalls"]),
                          np.array(results["per_class"][cat]["precisions"]),
                          color=color, lw=2)
            lines.append(l)
            area_str = '(area = {:0.2f})'.format(results["per_class"][cat]['average_precision'])
            labels.append(f'Precision-recall for class {cat} {area_str}')
        l, = plt.plot(np.zeros(1), np.zeros(1), color="white", lw=2)
        lines.append(l)
        mAP_str = '{:0.2f}'.format(results["one_class"]["mAP"])
        labels.append(f'mAP for the model: {mAP_str}')

        fig = plt.gcf()
        fig.subplots_adjust(bottom=0.25)
        plt.xlim([0.0, 1.05])
        plt.ylim([0.0, 1.05])
        plt.xlabel('Recall')
        plt.ylabel('Precision')
        plt.legend(lines, labels, loc=(0, -.38), prop=dict(size=14))
        if filename_to_save is None:
            plt.show()
        else:
            plt.savefig(filename_to_save, dpi=200)

    class Sets():
        """
        Allowed types of metrics sets
        """
        PASCAL_VOC = "PASCAL_VOC"
        CLASSIFICATION_LIKE = "CLASSIFICATION_LIKE"
        NAMES = [PASCAL_VOC, CLASSIFICATION_LIKE]


class ImageObjectDetectionEvaluator(Evaluator):
    """
    Class to calculate the classification metrics of a model on a set of images

    Attributes
    ----------
    dataset_pred : ImagePredictionDataset
        Dataset of predictions that coming from Model.predict method
    dataset_true : ImageDataset
        Dataset of true labels
    eval_config : dict
        Options to define the evaluation params of the model
    """
    ImageClassificationEvaluatorType = TypeVar('ImageClassificationEvaluatorType',
                                               bound='ImageClassificationEvaluator')

    args_eval_def = {
        'metrics_set': {
            'type': dict,
            'default': {
                ImageObjectDetectionMetrics.Sets.PASCAL_VOC: {}
            }
        },
        'partition': {
            'type': [None, str],
            'default': Partitions.TEST
        },
        'eval_dir': {
            'type': str
        }
    }

    args_metrics_def = {
        ImageObjectDetectionMetrics.Sets.PASCAL_VOC: {
            'iou_threshold': {
                'type': float,
                'default': 0.5
            },
            'nms_iou_threshold': {
                'type': float,
                'default': 0.3
            },
            'nms_max_output_boxes': {
                'type': int,
                'default': 50
            },
            'use_weighted_mean_ap': {
                'type': bool,
                'default': False
            }
        },
        ImageObjectDetectionMetrics.Sets.CLASSIFICATION_LIKE: {
            'iou_threshold': {
                'type': float,
                'default': 0.5
            },
            'nms_iou_threshold': {
                'type': float,
                'default': 0.3
            },
            'nms_max_output_boxes': {
                'type': int,
                'default': 50
            },
            'use_weighted_mean_ap': {
                'type': bool,
                'default': False
            }
        }
    }

    def __init__(self,
                 dataset_true,
                 dataset_pred,
                 eval_config):
        self.dataset_pred = dataset_pred
        self.dataset_true = dataset_true
        self.eval_config = get_and_validate_args(eval_config, self.args_eval_def)

    def pascal_voc_detection(self,
                             dataset_true,
                             dataset_pred,
                             partition,
                             categories,
                             evaluation_type=ImageObjectDetectionMetrics.Sets.PASCAL_VOC,
                             iou_threshold=0.5,
                             nms_iou_threshold=0.3,
                             nms_max_output_boxes=50,
                             use_weighted_mean_ap=False,
                             per_image_gts_and_detections_file=None):
        raise Exception(
            f"This method has been removed due to deprecation. "
            f"If you need to use this type of evaluation, browse "
            f"for the implementation in the repository history, and include again the "
            f"conabio_ml_vision/trainer/models module that was removed.")

    @classmethod
    def eval(cls,
             dataset_true: ImageDataset.DatasetType,
             dataset_pred: ImagePredictionDataset.DatasetType,
             eval_config: dict = None,
             per_image_gts_and_detections_file=None) -> Metrics.MetricsType:
        """Method that performs the evaluation of a model from the prediction dataset resulting
        from the inference on a set of images, the original dataset, and an evaluation
        configuration

        Parameters
        ----------
        dataset_true : ImageDataset.DatasetType
            Dataset of true labels
        dataset_pred : ImagePredictionDataset.DatasetType
            Dataset of predictions that coming from Model.predict method
        eval_config : dict, optional
            Options to define the evaluation params of the model, by default {}
        per_image_gts_and_detections_file : str, optional
            Path of a JSON file where the per_image_gts_and_detections were previously stored and
            which will be loaded in order to reduce processing time, by default None

        Returns
        -------
        ImageClassificationMetrics
            Result with evaluation metrics

        Raises
        ------
        Exception
            In case prediction dataset does not contain categories
        """
        eval_config = {} if eval_config is None else eval_config
        instance = cls(dataset_true, dataset_pred, eval_config)
        # Evaluate only categories in dataset_pred
        categories = instance.dataset_pred.get_classes()
        if not categories:
            raise Exception("You cannot evaluate datasets that does not contain labels.")

        results = {}
        for metric_set_name, config in instance.eval_config['metrics_set'].items():
            config = get_and_validate_args(config, cls.args_metrics_def[metric_set_name])
            if (not dataset_true.is_detection_dataset()
                    and metric_set_name == ImageObjectDetectionMetrics.Sets.PASCAL_VOC):
                metric_set_name = ImageObjectDetectionMetrics.Sets.CLASSIFICATION_LIKE
                logger.warning("Using classification like evaluation instead")
            results[metric_set_name] = instance.pascal_voc_detection(
                dataset_true,
                dataset_pred,
                instance.eval_config['partition'],
                categories,
                evaluation_type=metric_set_name,
                iou_threshold=config['iou_threshold'],
                nms_iou_threshold=config['nms_iou_threshold'],
                nms_max_output_boxes=config['nms_max_output_boxes'],
                use_weighted_mean_ap=config['use_weighted_mean_ap'],
                per_image_gts_and_detections_file=per_image_gts_and_detections_file)

        return ImageObjectDetectionMetrics(results, dataset_true, dataset_pred, eval_config)
