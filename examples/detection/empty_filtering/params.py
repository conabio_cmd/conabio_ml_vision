import os

RAND_STATE = 76273
ANIMAL_BIN_MAP = {'empty': 'empty', 'homo sapiens': 'empty', '*': 'animal'}
PERSON_BIN_MAP = {'homo sapiens': 'person', '*': 'empty'}
DETECTIONS_MAP = {'animal': 'animal', '*': 'person'}
INIT_THRES = 0.
END_THRES = 1.
STEP_THRES = 0.05
NUM_STEPS = int((END_THRES-INIT_THRES)*(.1/(STEP_THRES))*10)+1

conabio_ml_path = '/LUSTRE/users/ecoinf_admin/conabio_ml_vision'
files_base_path = os.path.join(conabio_ml_path, 'examples', 'files')
dets_base_path = os.path.join(files_base_path, 'detections')

md_dets_csvs = {
    'v4': {
        'animal': os.path.join(dets_base_path, 'snmb.csv'),
        'empty': os.path.join(dets_base_path, 'snmb_empty.csv')
    },
    'v5': {
        'animal': os.path.join(dets_base_path, 'snmb_mdv5.csv'),
        'empty': os.path.join(dets_base_path, 'snmb_empty_mdv5.csv')
    },
}

snmb_json = os.path.join(files_base_path, 'datasets_JSON', 'snmb', 'snmb_2022_detection.json')
snmb_images_dir = os.path.join(conabio_ml_path, 'examples', 'data', 'snmb')

aves_ft_csv = os.path.join(files_base_path, "AvesFT.csv")
mammals_ft_csv = os.path.join(files_base_path, "MamiferosFT.csv")
