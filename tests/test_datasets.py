
import pytest

from conabio_ml_vision.utils.aux_utils import get_species_of_interest
from conabio_ml_vision.datasets import ImageDataset
from conabio_ml_vision.utils.scripts import get_samples_counts_by_partitions

from params_test_dataset import *


@pytest.fixture
def snmb_dataset():
    species_of_int = get_species_of_interest()

    snmb_animal_ds = ImageDataset.from_json(
        collection='snmb',
        collection_version='detection-bboxes-curated',
        categories=species_of_int,
        extra_fields=[STATE_COL, MUNICIPALITY_COL],
        max_elements_per_category=5,
        random_state=RANDOM_STATE,
        download_media_to=images_path,
        media_base_url='https://monitoreo.conabio.gob.mx/snmb_pics',
    )
    snmb_empty_ds = ImageDataset.from_json(
        collection='snmb',
        collection_version='empty',
        extra_fields=[STATE_COL, MUNICIPALITY_COL],
        max_elements_per_category=100,
        random_state=RANDOM_STATE,
        download_media_to=images_path,
        media_base_url='https://monitoreo.conabio.gob.mx/snmb_pics',
    )

    snmb_ds = (
        ImageDataset.from_datasets(
            snmb_animal_ds, snmb_empty_ds, extra_fields=[STATE_COL, MUNICIPALITY_COL])
    )
    
    snmb_ds.split(
        train_perc=0.7, test_perc=0.2, val_perc=0.1, group_by_field='location', stratify=True)

    assert snmb_ds.get_num_rows() > 0, "No elements found in dataset"
    assert snmb_ds.get_num_partitions() == 3, "Not partitioned"
    assert snmb_ds.get_num_categories() > 0, "Not categories"

    return snmb_ds


def test_to_csv(snmb_dataset):
    snmb_dataset.to_csv(csv_path)
    
    snmb_df = snmb_dataset.as_dataframe()

    snmb_ds_2 = ImageDataset.from_csv(csv_path, images_dir=images_path,
                                      extra_fields=[STATE_COL, MUNICIPALITY_COL])
    snmb_df_2 = snmb_ds_2.as_dataframe()
    
    get_samples_counts_by_partitions(csv_path, csv_partitions_path)
    
    assert len(snmb_df) == len(snmb_df_2), "len(snmb_df) != len(snmb_df_2)"
    assert len(snmb_df['item'].values) == len(snmb_df_2['item'].values), "Not the same items"
    assert len(snmb_df['label'].values) == len(snmb_df_2['label'].values), "Not the same labels"
    assert set(snmb_df.columns.values) == set(snmb_df_2.columns.values), "Not the same columns"
    assert snmb_dataset.is_partitioned() and snmb_ds_2.is_partitioned(), "Not partitioned"


def test_as_dataframe(snmb_dataset):
    snmb_df = snmb_dataset.as_dataframe()

    # Check for columns
    ds_cols = snmb_df.columns.values
    check_cols = ['item', 'label', 'bbox', 'image_id', 'id', 'location', STATE_COL, MUNICIPALITY_COL]
    assert all([col in ds_cols for col in check_cols]), "Some columns are missing"

    # Check for
    assert snmb_dataset.label_counts().sum() > 0, "There is no labels in dataset"

def test_to_folder(snmb_dataset):
    snmb_dataset.to_folder(partitioned_images_path,
                           split_in_partitions=True, split_in_labels=True,
                           set_images_dir=True)
    snmb_ds_2 = ImageDataset.from_folder(partitioned_images_path, 
                                         label_by_folder_name=True, split_by_folder=True)
    
    assert snmb_dataset.is_partitioned() and snmb_ds_2.is_partitioned(), "Not partitioned"
    assert len(snmb_dataset.label_counts().index.values) == len(snmb_ds_2.label_counts().index.values), "Not the same labels"
    

# if __name__ == '__main__':
#     __spec__ = None

#     snmb_ds = snmb_dataset()
#     test_to_folder(snmb_ds)