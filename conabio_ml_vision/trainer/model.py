import os
import sys
import numpy as np
import pandas
import json
import uuid
import multiprocessing
from collections import defaultdict
from typing import TypeVar
from shutil import rmtree, move
import time
from tqdm.auto import tqdm

from conabio_ml.trainer.model import Model
from conabio_ml.datasets.dataset import Dataset, Partitions
from conabio_ml.utils.utils import get_and_validate_args, get_chunk, get_temp_folder, download_file
from conabio_ml.utils.report_params import languages
from conabio_ml.utils.logger import get_logger

from conabio_ml_vision.utils import coords_utils
from conabio_ml_vision.datasets import ImageDataset, ImagePredictionDataset
from conabio_ml_vision.models.models import MegadetectorModel as Megadetector
from conabio_ml_vision.datasets import VideoDataset, VideoPredictionDataset
from conabio_ml_vision.trainer.predictor_config import TFPredictorConfig, BasicPredictorConfig
import conabio_ml_vision.utils.images_utils as images_utils
from conabio_ml_vision.preprocessing.preprocess import ImageProcessing
from conabio_ml_vision.utils.evaluator_utils import get_video_level_pred_dataset
from conabio_ml_vision.utils.evaluator_utils import get_image_level_pred_dataset

logger = get_logger(__name__)


class ClassificationModel(Model):
    """Class that allows to create a classification model that can be fitted
    by a trainer with the information of a dataset.
    """
    ClassificationModelType = TypeVar('ClassificationModelType', bound='ClassificationModel')
    LABELS_FILENAME = 'labels.txt'
    INCEPTION_V3 = 'inception_v3'
    INCEPTION_V4 = 'inception_v4'
    MODELS = [INCEPTION_V3, INCEPTION_V4]

    args_pred_def = {
        'partition': {
            'type': [None, str],
            'default': Partitions.TEST
        },
        'labelmap': {
            'type': [dict, str],
            'default': None
        },
        'use_partitions': {
            'type': bool,
            'default': False
        },
        'include_id': {
            'type': bool,
            'default': False
        },
        'keep_detection_id': {
            'type': bool,
            'default': False
        },
        'max_classifs': {
            'type': int,
            'default': 5
        }
    }

    report_data = {
        INCEPTION_V3: {
            "model_name": {
                languages.EN: "Inception v3",
                languages.ES: "Inception v3"
            },
            "model_type": {
                languages.EN: f"Convolutional neural network that is 48 layers deep. "
                f"The pretrained network can classify images into 1000 object categories.",
                languages.ES: f"Red neuronal convolucional que tiene 48 capas de profundidad. "
                f"La red preentrenada puede clasificar imágenes en 1000 categorías de objetos."
            },
            "input_data": {
                languages.EN: "The network has an image input size of 299-by-299.",
                languages.ES: "La red tiene un tamaño de entrada de imagen de 299 por 299."
            },
            "output_data": {
                languages.EN: f"Output layer is softmax, which means it has "
                f"predefined number of neurons, each one is defined for one specific class.",
                languages.ES: f"La capa de salida es softmax, lo que significa que tiene un número"
                f" predefinido de neuronas, cada una está definida para una clase específica.",
            }
        },
        INCEPTION_V4: {
            "model_name": {
                languages.EN: "Inception v4",
                languages.ES: "Inception v4"
            },
            "model_type": {
                languages.EN: f"Convolutional neural network and a pure Inception variant without "
                f"residual connections. "
                f"The pretrained network can classify images into 1000 object categories.",
                languages.ES: f"Red neuronal convolucional y una variante de Inception sin "
                f"conexiones residuales. "
                f"La red preentrenada puede clasificar imágenes en 1000 categorías de objetos."
            },
            "input_data": {
                languages.EN: "The network has an image input size of 299-by-299.",
                languages.ES: "La red tiene un tamaño de entrada de imagen de 299 por 299."
            },
            "output_data": {
                languages.EN: f"Output layer is softmax, which means it has "
                f"predefined number of neurons, each one is defined for one specific class.",
                languages.ES: f"La capa de salida es softmax, lo que significa que tiene un número"
                f" predefinido de neuronas, cada una está definida para una clase específica.",
            }
        }
    }

    def __init__(self, name):
        super().__init__(name)
        self.model_config = None
        self.saved_model = None

    @classmethod
    def load_saved_model(cls,
                         source_path: str,
                         model_config: dict = None,
                         pipeline_filepath: str = None,
                         model_name: str = None) -> ClassificationModelType:
        """Create an instance of a ClassificationModel from a previously saved
        model and either a `model_config` dictionary or a `pipeline_filepath`
        route.

        Parameters
        ----------
        source_path : str
            The path to a saved model. E.g. a checkpoint path.
        model_config : dict
            A dictionary that contains a model configuration.
        pipeline_filepath : str
            File path of the JSON that defines a filepath
        model_name : str
            Name of the model, it has to be defined inside the
            pipeline filepath

        Returns
        -------
        Model
            Instance of the created model.
        """
        assert model_config is None or (type(model_config) == dict and model_config), \
            "Invalid model_config parameter."
        model_name = cls.get_model_name(model_config)
        instance = cls(model_name)
        if model_config is not None:
            config = model_config[model_name]
            instance.model_config = {
                "train_image_size": config.get("train_image_size", None),
                "weight_decay": config.get("weight_decay", None),
                "label_smoothing": config.get("label_smoothing", None)
            }
        instance.saved_model = {
            "checkpoint_path": source_path
        }
        return instance

    def predict(self: ClassificationModelType,
                dataset: Dataset.DatasetType,
                execution_config: TFPredictorConfig.TFPredictorConfigType,
                prediction_config: dict = {}) -> Dataset.DatasetType:
        raise Exception(
            f"This method has been removed due to deprecation. "
            f"If you need to use this type of prediction, browse "
            f"for the implementation in the repository history, and include again the "
            f"conabio_ml_vision/trainer/models module that was removed.")

    """
    GET METHODS
    """
    @classmethod
    def get_model_name(cls, model_config):
        """Gets the model name

        Parameters
        ----------
        model_config : dict
            Dictionary containing the configuration of the model

        Returns
        -------
        str
            The name of the model

        Raises
        ------
        ValueError
            In case the dict does not contains a valid structure
        """
        if model_config is None:
            return None
        model_names = [x for x in model_config.keys() if x in cls.MODELS]
        if len(model_names) == 0:
            raise ValueError(f"Model name must be one of: {cls.MODELS}")
        return model_names[0]


class ObjectDetectionModel(Model):
    """Class that allows to create a object detection model that can be fitted
    by a trainer with the information of a dataset.
    """
    ObjectDetectionModelType = TypeVar('ObjectDetectionModelType', bound='ObjectDetectionModel')
    LABELS_FILENAME = 'labels.txt'
    FASTER_RCNN = 'faster_rcnn'
    SSD = 'ssd'
    MODELS = [FASTER_RCNN, SSD]
    BOX_PREDICTORS = ['convolutional_box_predictor', 'mask_rcnn_box_predictor',
                      'rfcn_box_predictor', 'weight_shared_convolutional_box_predictor']
    IOU_THRESHOLD = 0.5

    args_pred_def = {
        'partition': {
            'type': [None, str],
            'default': Partitions.TEST
        },
        'labelmap': {
            'type': [dict, None],
            'default': None
        },
        'use_partitions': {
            'type': bool,
            'default': False
        },
        'min_score_threshold': {
            'type': float,
            'default': 0.
        },
        'max_number_of_boxes': {
            'type': int,
            'default': 100
        }
    }

    report_data = {
        FASTER_RCNN: {
            "model_name": {
                languages.EN: "Faster-RCNN",
                languages.ES: "Faster-RCNN"
            },
            "model_type": {
                languages.EN: f"Region-based convolutional neural networks or regions"
                f" with CNN features (R-CNNs) are a pioneering approach"
                f" that applies deep models to object detection. "
                f"Faster R-CNN replaces selective search with a region proposal network (RPN).",
                languages.ES: f"Las redes neuronales convolucionales basadas en regiones o "
                f"regiones con características CNN (R-CNN) son un enfoque pionero que aplica "
                f"modelos profundos a la detección de objetos. Faster R-CNN reemplaza la búsqueda "
                f"selectiva con una red de propuesta de región (RPN).",
            },
            "input_data": {
                languages.EN: "The network has an image input size of 800-by-600.",
                languages.ES: "La red tiene un tamaño de entrada de imagen de 800 por 600."
            },
            "output_data": {
                languages.EN: f"For each image the model returns a set of up to 100 detections. "
                f"Each detection has a bounding box, a score and a label of one category.",
                languages.ES: f"Para cada imagen, el modelo devuelve un conjunto de hasta 100 "
                f"detecciones. Cada detección tiene un recuadro, un score y una etiqueta.",
            }
        },
        SSD: {
            "model_name": {
                languages.EN: "SSD",
                languages.ES: "SSD"
            },
            "model_type": {
                languages.EN: f"Single Shot Detector is a single-shot detector. "
                f"It has no delegated region proposal network and "
                f"predicts the boundary boxes and the classes directly "
                f"from feature maps in one single pass.",
                languages.ES: f"Single Shot Detector es un detector de disparo único. No tiene una"
                f" red de propuesta de región delegada y predice los cuadros de límite y las "
                f"clases directamente de los mapas de características en una sola pasada."
            },
            "input_data": {
                languages.EN: "The network has an image input size of 300-by-300.",
                languages.ES: "La red tiene un tamaño de entrada de imagen de 300 por 300."
            },
            "output_data": {
                languages.EN: f"For each image the model returns a set of up to 100 "
                f"detections. Each detection has a bounding box, a "
                f"score and a label for one of the categories.",
                languages.ES: f"Para cada imagen, el modelo devuelve un conjunto de hasta 100 "
                f"detecciones. Cada detección tiene un recuadro, un score y una etiqueta.",
            }
        }
    }

    def __init__(self, name):
        super().__init__(name)
        self.model_config = None
        self.saved_model = None

    @classmethod
    def load_saved_model(cls,
                         source_path: str,
                         model_config: dict = None,
                         pipeline_filepath: str = None,
                         model_name: str = None) -> ObjectDetectionModelType:
        """Create an instance of a ObjectDetectionModel from a previously
        saved model and either a `model_config` dictionary or a
        `pipeline_filepath` route.

        Parameters
        ----------
        source_path : str
            The path to a saved model. E.g. a checkpoint path or a frozen
            inference graph.
        model_config : dict
            A dictionary that contains a model configuration.
        pipeline_filepath : str
            File path of the JSON that defines a filepath
        model_name : str
            Name of the model, it has to be defined inside the
            pipeline filepath

        Returns
        -------
        Model
            Instance of the created model.
        """
        assert model_config is None or (type(model_config) == dict and model_config), \
            "Invalid model_config parameter."
        model_name = cls.get_model_name(model_config)
        instance = cls(model_name)
        if not model_config is None:
            instance.model_config = model_config
        instance.saved_model = {}
        if source_path.endswith('.pb'):
            instance.saved_model["checkpoint_path"] = None
            instance.saved_model["frozen_graph_path"] = source_path
        else:
            instance.saved_model["checkpoint_path"] = source_path
            instance.saved_model["frozen_graph_path"] = None
        return instance

    def predict(self: ObjectDetectionModelType,
                dataset: Dataset.DatasetType,
                execution_config: TFPredictorConfig.TFPredictorConfigType,
                prediction_config: dict = {}) -> Dataset.DatasetType:
        """Performs the prediction of the model on a set of images

        Parameters
        ----------
        dataset : Dataset.DatasetType
            Dataset on which the model prediction will be made
        execution_config : TFPredictorConfig.TFPredictorConfigType
            Contains the prediction execution configuration parameters
        prediction_config : dict, optional
            Dictionary containing the prediction configuration parameters.
            It will be validated against `args_pred_def`. By default {}

        Returns
        -------
        ImagePredictionDataset
            Prediction dataset containing the predictions made by the model on the images in the
            dataset partition `prediction_config['partition']`
        """
        import tensorflow as tf

        if execution_config is None:
            execution_config = TFPredictorConfig.create()
        batch_size = execution_config.batch_size
        prediction_config = get_and_validate_args(prediction_config, self.args_pred_def)

        partition = prediction_config['partition']
        labelmap = prediction_config['labelmap']
        use_partitions = prediction_config['use_partitions']
        min_thres = prediction_config['min_score_threshold']
        max_boxes = prediction_config['max_number_of_boxes']

        split_by_column = "partition" if use_partitions == True else None
        image_sizes = dataset.get_images_sizes()
        results = defaultdict(list)

        item_partition = {}
        fields_inherited = defaultdict(dict)

        ds_cols = dataset.as_dataframe().columns

        for row in dataset.as_dataframe().to_dict('records'):
            if use_partitions == True:
                item_partition[row["item"]] = row["partition"]

        if type(labelmap) is dict:
            pass
        else:
            labelmap = dataset.get_labelmap()
        labelmap_str = [{k: v} for k, v in labelmap.items()][:10]
        logger.info(f"Using labelmap for prediction: {labelmap_str}...")

        images = dataset.get_unique_items(partition=partition)
        if execution_config.num_tasks and execution_config.task_num:
            images = get_chunk(images, execution_config.num_tasks, execution_config.task_num)
        detection_graph = tf.Graph()
        with detection_graph.as_default():
            od_graph_def = tf.compat.v1.GraphDef()
            with tf.io.gfile.GFile(self.saved_model['frozen_graph_path'], 'rb') as fid:
                serialized_graph = fid.read()
                od_graph_def.ParseFromString(serialized_graph)
                tf.import_graph_def(od_graph_def, name='')
            with tf.Session() as sess:
                # Get handles to input and output tensors
                ops = tf.get_default_graph().get_operations()
                all_tensor_names = {output.name for op in ops for output in op.outputs}
                tensor_dict = {}
                for key in ['num_detections', 'detection_boxes',
                            'detection_scores', 'detection_classes', 'detection_masks']:
                    tensor_name = key + ':0'
                    if tensor_name in all_tensor_names:
                        tensor_dict[key] = tf.get_default_graph().get_tensor_by_name(tensor_name)
                image_tensor = tf.get_default_graph().get_tensor_by_name('image_tensor:0')
                image_batches = []
                batch_size = multiprocessing.cpu_count()
                for i in range(0, len(images), batch_size):
                    image_batches.append(images[i:i + batch_size])
                with tqdm(total=len(images), desc="Applying model inference",
                          bar_format="{l_bar}{bar} [ time left: {remaining} ]") as pbar:
                    for image_batch in image_batches:
                        images_dict = images_utils.load_image_into_numpy_array_batch(image_batch)
                        for item, image_np in images_dict.items():
                            if image_np is None:
                                continue
                            # Run inference
                            output_dict = sess.run(
                                tensor_dict, feed_dict={image_tensor: np.expand_dims(image_np, 0)})

                            output_dict['num_detections'] = int(output_dict['num_detections'][0])
                            output_dict['detection_classes'] = \
                                output_dict['detection_classes'][0].astype(np.uint8)
                            output_dict['detection_boxes'] = output_dict['detection_boxes'][0]
                            output_dict['detection_scores'] = output_dict['detection_scores'][0]
                            valid_bboxes = defaultdict(list)
                            for i in range(output_dict['num_detections']):
                                score = output_dict['detection_scores'][i]
                                if score < min_thres or i+1 > max_boxes:
                                    break
                                ind = output_dict['detection_classes'][i].astype(np.uint8)
                                if ind not in labelmap or labelmap[ind] == 'empty':
                                    # Skip labels out of the categories of the labelmap or `empty` dets
                                    continue
                                bbox_str = coords_utils.transform_coordinates(
                                    bbox=output_dict['detection_boxes'][i],
                                    input_format=coords_utils.COORDINATES_FORMATS.Y1_X1_Y2_X2,
                                    output_format=coords_utils.COORDINATES_FORMATS.X_Y_WIDTH_HEIGHT,
                                    output_coords_type=coords_utils.COORDINATES_TYPES.ABSOLUTE,
                                    output_data_type=coords_utils.COORDINATES_DATA_TYPES.STRING,
                                    image_width=image_sizes.loc[item]['width'],
                                    image_height=image_sizes.loc[item]['height'])
                                f_box = coords_utils.transform_coordinates(
                                    bbox=bbox_str,
                                    input_format=coords_utils.COORDINATES_FORMATS.X_Y_WIDTH_HEIGHT,
                                    output_format=coords_utils.COORDINATES_FORMATS.X1_Y1_X2_Y2,
                                    output_coords_type=coords_utils.COORDINATES_TYPES.RELATIVE,
                                    output_data_type=coords_utils.COORDINATES_DATA_TYPES.LIST,
                                    image_width=image_sizes.loc[item]['width'],
                                    image_height=image_sizes.loc[item]['height'])
                                cont = False
                                for valbox in valid_bboxes[item]:
                                    if not coords_utils.non_max_supression(f_box, valbox,
                                                                           self.IOU_THRESHOLD):
                                        break
                                else:
                                    valid_bboxes[item].append(f_box)
                                    cont = True
                                if not cont:
                                    continue
                                results["item"].append(item)
                                results["label"].append(labelmap[ind])
                                results["bbox"].append(bbox_str)
                                results["score"].append(score)
                                results["id"].append(str(uuid.uuid4()))
                                if use_partitions:
                                    results["partition"].append(item_partition[item])
                            pbar.update(1)
        anns_info = pandas.DataFrame(results)
        prediction_dataset = ImagePredictionDataset(anns_info,
                                                    images_dir=dataset.get_images_dir(),
                                                    images_info=dataset.get_images_info(),
                                                    split_by_column=split_by_column)
        return prediction_dataset

    """
    GET METHODS
    """

    def get_config(self):
        """Gets the configuration of the model

        Returns
        -------
        dict
            Dictionary containing the configuration of the model

        Raises
        ------
        ValueError
            In case no configuration associated to the model instance
        """
        if self.model_config is None:
            raise ValueError("No configuration associated to Model instance.")
        return self.model_config[self.name]

    def get_box_predictor_config(self):
        """Gets the configuration of the box predictor according to the model name and the
        configuration of the model

        Returns
        -------
        dict
            Dictionary containing the configuration of the box predictor in the form
            {'name': `name_of_the_box_predictor`, 'config': `config_of_the_box_predictor`}

        Raises
        ------
        ValueError
            In case the name of the model is not supported
        ValueError
            In case invalid box predictor configuration
        """
        config = self.get_config()
        if self.name == self.FASTER_RCNN:
            box_predictor_stage = 'second_stage_box_predictor'
        elif self.name == self.SSD:
            box_predictor_stage = 'box_predictor'
        else:
            raise ValueError(f"Detection model {self.name} not supported.")
        if box_predictor_stage in config:
            box_predictor_config = config[box_predictor_stage]
            box_pred_types = [k for k in box_predictor_config.keys()
                              if k in self.BOX_PREDICTORS]
            if len(box_pred_types) == 0:
                raise ValueError("Invalid box predictor configuration.")
            return {
                'name': box_pred_types[0],
                'config': box_predictor_config[box_pred_types[0]]
            }
        return None

    @classmethod
    def get_model_name(cls, model_config):
        """Gets the model name

        Parameters
        ----------
        model_config : dict
            Dictionary containing the configuration of the model

        Returns
        -------
        str
            The name of the model
        """
        if model_config is None:
            return None
        model_names = [x for x in model_config.keys() if x in cls.MODELS]
        if len(model_names) == 0:
            raise ValueError(f"Model name must be one of: {cls.MODELS}")
        return model_names[0]

def run_megadetector_inference(dataset,
                               out_predictions_csv,
                               images_dir=None,
                               model_path=None,
                               min_score_threshold=0.01,
                               partition=None,
                               labelmap=Megadetector.Labels.labelmap,
                               num_tasks=None,
                               task_num=None,
                               num_gpus_per_node=2,
                               exit_if_not_last=True,
                               **kwargs):
    """Function to run the inference of an object detection model (Megadetector) on the images of
    the `dataset`.
    The detections are stored in the prediction CSV file `out_predictions_csv`.
    The processing can be performed in parallel by setting the parameters `num_tasks` and
    `task_num`.

    Parameters
    ----------
    dataset : Dataset.DatasetType
        Dataset on which the model prediction will be made
    out_predictions_csv : str or Path
        Path where the csv file(s) will be stored
    images_dir : str
        Folder where images of `dataset` are stored
    model_path : str
        Path to the file (E.g., *.pb) containing the weights of the object detection model to be
        used for inference
    min_score_threshold : float, optional
        Minimum score that the resulting detections must have. The rest will be discarded,
        by default 0.01
    partition : str, optional
        Partition name of `dataset` on which the inference of the detector is to be applied,
        by default None
    labelmap : dict, optional
        Dictionary with ids to class names, by default Megadetector.Labels.labelmap
    num_tasks : int, optional
        In case of parallel processing (e.g., on several nodes), this is the total number of tasks
        into which the processing is divided. E.g., if you have 4 nodes with 2 GPUs on each node,
        the work can be divided into `num_tasks = 8` parallel tasks, in order to maximize the use
        of resources and perform the processing in the shortest possible time.
        By default None
    task_num : int, optional
        In case of parallel processing, this is the number of the current task.
        It must be an integer in the range [1, `num_tasks`]. By default None
    num_gpus_per_node : int, optional
        Number of GPUs per node in case of parallel processing, by default 2
    exit_if_not_last : bool, optional
        Whether to finish if it is not the last task, by default True

    Returns
    -------
    ImagePredictionDataset
        Predictions resulting from the inference of the Megadetector
    """
    from conabio_ml_vision.models import run_megadetector_inference
    return run_megadetector_inference(dataset=dataset,
                                      out_predictions_csv=out_predictions_csv,
                                      images_dir=images_dir,
                                      model_path=model_path,
                                      min_score_threshold=min_score_threshold,
                                      partition=partition,
                                      labelmap=labelmap,
                                      num_tasks=num_tasks,
                                      task_num=task_num,
                                      num_gpus_per_node=num_gpus_per_node,
                                      exit_if_not_last=exit_if_not_last,
                                      **kwargs)
