#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import annotations
import os
import json
import uuid
from datetime import datetime
from collections import defaultdict
from abc import abstractmethod
from typing import List, Tuple
from shutil import move

import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split, GroupShuffleSplit

from conabio_ml.datasets.collection import Collection
from conabio_ml.assets import AssetTypes
from conabio_ml.datasets.dataset import PredictionDataset, Partitions
from conabio_ml.utils import report_params
from conabio_ml.utils.report_params import languages
from conabio_ml.utils.utils import is_array_like
from conabio_ml.utils.logger import get_logger
from conabio_ml.utils.dataset_utils import get_cleaned_label
from conabio_ml.utils.utils import parallel_exec

import conabio_ml_vision.utils.dataset_utils as utils
from conabio_ml_vision.datasets.media import MediaDataset
from conabio_ml_vision.preprocessing.process_images import parallel_process_images
from conabio_ml_vision.utils.dataset_utils import get_filename
from conabio_ml_vision.utils.dataset_utils import get_bbox_from_json_row, get_bbox_from_dataset_row
from conabio_ml_vision.utils.dataset_utils import STD_DATEFORMAT
from conabio_ml_vision.utils import coords_utils
from conabio_ml_vision.utils import lila_utils
from conabio_ml_vision.utils.stratified_group_split import gradient_group_stratify
from conabio_ml_vision.utils.dataset_utils import download_with_azcopy
from conabio_ml.utils.utils import get_temp_folder, download_file

logger = get_logger(__name__)


class ImageDataset(MediaDataset):
    """Represent a ImageDataset specification."""

    class MEDIA_FIELDS():
        """Field names allowed in the creation of image datasets."""
        ID = "id"
        FILENAME = "file_name"
        WIDTH = "width"
        HEIGHT = "height"
        DATE_CAPTURED = "date_captured"
        LOCATION = "location"

        SEQ_ID = "seq_id"
        SEQ_NUM_FRAMES = "seq_num_frames"
        FRAME_NUM = "frame_num"
        SEQ_FRAME_NUM = "seq_frame_num"
        # Fields used in the creation of image datasets from video frames
        VID_FRAME_NUM = "video_frame_num"
        VID_NUM_FRAMES = "video_num_frames"
        VID_ID = "video_id"

        NAMES = [ID, FILENAME, WIDTH, HEIGHT, DATE_CAPTURED, LOCATION, SEQ_ID, SEQ_NUM_FRAMES,
                 FRAME_NUM, SEQ_FRAME_NUM, VID_FRAME_NUM, VID_NUM_FRAMES, VID_ID]

        TYPES = {
            ID: str,
            LOCATION: str,
            # TODO: Think about what to do when these are incorrect
            WIDTH: float,
            HEIGHT: float,
            SEQ_ID: str,
            VID_ID: str
        }

    class ANNOTATIONS_FIELDS():
        ITEM = "item"
        LABEL = "label"
        BBOX = "bbox"
        ID = "id"
        MEDIA_ID = "image_id"
        PARTITION = "partition"
        SCORE_DET = 'score_det'

        NAMES = [ITEM, LABEL, BBOX, ID,
                 MEDIA_ID, PARTITION, SCORE_DET
                 ]

        TYPES = {
            ID: str,
            MEDIA_ID: str
        }

    """
    MEDIA FIELD NAMES FOR IMAGES
    """
    # TODO: agregar a MEDIA_FIELDS
    MEDIA_FIELD_INFO = 'images_info'
    MEDIA_FIELD_DIR = 'images_dir'

    """
    DATASET CALLBACKS
    """

    def _callback_dataset(self, **kwargs):
        """Method that is called each time a dataset instance is created and that initializes the
        required variables and properties of the dataset.

        Parameters
        ----------
        **kwargs
            Extra named arguments passed to initialization methods and may contains the
            following parameters:
            * max_elements_per_category : str, int or dict
                If int, indicates the maximum number of samples to be taken from among the elements
                of each label.
                If float (0, 1), it refers to the percentage of elements to be taken from each
                label.
                If str, indicates the name of the method to be used in the sampling operation.
                The possible method names are:
                * fewer: will take as the sample number the smallest value of the elements grouped
                by label in the set.
                * mean: calculates the average of the element counts for each category and takes
                this value as the maximum number of elements for each category.
                If dict, it must contain an integer for each category that you want to configure,
                in the form {'label_name': max_elements} (default is None)
            * media_info : dict
                Dictionary that contains the information of each of the fields of the dataset for
                each of the items it contains, in the form {`field_i`: {`item_j`: `value`}}
            * media_dir : str
                The folder path where the media are already stored.
            * random_state : int
                Value of the random_state variable used in functions that make use of random values
                (e.g., `sample_data_elements`).
        """
        kwargs['mapping_media_fields'] = (
            kwargs.get('mapping_media_fields') or kwargs.get('mapping_images_fields')
        )
        kwargs['download_media_to'] = kwargs.get('download_media_to') or kwargs.get('dest_path')
        kwargs['actual_media_dir'] = (
            kwargs.get("actual_media_dir") or kwargs.get("actual_images_dir")
        )
        super()._callback_dataset(**kwargs)

    @classmethod
    def _callback_from_json(cls,
                            json_path: str,
                            **kwargs) -> Tuple[pd.DataFrame, dict]:
        """Build an ImageDataset from a json file.

        Parameters
        ----------
        json_path: str
            Path to a JSON file in COCO format that contains the information of the dataset.
        categories : list of str, str or None, optional
            List, string or path of a CSV or a text file with the categories to be included in the
            dataset.
            If None, registers of all categories will be included.
            If path to a CSV file, it should have the categories in the column `0` and should not
            have a header.
            If path to a text file, it must have the categories separated by a line break.
            If string, it must contain the categories separated by commas.
            If empty list, labeled images will not be included.
        **kwargs
            Extra named arguments that may contains the following parameters:
            * mapping_classes : str or dict
                Dictionary or path to a CSV file containing a mapping that will rename the
                categories present, either to group them into super-categories or to match those in
                other datasets.
                In the case of a dictionary, the `key` of each entry will be the current name
                of the category, and the `value` will be the new name that will be given to that
                category.
                In the case of the path of a CSV file, the file must contain two columns and
                have no header. The column `0` will be the current name of the category and the
                column `1` will be the new name (or the `id` of the category) that will be given to
                that category.
                In both cases you can use the wildcard `*` (as the `key` or in the column `0`)
                to indicate 'all other current categories in the data set'.
                E.g., {'Homo sapiens': 'Person', '*': 'Animal'} will designate the Homo
                sapiens category as 'Person' and the rest of the categories as 'Animal'.
                categories.
            * exclude_categories : list of str, str or None
                List, string or path of a CSV file or a text file with categories to be excluded.
                If it is a path of a CSV file, it should have the categories in the column `0`
                and should not have a header. If it is a path of a text file, it must have the
                categories separated by a line break. If it is a string, it must contain the
                categories separated by commas. (default is None)
            * mapping_images_fields : dict
                Dictionary with a mapping of the specific field names contained in a dataset,
                to the standard names for the specific type of `ImageDataset`
                (e.g., from a JSON file in COCO format).
                E.g., {'image_id': 'id', 'datetime': 'date_captured', 'image_width': 'width'}
                for a JSON file with the field name 'image_id' for the id of the images, the
                field name 'datetime' for the date and time the photo was captured and the
                field name 'image_width' for the width of images (default is {})
            * set_filename_with_id_and_extension : str
                Extension to be added to the id of each item to form the file name
                (default is None)
            * include_bboxes_with_label_empty : bool
                Whether to allow annotations with label 'empty' or not.
                (default is False)
            * clean_cat_names : bool
                Whether to clean or not the category names, converting to lower case and removing
                spaces at the beginning and at the end. By default True
            * mapping_classes_from_col : str or int
                Name or position (0-based) of the column to be used as 'from' in the mapping,
                in case of `mapping_classes` is a CSV. By default None
            * mapping_classes_to_col : str or int
                Name or position (0-based) of the column to be used as 'to' in the mapping,
                in case of `mapping_classes` is a CSV. By default None
            * mapping_classes_filter_expr : Callable, optional
                A Callable that will be used to filter the CSV records in which the mapping is found,
                in case of `mapping_classes` is a CSV. By default None
            * media_base_url : str
                URL where the images of the collection are located. If it is not set, an attempt
                will be made to obtain this URL with the collection name.
            * dest_path : str
                Folder in which images will be downloaded
            * collection : str
                Name of the collection od the dataset
            * collection_year : int
                Year of collection of the dataset
            * collection_version : str
                Version of collection of the dataset
            * extra_fields : list of str
                List of additional fields to take into account within the dataset information,
                besides those specified in `ImageDataset.MEDIA_FIELDS`.

        Returns
        -------
        (pd.DataFrame, dict)
            Tuple of DataFrame object and info dict
        """
        include_bboxes_with_label_empty = kwargs.get("include_bboxes_with_label_empty", False)
        fname_w_id_and_ext = kwargs.get("set_filename_with_id_and_extension")
        collection = kwargs.get('collection')

        # In case you didn't specify a json_path, you can download the json of the `collection`
        if json_path is None:
            collection_ver = kwargs.get('collection_version', 'detection')
            collection_year = kwargs.get('collection_year', datetime.now().year)
            json_path = Collection.Images.fetch(dest_path=get_temp_folder(),
                                                year=collection_year,
                                                version=collection_ver,
                                                collection=collection)

        json_handler = ImagesJsonHandler(json_path)

        info = json_handler.dataset.get('info', {})
        collection = collection or info.get("collection")

        # TODO: refactor this. This is to ensure that items does not repeat among differents images
        img_id_to_item = {
            img_id: get_filename(json_handler.loadImgs(img_id), fname_w_id_and_ext)
            for img_id in json_handler.imgs.keys()
        }

        # Information of the annotations
        if len(json_handler.imgToAnns) > 0:
            anns_info = (
                pd.DataFrame([{cls.ANNOTATIONS_FIELDS.MEDIA_ID: img_id, **ann}
                             for img_id, anns in json_handler.imgToAnns.items() for ann in anns])
            )
            if not 'label' in anns_info.columns:
                anns_info['label'] = (
                    anns_info['category_id']
                    .apply(lambda x: json_handler.cats[x]['name'])
                )
            if 'bbox' in anns_info.columns:
                anns_info['bbox'] = (
                    anns_info.apply(
                        get_bbox_from_json_row, axis=1,
                        include_bboxes_with_label_empty=include_bboxes_with_label_empty)
                )
            anns_info['item'] = (
                anns_info[cls.ANNOTATIONS_FIELDS.MEDIA_ID]
                .apply(lambda x: img_id_to_item[x])
            )

        elif len(img_id_to_item) > 0:
            # Dataset with images but no annotations (e.g., a test dataset)
            anns_info = (
                pd.DataFrame([{'item': item,
                               cls.ANNOTATIONS_FIELDS.MEDIA_ID: img_id,
                               'id': str(uuid.uuid4())}
                             for img_id, item in img_id_to_item.items()])
            )
        else:
            raise Exception("No images or annotations were found in the dataset.")

        # Information of the images
        images_info = pd.DataFrame(
            [{**img} for img in json_handler.imgs.values()]
        )
        # To avoid collision between fields cls.MEDIA_FIELDS.ID and cls.ANNOTATIONS_FIELDS.ID
        if cls.ANNOTATIONS_FIELDS.MEDIA_ID in images_info.columns:
            images_info.drop(cls.ANNOTATIONS_FIELDS.MEDIA_ID, axis=1, inplace=True)
        images_info = (
            images_info.rename(columns={cls.MEDIA_FIELDS.ID: cls.ANNOTATIONS_FIELDS.MEDIA_ID})
        )

        if len(images_info) > 0:
            logger.debug(f"{len(images_info)} images found with {len(anns_info)} annotations")
        else:
            logger.debug(f"No images found for the dataset")

        info["collection"] = collection

        anns_info_cols = list(
            (set(anns_info.columns.values) -
             (set(anns_info.columns.values) & set(images_info.columns.values))) |
            {cls.ANNOTATIONS_FIELDS.MEDIA_ID}
        )
        data = pd.merge(left=anns_info[anns_info_cols], right=images_info,
                        how='left', on=cls.ANNOTATIONS_FIELDS.MEDIA_ID)

        return data, info

    @classmethod
    def _download(cls,
                  dest_path,
                  media_info,
                  set_filename_with_id_and_ext,
                  **kwargs):
        """Function that downloads images from the collection

        Parameters
        ----------
        dest_path : str
            Folder in which images will be downloaded
        collection : str
            Name of the collection
        media_info : pd.DataFrame
            DataFrame containing information about the images in the collection.
            Each row must contain at least the fields {`id`, `file_name`}.
        set_filename_with_id_and_ext : str, optional
            Extension to be added to the id of each item to set the file name (default is None)
        **kwargs :
            Extra named arguments that may contains the following parameters:
            * media_base_url : str
                URL where the images are located. If it is not set, an attempt will be made to
                obtain this URL with the `collection` field.
        """
        media_base_url = kwargs.get('media_base_url')
        cond = media_base_url is not None
        assert cond, f"In order to download media you must assign media_base_url parameter"
        os.makedirs(dest_path, exist_ok=True)
        logger.info(f"Downloading {len(media_dict)} images...")

        def get_dest_filename(img_dict):
            return os.path.join(dest_path, get_filename(img_dict, set_filename_with_id_and_ext))

        media_dict = media_info[['id', 'file_name']].to_dict('records')

        parallel_exec(
            func=download_file,
            elements=media_dict,
            url=lambda img_dict: f"{media_base_url}/{img_dict['file_name']}",
            dest_filename=get_dest_filename,
            verbose=False)

    def _callback_to_folder(self,
                            dest_path,
                            split_in_partitions,
                            split_in_labels,
                            **kwargs):
        """Callback to the `to_folder` method of ImageDataset that allows to assign the values of
        `self.data` items according to the call configuration

        Parameters
        ----------
        dest_path : str
            Path where the dataset files will be stored
        split_in_partitions : bool
            Whether or not to split items in folders with partition names.
            Only works if the dataset has been partitioned previously.
        split_in_labels : bool
            Whether or not to split items in folders with label names.
            These folders will be below the partition folders.
        **kwargs
            Extra named arguments that may contains the following parameters:
            * parallel_set_images_dir : bool
                Whether or not to process the elements in parallel in method `set_images_dir`.
                Recommended for very large datasets (e.g., those with > 500 K items).
                By default False
            * set_images_dir : bool
                Whether to set `images_dir` to `dest_path` at the end of the processing or not,
                by default False
        """
        set_images_dir = kwargs.get("set_images_dir", False)
        if set_images_dir:
            # TODO: check the case when preserve_directories_structure is True
            self.set_images_dir(images_dir=dest_path,
                                append_partition_to_item_path=split_in_partitions,
                                append_label_to_item_path=split_in_labels,
                                from_basename=True,
                                actual_images_dir='',
                                **kwargs)

    """
    OVERLOADED CONSTRUCTORS
    """

    @classmethod
    def from_csv(cls, source_path: str, **kwargs) -> ImageDataset:
        """Create an `ImageDataset` from a csv file.

        Parameters
        ----------
        source_path : str
            Path of a csv file or a folder containing csv files that will be
            converted into a `Dataset`.
            The csv file(s) must have at least two columns, which represents the `item` data.
        columns : list of str, list of int, or None, optional
            List of column names, or list of column indexes, to load from the
            csv file(s). If None, load all columns. (default is None)
        header : bool, optional
            Whether or not csv contains header at row 0 (default is True).
            If False, you need to map at least `item` and `label` columns by
            position in column_mapping parameter.
        recursive : bool
            Whether or not to seek files also in subdirectories
            (default is True)
        column_mapping : dict, optional
            Dictionary to map column names in dataset.
            If header=True mapping must be done by the column names,
            otherwise must be done by column positions.
            E.g.::
                header=True: column_mapping={"c1": "item", "c2": "label"}
                header=False: column_mapping={0: "item", 1: "label"}
            (default is None)
        split_by_filenames : bool, optional
            If `source_path` is the path of a folder containing csv files,
            splits the dataset by the file names. (default is False).
            The file names must be the following::
                `train.csv` for `train` partition
                `test.csv` for `test` partition
                `validation.csv` for `validation` partition
        split_by_column : str or int, optional
            Column name or column index in the csv file(s) to split the dataset
            (default is "partition").
            The values in the column must be the following::
                `train` for `train` partition
                `test` for `test` partition
                `validation` for `validation` partition
        info : dict, optional
            Information of the csv file (default is {})
        **kwargs
            Extra named arguments passed to the `from_csv` method of `Dataset`

        Returns
        -------
        ImageDataset
            Instance of the created ImageDataset
        """
        kwargs['include_id'] = True
        return super().from_csv(source_path=source_path, **kwargs)

    @classmethod
    def from_folder(cls,
                    source_path: str,
                    extensions: List[str] = [".JPG", ".jpg", ".png", ".PNG", ".jpeg", ".JPEG"],
                    label_by_folder_name: bool = True,
                    split_by_folder: bool = True,
                    **kwargs) -> ImageDataset:
        """Create an ImageDataset from a folder structure.

        Parameters
        ----------
        source_path : str
            Path of a folder to be loaded and converted into a Dataset object.
        extensions : list of str, optional
            List of extensions to seek files in folders.
            [""] to seek all files in folders (default is [".JPG", ".jpg", ".png", ".bmp"])
        label_by_folder_name : bool
            Whether or not you want to label each item of the dataset according
            to the name of the folder that contains it.
            (default is True)
        split_by_folder : bool, optional
            Split the dataset from the directory structure (default is True).
            For this option, directories must be in the following structure::
                - source_path
                - `train`
                    - class folders
                - `test`
                    - class folders
                - `validation`
                    - class folders
        **kwargs
            Extra named arguments passed to `from_folder` method of `Dataset`

        Returns
        -------
        Dataset
            Instance of the created dataset
        """
        return super().from_folder(source_path=source_path,
                                   extensions=extensions,
                                   label_by_folder_name=label_by_folder_name,
                                   split_by_folder=split_by_folder,
                                   **kwargs)

    """
    GET METHODS
    """

    def get_partitioned(self, partitions=[Partitions.NAMES]):
        """Method to get a DataFrame for each partition in the dataset

        Parameters
        ----------
        partitions : list, str or None, optional
            List of partition names (or the name of one partition) to get,
            by default [Partitions.NAMES]

        Returns
        -------
        n-tuple of DataFrame or DataFrame
            A DataFrame for each partition in `partitions`.
            If `partitions` is None, return `self.as_dataframe()`
            If `partitions` is of type str, return a single `DataFrame` instance

        Raises
        ------
        Exception
            In case the dataset does not contain a partition in `partitions`
        """
        return_list = True
        if partitions is None:
            return self.as_dataframe()
        if not is_array_like(partitions):
            partitions = [partitions]
            return_list = False
        assert all([part in self.params["partitions"] for part in partitions]),\
            "The dataset does not contain all the specified partitions"
        df = self.as_dataframe()
        if return_list:
            return [df.iloc[self.params["partitions"][part]] for part in partitions]
        else:
            return [df.iloc[self.params["partitions"][part]] for part in partitions][0]

    """
    OVERLOADED METHODS
    """
    # TODO: move to media.py

    def split(self: ImageDataset,
              train_perc: float = 0.8,
              test_perc: float = 0.2,
              val_perc: float = 0.,
              group_by_field='image_id',
              stratify=True,
              **kwargs) -> ImageDataset:
        """Split the dataset between `train`, `test` and `validation`
        partitions. `train_perc`, `test_perc` and `val_perc` must sum to 1.

        Parameters
        ----------
        train_perc : float, optional
            Train set percentage. Should be between 0 and 1 (default is 0.8)
        test_perc : float, optional
            Test set percentage. Should be between 0 and 1 (default is 0.2)
        val_perc : float, optional
            Validation set percentage. Should be between 0 and 1 (default is 0.)
        group_by_field : str, optional
            Field of the images by which to group the elements of the dataset when partitioning,
            and ensures that there are no elements of the same group in more than one partition
            (default is None)
        stratify : bool, optional
            Whether or not to split in a stratified fashion, using the class labels
            (default is True)

        Returns
        -------
        ImageDataset
            Instance of the Dataset
        """
        random_state = kwargs.get("random_state", 42)

        assert_cond = np.isclose(train_perc + test_perc + val_perc, 1.)
        assert assert_cond, 'The proportions of the partitions must add up to 1'
        logger.info(f"Partitioning the dataset with proportions (train/test/val): "
                    f"{train_perc}/{test_perc}/{val_perc}")
        if stratify:
            logger.debug("Partitioning will be performed in stratified fashion")
        if group_by_field is not None:
            logger.debug(
                f"During partitioning, grouping by the field {group_by_field} "
                f"will be done to avoid elements of the same groups being in different partitions")

        # Delete previous partitions
        self.params["partitions"] = {}

        partitions = {
            Partitions.TRAIN: train_perc,
            Partitions.TEST: test_perc,
            Partitions.VALIDATION: val_perc
        }
        reverse = group_by_field is None
        ordered_partitions = dict(sorted(partitions.items(), key=lambda k: k[1], reverse=reverse))

        df = self.as_dataframe()
        label_counts = df.label.value_counts().rename_axis('label').to_frame('count')
        one_sample_labels = label_counts[label_counts['count'] == 1].index.values
        one_sample_ordinals = df[df.label.isin(one_sample_labels)].index.values
        ordinals = df[~df.label.isin(one_sample_labels)].index.values

        if group_by_field is not None and group_by_field in df:
            total = 0.
            for part, perc in ordered_partitions.items():
                if np.isclose(perc, 0.):
                    continue
                if np.isclose(total + perc, 1.):  # Train partition (the biggest one last)
                    ordinals = np.concatenate([ordinals, one_sample_ordinals])
                    self._append_to_partition(part, ordinals)
                else:
                    part_size = perc / (1. - total)

                    if stratify:
                        rest, ordinals_part = (
                            gradient_group_stratify(df.loc[ordinals],
                                                    test_size=part_size,
                                                    group_field=group_by_field,
                                                    random_state=random_state)
                        )
                        self._append_to_partition(part, ordinals_part)
                        ordinals = rest
                    else:
                        grp_kfold = (
                            GroupShuffleSplit(n_splits=1,
                                              test_size=part_size,
                                              random_state=random_state)
                        )
                        groups = df.loc[ordinals][group_by_field].values
                        for rest_idx, ords_idx_part in grp_kfold.split(X=ordinals, groups=groups):
                            self._append_to_partition(part, ordinals[ords_idx_part])
                            ordinals = ordinals[rest_idx]
                    total += perc
        else:
            total = 0.
            for part, perc in ordered_partitions.items():
                if np.isclose(perc, 0.):
                    break
                if np.isclose(total + perc, 1.):
                    self._append_to_partition(part, ordinals)
                else:
                    strat_labels = df.loc[ordinals].label.values if stratify else None
                    part_size = perc / (1. - total)
                    rest, ordinals_part = train_test_split(
                        ordinals, test_size=part_size, random_state=random_state,
                        stratify=strat_labels)
                    if np.isclose(total, 0.):  # Training partition (the biggest one first)
                        ordinals_part = np.concatenate([ordinals_part, one_sample_ordinals])
                    self._append_to_partition(part, ordinals_part)
                    ordinals = rest
                    total += perc

        utils.fix_partitioning_by_priorities(self)

        return self

    """
    AUX METHODS
    """
    # region reporter

    # TODO: change name to _reporter (check conabio_ml_text)
    def reporter(self,
                 dest_path: str,
                 process_args: dict,
                 **kwargs):
        """Method that generates the report elements of a dataset. It Contains:
            - samples_per_class: CSV file which contains the number of samples for each class
            per partition in format (label, count_label, partition).
            - samples_per_class_plot: bar plot of samples_per_class information that will be stored
            in the `dest_path/plot_samples_per_class.png` file.
            - samples_per_class_plot_trunc: bar plot of samples_per_class information with at most
            `MAX_REGISTERS_RESUME` records that will be stored in the
            `dest_path/plot_samples_per_class_trunc.png` file.
            - info: Information of the datasets that includes `version`, `description`, `year`,
            `collection` and `url`.
            - preprocessing: Preprocessing operations that were performed on the dataset elements.
            - augmentation: Data augmentation operations that were performed on the dataset
            elements.
            - additional_criteria: Additional criteria that were considered to partition the
            dataset. I. e., if the elements were grouped according to some criteria
            - dataset: Path of the csv file in wich the dataset is stored.

        Parameters
        ----------
        dest_path : str
            Path of the folder to store the resulting files
        process_args : dict
            The args of the process that wraps this class

        Returns
        -------
        dict
            Params to be reported in the Pipeline
        """
        lang = kwargs.get("lang", languages.EN)
        x_label = report_params.report_transl["dataset_distr_tbl_hdr"]["label"][lang]
        y_label = report_params.report_transl["dataset_distr_tbl_hdr"]["counts"][lang]
        results = {
            "samples_per_class": {
                "asset": self._samples_per_class(dest_path),
                "type": AssetTypes.FILE
            },
            "samples_per_class_plot": {
                "asset": self.plot_samples_per_class(dest_path=dest_path,
                                                     filename="plot_samples_per_class.png",
                                                     x_axis="label", y_axis="count_label",
                                                     x_label=x_label,
                                                     y_label=y_label),
                "type": AssetTypes.PLOT
            },
            "samples_per_class_plot_trunc": {
                "asset":
                    self.plot_samples_per_class(
                        dest_path=dest_path,
                        filename="plot_samples_per_class_trunc.png",
                        x_axis="label", y_axis="count_label",
                        x_label=x_label,
                        y_label=y_label,
                        truncate_categories=report_params.MAX_REGISTERS_RESUME),
                "type": AssetTypes.PLOT
            },
            "info": {
                "asset": self._get_info(),
                "type": AssetTypes.DATA
            },
            "preprocessing": {
                "asset": self._get_preprocessing_ops(),
                "type": AssetTypes.DATA
            },
            "augmentation": {
                "asset": self._get_augmentation_ops(),
                "type": AssetTypes.DATA
            },
            "additional_criteria": {
                "asset": self._get_additional_criteria(),
                "type": AssetTypes.DATA
            },
            "dataset": {
                "asset": self.to_csv(dest_path=dest_path),
                "type": AssetTypes.FILE
            }
        }
        return results

    def _samples_per_class(self, dest_path, **kwargs):
        """Creates and stores in the `dest_path`/samples_per_class.csv file, using
        a DataFrame which contains the number of samples for each class
        per partition in format (label, count_label, partition)

        Parameters
        ----------
        dest_path : str
            Path of the folder in which the file will be stored
        **kwargs
            Extra named arguments that may contains the following parameters:
            * partitions_in_columns : bool
                Whether to create a file named `dest_path`/samples_per_class_cols_parts.csv
                with the partitions counts separated in columns or not (default is False)

        Returns
        -------
        str
            Path of the CSV file
        """
        partitions_in_columns = kwargs.get('partitions_in_columns', False)
        os.makedirs(dest_path, exist_ok=True)
        samplesperclass_file = super()._samples_per_class(dest_path)
        if partitions_in_columns:
            res = (
                pd.read_csv(samplesperclass_file, index_col=0)
                .set_index(['label', 'partition'])
                .unstack()
                .fillna(0)
            )
            res.columns = res.columns.droplevel(0)
            res = res.reset_index()
            columns = list(res.columns.values)
            file_path = os.path.join(dest_path, "samples_per_class_cols_parts.csv")
            res.to_csv(file_path, columns=columns, index=False)

        return samplesperclass_file

    def plot_samples_per_class(self,
                               dest_path,
                               filename='plot_samples_per_class.png',
                               x_axis="label",
                               y_axis="count_label",
                               x_label='Label',
                               y_label='Counts',
                               partitioned=False,
                               truncate_categories: int = None,
                               **kwargs):
        """Plot the bar plot of samples_per_class information and will be stored in the file
        `dest_path/filename`.

        Parameters
        ----------
        dest_path : str
            Path of the folder to store the resulting image file
        filename : str, optional
            Name of the image resulting file, by default 'plot_samples_per_class.png'
        x_axis : str, optional
            Name of the column from which the data is taken for the x-axis in the `data` property.
            By default 'label'
        y_axis : str, optional
            Name of the column from which the data is taken for the y-axis in the `data` property.
            By default 'count_label'
        x_label : str, optional
            The label text for the x-axis in the plot, by default 'Label'
        y_label : str, optional
            The label text for the y-axis in the plot, by default 'Counts'
        partitioned : bool, optional
            Whether to generate a plot for each partition of the dataset (default is False)
        truncate_categories : int, optional
            Maximum number of records that will be shown in the graph.
            The `truncate_categories` records with the highest number of samples will be displayed.
            In case there are fewer labels than `truncate_categories`, `None` will be returned.
            (default is None)
        **kwargs
            Extra named arguments passed to `samples_per_class` method

        Returns
        -------
        str
            Path of the generated image file
        """
        import seaborn as sns
        import matplotlib.pyplot as plt

        columns = ['label', 'count_label'] + (['partition'] if partitioned else [])
        samplesperclass_file = self._samples_per_class(
            dest_path, csv_columns=columns, csv_index=False, **kwargs)
        df = pd.read_csv(samplesperclass_file)

        if partitioned:
            partitions = df["partition"].unique().tolist()
            f, ax = plt.subplots(len(partitions), 1, figsize=(10, 10), squeeze=False)
            ax = ax.flatten()

            for ix, partition in enumerate(partitions):
                data = df.loc[df["partition"] == partition]

                if truncate_categories is not None:
                    if len(data) <= truncate_categories:
                        return None
                    data = data.head(truncate_categories)

                if len(data) > report_params.MAX_REGISTERS_RESUME:
                    x_axis, y_axis = y_axis, x_axis
                    x_label, y_label = y_label, x_label
                    rotation = 0
                else:
                    rotation = 30

                g = sns.barplot(data=data, x=x_axis, y=y_axis, hue_order=data.index,  ax=ax[ix])
                if rotation > 0:
                    g.set_xticklabels(g.get_xticklabels(), rotation=rotation)
        else:
            results = {
                "label": [],
                "count_label": []
            }
            for lbl in df["label"].unique():
                results["label"].append(lbl)
                results["count_label"].append(np.sum(df.loc[df["label"] == lbl]["count_label"]))
            data = pd.DataFrame(results)
            data.sort_values(by=["count_label"], axis=0, ascending=False, inplace=True)
            if truncate_categories is not None:
                if len(data) <= truncate_categories:
                    return None
                data = data.head(truncate_categories)

            if len(data) > report_params.MAX_REGISTERS_RESUME:
                x_axis, y_axis = y_axis, x_axis
                x_label, y_label = y_label, x_label
                rotation = 0
                figsize = (17, 30)
            else:
                rotation = 30
                figsize = (10, 10)

            f, ax = plt.subplots(1, 1, figsize=figsize)
            g = sns.barplot(data=data, x=x_axis, y=y_axis, hue_order=data.index,  ax=ax)
            if rotation > 0:
                g.set_xticklabels(g.get_xticklabels(), rotation=rotation)

        filepath = os.path.join(dest_path, filename)
        plt.xlabel(x_label)
        plt.ylabel(y_label)
        plt.savefig(filepath, dpi=100)

        return filepath
    # endregion
    """
    TO METHODS
    """

    def to_folder(self: ImageDataset,
                  dest_path: str,
                  split_in_partitions: bool = True,
                  split_in_labels: bool = True,
                  keep_originals: bool = True,
                  set_images_dir: bool = False,
                  **kwargs) -> str:
        """Create a filesystem representation of the dataset.

        Parameters
        ----------
        dest_path : str
            Path where the dataset files will be stored
        split_in_partitions : bool, optional
            Whether or not to split items in folders with partition names.
            Only works if the dataset has been partitioned previously.
            (default is True)
        split_in_labels : bool, optional
            Whether or not to split items in folders with label names.
            These folders will be below the partition folders (default is True)
        keep_originals : bool, optional
            Whether to keep original files or delete them (default is True)
        set_images_dir : bool, optional
            Whether to set `images_dir` to `dest_path` at the end of the processing or not,
            by default False
        **kwargs
            Extra named arguments passed to `to_folder` method of `Dataset`

        Returns
        -------
        str
            Path of folder created
        """
        kwargs['parallel_processing'] = kwargs.get('parallel_processing', True)
        return super().to_folder(dest_path=dest_path,
                                 split_in_partitions=split_in_partitions,
                                 split_in_labels=split_in_labels,
                                 keep_originals=keep_originals,
                                 set_images_dir=set_images_dir,
                                 **kwargs)

    # TODO: add bbox field
    # TODO: document
    def to_json(self, dest_path, split_in_partitions=True, include_annotations_info=True):
        if split_in_partitions and self.get_num_partitions() > 1:
            for part in self.get_partitions_names():
                if os.path.isdir(dest_path):
                    dest_path = os.path.join(dest_path, f"dataset-{part}.json")
                else:
                    dest_path_base, dest_path_ext = os.path.splitext(dest_path)
                    dest_path_part = f'{dest_path_base}-{part}{dest_path_ext}'
                part_ds = self.filter_by_partition(part, inplace=False)
                part_ds.to_json(dest_path_part, split_in_partitions=False)
            return

        annotations = self._get_data()
        images_info = self.get_images_info()
        labelmap = self.get_labelmap()
        categories = [{'id': int(k), 'name': v} for k, v in labelmap.items()]
        inv_labelmap = self._get_inverse_labelmap()
        annotations['category_id'] = annotations['label'].apply(lambda x: int(inv_labelmap[x]))
        if 'bbox' in annotations:
            annotations['bbox'] = annotations['bbox'].apply(get_bbox_from_dataset_row, axis=1)
        annotations = annotations[['image_id', 'id', 'category_id']]

        final_output = {
            'images': images_info.to_dict('records')
        }
        if include_annotations_info:
            final_output['annotations'] = annotations.to_dict('records')
            final_output['categories'] = categories
        if os.path.isdir(dest_path):
            dest_path = os.path.join(dest_path, "dataset.json")
        with open(dest_path, 'w') as f:
            json.dump(final_output, f, indent=1)

    """
    CREATE METHODS
    """

    def create_image_level_ds(self: ImageDataset,
                              how='keep_first') -> ImageDataset:
        """Method that create an image-level dataset from the current
        object-level dataset, assigning only one annotation label to each image,
        accordingly to the parameter `keep`, that indicates which label should be taken

        Parameters
        ----------
        keep : str one of {'first', 'last'}, optional
            Specify which of the annotations should be considered to obtain the label of each image
            - `first` : Drop duplicates except for the first occurrence.
            - `last` : Drop duplicates except for the last occurrence.
            - `multilabel` : Drop duplicates that have the same label, leaving those with different
            labels.
            By default 'first'

        Returns
        -------
        ImageDataset
            Dataset of type `classification`
        """
        subset = ['item']
        if how == 'multilabel':
            subset += ['label']
        anns_cols = set(self.data.columns.values)
        anns_cols -= {"bbox"}
        if self.ANNOTATIONS_FIELDS.SCORE_DET in anns_cols:
            anns_cols -= {self.ANNOTATIONS_FIELDS.SCORE_DET}
        split_by_column = None
        if self.is_partitioned():
            anns_cols |= {'partition'}
            split_by_column = 'partition'
        anns_info = self.as_dataframe(columns=list(anns_cols))
        anns_info.drop_duplicates(subset=subset, keep='first', ignore_index=True, inplace=True)
        # TODO: Check extra_fields info
        instance = type(self)(anns_info,
                              info=self.info,
                              images_dir=self.get_images_dir(),
                              images_info=self.get_images_info(),
                              split_by_column=split_by_column,
                              extra_fields=self._get_extra_fields())
        return instance

    def create_classif_ds_from_bboxes_crops(self: ImageDataset,
                                            dest_path: str = None,
                                            use_partitions=False,
                                            info: dict = None,
                                            **kwargs) -> ImageDataset:
        """Method that generates crops with the coordinates of the bounding boxes from the
        annotations of a dataset of type `object detection`, and assigns the labels to that
        new images in order to create a dataset of type `classification`

        Parameters
        ----------
        dest_path : str, optional
            Folder in which the images created from the crops of the bouding boxes are saved.
            If None, the images will be saved in the folder `./crops_images`.
            By default None
        use_partitions : bool, optional
            Whether to use the partitions from the original dataset or not, by default False
        info : dict, optional
            Information to be stored in the new dataset, by default {}
        **kwargs
            Extra named arguments passed to the `ImageDataset` constructor and also may include the
            parameters:
            * allow_label_empty : bool
                Whether to allow annotations with label 'empty' or not, by default False
            * force_crops_creation : bool
                Whether to force the creation of the crops or not, by default False

        Returns
        -------
        ImageDataset
            Instance of the created `classification` dataset

        Raises
        ------
        Exception
            in case the original dataset is not of type `object detection`
        """
        info = {} if info is None else info
        assert self.is_detection_dataset(), "The dataset must be of detection type"
        logger.info("Creating classification dataset from detection bounding boxes")
        dest_path = dest_path if dest_path is not None else "crops_images"
        allow_label_empty = kwargs.get("allow_label_empty", False)
        force_crops_creation = kwargs.get("force_crops_creation", False)

        kwargs['split_by_column'] = "partition" if use_partitions else None

        process_images = []

        self.get_images_sizes(size_correction=True)
        df = self.as_dataframe()
        create_crops = None

        anns_info_crops = defaultdict(list)
        images_info_crops = defaultdict(list)
        use_id_as_crop_item = df[self.ANNOTATIONS_FIELDS.ID].nunique() == len(df)
        for row in df.to_dict('records'):
            if row["label"] == 'empty' and not allow_label_empty:
                continue
            if use_id_as_crop_item:
                crop_item = f"{row[self.ANNOTATIONS_FIELDS.ID]}.jpg"
            else:
                crop_item = f"{str(uuid.uuid4())}.jpg"
            crop_path = os.path.join(dest_path, crop_item)
            if create_crops is None:
                create_crops = not os.path.isfile(crop_path)
            try:
                (x1, y1, x2, y2) = coords_utils.get_bbox_from_dataset_row(
                    row,
                    output_format=coords_utils.COORDINATES_FORMATS.X1_Y1_X2_Y2,
                    image_width=row.get('width'),
                    image_height=row.get('height')
                )
            except Exception:
                logger.exception(
                    f"An error occurred while processing image {row['item']} and has been skipped")
                continue
            process_images.append(
                (row[self.ANNOTATIONS_FIELDS.ITEM], crop_path, None, None, None, (x1, y1, x2, y2)))

            anns_info_crops[self.ANNOTATIONS_FIELDS.ITEM].append(crop_item)
            anns_info_crops[self.ANNOTATIONS_FIELDS.ID].append(row[self.ANNOTATIONS_FIELDS.ID])

            images_info_crops[self.FIELD_TO_JOIN_IMGS_ANNS].append(
                row[self.FIELD_TO_JOIN_IMGS_ANNS])

        if create_crops or force_crops_creation:
            n_crops = len(process_images)
            logger.info(f"Generating {n_crops} crops from detection dataset in folder {dest_path}")
            os.makedirs(dest_path, exist_ok=True)
            parallel_process_images(process_images)
        else:
            logger.info(f"Using already created crops in {dest_path}")

        # region annotation-info
        anns_info_crops_df = pd.DataFrame(anns_info_crops)
        anns_cols = self._get_current_annotations_info_cols(
            use_partitions,
            remove_cols=[self.ANNOTATIONS_FIELDS.ITEM, self.ANNOTATIONS_FIELDS.BBOX])
        anns_info_crops_df = anns_info_crops_df.merge(df[anns_cols], how='inner',
                                                      on=self.ANNOTATIONS_FIELDS.ID)
        # endregion
        # region images-info
        imgs_level_info = self.get_images_info().reset_index()
        if 'width' in imgs_level_info.columns and 'height' in imgs_level_info.columns:
            imgs_level_info = imgs_level_info.drop(['width', 'height'], axis=1)
        images_info_new = pd.DataFrame(
            images_info_crops, index=anns_info_crops[self.FIELD_TO_JOIN_IMGS_ANNS]).reset_index()
        crops_images_info = images_info_new.merge(
            imgs_level_info, how='left', on=self.FIELD_TO_JOIN_IMGS_ANNS)
        crops_images_info = crops_images_info.drop(self.FIELD_TO_JOIN_IMGS_ANNS, axis=1)
        crops_images_info = (
            crops_images_info
            .rename(columns={'index': self.FIELD_TO_JOIN_IMGS_ANNS})
            .set_index(self.FIELD_TO_JOIN_IMGS_ANNS))
        # endregion

        kwargs['extra_fields'] = self._get_extra_fields()
        instance = type(self)(data=anns_info_crops_df,
                              info=info,
                              images_dir=dest_path,
                              images_info=crops_images_info,
                              **kwargs)
        # Correct the crops sizes because 'square_crop' option was used
        instance.get_images_sizes(size_correction=True)
        return instance

    """
    GET METHODS
    """

    def is_detection_dataset(self):
        """Determines whether a dataset is of type `object detection` or not

        Returns
        -------
        bool
            Whether a dataset is of type `object detection` or not
        """
        return "bbox" in self.data.columns

    def get_images_info(self, as_dataframe=True, rename_image_id=False):
        """Gets the information about the images stored in the `params['images_info']` property

        Parameters
        ----------
        as_dataframe : bool, optional
            Whether or not to return the maedia info as a `pd.DataFrame`, by default True
        rename_media_id : bool, optional
            Whether or not to rename the column 'id' to self.ANNOTATIONS_FIELDS.MEDIA_ID in case
            `as_dataframe` is True, by default False

        Returns
        -------
        dict
            Dictionary with the information of the images
        """
        return self.get_media_info(as_dataframe=as_dataframe, rename_media_id=rename_image_id)

    def get_collection(self):
        """Gets the collection to which the dataset belongs

        Returns
        -------
        str
            Colection of the dataset
        """
        return self.info.get("collection", None)

    def get_images_sizes(self, as_dict=False, size_correction=False) -> pd.DataFrame:
        """Get the image sizes of the elements in the dataset.
        In case there are elements without assigned size, this function will try to read the sizes
        of the stored files.

        Parameters
        ----------
        as_dict : bool, optional
            Whether to return a dict or not. If True the results will be returned in the form
            {`item`: {"width": `width`, "height": `height`}}. Otherwise, it must be a
            Dataframe containing 'width' and 'height' columns and the item as index.
            By default False
        size_correction :  bool, optional
            Whether to correct the sizes of the images or not, i.e., calculate the size of all the
            images by obtaining it from the files. By default False

        Returns
        -------
        pd.DataFrame
            Dataframe containing width and height for each item
        """
        flds = self.MEDIA_FIELDS
        images_info = self.get_images_info()
        if flds.WIDTH not in images_info or flds.HEIGHT not in images_info or size_correction:
            elems_wo_size = images_info.index.values
        else:
            elems_wo_size = images_info[
                (images_info[flds.WIDTH].isna()) | (images_info[flds.WIDTH] == '') |
                (images_info[flds.HEIGHT].isna()) | (images_info[flds.HEIGHT] == '')].index.values

        if len(elems_wo_size) > 0:
            sizes_of_imgs_wo_size_info = utils.get_images_sizes(elems_wo_size)
            if len(sizes_of_imgs_wo_size_info) != len(images_info):
                # Some images have size info and some of them not.
                imgs_wo_size = sizes_of_imgs_wo_size_info.index
                images_w_size_info = images_info[~images_info.index.isin(imgs_wo_size)]
                sizes_of_imgs_w_size_info = images_w_size_info[[flds.HEIGHT, flds.WIDTH]]
                imgs_sizes = pd.concat([sizes_of_imgs_wo_size_info, sizes_of_imgs_w_size_info])
            else:
                imgs_sizes = sizes_of_imgs_wo_size_info
            self.set_images_sizes(imgs_sizes)
            images_info = self.get_images_info()

        if as_dict:
            return images_info[[flds.HEIGHT, flds.WIDTH]].to_dict(orient='index')
        return images_info[[flds.HEIGHT, flds.WIDTH]]

    def _get_coordinates_type(self):
        """Determines if the dataset has absolute or relative coordinates

        Returns
        -------
        str
            Either `COORDINATES_TYPES.RELATIVE` ('relative'), `COORDINATES_TYPES.ABSOLUTE`
            ('absolute'), or None in case the type cannot be determined from the coordinates

        Raises
        ------
        ValueError
            In case the data type of the coordinates is invalid
        """
        df = self.as_dataframe()
        if len(df) == 0:
            return None
        bbox = df.iloc[0]["bbox"]
        if type(bbox) == str:
            [coord1, coord2, coord3, coord4] = [float(x) for x in bbox.split(',')]
        elif is_array_like(bbox):
            [coord1, coord2, coord3, coord4] = [x for x in bbox]
        else:
            raise ValueError(f"'{type(bbox)}' is not a valid type for a bounding box.")
        return coords_utils.get_coordinates_type_from_coords(coord1, coord2, coord3, coord4)

    def get_images_dir(self):
        """Gets the directory where the images are stored.

        Returns
        -------
        str
            Base path in which images are stored. In case this variable has not been assigned,
            the empty string will be returned
        """
        return self.get_media_dir()

    """
    SET METHODS
    """

    def _set_images_info(self, images_info):
        """Assign the information of the images to the property `params['images_info']`

        Parameters
        ----------
        images_info : pd.DataFrame
            Dataframe that contains the information of each of the fields of the dataset for each
            of the items it contains
        """
        self._set_media_info(images_info)

    def set_images_sizes(self, images_sizes):
        """Set the images_sizes dict in the property `params['images_info']`

        Parameters
        ----------
        images_sizes : pd.DataFrame or dict
            If DataFrame, it must contain columns 'width' and 'height' and the item as index.
            If dictionary, it must contain 'width' and 'height' of each item in the form
            {`item`: {'width': `width`, 'height': `height`}}.
        """
        if type(images_sizes) is dict:
            images_sizes = pd.DataFrame(data=images_sizes.values(), index=images_sizes.keys())

        images_info = self.get_images_info()
        images_info[self.MEDIA_FIELDS.WIDTH] = (
            images_sizes[self.MEDIA_FIELDS.WIDTH].astype(float))
        images_info[self.MEDIA_FIELDS.HEIGHT] = (
            images_sizes[self.MEDIA_FIELDS.HEIGHT].astype(float))
        self._set_images_info(images_info)

    def _set_labelmap_filename(self, labelmap_filename):
        """Sets the `labelmap_filename` variable in the property `params`

        Parameters
        ----------
        labelmap_filename : str
            Path to the labelmap filename of the dataset.
        """
        self.params["labelmap_filename"] = labelmap_filename
        logger.debug(f"Assigning labelmap filename with {labelmap_filename}")

    def set_images_dir(self, images_dir, **kwargs):
        """Sets the `images_dir` variable in the property `params`

        Parameters
        ----------
        images_dir : str
            Path of the base folder where the images are saved
        **kwargs
            Extra named arguments that may include the parameters:
            * append_partition_to_item_path : bool
                Whether to append the `partition` of each row in the `item` path or not,
                by default False
            * append_label_to_item_path : bool
                Whether to append the `label` of each row in the `item` path or not,
                by default False
            * from_basename : bool
                Whether the initial value of each `item` is obtained from the basename of the
                `item` or not, by default False
            * actual_images_dir : bool
                The actual value of the `images_dir` property. If None, it will be obtained from
                the current instance. By default None
            * validate_filenames : bool
                Whether or not to validate that the items exist. By default True
            * not_exist_ok : bool
                Whether to include images that exist in `images_dir` and silently pass exceptions
                for those images that are not present (default is False)
            * parallel_processing : bool
                Whether or not to process the elements in parallel. Recommended for very large
                datasets (e.g., those with > 500 K items). By default False
        """
        kwargs['actual_media_dir'] = kwargs.get("actual_images_dir", None)
        self.set_media_dir(media_dir=images_dir, **kwargs)

    """
    UTILS METHODS
    """

    def _convert_coordinates(self, input_format, output_format, output_coords_type):
        """Converts the 'bbox' field of the dataset to `output_format` coordinate format and
        `output_coords_type` type, starting from the current coordinate type and `input_format`
        input format

        Parameters
        ----------
        input_format : `COORDINATES_FORMATS`
            Determine the order and meaning of the elements of `bbox`
        output_format : `COORDINATES_FORMATS`
            Determine the order and meaning of the elements of the output
        output_coords_type : `COORDINATES_TYPES`
            Determines if the value of the output coordinates will be given in absolute pixel
            values, or normalized in relation to the image size and therefore in the range [0,1].
        """
        in_coords_type = self._get_coordinates_type()
        if input_format == output_format and in_coords_type == output_coords_type:
            return
        images_sizes = self.get_images_sizes()
        df = self.as_dataframe()
        id_to_bbox = {}
        for row in df.to_dict('records'):
            bbox = coords_utils.transform_coordinates(
                row['bbox'],
                input_format=input_format,
                output_format=output_format,
                output_coords_type=output_coords_type,
                output_data_type=coords_utils.COORDINATES_DATA_TYPES.STRING,
                image_width=images_sizes.loc[row['item']]['width'],
                image_height=images_sizes.loc[row['item']]['height'])
            id_to_bbox[row['id']] = bbox
        self.set_anns_info_field_by_expr('bbox', lambda x: id_to_bbox[x['id']], inplace=True)

    def set_dataset_field_by_expr(self, field, values_expr, inplace=True):
        anns_info_cols = self.data.columns.values
        images_info_cols = self.get_images_info(as_dataframe=True).columns.values
        # TODO: add special cases (e.g., image_id, id, item)
        if field in anns_info_cols:
            return self.set_anns_info_field_by_expr(field, values_expr, inplace)
        elif field in images_info_cols:
            return self.set_images_info_field_by_expr(field, values_expr, inplace)
        else:
            raise ValueError(f'Field {field} does not exist in the dataset')

    # TODO: Move to media.py
    def set_images_info_field_by_expr(self, field, values_expr, inplace=True):
        """Method that assigns the information of the `field` from the information contained in
        `images_info` by applying the expression `values_expr`

        Parameters
        ----------
        field : str
            Name of the field in `images_info`
        values_expr : Callable
            Expression to be applied in order to change the value of `field` in `images_info`
        inplace : bool, optional
            If True, perform operation in-place, by default True

        Returns
        -------
        ImageDataset or None
            Instance of the Dataset or None if `inplace=True`
        """
        instance = self if inplace else self.copy()

        rename_image_id = field == 'image_id'
        df = instance.get_images_info(rename_image_id=rename_image_id)

        df[field] = df.apply(values_expr, axis=1)

        if rename_image_id:
            df = df.rename(columns={'image_id': 'id'})
        instance._set_images_info(df)

        if field not in self.MEDIA_FIELDS.NAMES and field != self.ANNOTATIONS_FIELDS.MEDIA_ID:
            instance._set_extra_fields([field])

        if not inplace:
            return instance

    def fix_sizes_and_bboxes(self):
        """Fix the size and bbox coordinates of the images. It is useful, for example, when you
        have the sizes of the original images and the coordinates of the boxes are of absolute
        type, but you use resized images
        """
        self._convert_coordinates(
            input_format=coords_utils.COORDINATES_FORMATS.X_Y_WIDTH_HEIGHT,
            output_format=coords_utils.COORDINATES_FORMATS.X_Y_WIDTH_HEIGHT,
            output_coords_type=coords_utils.COORDINATES_TYPES.RELATIVE)
        self.get_images_sizes(size_correction=True)
        self._convert_coordinates(
            input_format=coords_utils.COORDINATES_FORMATS.X_Y_WIDTH_HEIGHT,
            output_format=coords_utils.COORDINATES_FORMATS.X_Y_WIDTH_HEIGHT,
            output_coords_type=coords_utils.COORDINATES_TYPES.ABSOLUTE)

    """
    ABSTRACT METHODS
    """

    @abstractmethod
    def write_labelmap_file(self, dest_path):
        """Writes a file with the map of (integer) labels to class names.

        Parameters
        ----------
        dest_path: str
            The path of the file (or directory) in which the labelmap file should be written.

        Returns
        -------
        str
            Path of the file where the labelmap file was written
        """
        if self.is_detection_dataset():
            labelmap = self.get_labelmap()
            assert labelmap is not None, "Dataset without categories"
            return utils.write_pbtxt(labelmap, dest_path)
        else:
            return super().write_labelmap_file(dest_path)

    @abstractmethod
    def read_labelmap_file(self, labelmap_path):
        """Reads the labels file and returns a mapping from ID to class name.

        Parameters
        ----------
        labelmap_path: str
            The filename where the class names are written.

        Returns
        -------
        dict
            A map from a label (integer) to class name.
        """
        if self.is_detection_dataset():
            raise Exception(
                f"This method has been removed due to deprecation. "
                f"If you need to use this type of functionality, browse "
                f"for the implementation in the repository history, and include again the "
                f"conabio_ml_vision/trainer/models module that was removed.")
        else:
            return super().read_labelmap_file(labelmap_path)

    @abstractmethod
    def _generate_labelmap_from_list(self, labelmap_list):
        """Generates the labelmap from a list of class names, enumerating the elements that the
        list contains.
        If the dataset is of type object detection, the indexes will be 1-based,
        otherwise will be 0-based.

        Parameters
        ----------
        labelmap_list: list of str
            List that contains the class names of the labelmap

        Returns
        -------
        dict
            Dictionary of the form {index: class_name} with the elements of the labelmap
        """
        if self.is_detection_dataset():
            return {i+1: lbl for i, lbl in enumerate(labelmap_list)}
        return {i: lbl for i, lbl in enumerate(labelmap_list)}

    @abstractmethod
    def _generate_labelmap_from_categories(self):
        """Generates the labelmap from the different categories that the dataset contains.
        If the dataset does not contain categories, `None` will be returned.
        If the dataset is of type object detection, the indexes will be 1-based,
        otherwise will be 0-based.

        Returns
        -------
        dict
            Dictionary of the form {index: class_name} with the elements of the labelmap
        """
        cat_names = self.get_categories()
        if cat_names is not None:
            if self.is_detection_dataset():
                return dict(zip(range(1, len(cat_names)+1), cat_names))
            return dict(zip(range(len(cat_names)), cat_names))
        return None


class COCOImageDataset(ImageDataset):
    """Represent a ImageDataset specification for COCO Camera Traps databases.
    """

    class MEDIA_FIELDS():
        ID = "id"
        FILENAME = "file_name"
        WIDTH = "width"
        HEIGHT = "height"
        DATE_CAPTURED = "date_captured"
        LOCATION = "location"

        SEQ_ID = "seq_id"
        SEQ_NUM_FRAMES = "seq_num_frames"
        FRAME_NUM = "frame_num"
        SEQ_FRAME_NUM = "seq_frame_num"
        VID_FRAME_NUM = "video_frame_num"
        VID_NUM_FRAMES = "video_num_frames"
        VID_ID = "video_id"

        NAMES = [ID, FILENAME, WIDTH, HEIGHT, DATE_CAPTURED,
                 LOCATION, SEQ_ID, SEQ_NUM_FRAMES, FRAME_NUM,
                 SEQ_FRAME_NUM, VID_FRAME_NUM, VID_NUM_FRAMES, VID_ID]

        TYPES = {
            ID: str,
            LOCATION: str,
            VID_ID: str,
            WIDTH: float,
            HEIGHT: float
        }


class ImagePredictionDataset(ImageDataset, PredictionDataset):
    """Represent a ImageDataset specification for predictions."""

    class MEDIA_FIELDS():
        ID = "id"
        FILENAME = "file_name"
        WIDTH = "width"
        HEIGHT = "height"
        DATE_CAPTURED = "date_captured"
        LOCATION = "location"

        SEQ_ID = "seq_id"
        SEQ_NUM_FRAMES = "seq_num_frames"
        FRAME_NUM = "frame_num"
        SEQ_FRAME_NUM = "seq_frame_num"
        VID_FRAME_NUM = "video_frame_num"
        VID_NUM_FRAMES = "video_num_frames"
        VID_ID = "video_id"

        NAMES = [ID, FILENAME, WIDTH, HEIGHT, DATE_CAPTURED, LOCATION, SEQ_ID, SEQ_NUM_FRAMES,
                 FRAME_NUM, SEQ_FRAME_NUM, VID_FRAME_NUM, VID_NUM_FRAMES, VID_ID]

        TYPES = {
            ID: str,
            LOCATION: str,
            VID_ID: str,
            WIDTH: float,
            HEIGHT: float
        }

    class ANNOTATIONS_FIELDS():
        ITEM = "item"
        LABEL = "label"
        BBOX = "bbox"
        ID = "id"
        SCORE = "score"
        MEDIA_ID = "image_id"
        PARTITION = "partition"

        NAMES = [ITEM, LABEL, BBOX, ID, SCORE,
                 MEDIA_ID, PARTITION
                 ]

        TYPES = {
            ID: str,
            MEDIA_ID: str
        }

    def _callback_dataset(self, **kwargs):
        """Method that is called each time a dataset instance is created and that initializes the
        required variables and properties of the dataset.

        Parameters
        ----------
        **kwargs
            Extra named arguments passed to initialization methods and may contains the
            following parameters:
            * round_score_digits: int
                Number of digits of which the field `score` will be rounded
        """
        round_score_digits = kwargs.get("round_score_digits", None)
        min_score = kwargs.get("min_score", None)
        max_score = kwargs.get("max_score", None)

        super()._callback_dataset(**kwargs)

        if min_score is not None or max_score is not None:
            self.filter_by_score(min_score=min_score, max_score=max_score, inplace=True)

        if round_score_digits is not None:
            self.set_anns_info_field_by_expr(
                'score', lambda x: round(x.score, round_score_digits), inplace=True)

    """
    FROM METHODS
    """

    @classmethod
    def from_csv(cls, source_path: str, **kwargs) -> ImagePredictionDataset:
        """Create an `ImagePredictionDataset` from a csv file.

        Parameters
        ----------
        source_path : str
            Path of a csv file or a folder containing csv files that will be
            converted into a `Dataset`.
            The csv file(s) must have at least two columns, which represents the `item` data.
        columns : list of str, list of int, or None, optional
            List of column names, or list of column indexes, to load from the
            csv file(s). If None, load all columns. (default is None)
        header : bool, optional
            Whether or not csv contains header at row 0 (default is True).
            If False, you need to map at least `item` and `label` columns by
            position in column_mapping parameter.
        recursive : bool
            Whether or not to seek files also in subdirectories
            (default is True)
        column_mapping : dict, optional
            Dictionary to map column names in dataset.
            If header=True mapping must be done by the column names,
            otherwise must be done by column positions.
            E.g.::
                header=True: column_mapping={"c1": "item", "c2": "label"}
                header=False: column_mapping={0: "item", 1: "label"}
            (default is None)
        include_id : bool, optional
            Wheter or not to include an id for each register (default is False)
        info : dict, optional
            Information of the csv file (default is {})
        **kwargs
            Extra named arguments passed to the `from_csv` method of `Dataset`

        Returns
        -------
        ImagePredictionDataset
            Instance of the created Dataset
        """
        kwargs['split_by_column'] = None
        kwargs['split_by_filenames'] = False
        return super().from_csv(source_path=source_path, **kwargs)

    @classmethod
    def from_json(cls,
                  source_path: str,
                  **kwargs) -> ImageDataset:
        """Create an ImagePredictionDataset from a JSON file that contains predictions from an
        object detection model and are in the following format:

        ```
        {
            'images':[image],
            'detection_categories': {id_cat_str: cat_name, ...},
            'info': info
        }

        image{
            'id': str,
            'max_detection_conf': float,
            'detections':[detection]
        }

        detection{
            'bbox' : [x, y, width, height],
            'category': str,
            'conf': float
        }
        ```

        Parameters
        ----------
        source_path : str
            Path of a json file that contains the detections.
        **kwargs :
            Extra named arguments that may contains the following parameters:
            * categories : list of str, str or None
                List, string or path of a CSV or a text file with the categories to be included in
                the dataset. If None, registers of all categories will be included.
                If path to a CSV file, it should have the categories in the column `0` and should
                not have a header. If path to a text file, it must have the categories separated by
                a line break. If string, it must contain the categories separated by commas.
                If empty list, labeled images will not be included. (default is None)
            * exclude_categories : list of str, str or None
                List, string or path of a CSV file or a text file with categories to be excluded.
                If it is a path of a CSV file, it should have the categories in the column `0`
                and should not have a header. If it is a path of a text file, it must have the
                categories separated by a line break. If it is a string, it must contain the
                categories separated by commas. (default is None)
            * clean_cat_names : bool
                Whether to clean or not the category names, converting to lower case and removing
                spaces at the beginning and at the end. By default True
            * score_threshold : float
                Threshold for which detections will be considered or ignored, by default 0.3
            * set_filename_with_id_and_extension : str
                Extension to be added to the id of each item to form the file name
                (default is None)
            * images_dir : str
                The folder path where the images are already stored.
                (default is None)

        Returns
        -------
        Dataset
            Instance of the created Dataset
        """
        assert os.path.isfile(source_path), f"{source_path} is not a valid file"

        validate_filenames = kwargs.get('validate_filenames', True)
        score_threshold = kwargs.get("score_threshold", 0.01)
        file_w_ext = kwargs.get("set_filename_with_id_and_extension", None)
        clean_cats = kwargs.get("clean_cat_names", True)
        categories = utils.get_cats_from_source(kwargs.get("categories", None), clean_cats)
        excld_cats = utils.get_cats_from_source(kwargs.get("exclude_categories", None), clean_cats)
        convert_to_abs_coords = kwargs.get('convert_to_abs_coords', False)

        results = defaultdict(list)
        detections_data = json.load(open(source_path))
        labelmap = detections_data['detection_categories']
        images_dets = detections_data.get('images')
        assert images_dets is not None, f"File {source_path} does not contain images information"

        for image_dets in images_dets:
            detections = image_dets.get('detections', [])
            for detection in detections:
                if detection['conf'] < score_threshold:
                    continue
                if 'file' in image_dets:
                    item = image_dets['file']
                else:
                    item = f"{image_dets['id']}.{file_w_ext}" if file_w_ext else image_dets['id']
                bbox = ",".join([str(x) for x in detection['bbox']])
                results["item"].append(item)
                results["label"].append(labelmap[detection['category']])
                results["bbox"].append(bbox)
                results["score"].append(detection['conf'])

        data = pd.DataFrame(results)

        data["id"] = data['item'].apply(lambda _: str(uuid.uuid4()))
        # Remove from images_info elements that doesn't appear in data
        if kwargs.get('images_info') is not None:
            images_info = kwargs['images_info']
            images_info = images_info[images_info.index.isin(data['item'].values)]
        else:
            images_info = pd.DataFrame({'item': data['item'].unique()})
            images_info[cls.MEDIA_FIELDS.ID] = images_info['item']
            images_info = images_info.set_index(cls.FIELD_TO_JOIN_IMGS_ANNS)
        kwargs['images_info'] = images_info

        # Filter categories
        if clean_cats:
            data['label'] = data['label'].apply(get_cleaned_label)
        if categories is not None:
            data = data[data['label'].isin(categories)].reset_index(drop=True)
        if excld_cats is not None:
            data = data[~data['label'].isin(excld_cats)].reset_index(drop=True)

        kwargs['info'] = detections_data['info']
        prediction_dataset = cls(data, **kwargs)

        # TODO: refactor this
        if convert_to_abs_coords:
            prediction_dataset._convert_coordinates(
                input_format=coords_utils.COORDINATES_FORMATS.X_Y_WIDTH_HEIGHT,
                output_format=coords_utils.COORDINATES_FORMATS.X_Y_WIDTH_HEIGHT,
                output_coords_type=coords_utils.COORDINATES_TYPES.ABSOLUTE)
        elif validate_filenames:
            prediction_dataset.get_images_sizes()

        return prediction_dataset

    """
    TO METHODS
    """

    def to_folder(self: ImageDataset,
                  dest_path: str,
                  split_in_labels: bool = True,
                  keep_originals: bool = True,
                  set_images_dir: bool = False,
                  **kwargs) -> str:
        """Create a filesystem representation of the dataset.

        Parameters
        ----------
        dest_path : str
            Path where the dataset files will be stored
        split_in_partitions : bool, optional
            Whether or not to split items in folders with partition names.
            Only works if the dataset has been partitioned previously.
            (default is True)
        split_in_labels : bool, optional
            Whether or not to split items in folders with label names.
            These folders will be below the partition folders (default is True)
        keep_originals : bool, optional
            Whether to keep original files or delete them (default is True)
        set_images_dir : bool, optional
            Whether to set `images_dir` to `dest_path` at the end of the processing or not
            (default is False)
        **kwargs
            Extra named arguments passed to `to_folder` method of `Dataset`

        Returns
        -------
        str
            Path of folder created
        """
        return super().to_folder(dest_path=dest_path,
                                 split_in_labels=split_in_labels,
                                 keep_originals=keep_originals,
                                 set_images_dir=set_images_dir,
                                 **kwargs)

    def to_json(self,
                dest_path,
                out_coordinates_type=coords_utils.COORDINATES_TYPES.RELATIVE,
                score_threshold=0.3,
                form_id_with_filename_without_base_dir=False,
                add_detection_id=False,
                images_sizes=None):
        """Creates a JSON representation of the detections contained in the dataset.
        The dataset must have the column 'image_id'.
        Detections are provided in the following format:
        ```
        {
            'images':[image],
            'detection_categories': {'1': 'animal', '2': 'person', 'vehicle'},
            'info': info
        }

        image{
            'id': str,
            'max_detection_conf': float,
            'detections':[detection]
        }

        detection{
            'bbox' : [x, y, width, height],
            'category': str,
            'conf': float,
            'id': str, optional
        }
        ```

        Parameters
        ----------
        dest_path : str or None
            Path of the resulting JSON file. If it is a directory path, file will be named
            `detections.json`. In case it is None, the file information will only be returned and
            will not be saved
        out_coordinates_type : `COORDINATES_TYPES`, optional
            Determines if the value of the output coordinates in 'bbox' will be given in absolute
            pixel values, or normalized in relation to the image size and therefore in the range
            [0,1]. By default COORDINATES_TYPES.RELATIVE
        score_threshold : float, optional
            Threshold for which detections will be considered or ignored, by default 0.3
        form_id_with_filename_without_base_dir : bool, optional
            Whether or not the 'id' of each element will be formed with the path of each item,
            removing the base directory where the images were stored (generally, the `image_dir` or
            the `dest_path` parameters of some dataset constructors).
            If False, the 'id' will be the 'image_id' field of each item.
            By default False
        add_detection_id : bool, optional
            Whether to include the `id` in the `detections` field or not, by default False
        images_sizes : dict, optional
            Dictionary containing 'width' and 'height' of each image, by default None

        Returns
        -------
        dict
            Dictionary with the resulting information
        """
        df = self.as_dataframe(columns=["item", "label", "bbox", "image_id", "score"])
        coords_type = self._get_coordinates_type()
        if images_sizes is None and out_coordinates_type != coords_type:
            images_sizes = self.get_images_sizes(as_dict=True)
        form_id_with_filename_without_prefix = \
            self.get_images_dir() if form_id_with_filename_without_base_dir else None
        results = utils.get_list_of_detections(df,
                                               self._get_inverse_labelmap(),
                                               out_coordinates_type,
                                               score_threshold,
                                               form_id_with_filename_without_prefix,
                                               images_sizes,
                                               add_detection_id)
        final_output = {
            'images': results,
            'detection_categories': {str(k): v for k, v in self.get_labelmap().items()},
            'info': {
                'detection_completion_time': datetime.utcnow().strftime(STD_DATEFORMAT),
                'format_version': '1.0'
            }
        }
        if dest_path is not None:
            if os.path.isdir(dest_path):
                dest_path = os.path.join(dest_path, "detections.json")
            with open(dest_path, 'w') as f:
                json.dump(final_output, f, indent=1)
        return final_output

    """
    FILTER METHODS
    """

    def filter_by_score(self,
                        max_score=None,
                        min_score=None,
                        score=None,
                        column_name="score",
                        inplace=True):
        """Method that filters the predictions by the `score` column

        Parameters
        ----------
        max_score : float, optional
            Float number in [0.,1.] that indicates the maximum value of the `score` column,
            by default None
        min_score : float, optional
            Float number in [0.,1.] that indicates the minimum value of the `score` column,
            by default None
        score : float, optional
            Float number in [0.,1.] that indicates the exact value of the `score` column,
            by default None
        column_name : str, optional
            Column name of the score field in `self.data`, by default "score"
        inplace : bool, optional
            If True, perform operation in-place, by default True

        Returns
        -------
        ImagePredictionDataset or None
            Instance of the resulting dataset or None if `inplace=True`
        """
        instance = self if inplace else self.copy()
        if max_score is not None:
            instance.data = instance.data.loc[instance.data[column_name] <= float(max_score)]
        if min_score is not None:
            instance.data = instance.data.loc[instance.data[column_name] >= float(min_score)]
        if score is not None:
            instance.data = instance.data.loc[instance.data[column_name] == float(score)]
        instance.data = instance.data.reset_index(drop=True)
        instance._update_dataset_info()
        if not inplace:
            return instance

    def only_first_prediction(self):
        """Filters out all predictions in the dataset except the one with the highest score

        Returns
        -------
        ImagePredictionDataset
            Instance of the Dataset
        """
        self.data.sort_values(by='score', axis=0, ascending=False, inplace=True)
        self.data.drop_duplicates(subset='item', keep='first', inplace=True)
        self.data = self.data.reset_index(drop=True)
        self._update_dataset_info()

        return self


class ImagesJsonHandler():
    """ JSON File Handler.
    """

    def __init__(self, json_path, cat_mappings=None, clean_cat_names=True):
        """Constructor of Conabio JSON handler class for reading annotations.

        Parameters
        ----------
        json_path : str
            Location of annotation file
        cat_mappings : dict, optional
            Dictionary containing category mappings, by default an empty dictionary
        clean_cat_names : bool, optional
            Whether to clean or not the category names, converting to lower case and removing
            spaces at the beginning and at the end. By default True
        """
        cat_mappings = {} if cat_mappings is None else cat_mappings
        dataset = json.load(open(json_path, 'r'))
        self.cats = {}
        self.catsIds = []
        self.catsNames = []
        self.imgs = {}
        self.imgToAnns = defaultdict(list)
        self.dataset = dataset
        self.clean_cat_names = clean_cat_names
        self.cat_mappings = cat_mappings or {}
        self.createIndex()

    def createIndex(self):
        """Create index
        """
        for ann in self.dataset.get('annotations', []):
            # Convert 'image_id' to str
            self.imgToAnns[str(ann['image_id'])].append({**ann, 'image_id': str(ann['image_id'])})
        for image in self.dataset.get('images', []):
            # Convert 'id' of images to str
            self.imgs[str(image['id'])] = {**image, 'id': str(image['id'])}
        for cat in self.dataset.get('categories', []):
            catNm = cat['name']
            if catNm in self.cat_mappings:
                cat['name'] = self.cat_mappings[catNm]
            elif '*' in self.cat_mappings:
                cat['name'] = self.cat_mappings['*']
            else:
                cat['name'] = catNm
            self.cats[cat['id']] = cat
        self.catsIds = list(self.cats.keys())
        self.catsNames = [cat["name"] for cat in self.cats.values()]

    def getCatIds(self, catNms=[]):
        """Get category ids from category names.

        Parameters
        ----------
        catNms : list of str or None, optional
            List of category names to get the ids (default is [])

        Returns
        -------
        list of int
            List of category ids.
        """
        if catNms is None:
            return None
        else:
            catNms = [cat_nm for cat_nm in catNms]
            return [cat_id for cat_id, cat in self.cats.items() if cat['name'] in catNms]

    def loadImgs(self, ids=[]):
        """Load images with the specified ids.

        Parameters
        ----------
        ids : str or list of str, optional
            Image id or list of image ids to filter (default is [])

        Returns
        -------
        list of dict or dict
            List of image dicts, if `ids` is an array-like object, a single image dict otherwise
        """
        if is_array_like(ids):
            return [self.imgs[id] for id in ids]
        else:
            return self.imgs[ids]


class LILADataset(ImageDataset):

    DEFAULTS = {
        'lila-taxonomy-mapping-url': 'https://lila.science/wp-content/uploads/2022/07/lila-taxonomy-mapping_release.csv',
        'azcopy-download-linux': 'https://aka.ms/downloadazcopy-v10-linux'
    }

    class MEDIA_FIELDS(ImageDataset.MEDIA_FIELDS):
        COLLECTION = 'collection'

        NAMES = [COLLECTION] + ImageDataset.MEDIA_FIELDS.NAMES

    class ANNOTATIONS_FIELDS(ImageDataset.ANNOTATIONS_FIELDS):
        KINGDOM = 'kingdom'
        PHYLUM = 'phylum'
        CLASS = 'class'
        ORDER = 'order'
        FAMILY = 'family'
        GENUS = 'genus'
        SPECIES = 'species'
        SUBSPECIES = 'subspecies'
        VARIETY = 'variety'

        TAXA_LEVEL = 'taxonomy_level'
        SCIENTIFIC_NAME = 'scientific_name'

        TAXA_RANKS_NAMES = [
            KINGDOM, PHYLUM, CLASS, ORDER, FAMILY, GENUS, SPECIES, SUBSPECIES, VARIETY]
        TAXA_FIELD_NAMES = TAXA_RANKS_NAMES + [TAXA_LEVEL, SCIENTIFIC_NAME]

        NAMES = TAXA_FIELD_NAMES + ImageDataset.ANNOTATIONS_FIELDS.NAMES

    class Collections():
        IDAHO = 'Idaho Camera Traps'
        SNAPSHOT_CAMDEBOO = 'Snapshot Camdeboo'
        SNAPSHOT_ENONKISHU = 'Snapshot Enonkishu'
        SNAPSHOT_KAROO = 'Snapshot Karoo'
        SNAPSHOT_KGALAGADI = 'Snapshot Kgalagadi'
        SNAPSHOT_KRUGER = 'Snapshot Kruger'
        SNAPSHOT_MOUNTAIN_ZEBRA = 'Snapshot Mountain Zebra'
        ORINOQUIA = 'Orinoquia Camera Traps'
        CALTECH = 'Caltech Camera Traps'
        CALTECH_BBOX = 'Caltech Camera Traps_bbox'
        WELLINGTON = 'Wellington Camera Traps'
        MISSOURI = 'Missouri Camera Traps'
        NACTI = 'NACTI'
        NACTI_BBOX = 'NACTI_bbox'
        ENA24 = 'ENA24'
        SWG = 'SWG Camera Traps'
        SWG_BBOX = 'SWG Camera Traps_bbox'
        SNAPSHOT_SERENGETI = 'Snapshot Serengeti'
        SNAPSHOT_SERENGETI_BBOX = 'Snapshot Serengeti_bbox'
        WCS = 'WCS Camera Traps'
        WCS_BBOX = 'WCS Camera Traps_bbox'
        ISLAND_CONSERVATION = 'Island Conservation Camera Traps'
        CHANNEL_ISLANDS = 'Channel Islands Camera Traps'

        NAMES = [IDAHO, SNAPSHOT_CAMDEBOO, SNAPSHOT_ENONKISHU, SNAPSHOT_KAROO, SNAPSHOT_KGALAGADI,
                 SNAPSHOT_KRUGER, SNAPSHOT_MOUNTAIN_ZEBRA, ORINOQUIA, CALTECH, CALTECH_BBOX,
                 WELLINGTON, MISSOURI, NACTI, NACTI_BBOX, ENA24, SWG, SWG_BBOX, SNAPSHOT_SERENGETI,
                 SNAPSHOT_SERENGETI_BBOX, WCS, WCS_BBOX, ISLAND_CONSERVATION, CHANNEL_ISLANDS]

    mapping_images_fields = {
        Collections.IDAHO: {'datetime': 'date_captured', 'frame_num': 'seq_frame_num'},
        Collections.SNAPSHOT_CAMDEBOO: {'datetime': 'date_captured', 'frame_num': 'seq_frame_num'},
        Collections.SNAPSHOT_ENONKISHU: {'datetime': 'date_captured', 'frame_num': 'seq_frame_num'},
        Collections.SNAPSHOT_KAROO: {'datetime': 'date_captured', 'frame_num': 'seq_frame_num'},
        Collections.SNAPSHOT_KGALAGADI: {'datetime': 'date_captured', 'frame_num': 'seq_frame_num'},
        Collections.SNAPSHOT_KRUGER: {'datetime': 'date_captured', 'frame_num': 'seq_frame_num'},
        Collections.SNAPSHOT_MOUNTAIN_ZEBRA: {'datetime': 'date_captured', 'frame_num': 'seq_frame_num'},
        Collections.ORINOQUIA: {'datetime': 'date_captured', 'frame_num': 'seq_frame_num'},
        Collections.CALTECH: {'frame_num': 'seq_frame_num'},
        Collections.CALTECH_BBOX: {'frame_num': 'seq_frame_num'},
        Collections.WELLINGTON: {'datetime': 'date_captured', 'frame_num': 'seq_frame_num', 'site': 'location'},
        Collections.MISSOURI: {'frame_num': 'seq_frame_num'},
        Collections.NACTI: {},
        Collections.NACTI_BBOX: {},
        Collections.ENA24: {},
        Collections.SWG: {'datetime': 'date_captured', 'frame_num': 'seq_frame_num'},
        Collections.SWG_BBOX: {'datetime': 'date_captured', 'frame_num': 'seq_frame_num'},
        Collections.SNAPSHOT_SERENGETI: {'datetime': 'date_captured', 'frame_num': 'seq_frame_num'},
        Collections.SNAPSHOT_SERENGETI_BBOX: {'datetime': 'date_captured', 'frame_num': 'seq_frame_num'},
        Collections.WCS: {'datetime': 'date_captured', 'frame_num': 'seq_frame_num'},
        Collections.WCS_BBOX: {'datetime': 'date_captured', 'frame_num': 'seq_frame_num'},
        Collections.ISLAND_CONSERVATION: {},
        Collections.CHANNEL_ISLANDS: {'frame_num': 'seq_frame_num'}
    }

    mapping_image_id_rules = {
        Collections.IDAHO: lambda x: f"public/{x['file_name']}",
        Collections.SNAPSHOT_CAMDEBOO: lambda x: f"CDB_public/{x['file_name']}",
        Collections.SNAPSHOT_ENONKISHU: lambda x: f"ENO_public/{x['file_name']}",
        Collections.SNAPSHOT_KAROO: lambda x: f"KAR_public/{x['file_name']}",
        Collections.SNAPSHOT_KGALAGADI: lambda x: f"KGA_public/{x['file_name']}",
        Collections.SNAPSHOT_KRUGER: lambda x: f"KRU_public/{x['file_name']}",
        Collections.SNAPSHOT_MOUNTAIN_ZEBRA: lambda x: f"MTZ_public/{x['file_name']}",
        Collections.ORINOQUIA: lambda x: x['file_name'],
        Collections.CALTECH: lambda x: f"cct_images/{x['file_name']}",
        Collections.CALTECH_BBOX: lambda x: f"cct_images/{x['file_name']}",
        Collections.WELLINGTON: lambda x: x['file_name'],
        Collections.MISSOURI: lambda x: "images/" + x['file_name'].replace('\\', '/'),
        Collections.NACTI: lambda x: x['file_name'],
        Collections.NACTI_BBOX: lambda x: x['file_name'],
        Collections.ENA24: lambda x: f"images/{x['file_name']}",
        Collections.SWG: lambda x: f"{x['file_name'].split('public/')[1]}" if x['file_name'].startswith('public/') else f"{x['file_name'].split('private/')[1]}",
        Collections.SWG_BBOX: lambda x: f"{x['file_name'].split('public/')[1]}" if x['file_name'].startswith('public/') else f"{x['file_name'].split('private/')[1]}",
        Collections.SNAPSHOT_SERENGETI: lambda x: '/'.join(x['file_name'].split('/')[1:]),
        Collections.SNAPSHOT_SERENGETI_BBOX: lambda x: '/'.join(x['file_name'].split('/')[1:]),
        Collections.WCS: lambda x: f"{x['file_name'].split('/', maxsplit=1)[1]}",
        Collections.WCS_BBOX: lambda x: f"{x['file_name'].split('animals/')[1]}",
        Collections.ISLAND_CONSERVATION: lambda x: f"public/{x['file_name']}",
        Collections.CHANNEL_ISLANDS: lambda x: f"images/{x['file_name']}"
    }

    mapping_downloaded_imgs_azcopy = {
        Collections.IDAHO: lambda x: f"public/{x['item']}",
        Collections.SNAPSHOT_CAMDEBOO: lambda x: f"CDB_public/{x['item']}",
        Collections.SNAPSHOT_ENONKISHU: lambda x: f"ENO_public/{x['item']}",
        Collections.SNAPSHOT_KAROO: lambda x: f"KAR_public/{x['item']}",
        Collections.SNAPSHOT_KGALAGADI: lambda x: f"KGA_public/{x['item']}",
        Collections.SNAPSHOT_KRUGER: lambda x: f"KRU_public/{x['item']}",
        Collections.SNAPSHOT_MOUNTAIN_ZEBRA: lambda x: f"MTZ_public/{x['item']}",
        Collections.ORINOQUIA: lambda x: f"public/{x['item']}",
        Collections.CALTECH: lambda x: f"cct_images/{x['item']}",
        Collections.CALTECH_BBOX: lambda x: f"cct_images/{x['item']}",
        Collections.WELLINGTON: lambda x: f"images/{x['item']}",
        Collections.MISSOURI: lambda x: f"images/{x['item']}",
        Collections.NACTI: lambda x: f"nacti-unzipped/{x['item']}",
        Collections.NACTI_BBOX: lambda x: f"nacti-unzipped/{x['item']}",  # duda
        Collections.ENA24: lambda x: f"images/{x['item']}",
        Collections.SWG: lambda x: f"swg-camera-traps/{x['item']}",
        Collections.SWG_BBOX: lambda x: f"swg-camera-traps/{x['item']}",
        Collections.SNAPSHOT_SERENGETI: lambda x: f"snapshotserengeti-unzipped/{x['item']}",
        Collections.SNAPSHOT_SERENGETI_BBOX: lambda x: f"snapshotserengeti-unzipped/{x['item']}",
        Collections.WCS: lambda x: f"wcs-unzipped/{x['item']}",
        Collections.WCS_BBOX: lambda x: f"wcs-unzipped/{x['item']}",  # duda
        Collections.ISLAND_CONSERVATION: lambda x: f"public/{x['item']}",
        Collections.CHANNEL_ISLANDS: lambda x: f"images/{x['item']}"
    }

    @classmethod
    def _download(cls,
                  dest_path,
                  media_info,
                  **kwargs):
        azcopy_exec = kwargs.get('azcopy_exec')
        batch_size = kwargs.get('azcopy_batch_size', 5000)
        set_filename_with_id_and_ext = kwargs.get('set_filename_with_id_and_ext')
        separate_in_dirs_per_collection = kwargs.get('separate_in_dirs_per_collection', True)
        metadata_dir = kwargs.get('metadata_dir')
        use_azcopy_for_download = kwargs.get('use_azcopy_for_download', True)

        metadata_dir = metadata_dir or os.path.join(get_temp_folder(), 'metadata_dir')
        metadata_table = lila_utils.read_lila_metadata(metadata_dir)

        os.makedirs(dest_path, exist_ok=True)

        logger.info(f"Downloading {len(media_info)} images...")

        azcopy_url = os.environ.get('AZCOPY_URL', cls.DEFAULTS['azcopy-download-linux'])
        lila_utils.get_azcopy_exec(azcopy_exec, azcopy_url=azcopy_url)

        files_azcopy_dir = os.path.join(dest_path, '__files_azcopy__')
        collections = media_info[cls.MEDIA_FIELDS.COLLECTION].unique()

        for collection in collections:
            coll_df = media_info[media_info[cls.MEDIA_FIELDS.COLLECTION] == collection]
            filenames = coll_df.index.values
            aux_files_folder = os.path.join(files_azcopy_dir, collection)
            if separate_in_dirs_per_collection:
                output_imgs_dir = os.path.join(dest_path, collection)
            else:
                output_imgs_dir = dest_path
            sas_url = metadata_table[collection]['sas_url']

            if use_azcopy_for_download:
                download_with_azcopy(filenames=filenames,
                                     azcopy_exec=azcopy_exec,
                                     batch_size=batch_size,
                                     aux_files_folder=aux_files_folder,
                                     output_imgs_dir=output_imgs_dir,
                                     sas_url=sas_url)
            else:
                def get_dest_filename(fname):
                    filename = cls.mapping_downloaded_imgs_azcopy[collection]({'item': fname})
                    return os.path.join(output_imgs_dir, filename)
                parallel_exec(
                    func=download_file,
                    elements=filenames,
                    url=lambda fname: f'{sas_url}/{fname}',
                    dest_filename=get_dest_filename,
                    verbose=False)


    def download(self, dest_path, **kwargs):
        num_tasks = kwargs.get('num_tasks')
        task_num = kwargs.get('task_num')

        if num_tasks is not None and task_num is not None:
            split_dataset = self.get_chunk(num_tasks, task_num)
            media_info = split_dataset.get_images_info()
            split_dataset._download(dest_path, media_info=media_info, **kwargs)
        else:
            media_info = self.get_images_info()
            self._download(dest_path, media_info=media_info, **kwargs)

    def set_items_after_downloading(self, separated_in_dirs_per_collection=True):
        self.set_anns_info_field_by_expr(
            'item',
            lambda row: self.mapping_downloaded_imgs_azcopy[row[self.MEDIA_FIELDS.COLLECTION]](
                row),
            inplace=True)
        if separated_in_dirs_per_collection:
            self.set_anns_info_field_by_expr(
                'item',
                lambda row: os.path.join(row[self.MEDIA_FIELDS.COLLECTION], row['item']),
                inplace=True)

    @classmethod
    def from_json(cls,
                  source_path: str = None,
                  **kwargs) -> LILADataset:
        """Create a LILADataset from a JSON file in COCO format.
        This method allows you to create a dataset by simply providing the name of a LILA
        collection or the local `source_path` of the JSON.

        Parameters
        ----------
        source_path : str, optional
            Path of a json file that will be converted into a LILADataset.
            If None, it will be downloaded by using the `collection` name.
            By default None
        **kwargs :
            Extra named arguments that may contains the following parameters:
            * collection : str
                A valid collection name of a LILA dataset
            * map_to_scientific_names : bool
                Whether to map or not the common names of species given by LILA to scientific
                names. The mapping will be performed using the taxonomy mapping released by LILA,
                and the URL used to download that CSV file could be set in the env variable
                `LILA_TAXONOMY_MAPPING_URL`.
                By default True
            * exclude_invalid_scientific_names : bool
                Whether to delete or not the elements of the dataset with no valid scientific names
                (e.g. vehicle, empty, unknown, etc).
                By default True
            * mapping_classes_csv : str
                The path to the CSV taxonomy mapping released by LILA, in case you've aready
                downloaded it or if you want to store it in this specific path

        Returns
        -------
        LILADataset
            The instance of the created dataset
        """
        collection = kwargs.get('collection')
        map_to_scientific_names = kwargs.get('map_to_scientific_names', True)
        exclude_invalid_scinames = kwargs.get('exclude_invalid_scientific_names', True)
        map_image_id_field = kwargs.get('map_image_id_field', True)
        taxonomy_level = kwargs.get('taxonomy_level', None)

        if source_path is None:
            assert collection is not None, "You must specify the collection name"
            all_json_files_dict = lila_utils.get_all_json_files(
                get_temp_folder(), collections=[collection])
            source_path = all_json_files_dict[collection]

        if collection is not None and 'mapping_images_fields' not in kwargs:
            assert_msg = f'Invalida collection name: {collection}'
            assert collection in LILADataset.Collections.NAMES, assert_msg
            kwargs['mapping_images_fields'] = LILADataset.mapping_images_fields[collection]

        if map_to_scientific_names:
            mapping_classes_csv = kwargs.get('mapping_classes_csv')
            if mapping_classes_csv is None or not os.path.isfile(mapping_classes_csv):
                mapping_url = os.environ.get(
                    'LILA_TAXONOMY_MAPPING_URL', LILADataset.DEFAULTS['lila-taxonomy-mapping-url'])
                file_path = download_file(mapping_url, get_temp_folder())
                if mapping_classes_csv is None:
                    mapping_classes_csv = file_path
                else:
                    move(file_path, mapping_classes_csv)
            coll_name = (
                collection.split('_bbox')[0] if collection.endswith('_bbox') else collection)
            kwargs['mapping_classes'] = mapping_classes_csv
            kwargs['mapping_classes_from_col'] = 'query'
            kwargs['mapping_classes_to_col'] = 'scientific_name'
            kwargs['mapping_classes_filter_expr'] = lambda row: row['dataset_name'] == coll_name

        instance = super().from_json(source_path=source_path, **kwargs)

        if map_to_scientific_names and exclude_invalid_scinames:
            instance.filter_by_categories('', mode='exclude', inplace=True)

        if map_image_id_field:
            if collection in LILADataset.mapping_image_id_rules:
                mapping_img_id_rule = LILADataset.mapping_image_id_rules[collection]
                instance.set_images_info_field_by_expr(
                    cls.MEDIA_FIELDS.ID, mapping_img_id_rule, inplace=True)

        instance.set_images_info_field_by_expr(
            LILADataset.MEDIA_FIELDS.COLLECTION, lambda _: collection, inplace=True)

        if map_to_scientific_names:
            taxa_fields = cls.ANNOTATIONS_FIELDS.TAXA_FIELD_NAMES
            _coll = collection.replace('_bbox', '') if collection.endswith('_bbox') else collection

            tax = pd.read_csv(mapping_classes_csv).fillna('')
            taxa = (
                tax[(~tax['scientific_name'].isna()) & (tax['dataset_name'] == _coll)][taxa_fields]
                .drop_duplicates(subset='scientific_name')
            )
            _data = instance._get_data()
            _data = pd.merge(left=_data, right=taxa, left_on='label', right_on='scientific_name')
            _data = _data.drop(['scientific_name'], axis=1)
            instance.data = _data

        if taxonomy_level is not None:
            instance.mapping_to_taxonomy_level(taxonomy_level)

        return instance

    def mapping_to_taxonomy_level(self, taxonomy_level_to):
        assert_cond = taxonomy_level_to in self.ANNOTATIONS_FIELDS.TAXA_RANKS_NAMES
        assert assert_cond, "Invalid taxonomy_level_to"

        self.filter_by_column(taxonomy_level_to, '', mode='exclude', inplace=True)
        self.set_anns_info_field_by_expr('label', lambda row: row[taxonomy_level_to], inplace=True)

    @classmethod
    def from_csv(cls, source_path: str, **kwargs) -> LILADataset:
        taxonomy_level = kwargs.get('taxonomy_level', None)
        taxons = kwargs.get('taxons', None)
        set_items_after_downloading = kwargs.get('set_items_after_downloading', True)

        images_dir_prov = None
        if set_items_after_downloading and ('images_dir' in kwargs or 'media_dir' in kwargs):
            images_dir_prov = kwargs.get('images_dir') or kwargs.get('media_dir')
            if 'images_dir' in kwargs:
                del kwargs['images_dir']
            if 'media_dir' in kwargs:
                del kwargs['media_dir']

        instance = super().from_csv(source_path=source_path, **kwargs)

        if taxonomy_level is not None:
            instance.mapping_to_taxonomy_level(taxonomy_level)

        if taxons is not None:
            instance.filter_by_categories(taxons, inplace=True)

        if images_dir_prov is not None:
            instance.set_items_after_downloading(separated_in_dirs_per_collection=True)
            instance.set_images_dir(images_dir_prov, not_exist_ok=True)

        return instance
