import argparse
import os
import pandas as pd
from typing import List, Union
import shutil

from conabio_ml.utils.logger import get_logger
from conabio_ml.utils.utils import str2bool
from conabio_ml_vision.datasets import ImageDataset, ImagePredictionDataset
from conabio_ml_vision.datasets import VideoDataset
from conabio_ml_vision.preprocessing.preprocess import ImageProcessing
from conabio_ml_vision.utils.images_utils import draw_detections_of_image
from conabio_ml_vision.utils.images_utils import COLORS_MAP
from conabio_ml_vision.models import run_megadetector_inference, MegadetectorModel
from conabio_ml.utils.utils import parallel_exec
from conabio_ml_vision.utils import coords_utils

logger = get_logger(__name__)


NUM_GPUS_PER_NODE = 1


def apply_megadetector_to_dataset(dataset_file: str,
                                  images_dir: str,
                                  categories: Union[List[str], str] = None,
                                  exclude_cats: Union[List[str], str] = None,
                                  out_detections_csv: str = None,
                                  detector_model_path: str = None,
                                  num_tasks: int = None,
                                  task_num: int = None,
                                  partition: str = None,
                                  num_gpus_per_instance: int = NUM_GPUS_PER_NODE,
                                  min_score_threshold: float = 0.01,
                                  **kwargs):
    """Function to create a dataset using `dataset_file` and apply Megadetector inference to it,
    storing the results in `out_detections_csv`

    Parameters
    ----------
    dataset_file : str or None
        Path to a CSV or JSON file which contains the information of the dataset. If None, you must
        specify the parameter `images_dir` from which the dataset will be created
    images_dir : str
        Folder where images of `dataset` are stored. If `None` the `images_dir` of `dataset` will
        be used. By default None
    categories : Union[List[str], str], optional
        List, string or path of a CSV or a text file with the categories to be included in the
        dataset.
        If None, registers of all categories will be included.
        If path to a CSV file, it should have the categories in the column `0` and should not
        have a header.
        If path to a text file, it must have the categories separated by a line break.
        If string, it must contain the categories separated by commas.
        If empty list, labeled images will not be included.
        By default None
    exclude_cats : Union[List[str], str], optional
        List, string or path of a CSV file or a text file with categories to be excluded.
        If it is a path of a CSV file, it should have the categories in the column `0` and should
        not have a header. If it is a path of a text file, it must have the categories separated by
        a line break. If it is a string, it must contain the categories separated by commas.
        By default None
    out_detections_csv : str, optional
        Path to the CSV file in which the model predictions will be saved, by default None
    detector_model_path : str, optional
        Path of the file with the model weights that is either already on the local computer,
        or where it will be downloaded if it does not exist. If `None`, the file will be
        downloaded to a temporary folder. By default None
    num_tasks : int, optional
        In case of parallel processing (e.g., on several instances), this is the total number
        of tasks into which the processing is divided. E.g., if you have 4 instances with 2
        GPUs on each instance, the work can be divided into `num_tasks = 8` parallel tasks, in
        order to maximize the use of resources and perform the processing in the shortest
        possible time. By default None
    task_num : int, optional
        In case of parallel processing, this is the number of the current task.
        It must be an integer in the range `[1, num_tasks]`. By default None
    partition : str, optional
        The value of the partition by which the dataset is going to be filtered, by default None
    num_gpus_per_instance : int, optional
        Number of GPUs per instance in case of parallel processing, by default NUM_GPUS_PER_NODE
    min_score_threshold : float, optional
        Minimum score of predictions to be taken into account, by default 0.01
    """
    out_detections_csv = kwargs.get('out_predictions_csv', out_detections_csv)
    if dataset_file is not None and dataset_file.lower().endswith('.json'):
        dataset = ImageDataset.from_json(source_path=dataset_file,
                                         images_dir=images_dir,
                                         categories=categories,
                                         exclude_categories=exclude_cats,
                                         not_exist_ok=True)
    elif dataset_file is not None and dataset_file.lower().endswith('.csv'):
        dataset = ImageDataset.from_csv(source_path=dataset_file,
                                        images_dir=images_dir,
                                        categories=categories,
                                        exclude_categories=exclude_cats)
    else:
        dataset = ImageDataset.from_folder(
            source_path=images_dir,
            extensions=['jpg', 'JPG', 'bmp', 'jpeg', 'JPEG', 'mp4', 'MP4' 'AVI', 'avi'])
    if partition is not None:
        dataset.filter_by_partition(partition, inplace=True)

    run_megadetector_inference(dataset=dataset,
                               out_detections_csv=out_detections_csv,
                               images_dir=images_dir,
                               model_path=detector_model_path,
                               min_score_threshold=min_score_threshold,
                               num_tasks=num_tasks,
                               task_num=task_num,
                               num_gpus_per_instance=num_gpus_per_instance)


def apply_clahe_wb_to_ds(dataset_file,
                         images_dir_orig,
                         images_dir_dest,
                         categories=None,
                         exclude_cats=None,
                         discard_exist=True,
                         num_tasks=None,
                         task_num=None):
    """Function that applies CLAHE and SimpleWB pre-processing operations to the images of a
    dataset and saves them in `images_dir_dest`.

    Parameters
    ----------
    dataset_file : str or None
        Path of the file in which the dataset is stored. This could be a COCO JSON file or a CSV
        file. In case of None, the dataset will be created from the images stored in
        `images_dir_orig`
    images_dir_orig : str
        Path of the folder containing the images of the dataset
    images_dir_dest : str
        Path to the folder where the resulting images will be saved
    categories : list of str, str or None
        List, string or path of a CSV or a text file with the categories to be included in
        the dataset. If None, registers of all categories will be included.
        If path to a CSV file, it should have the categories in the column `0` and should
        not have a header. If path to a text file, it must have the categories separated by
        a line break. If string, it must contain the categories separated by commas.
        If empty list, labeled images will not be included. (default is None)
    exclude_cats : list of str, str or None
        List, string or path of a CSV file or a text file with categories to be excluded.
        If it is a path of a CSV file, it should have the categories in the column `0`
        and should not have a header. If it is a path of a text file, it must have the
        categories separated by a line break. If it is a string, it must contain the
        categories separated by commas. (default is None)
    discard_exist : bool, optional
        Whether to discard images that already exists in `images_dir_dest` or not, by default True
    num_tasks : int, optional
        In case of parallel processing (e.g., on several nodes), this is the total number of tasks
        into which the processing is divided. E.g., if you have 4 nodes with 2 GPUs on each node,
        the work can be divided into `num_tasks = 8` parallel tasks, in order to maximize the use
        of resources and perform the processing in the shortest possible time.
        By default None
    task_num : int, optional
        In case of parallel processing, this is the number of the current task.
        It must be an integer in the range [1, `num_tasks`]. By default None
    """
    if dataset_file is not None and dataset_file.lower().endswith('.json'):
        dataset = ImageDataset.from_json(source_path=dataset_file,
                                         images_dir=images_dir_orig,
                                         categories=categories,
                                         exclude_categories=exclude_cats,
                                         not_exist_ok=True)
    elif dataset_file is not None and dataset_file.lower().endswith('.csv'):
        dataset = ImageDataset.from_csv(source_path=dataset_file,
                                        images_dir=images_dir_orig,
                                        categories=categories,
                                        exclude_categories=exclude_cats)
    else:
        dataset = ImageDataset.from_folder(source_path=images_dir_orig,
                                           extensions=['jpg', 'JPG', 'bmp'])

    ImageProcessing.apply_clahe_wb(
        dataset,
        images_dir_dest=images_dir_dest,
        discard_exist=discard_exist,
        num_tasks=num_tasks,
        task_num=task_num)


def visualize_detections_in_ds(images_ds,
                               detections,
                               images_dir,
                               dest_dir,
                               min_score_dets=0.4,
                               score_thres_colors=None,
                               include_score=False,
                               colors_by_label=False,
                               blur_people=False,
                               set_images_dir_to_dest_dir=False):
    # TODO: Document
    logger.info(f"Visualizing detections of images in folder {dest_dir}")
    os.makedirs(dest_dir, exist_ok=True)

    if isinstance(detections, str) and detections.lower().endswith('.csv'):
        dets_ds = ImagePredictionDataset.from_csv(source_path=detections, images_dir=images_dir)
    else:
        dets_ds = detections

    if isinstance(images_ds, str) and images_ds.lower().endswith('.json'):
        imgs_ds = ImageDataset.from_json(source_path=images_ds, images_dir=images_dir)
    elif isinstance(images_ds, str) and images_ds.lower().endswith('.csv'):
        imgs_ds = ImageDataset.from_csv(source_path=images_ds, images_dir=images_dir)
    elif images_ds is not None:
        imgs_ds = images_ds
    else:
        imgs_ds = None

    dets_ds.filter_by_score(min_score=min_score_dets, inplace=True)
    dets_ds.get_images_sizes()
    dets_ds._convert_coordinates(
        input_format=coords_utils.COORDINATES_FORMATS.X_Y_WIDTH_HEIGHT,
        output_format=coords_utils.COORDINATES_FORMATS.X_Y_WIDTH_HEIGHT,
        output_coords_type=coords_utils.COORDINATES_TYPES.ABSOLUTE)

    dets_df = dets_ds.as_dataframe()

    def func_dest_dir(item):
        if dest_dir != images_dir:
            reldir_img = os.path.dirname(os.path.relpath(item, images_dir))
            return os.path.join(dest_dir, reldir_img)
        return None

    if colors_by_label:
        colors_names = list(COLORS_MAP.keys())
        n_colors = len(colors_names)
        lbl2col = {lbl: colors_names[i % n_colors] for i, lbl in enumerate(dets_df.label.unique())}
        dets_df['color'] = dets_df['label'].apply(lambda lbl: lbl2col[lbl])

    items = dets_df["item"].unique() if imgs_ds is None else imgs_ds.get_unique_items()
    parallel_exec(
        func=draw_detections_of_image,
        elements=items,
        item=lambda item: item,
        dets_df=dets_df,
        dest_dir=func_dest_dir,
        score_thres_colors=score_thres_colors,
        copy_img_if_has_no_dets=True,
        include_score=include_score,
        blur_people=blur_people)

    if set_images_dir_to_dest_dir:
        images_ds.set_images_dir(dest_dir)


def visualize_detections_on_videos(videos_dataset=None,
                                   videos_dir=None,
                                   dest_folder=None,
                                   min_score_threshold=0.4,
                                   out_detections_csv=None,
                                   min_score_dets_to_store=0.1,
                                   out_classification_csv=None,
                                   out_frames_folder=None,
                                   out_frames_ds_csv=None,
                                   out_frames_w_bboxes_folder=None,
                                   freq_sampling=1,
                                   blur_people=False,
                                   resize_images=None,
                                   pred_method_for_classification='highest_pred',
                                   checkpoint_frequency=None,
                                   checkpoint_path=None,
                                   delete_aux_files_after_finish=True,
                                   model_path=None,
                                   separate_into_folders_by_classification=True,
                                   **kwargs):
    # TODO: Document
    out_detections_csv = kwargs.get('out_predictions_csv', out_detections_csv)
    if isinstance(videos_dataset, VideoDataset):
        vids_ds = videos_dataset
    elif type(videos_dataset) is str and videos_dataset.lower().endswith('.csv'):
        vids_ds = VideoDataset.from_csv(source_path=videos_dataset, videos_dir=videos_dir)
    elif videos_dir is not None:
        vids_ds = VideoDataset.from_folder(
            videos_dir, label_by_folder_name=False, split_by_folder=False, recursive=True)
    else:
        raise Exception(f'You must specify a valid videos_dataset or videos_dir')

    if dest_folder is None:
        dest_folder = vids_ds.get_media_dir()
        assert dest_folder is not None, f"Invalid dest_folder argument: {dest_folder}"
        logger.info(
            f'No dest_folder has been specified, so the resulting videos will be stored '
            f'in {dest_folder}')

    delete_frames_folder = False
    delete_frames_w_bbox_folder = False
    delete_detections_csv = False
    delete_frames_ds_csv = False
    delete_classification_csv = False
    if out_frames_folder is None:
        out_frames_folder = os.path.join(dest_folder, 'frames')
        delete_frames_folder = delete_aux_files_after_finish
    if out_frames_w_bboxes_folder is None:
        out_frames_w_bboxes_folder = os.path.join(dest_folder, 'frames_w_bbox')
        delete_frames_w_bbox_folder = delete_aux_files_after_finish
    if out_detections_csv is None:
        out_detections_csv = os.path.join(dest_folder, 'detections.csv')
        delete_detections_csv = delete_aux_files_after_finish
    if out_classification_csv is None:
        out_classification_csv = os.path.join(dest_folder, 'classifications.csv')
        delete_classification_csv = delete_aux_files_after_finish
    if out_frames_ds_csv is None:
        out_frames_ds_csv = os.path.join(dest_folder, 'frames_ds.csv')
        delete_frames_ds_csv = delete_aux_files_after_finish

    classified_vids_ds = MegadetectorModel.classify_dataset(
        dataset=vids_ds,
        dets_threshold=min_score_threshold,
        pred_method=pred_method_for_classification,
        model_path=model_path,
        out_detections_csv=out_detections_csv,
        min_score_dets_to_store=min_score_dets_to_store,
        out_classification_csv=out_classification_csv,
        out_frames_folder=out_frames_folder,
        out_frames_ds_csv=out_frames_ds_csv,
        freq_video_sampling=freq_sampling,
        checkpoint_frequency=checkpoint_frequency,
        checkpoint_path=checkpoint_path,
        **kwargs)

    frames_ds = ImageDataset.from_csv(out_frames_ds_csv, images_dir=out_frames_folder)
    visualize_detections_in_ds(images_ds=frames_ds,
                               detections=out_detections_csv,
                               images_dir=out_frames_folder,
                               dest_dir=out_frames_w_bboxes_folder,
                               min_score_dets=min_score_threshold,
                               blur_people=blur_people,
                               set_images_dir_to_dest_dir=True)
    if resize_images is not None:
        ImageProcessing.resize(frames_ds, resize_images)

    VideoDataset.create_videos_from_images_ds(
        frames_ds, dest_folder, freq_sampling=freq_sampling, original_vids_ds=vids_ds)

    if separate_into_folders_by_classification:
        classification_df = classified_vids_ds.as_dataframe()
        # TODO: improve this
        classification_df['video_id'] = (
            classification_df['video_id'].apply(
                lambda x: os.path.join(dest_folder, os.path.relpath(x, vids_ds.get_media_dir())))
        )
        classification_df = classification_df.set_index('video_id')

        def get_label_fn(x):
            return classification_df.loc[x['video_id']]['label']

        vids_wboxes_ds = VideoDataset.from_folder(
            dest_folder, label_by_folder_name=False, split_by_folder=False, recursive=True)

        vids_wboxes_ds.set_anns_info_field_by_expr('label', get_label_fn, inplace=True)
        vids_wboxes_ds.to_folder(
            dest_folder, split_in_labels=True, preserve_directories_structure=True)

    if delete_frames_folder:
        shutil.rmtree(out_frames_folder, ignore_errors=True)
    if delete_frames_w_bbox_folder:
        shutil.rmtree(out_frames_w_bboxes_folder, ignore_errors=True)
    if delete_detections_csv:
        os.remove(out_detections_csv)
    if delete_classification_csv:
        os.remove(out_classification_csv)
    if delete_frames_ds_csv:
        os.remove(out_frames_ds_csv)


def create_obj_level_ds_from_dets_and_anns(detections,
                                           annotations,
                                           images_dir=None,
                                           use_partitions=False,
                                           annot_categories=None,
                                           exclude_annot_categories=None,
                                           det_categories=None,
                                           exclude_det_categories=None,
                                           min_score_detections=0.1,
                                           **kwargs):
    """Function that creates a dataset with object-level annotations from two files (or instances):
    one with the predictions of an object detector (e.g. Megadetector) with object-level
    'annotations' and generic classes (e.g. Animal, Person), and other with
    image-level annotations and specific classes (e.g. Canis latrans).
    For each image the bounding boxes of the object detector are taken and each one is assigned the
    given image-level class, to form object-level annotations with specific classes.

    Parameters
    ----------
    detections : str or ImagePredictionDataset
        Path to a CSV file with the predictions of an object detector (Megadetector) or an instance
        of ImagePredictionDataset. It must contain the `image_id` column
    annotations : str or ImageDataset
        Path to the JSON or CSV file or an instance of ImageDataset with image-level annotations
        and specific classes
    images_dir : str
        The folder path where the images are stored
    use_partitions : bool, optional
        Whether to inherit the partitions from the original dataset or not, by default False
    annot_categories : list of str, str or None, optional
        Categories to include in the annotations dataset, by default None
    exclude_annot_categories : list of str, str or None, optional
        Categories to be excluded in the annotations dataset, by default None
    min_score_detections : float, optional
        Minimum score that the detections in `detections` must have. The rest will be
        discarded, by default 0.1
    **kwargs
        Extra named arguments passed to the `ImageDataset` and `ImagePredictionDataset` constructor

    Returns
    -------
    ImageDataset
        Resulting object detection dataset
    """
    kwargs['validate_filenames'] = kwargs.get('validate_filenames', False)
    if type(detections) is str and detections.lower().endswith('.csv'):
        dets_ds = ImagePredictionDataset.from_csv(source_path=detections,
                                                  images_dir=images_dir,
                                                  categories=det_categories,
                                                  exclude_categories=exclude_det_categories,
                                                  **kwargs)
    else:
        dets_ds = detections
        if det_categories is not None:
            dets_ds.filter_by_categories(det_categories, mode='include', inplace=True)
        if exclude_det_categories is not None:
            dets_ds.filter_by_categories(exclude_det_categories, mode='exclude', inplace=True)

    if type(annotations) is str and annotations.lower().endswith('.json'):
        anns_ds = ImageDataset.from_json(annotations,
                                         images_dir=images_dir,
                                         categories=annot_categories,
                                         exclude_categories=exclude_annot_categories,
                                         **kwargs)
    elif type(annotations) is str and annotations.lower().endswith('.csv'):
        anns_ds = ImageDataset.from_csv(source_path=annotations,
                                        images_dir=images_dir,
                                        categories=annot_categories,
                                        exclude_categories=exclude_annot_categories,
                                        **kwargs)
    else:
        anns_ds = annotations.copy()
        if annot_categories is not None:
            anns_ds.filter_by_categories(annot_categories, mode='include', inplace=True)
        if exclude_annot_categories is not None:
            anns_ds.filter_by_categories(exclude_annot_categories, mode='exclude', inplace=True)

    dets_ds.filter_by_score(min_score=min_score_detections, inplace=True)
    dets_df = dets_ds.as_dataframe(columns=[dets_ds.ANNOTATIONS_FIELDS.MEDIA_ID,
                                            dets_ds.ANNOTATIONS_FIELDS.BBOX,
                                            dets_ds.ANNOTATIONS_FIELDS.SCORE,
                                            dets_ds.ANNOTATIONS_FIELDS.ID])

    anns_cols = anns_ds._get_current_annotations_info_cols(
        use_partitions=use_partitions, remove_cols=[anns_ds.ANNOTATIONS_FIELDS.ID])
    anns_df = anns_ds.create_image_level_ds().as_dataframe(columns=anns_cols)

    dets_anns_df = pd.merge(
        left=dets_df, right=anns_df, how='inner',
        on=anns_ds.ANNOTATIONS_FIELDS.MEDIA_ID, sort=False)
    dets_anns_df.rename(
        columns={dets_ds.ANNOTATIONS_FIELDS.SCORE: anns_ds.ANNOTATIONS_FIELDS.SCORE_DET},
        inplace=True)

    instance_ds = type(anns_ds)(dets_anns_df,
                                images_info=anns_ds.get_images_info(),
                                extra_fields=anns_ds._get_extra_fields())
    return instance_ds


def get_samples_counts_by_partitions(dataset_path, out_csv_path):
    ds = ImageDataset.from_csv(dataset_path)
    df = ds.as_dataframe()
    partitions = ds.get_partitions_names()
    data = (
        df.label
        .value_counts()
        .rename_axis('label')
        .to_frame('count')
        .rename(columns={'count': 'Total images'})
    )

    for partition in partitions:
        data[f'{partition} images'] = df[df.partition == partition].label.value_counts()
    data.fillna(0, inplace=True)
    for partition in partitions:
        data[f'% {partition} images'] = data.apply(
            lambda x: f"{(x[f'{partition} images'] / x['Total images']) * 100: .2f}", axis=1)

    data.to_csv(out_csv_path, index=True)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("--apply_clahe_wb_to_ds", default=False, action="store_true")
    parser.add_argument("--visualize_detections_in_ds", default=False, action="store_true")
    parser.add_argument("--apply_megadetector_to_dataset", default=False, action="store_true")
    parser.add_argument("--create_obj_level_ds_from_dets_and_anns",
                        default=False, action="store_true")
    parser.add_argument("--get_samples_counts_by_partitions", default=False, action="store_true")

    parser.add_argument('--dataset_file', default=None, type=str)
    parser.add_argument('--images_dir', default=None, type=str)
    parser.add_argument('--images_dir_orig', default=None, type=str)
    parser.add_argument('--images_dir_dest', default=None, type=str)
    parser.add_argument("--categories", default=None, type=str)
    parser.add_argument("--exclude_cats", default=None, type=str)
    parser.add_argument('--num_tasks', default=None, type=int)
    parser.add_argument('--task_num', default=None, type=int)
    parser.add_argument("--detections_csv", default=None, type=str)
    parser.add_argument("--output_csv", default=None, type=str)
    parser.add_argument('--min_score_dets', default=0.1, type=float)
    parser.add_argument('--score_thres_colors', default=None, type=float)
    parser.add_argument("--discard_exist", type=str2bool, nargs='?', const=True, default=True)
    parser.add_argument("--detector_model_path", default=None, type=str)
    parser.add_argument("--partition", default=None, type=str)
    parser.add_argument("--num_gpus_per_instance", default=NUM_GPUS_PER_NODE, type=int)

    args = parser.parse_args()

    if args.apply_clahe_wb_to_ds:
        apply_clahe_wb_to_ds(dataset_file=args.dataset_file,
                             images_dir_orig=args.images_dir_orig,
                             images_dir_dest=args.images_dir_dest,
                             categories=args.categories,
                             exclude_cats=args.exclude_cats,
                             discard_exist=args.discard_exist,
                             num_tasks=args.num_tasks,
                             task_num=args.task_num)
    if args.visualize_detections_in_ds:
        visualize_detections_in_ds(images_ds=args.dataset_file,
                                   detections=args.detections_csv,
                                   images_dir=args.images_dir_orig,
                                   dest_dir=args.images_dir_dest,
                                   min_score_dets=args.min_score_dets,
                                   score_thres_colors=args.score_thres_colors)
    if args.apply_megadetector_to_dataset:
        apply_megadetector_to_dataset(dataset_file=args.dataset_file,
                                      images_dir=args.images_dir,
                                      categories=args.categories,
                                      exclude_cats=args.exclude_cats,
                                      out_detections_csv=args.output_csv,
                                      detector_model_path=args.detector_model_path,
                                      num_tasks=args.num_tasks,
                                      task_num=args.task_num,
                                      partition=args.partition,
                                      num_gpus_per_instance=args.num_gpus_per_instance,
                                      min_score_threshold=args.min_score_dets)
    if args.create_obj_level_ds_from_dets_and_anns:
        create_obj_level_ds_from_dets_and_anns(detections=args.detections_csv,
                                               annotations=args.dataset_file,
                                               images_dir=args.images_dir,
                                               annot_categories=args.categories,
                                               exclude_annot_categories=args.exclude_cats,
                                               min_score_detections=args.min_score_dets)
    if args.get_samples_counts_by_partitions:
        get_samples_counts_by_partitions(dataset_path=args.dataset_file,
                                         out_csv_path=args.output_csv)
