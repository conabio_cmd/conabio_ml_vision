import os

query_videos = """
SELECT
   	distinct ac.nombre_archivo_md5, ac.fecha_camara, ac.conglomerado
FROM especies_anps_archivo_camara_pruebas AS ac
where ac.nombre_archivo_md5 ILIKE '%.AVI' and ac.fecha_camara > DATE('1900-01-01');
"""

query_images_of_a_video = """
select distinct ac_orig.id, ac.conglomerado, ac.nombre_archivo_md5, ac.fecha_camara
FROM especies_anps_archivo_camara_pruebas AS ac
join especies_anps_archivo_camara as ac_orig on ac.nombre_archivo_md5 = ac_orig.nombre_archivo_md5
where ac.conglomerado = '{0}' and
	ac.nombre_archivo_md5 not ILIKE '%.AVI' and
	ac.fecha_camara::timestamp >= timestamp '{1}' - interval '{2}' second and
	ac.fecha_camara::timestamp <= timestamp '{1}' + interval '{2}' second
order by ac.fecha_camara desc;
"""

BASE_VIDEOS_PATH = os.environ.get('SACMOD_DATA', '')
SNMB_IMGS_DIR = '/LUSTRE/users/ecoinf_admin/conabio_ml_vision/examples/data/snmb'
date_fmt = '%Y%m%d%H%M%S'
