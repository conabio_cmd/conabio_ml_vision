#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import annotations
import os
import re
from abc import abstractmethod
from multiprocessing import Manager
from pathlib import Path
from shutil import copy2
from typing import List
import copy

import pandas as pd

from conabio_ml.datasets.dataset import Dataset, Partitions, PredictionDataset
from conabio_ml.utils.utils import is_array_like
from conabio_ml.utils.logger import get_logger, debugger
import conabio_ml_vision.utils.dataset_utils as utils
from conabio_ml.utils.dataset_utils import get_mapping_classes, get_cleaned_label
from conabio_ml.utils.utils import get_chunk as get_chunk_func
from conabio_ml.utils.utils import parallel_exec

logger = get_logger(__name__)
val_filenames_debug = debugger.get_validate_filenames_env()


class MediaDataset(Dataset):
    """MediaDataset' is an interface to the 'ImageDataset' and 'VideoDataset' classes
    and implements the common functionality of both."""

    """
    DATASET CALLBACKS
    """

    FIELD_TO_JOIN_IMGS_ANNS = 'item'

    def _callback_dataset(self, **kwargs):
        """Method that is called each time a dataset instance is created and that initializes the
        required variables and properties of the dataset.

        Parameters
        ----------
        **kwargs
            Extra named arguments passed to initialization methods and may contains the
            following parameters:
            * max_elements_per_category : str, int or dict
                If int, indicates the maximum number of samples to be taken from among the elements
                of each label.
                If float (0, 1), it refers to the percentage of elements to be taken from each
                label.
                If str, indicates the name of the method to be used in the sampling operation.
                The possible method names are:
                * fewer: will take as the sample number the smallest value of the elements grouped
                by label in the set.
                * mean: calculates the average of the element counts for each category and takes
                this value as the maximum number of elements for each category.
                If dict, it must contain an integer for each category that you want to configure,
                in the form {'label_name': max_elements} (default is None)
            * media_info : dict
                Dictionary that contains the information of each of the fields of the dataset for
                each of the items it contains, in the form {`field_i`: {`item_j`: `value`}}
            * media_dir : str
                The folder path where the media are already stored.
            * random_state : int
                Value of the random_state variable used in functions that make use of random values
                (e.g., `sample_data_elements`).
            * parallel_set_media_dir : bool
                Whether or not to process the elements in parallel in method `set_media_dir`.
                Recommended for very large datasets (e.g., those with > 500 K items).
                By default False
        """
        # region Args
        max_elements_per_category = kwargs.get("max_elements_per_category")
        random_state = kwargs.get("random_state")
        mapping_classes = kwargs.get("mapping_classes")
        clean_cat_names = kwargs.get("clean_cat_names", True)
        mapping_classes_from_col = kwargs.get("mapping_classes_from_col")
        mapping_classes_to_col = kwargs.get("mapping_classes_to_col")
        mapping_classes_filter_expr = kwargs.get("mapping_classes_filter_expr")
        categories = kwargs.get("categories")
        exclude_cats = kwargs.get("exclude_categories")
        mapping_media_fields = kwargs.get("mapping_media_fields")
        download_media_to = kwargs.get("download_media_to")
        fname_w_id_and_ext = kwargs.get("set_filename_with_id_and_extension")
        extra_fields = kwargs.get('extra_fields')
        # endregion

        # region Set Annotations fields, mapping/filtering categories
        actual_cols_in_data = set(self.data.columns.values)
        default_cols_in_anns = set(self._get_annotations_info_cols())
        anns_cols = actual_cols_in_data & default_cols_in_anns
        if extra_fields is not None:
            anns_cols |= actual_cols_in_data & set(extra_fields)
        anns_info = (
            self.data[list(anns_cols)].dropna(how='all', axis=1)
            .fillna('')
        )

        if 'label' in anns_info.columns:
            if clean_cat_names is True:
                anns_info['label'] = anns_info['label'].apply(get_cleaned_label)

            if mapping_classes is not None:
                cat_mappings = get_mapping_classes(
                    mapping_classes,
                    clean_cat_names=clean_cat_names,
                    from_col=mapping_classes_from_col,
                    to_col=mapping_classes_to_col,
                    filter_expr=mapping_classes_filter_expr
                )

                def map_cats(label):
                    if label in cat_mappings:
                        return cat_mappings[label]
                    elif '*' in cat_mappings:
                        return cat_mappings['*']
                    return label

                anns_info['label'] = anns_info['label'].apply(map_cats)

            if categories is not None:
                cats = utils.get_cats_from_source(categories, clean_names=clean_cat_names)
                anns_info = (
                    anns_info[anns_info['label'].isin(cats)]
                    .reset_index(drop=True)
                )
            if exclude_cats is not None:
                exclude_cats = utils.get_cats_from_source(
                    exclude_cats, clean_names=clean_cat_names)
                anns_info = (
                    anns_info[~anns_info['label'].isin(exclude_cats)]
                    .reset_index(drop=True)
                )
        # endregion

        # region Get MEDIA_FIELD_INFO and set columns and types
        media_info_sent_in_data = False
        if self.info.get(self.MEDIA_FIELD_INFO) is not None:
            media_info = self.info[self.MEDIA_FIELD_INFO]
        elif kwargs.get(self.MEDIA_FIELD_INFO) is not None:
            media_info = kwargs[self.MEDIA_FIELD_INFO]
        else:
            # In case you passed all the data (anns_info and media_info) in the self.data property
            media_info_sent_in_data = True
            media_info_cols = (
                (actual_cols_in_data - anns_cols) |
                {self.FIELD_TO_JOIN_IMGS_ANNS, self.ANNOTATIONS_FIELDS.MEDIA_ID}
            )
            media_info_cols &= actual_cols_in_data
            media_info = self.data[list(media_info_cols)]

        if self.ANNOTATIONS_FIELDS.MEDIA_ID in media_info.columns:
            media_info = (
                media_info
                .rename(columns={self.ANNOTATIONS_FIELDS.MEDIA_ID: self.MEDIA_FIELDS.ID})
            )
        elif self.MEDIA_FIELDS.ID not in media_info.columns:
            self._set_id_field_to_media_info(media_info)

        if len(media_info) > 0:
            if media_info_sent_in_data:
                media_info = (
                    media_info
                    .drop_duplicates(subset=self.FIELD_TO_JOIN_IMGS_ANNS)
                    .set_index(self.FIELD_TO_JOIN_IMGS_ANNS))
            # To remove unused elements in media_info
            unique_elems_in_anns = anns_info[self.FIELD_TO_JOIN_IMGS_ANNS].unique()
            media_info = media_info.loc[unique_elems_in_anns]

        if mapping_media_fields is not None:
            media_info = media_info.rename(columns=mapping_media_fields)

        actual_media_cols = set(media_info.columns)
        media_info_cols = set(self._get_media_info_cols()) & actual_media_cols
        if extra_fields is not None:
            media_info_cols |= actual_media_cols & set(extra_fields)
        media_info = media_info[list(media_info_cols)]

        self._set_field_types_in_media_info(media_info)
        # endregion

        # region Set information of dataset: data and media_info
        self.data = anns_info
        super()._callback_dataset(**kwargs)
        self._set_media_info(media_info)
        # endregion

        if max_elements_per_category is not None:
            self.sample_dataset_elements(
                max_elements_per_category, random_state=random_state,
                by_items=True, inplace=True
            )

        # region Download and Set media_dir
        if download_media_to is not None:
            # Download remote media to download_media_to
            media_info = self.get_media_info()

            self._download(
                download_media_to,
                media_info=media_info,
                set_filename_with_id_and_ext=fname_w_id_and_ext,
                **kwargs
            )
            self.info[self.MEDIA_FIELD_DIR] = download_media_to

        media_dir = (
            self.info.get(self.MEDIA_FIELD_DIR) or kwargs.get(self.MEDIA_FIELD_DIR)
        )
        self.set_media_dir(media_dir=media_dir, **kwargs)
        # endregion

        self._set_extra_fields(extra_fields)
        if self.MEDIA_FIELD_INFO in self.info:
            del self.info[self.MEDIA_FIELD_INFO]
        if self.MEDIA_FIELD_DIR in self.info:
            del self.info[self.MEDIA_FIELD_DIR]

    @abstractmethod
    def _callback_to_csv(self, data, **kwargs):
        """Method that is executed in the calls to the `to_csv` method and allows to modify the
        default form that the information will be stored in the csv file.

        Parameters
        ----------
        data : pd.DataFrame
            DataFrame that contains in tabular format the main information of the elements of the
            dataset, such as the element-label pairs.
        **kwargs
            Extra named arguments that may contains the following parameters:
            * include_relative_path_in_items : bool
                Whether to include relative path of each item in the `item` column or not
                (default is False)
            * remove_partition_to_item_path : bool
                Whether to remove the `partition` in the `item` path or not (default is False)
            * remove_label_to_item_path : bool
                Whether to remove the `label` in the `item` path or not (default is False)

        Returns
        -------
        pd.DataFrame
            DataFrame with the modified information of the dataset
        """
        include_relative_path_in_items = kwargs.get("include_relative_path_in_items", False)
        remove_partition_to_item_path = kwargs.get("remove_partition_to_item_path", False)
        remove_label_to_item_path = kwargs.get("remove_label_to_item_path", False)

        media_dir = self.get_media_dir()
        if (not include_relative_path_in_items and media_dir and
                not os.path.relpath(data.iloc[0]["item"], media_dir).startswith('..')):
            data['item'] = (
                data.apply(utils.get_new_item_to_csv, axis=1,
                           media_dir=media_dir,
                           remove_partition_to_item_path=remove_partition_to_item_path,
                           remove_label_to_item_path=remove_label_to_item_path)
            )
        return data

    @classmethod
    def _callback_from_datasets(cls, datasets):
        """Method that is executed in the calls to the `from_datasets` constructor, and that
        assigns the content of the `info` and `media_info` fields with the information contained
        in the dataset instances.

        Parameters
        ----------
        datasets : list of Dataset
            List of Dataset instances from wich the new instance will be created
        """
        info = {}
        datas = []
        for ds in datasets:
            df = ds.as_dataframe()
            cols = list(ds.data.columns.values)
            if 'partition' in df.columns:
                cols += ['partition']
            datas.append(df[cols])

        # Generates media_info for the new dataset
        df_media_info = pd.DataFrame()
        for ds in datasets:
            df_media_info = pd.concat([df_media_info, ds.get_media_info()],
                                      ignore_index=False)
        df_media_info = df_media_info.fillna('')
        info[cls.MEDIA_FIELD_INFO] = df_media_info

        # Generates media_dir for the new dataset
        media_dirs = list(set([ds.get_media_dir() for ds in datasets]))
        # The same `media_dir` for all datasets
        if len(media_dirs) == 1 and media_dirs[0] != '':
            info[cls.MEDIA_FIELD_DIR] = media_dirs[0]

        return datas, info

    def _filesystem_writer(self, row, dest_path, keep_originals, preserve_directories_structure):
        """Auxiliar function to move items in a to_folder call

         Parameters
        ----------
        row : dict
            Dictionary that contains imformation of a row.
        dest_path : str
            Path where media will be copied
        keep_originals: bool
            Whether to conserve original media after a move operation or not
        preserve_directories_structure : bool
            Whether or not to preserve the original structure of the directories.
            If False, all the media will be stored directly under the directory `dest_path`
        """
        media_dir = self.get_media_dir()
        if preserve_directories_structure and media_dir is not None:
            media_name = os.path.relpath(row['item'], media_dir)
        else:
            media_name = os.path.basename(row['item'])
        dest_media = os.path.join(dest_path, media_name)

        try:
            os.makedirs(os.path.dirname(dest_media), exist_ok=True)
            copy2(row['item'], dest_media)
        except Exception:
            logger.exception(
                f"An error occurred while copying the media {row['item']} to {dest_media}")

        if not keep_originals:
            try:
                os.remove(row['item'])
            except Exception:
                logger.exception(f"An error occurred while removing the media {row['item']}")

    def _get_data(self, columns=None):
        """Gets a reference to the `pd.DataFrame` instance with the data of the dataset

        Parameters
        ----------
        columns : list, optional
            List with the columns to be obtained from the dataset, by default None

        Returns
        -------
        pd.DataFrame
            Instance with the data of the dataset
        """
        anns_info = self.data
        media_info = self.get_media_info(rename_media_id=True)
        common_cols = set(anns_info.columns.values) & set(media_info.columns.values)
        # Remove common columns between anns_info and media_info in anns_info
        # (e.g., ANNOTATIONS_FIELDS.MEDIA_ID)
        anns_info_cols = list(set(anns_info.columns.values) - common_cols)

        if len(anns_info) == 0 or len(media_info) == 0:
            return pd.DataFrame([])

        anns_info_df = anns_info[anns_info_cols]
        df = anns_info_df.join(media_info, how='left', on=self.FIELD_TO_JOIN_IMGS_ANNS)

        # Order fields
        if columns is None:
            default_cols = self._get_annotations_info_cols() + self._get_media_info_cols()
            # Drop duplicated cols
            default_cols = list(dict.fromkeys(default_cols))
            extra_cols = set(df.columns.values) - set(default_cols)
            columns = default_cols + list(extra_cols)
        cols = [col for col in columns if col in df.columns]

        if len(cols) > 0:
            return df[cols]
        else:
            return df

    """
    OVERLOADED CONSTRUCTORS
    """
    @classmethod
    def from_folder(cls,
                    source_path: str,
                    extensions: List[str],
                    label_by_folder_name: bool = True,
                    split_by_folder: bool = True,
                    **kwargs) -> Dataset.DatasetType:
        """Create a MediaDataset from a folder structure.

        Parameters
        ----------
        source_path : str
            Path of a folder to be loaded and converted into a Dataset object.
        extensions : list of str, optional
            List of extensions to seek files in folders.
            [""] to seek all files in folders (default is [".JPG", ".jpg", ".png", ".bmp"])
        label_by_folder_name : bool
            Whether or not you want to label each item of the dataset according
            to the name of the folder that contains it.
            (default is True)
        split_by_folder : bool, optional
            Split the dataset from the directory structure (default is True).
            For this option, directories must be in the following structure::
                - source_path
                - `train`
                    - class folders
                - `test`
                    - class folders
                - `validation`
                    - class folders
        **kwargs
            Extra named arguments passed to `from_folder` method of `Dataset`

        Returns
        -------
        MediaDataset
            Instance of the created dataset
        """
        kwargs[cls.MEDIA_FIELD_DIR] = source_path
        kwargs['include_id'] = True
        kwargs['lower_case_exts'] = False

        reg_parts = re.compile(f"{Partitions.TRAIN}|{Partitions.TEST}|{Partitions.VALIDATION}")
        valid_parts_dirs = []

        if split_by_folder:
            for _, parts_dirs, _ in os.walk(source_path):
                if len(parts_dirs) == 0:
                    continue
                valid_parts_dirs = [x for x in parts_dirs if reg_parts.match(x)]
                if len(valid_parts_dirs) > 0:
                    logger.debug(
                        f'{len(valid_parts_dirs)} valid folder names for partitions found')
                    break
            if len(valid_parts_dirs) == 0:
                logger.debug(
                    'No valid folder names for partitions. Set split_by_folder to False')
                split_by_folder = False

        if label_by_folder_name:
            n_valid_labels_names = 0
            if split_by_folder:
                for part_dir in valid_parts_dirs:
                    for _, label_dirs, _ in os.walk(os.path.join(source_path, part_dir)):
                        if len(label_dirs) == 0:
                            continue
                        n_valid_labels_names += len(label_dirs)
            else:
                for _, label_dirs, _ in os.walk(source_path):
                    n_valid_labels_names += len(label_dirs)
            if n_valid_labels_names == 0:
                label_by_folder_name = False

        # TODO: Check this
        if not 'recursive' in kwargs:
            kwargs['recursive'] = split_by_folder or label_by_folder_name

        return super().from_folder(source_path=source_path,
                                   extensions=extensions,
                                   label_by_folder_name=label_by_folder_name,
                                   split_by_folder=split_by_folder,
                                   **kwargs)

    """
    SET METHODS
    """

    def _set_media_info(self, media_info):
        """Assign the information of the media to the property `params[params_key_name]`

        Parameters
        ----------
        media_info : pd.DataFrame
            Dataframe that contains the information of each of the fields of the dataset for each
            of the items it contains
        """
        self.params[self.MEDIA_FIELD_INFO] = media_info

    def set_media_dir(self, media_dir, **kwargs):
        """Sets the `self.MEDIA_FIELD_DIR` variable in the property `params`

        Parameters
        ----------
        media_dir : str
            Path of the base folder where the media are saved
        **kwargs
            Extra named arguments that may include the parameters:
            * append_partition_to_item_path : bool
                Whether to append the `partition` of each row in the `item` path or not,
                by default False
            * append_label_to_item_path : bool
                Whether to append the `label` of each row in the `item` path or not,
                by default False
            * from_basename : bool
                Whether the initial value of each `item` is obtained from the basename of the
                `item` or not, by default False
            * actual_media_dir : bool
                The actual value of the `self.MEDIA_FIELD_DIR` property.
                If None, it will be obtained from the current instance. By default None
            * validate_filenames : bool
                Whether or not to validate that the items exist.
                Set to False to speed up execution time. By default True
            * not_exist_ok : bool
                Whether to include media that exist in `self.MEDIA_FIELD_DIR` and silently pass
                exceptions for those media that are not present (default is False)
        """
        append_partition_to_item_path = kwargs.get("append_partition_to_item_path", False)
        append_label_to_item_path = kwargs.get("append_label_to_item_path", False)
        from_basename = kwargs.get("from_basename", False)
        actual_media_dir = kwargs.get("actual_media_dir", None)
        not_exist_log_file = kwargs.get("not_exist_log_file", None)
        not_exist_ok = kwargs.get("not_exist_ok", False)
        not_exist_ok = not_exist_ok or not_exist_log_file is not None
        # If media_dir is None, only filenames validation is done if validate_filenames
        # is set explicitly.
        validate_filenames = kwargs.get("validate_filenames", media_dir is not None)
        # If VALIDATE_FILENAMES env variable is False, filenames validation won't be performed
        validate_filenames = validate_filenames and val_filenames_debug
        parallel_processing = kwargs.get("parallel_processing", True)

        if actual_media_dir is None:   # '' only from _callback_to_folder
            actual_media_dir = self.get_media_dir()

        self.params[self.MEDIA_FIELD_DIR] = media_dir

        # To avoid unnecessary processing
        change = append_partition_to_item_path or append_label_to_item_path
        if (media_dir is None and not change and
                (validate_filenames is False or os.path.dirname(self.data.iloc[0]['item']) == '')):
            return

        df = self.as_dataframe()
        if len(df) == 0:
            return
        old_items = self.data["item"].copy()

        if parallel_processing:
            new_items_dict = Manager().dict()
            rows = df.to_dict('records')
            parallel_exec(
                func=utils.get_new_item_set_media_dir,
                elements=rows,
                row=lambda row: row,
                new_items_dict=new_items_dict,
                actual_media_dir=actual_media_dir,
                new_media_dir=media_dir,
                append_partition_to_item_path=append_partition_to_item_path,
                append_label_to_item_path=append_label_to_item_path,
                from_basename=from_basename,
                validate_filenames=validate_filenames,
                not_exist_ok=not_exist_ok)
            self.data["item"] = df['item'].apply(lambda x: new_items_dict[x])
        else:
            self.data["item"] = (
                df.apply(
                    utils.get_new_item_set_media_dir, axis=1,
                    new_items_dict=None,
                    actual_media_dir=None,
                    new_media_dir=media_dir,
                    append_partition_to_item_path=append_partition_to_item_path,
                    append_label_to_item_path=append_label_to_item_path,
                    from_basename=from_basename,
                    validate_filenames=validate_filenames,
                    not_exist_ok=not_exist_ok)
            )

        mapping_items = (
            {old_items.iloc[i]: row['item'] for i, row in enumerate(self.data.to_dict('records'))}
        )
        media_info = self.get_media_info().reset_index()
        media_info[self.FIELD_TO_JOIN_IMGS_ANNS] = (
            media_info[self.FIELD_TO_JOIN_IMGS_ANNS]
            .apply(lambda x: mapping_items[x])
        )

        n_invalid = self.data["item"].isnull().sum()
        if n_invalid > 0:
            logger.info(f'{n_invalid} invalid items found that were ignored')
            if not_exist_log_file is not None:
                os.makedirs(os.path.dirname(not_exist_log_file), exist_ok=True)
                not_exist_items_idx = self.data[self.data['item'].isnull()].index
                not_exist_items = old_items.iloc[not_exist_items_idx].values.tolist()
                with open(not_exist_log_file, 'a') as f:
                    f.write('\n'.join(not_exist_items))
                    f.write('\n')

            self.data = self.data[~self.data['item'].isnull()].reset_index(drop=True)
            media_info = (
                media_info[~media_info[self.FIELD_TO_JOIN_IMGS_ANNS].isnull()]
                .reset_index(drop=True)
            )

        n_items = len(self.data["item"])
        logger.debug(f'{n_items} {"" if validate_filenames else "not"} validated items found')

        self._set_media_info(media_info.set_index(self.FIELD_TO_JOIN_IMGS_ANNS))
        self._set_items()

    def set_anns_info_field_by_expr(self, field, values_expr, inplace=True):
        """Method that assigns the information of the `field` from the information of the
        annotations fields by applying the expression `values_expr`

        Parameters
        ----------
        field : str
            Name of the field in `data`
        values_expr : Callable
            Expression to be applied in order to change the value of `field` in `data`
        inplace : bool, optional
            If True, perform operation in-place, by default True

        Returns
        -------
        MediaDataset or None
            Instance of the Dataset or None if `inplace=True`
        """
        # assert field in self.data.columns, "`field` must be in `data` columns"
        instance = self if inplace else self.copy()
        df = instance.as_dataframe()
        # TODO: change this for FIELD_TO_JOIN_IMGS_ANNS
        if field == 'item':
            old_items = instance.data[field].copy()

        instance.data[field] = df.apply(values_expr, axis=1)

        if field == 'item':
            mapping_items = {
                old_items.iloc[i]: row[field] for i, row in enumerate(instance.data.to_dict('records'))
            }
            media_info = (
                instance.get_media_info()
                .reset_index()
            )
            media_info[self.FIELD_TO_JOIN_IMGS_ANNS] = (
                media_info[self.FIELD_TO_JOIN_IMGS_ANNS]
                .apply(lambda x: mapping_items[x])
            )
            instance._set_media_info(media_info.set_index(self.FIELD_TO_JOIN_IMGS_ANNS))

        if field not in self.ANNOTATIONS_FIELDS.NAMES:
            instance._set_extra_fields([field])

        if not inplace:
            return instance

    def _set_extra_fields(self, extra_fields):
        assert_msg = f"extra_fields must be a list: {extra_fields}"
        assert extra_fields is None or is_array_like(extra_fields), assert_msg
        if self.params.get('extra_fields'):
            self.params['extra_fields'] += extra_fields
        else:
            self.params['extra_fields'] = extra_fields
    """
    GET METHODS
    """

    def get_media_info(self, as_dataframe=True, rename_media_id=False):
        """Gets the information about the media stored in the `params[self.MEDIA_FIELD_INFO]`
        property

        Parameters
        ----------
        as_dataframe : bool, optional
            Whether or not to return the maedia info as a `pd.DataFrame`, by default True
        rename_media_id : bool, optional
            Whether or not to rename the column 'id' to self.ANNOTATIONS_FIELDS.MEDIA_ID in case
            `as_dataframe` is True, by default False

        Returns
        -------
        dict or pd.DataFrame
            Value of the property `self.params[self.MEDIA_FIELD_INFO]` as a dict (default)
            or as a pd.DataFrame
        """
        df = self.params[self.MEDIA_FIELD_INFO]
        if as_dataframe:
            if rename_media_id:
                df = df.rename(columns={'id': self.ANNOTATIONS_FIELDS.MEDIA_ID})
            return df
        else:
            return df.to_dict(orient='dict')

    def get_media_dir(self):
        """Gets the directory where the media were stored.

        Returns
        -------
        str
            Base path in which media were stored. In case this variable has not been assigned,
            the empty string will be returned
        """
        return self.params.get(self.MEDIA_FIELD_DIR, '')

    def get_chunk(self,
                  num_chunks: int,
                  chunk_num: int,
                  partition: str = None,
                  verbose: bool = True) -> MediaDataset:
        """Function that divides the elements of a dataset into `num_chunks` chunks and
        returns a dataset with the elements of chunk number `chunk_num`

        Parameters
        ----------
        num_chunks : int
            Number of chunks into which the dataset is divided
        chunk_num : int
            Number of the chunk that will be taken to form the generated dataset. It must be in the range `[1, num_chunks]`
        partition : str, optional
            Partition from which the elements of the dataset will be taken, by default None
        verbose : bool, optional
            Whether or not to print the elements to be taken in the current process, by default True

        Returns
        -------
        MediaDataset
            Data set with the elements of chunk number `chunk_num` of the `num_chunks` chunks into which the original dataset was divided
        """
        items = self.get_unique_items(partition=partition)
        items = get_chunk_func(items, num_chunks, chunk_num, verbose=verbose, sort_elements=True)
        new_ds = self.filter_by_column('item', items, inplace=False)
        return new_ds

    @classmethod
    def _get_annotations_info_cols(cls):
        """Gets the fields from the annotation section of the dataset

        Returns
        -------
        list
            Fields from the annotation section of the dataset
        """
        return cls.ANNOTATIONS_FIELDS.NAMES

    def _get_current_annotations_info_cols(self, use_partitions=True, remove_cols=None):
        """Get the annotation fields for the current dataset, adding the 'partition' field if
        use_partitions is True and removing the `remove_cols` fields

        Parameters
        ----------
        use_partitions : bool, optional
            Whether or not to use the partition field, by default True
        remove_cols : _type_, optional
            Fielda to be removed from the annotation fields, by default None

        Returns
        -------
        list of str
            List with the resulting columns
        """
        # TODO: Document
        df = self.as_dataframe()
        actual_cols_in_ds_set = set(df.columns.values)
        default_cols_in_ds_set = set(self._get_annotations_info_cols())
        if not use_partitions:
            default_cols_in_ds_set -= {"partition"}
        extra_fields_in_ds_set = set(self._get_extra_fields() or []) or set()
        cols = actual_cols_in_ds_set & (default_cols_in_ds_set | extra_fields_in_ds_set)
        if remove_cols is not None:
            cols -= set(remove_cols)

        return list(cols)

    @classmethod
    def _get_media_info_cols(cls):
        """Gets the fields from the media_info section of the dataset

        Returns
        -------
        list
            Fields from the media_info section of the dataset
        """
        return cls.MEDIA_FIELDS.NAMES

    def _get_extra_fields(self):
        return self.params.get('extra_fields')
    """
    FILTER METHODS
    """

    def filter_by_categories(self, categories, mode='include', clean_cat_names=True, inplace=True):
        """Method that filters the dataset by `label`

        Parameters
        ----------
        categories : list of str or str
            List of categories to filter media, or path of a CSV file or a text file.
            If empty list or None, no filtering will be performed.
            If it is a CSV file, it should have the categories in the first column and should not
            have a header. If it is a text file, it must have the categories separated by a line
            break
        mode : str, optional
            Whether to 'include' or 'remove' registers with `label` in `categories` in the dataset,
            by default 'include'
        clean_cat_names : bool, optional
            Whether to clean or not the category names, converting to lower case and removing
            spaces at the beginning and at the end. By default True
        inplace : bool, optional
            If True, perform operation in-place, by default True

        Returns
        -------
        MediaDataset or None
            Instance of the Dataset or None if `inplace=True`
        """
        cats = utils.get_cats_from_source(categories, clean_cat_names)
        return self.filter_by_column(column='label', values=cats,
                                     mode=mode, inplace=inplace)

    def filter_by_column(self, column, values, mode='include', inplace=True):
        """Method that filters the dataset by field `column`

        Parameters
        ----------
        column : str
            Name of the column to filter
        values : list or str
            Values to filter
        mode : str, optional
            Whether to 'include' or 'remove' registers with `values` in `column` in the dataset,
            by default 'include'
        inplace : bool, optional
            If True, perform operation in-place, by default True

        Returns
        -------
        MediaDataset or None
            Instance of the Dataset or None if `inplace=True`
        """
        instance = self if inplace else self.copy()

        if not is_array_like(values):
            values = [values]

        df = instance.as_dataframe(columns=[column])

        if mode == 'include':
            cond_indx = df[column].isin(values)
        else:
            cond_indx = ~df[column].isin(values)

        df = df[cond_indx]
        instance.data = instance.data.iloc[df.index]
        instance.data.reset_index(drop=True, inplace=True)
        df.reset_index(drop=True, inplace=True)

        for part_name in instance.get_partitions_names():
            value = df[df["partition"] == part_name].index
            instance._set_partition(part_name, value=value)

        instance._update_dataset_info()

        logger.debug(f"The elements of the dataset were filtered by the field '{column}' "
                     f"and {instance.get_num_rows()} elements were obtained.")

        if not inplace:
            return instance

    def filter_by_partition(self, partition, mode='include', inplace=True):
        """Method that filters the dataset by field 'partition'

        Parameters
        ----------
        partition : str
            The value of the partition by which the dataset is going to be filtered
        mode : str, optional
            Whether to `include` or `exclude` the elements with the partition field equals to
            `partition`, by default 'include'
        inplace : bool, optional
            If True, perform operation in-place, by default True

        Returns
        -------
        MediaDataset or None
            Instance of the filtered Dataset or None if `inplace=True`
        """
        if partition is not None:
            Partitions.check_partition(partition)
            return self.filter_by_column('partition', partition, mode=mode, inplace=inplace)
        return self

    def filter_by_label_counts(self,
                               min_label_counts: int = None,
                               max_label_counts: int = None,
                               label_counts: int = None,
                               inplace: bool = True) -> MediaDataset:
        """Filter the dataset with respect to the number of samples per label it contains

        Parameters
        ----------
        min_label_counts : int, optional
            Minimum number of samples per label, by default None
        max_label_counts : int, optional
            Maximum number of samples per label, by default None
        label_counts : int, optional
            Exact number of samples per label, by default None
        inplace : bool, optional
            If True, perform operation in-place, by default True

        Returns
        -------
        MediaDataset or None
            Instance of the resulting dataset in case inplace is False, otherwise None
        """
        assert_cond = bool(min_label_counts) + bool(max_label_counts) + bool(label_counts) == 1
        msg = "You must configure only one of: min_label_counts, max_label_counts and label_counts"
        assert assert_cond, msg

        instance = self if inplace else self.copy()
        counts = self.label_counts()
        if min_label_counts is not None:
            labels_to_search = counts[counts >= min_label_counts].index.values
        elif max_label_counts is not None:
            labels_to_search = counts[counts <= min_label_counts].index.values
        else:
            labels_to_search = counts[counts == min_label_counts].index.values

        self.filter_by_categories(labels_to_search, mode='include', inplace=True)
        if not inplace:
            return instance

    def filter_by_expr(self, expr, groupby=None, inplace=True):
        """Method that filters the dataset by `expr`

        Parameters
        ----------
        expr : function
            Function to apply to each column or row.
        groupby : mapping, function, label, or list of labels
            Parameter passed as the `by` argument in the pd.DataFrame.groupby call in the
            returned value of `self.as_dataframe()`
        inplace : bool, optional
            If True, perform operation in-place, by default True

        Returns
        -------
        MediaDataset or None
            Instance of the filtered Dataset or None if `inplace=True`
        """
        instance = self if inplace else self.copy()
        df = instance.as_dataframe()

        if groupby is not None:
            df = df.groupby(groupby, as_index=False).apply(expr).reset_index(drop=True)
        else:
            df = df[df.apply(expr, axis=1)].reset_index(drop=True)

        instance.data = df[instance.data.columns]
        instance._update_dataset_info()

        for partition in instance.get_partitions_names():
            instance._set_partition(partition, df[df["partition"] == partition].index)
        # TODO: set partitions, categories and labelmap
        if not inplace:
            return instance

    def sample_dataset_elements(self,
                                n,
                                by_partition=False,
                                random_state=None,
                                by_items=False,
                                groupby='label',
                                n_min=None,
                                fields_to_sort_for_random='id',
                                inplace=True):
        """Method that samples the elements of a Dataset by grouping them by their `label` and
        taking a random number determined by the value of `n`.

        Parameters
        ----------
        n : str, int, float or dict
            If int, indicates the maximum number of samples to be taken from among the elements
            of each label.
            If float (0, 1), it refers to the percentage of elements to be taken from each
            label.
            If str, indicates the name of the method to be used in the sampling operation.
            The possible method names are:
            * fewer: will take as the sample number the smallest value of the elements grouped
            by label in the set.
            * mean: calculates the average of the element counts for each category and takes
            this value as the maximum number of elements for each category.
            If dict, it must contain an integer for each category that you want to configure,
            in the form {'label_name': max_elements} (default is None)
        by_partition : bool, optional
            Whether to permorm the sampling taking the elements of each partition or not,
            by default False
        random_state : int, optional
            Seed for random number generator, by default None
        by_items : bool, optional
            Whether to apply the sampling of the data from the elements of the data set.
            Otherwise it will be applied by annotations. By default False
        groupby : list of str, str or None, optional
            Field(s) of the dataset by which the data will be grouped for sampling.
            If None, no grouping will be performed and the data will be sampled from the whole
            dataset. By default 'label'
        n_min : int, optional
            Minimum number of elements that the elements of each `groupby` must have, otherwise
            they will be discarded. By default None
        fields_to_sort_for_random : str or list of str, optional
            Field(s) by which the elements of the dataset will be sorted to apply random sampling,
            by default 'id'
        inplace : bool, optional
            If True, perform operation in-place, by default True

        Returns
        -------
        ImageDataset or None
            Instance of the Dataset or None if `inplace=True`
        """
        instance = self if inplace else self.copy()

        if by_partition:
            partitions_dfs = []
            for partition in instance.get_partitions_names():
                df_part = instance.get_rows(partition=partition,
                                            drop_partition_column=False)
                elems = utils.sample_data_elements(
                    data=df_part,
                    n=n,
                    random_state=random_state,
                    by_items=by_items,
                    groupby=groupby,
                    n_min=n_min,
                    fields_to_sort_for_random=fields_to_sort_for_random
                )
                partitions_dfs.append(elems)
            df = pd.concat(partitions_dfs).reset_index(drop=True, inplace=False)
        else:
            df = instance.as_dataframe()
            df = utils.sample_data_elements(
                data=df,
                n=n,
                random_state=random_state,
                by_items=by_items,
                groupby=groupby,
                n_min=n_min,
                fields_to_sort_for_random=fields_to_sort_for_random
            )
        instance.data = df[instance.data.columns]
        instance._update_dataset_info()
        for partition in instance.get_partitions_names():
            instance._set_partition(partition, df[df["partition"] == partition].index)

        if not inplace:
            return instance

    def sample_n_random_elements(self,
                                 n,
                                 by_partition=False,
                                 random_state=None,
                                 by_items=False,
                                 inplace=True):
        """Method that gets n random elements from a dataset.
        It is a shortcut of the `sample_dataset_elements` method that does not group by any field.

        Parameters
        ----------
        n : str, int, float or dict
            If int, indicates the maximum number of samples to be taken from among the elements
            of each label.
            If float (0, 1), it refers to the percentage of elements to be taken from each
            label.
            If str, indicates the name of the method to be used in the sampling operation.
            The possible method names are:
            * fewer: will take as the sample number the smallest value of the elements grouped
            by label in the set.
            * mean: calculates the average of the element counts for each category and takes
            this value as the maximum number of elements for each category.
            If dict, it must contain an integer for each category that you want to configure,
            in the form {'label_name': max_elements} (default is None)
            _description_
        by_partition : bool, optional
            Whether to permorm the sampling taking the elements of each partition or not,
            by default False
        random_state : int, optional
            Seed for random number generator, by default None
        by_items : bool, optional
            Whether to apply the sampling of the data from the elements of the data set.
            Otherwise it will be applied by annotations. By default False
        inplace : bool, optional
            If True, perform operation in-place, by default True

        Returns
        -------
        ImageDataset or None
            Instance of the Dataset or None if `inplace=True`
        """
        return self.sample_dataset_elements(
            n=n, by_partition=by_partition, random_state=random_state, by_items=by_items,
            groupby=None, inplace=inplace)

    def _update_media_info_by_items(self, item_values=None):
        """Method that removes the records that are not present in `media_info` according to the
        elements contained in `item_values`

        Parameters
        ----------
        item_values : list, optional
            Items values that must be present in `media_info`.
            If None, all unique items of the dataset will be used.
            By default None
        """
        if item_values is None:
            item_values = self.data['item'].unique()
        self._set_items(item_values)
        df_media_info = self.get_media_info()
        df_media_info = df_media_info[df_media_info.index.isin(item_values)]
        self._set_media_info(df_media_info)

    def _update_dataset_info(self):
        """Calls to methods `self._set_categories`, `self._set_labelmap` and
        `self._update_media_info_by_items`
        """
        self._set_items()
        self._set_categories()
        self._set_labelmap()
        self._update_media_info_by_items()

    # region debugging
    def df(self, **kwargs):
        return self.as_dataframe(**kwargs)

    def labels(self):
        return self.label_counts()

    def item0(self):
        return self.df().iloc[0]['item']

    def valid0(self):
        return os.path.isfile(self.item0())
    # endregion

    """
    UTILS METHODS
    """

    def copy(self, **kwargs):
        """Creates a copy of the dataset with the information of `data`, `info`, `MEDIA_FIELD_INFO`
        and `MEDIA_FIELD_DIR`.

        Returns
        -------
        ImagePredictionDataset
            Copy of the original dataset
        """
        data = self.data.copy()
        info = copy.deepcopy(self.info)

        kwargs['exec_callback_dataset'] = False
        instance = type(self)(data, info=info, **kwargs)
        instance.params = copy.deepcopy(self.params)

        return instance

    @classmethod
    def _set_id_field_to_media_info(cls, media_info: pd.DataFrame):
        """Assigns the field `cls.MEDIA_FIELDS.ID` in `media_info` from the information contained
        in the field `cls.FIELD_TO_JOIN_IMGS_ANNS`

        Parameters
        ----------
        media_info : pd.DataFrame
            Dataframe with media file information from the dataset
        """
        if len(media_info) == 0:
            return

        stems = media_info[cls.FIELD_TO_JOIN_IMGS_ANNS].apply(lambda x: Path(x).stem)

        if stems.nunique() == media_info[cls.FIELD_TO_JOIN_IMGS_ANNS].nunique():
            media_info[cls.MEDIA_FIELDS.ID] = stems
        else:
            media_info[cls.MEDIA_FIELDS.ID] = media_info[cls.FIELD_TO_JOIN_IMGS_ANNS].copy()

    def _set_field_types_in_media_info(cls, media_info: pd.DataFrame):
        """Casts the data types in the fields contained in `cls.MEDIA_FIELDS.TYPES` in the
        DataFrame `media_info`.

        Parameters
        ----------
        media_info : pd.DataFrame
            Dataframe with media file information from the dataset
        """
        for field_name, field_type in cls.MEDIA_FIELDS.TYPES.items():
            if field_name in media_info.columns:
                media_info[field_name] = media_info[field_name].astype(field_type, errors='ignore')


class MediaPredictionDataset(PredictionDataset):
    class MEDIA_FIELDS():
        ID = "id"
        FILENAME = "file_name"
        WIDTH = "width"
        HEIGHT = "height"
        DATE_CAPTURED = "date_captured"
        LOCATION = "location"

        # TODO: check this fields
        SEQ_ID = "seq_id"
        SEQ_NUM_FRAMES = "seq_num_frames"
        FRAME_NUM = "frame_num"
        SEQ_FRAME_NUM = "seq_frame_num"
        VID_FRAME_NUM = "video_frame_num"
        VID_NUM_FRAMES = "video_num_frames"
        VID_ID = "video_id"

        NAMES = [ID, FILENAME, WIDTH, HEIGHT, DATE_CAPTURED, LOCATION, SEQ_ID, SEQ_NUM_FRAMES,
                 FRAME_NUM, SEQ_FRAME_NUM, VID_FRAME_NUM, VID_NUM_FRAMES, VID_ID]

        TYPES = {
            ID: str,
            LOCATION: str,
            VID_ID: str
        }
