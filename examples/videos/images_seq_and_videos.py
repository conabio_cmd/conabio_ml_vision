import os
import psycopg2
import argparse
import multiprocessing
import pandas as pd
from collections import defaultdict
from multiprocessing import Manager

from conabio_ml_vision.datasets import VideoDataset
from conabio_ml_vision.datasets import ImageDataset
from conabio_ml.utils.utils import str2bool

from params import *

"""
Script to link videos with its sequence of images in SNMB data.
To run it you must install:
    pip install psycopg2-binary
"""


def get_labels_of_imgs(snmb_json, images_dir):
    imgs_ds = ImageDataset.from_json(
        snmb_json, images_dir=images_dir, validate_filenames=False,
        mapping_classes={'empty': 'empty', 'homo sapiens': 'person', '*': 'animal'})
    imgs_ds = imgs_ds.create_image_level_ds()
    df = imgs_ds.as_dataframe()
    df = df[df.image_id.str.contains('PINT2-')]
    return {i: v for i, v in df[['image_id', 'label']].set_index('image_id')['label'].items()}


def add_video_label(imgs_labels, video_md5, fecha_video, conglomerado, video_label, n_secs):
    str_conn_pint2 = os.environ.get('ESPECIES_DB_CONN')
    if str_conn_pint2 is None:
        raise Exception("You must to assign ESPECIES_DB_CONN env variable")

    conn_2 = psycopg2.connect(str_conn_pint2)
    cur_2 = conn_2.cursor()
    try:
        cur_2.execute(query_images_of_a_video.format(conglomerado, fecha_video, n_secs))
        for row_image in cur_2.fetchall():
            image_id = f"PINT2-{row_image[0]}"
            label = imgs_labels.get(image_id)
            if label is None:
                continue
            video_path = video_md5[1:] if video_md5.startswith('/') else video_md5
            video_path = f"{BASE_VIDEOS_PATH}/{video_path}"
            video_label[video_path] = label
    except Exception as e:
        print(f"Error: {e}")
    finally:
        cur_2.close()
        conn_2.close()


def create_videos_dataset(snmb_json, out_csv_path, n_secs, parallel_proc):
    str_conn_pint2 = os.environ.get('ESPECIES_DB_CONN')
    if str_conn_pint2 is None:
        raise Exception("You must to assign ESPECIES_DB_CONN env variable")

    imgs_labels = get_labels_of_imgs(snmb_json, SNMB_IMGS_DIR)
    video_label = Manager().dict()

    conn = psycopg2.connect(str_conn_pint2)
    cur = conn.cursor()
    tuples = []
    try:
        cur.execute(query_videos)
        for row_video in cur.fetchall():
            data = (imgs_labels, row_video[0], row_video[1], row_video[2], video_label, n_secs)
            if parallel_proc:
                tuples.append(data)
            else:
                add_video_label(*data)
    except Exception as e:
        print(f"Error: {str(e)}")
    finally:
        cur.close()
        conn.close()

    if parallel_proc:
        with multiprocessing.Pool(processes=multiprocessing.cpu_count()) as pool:
            pool.starmap(add_video_label, tuples)

    results = defaultdict(list)
    for video_path, label in video_label.items():
        results["item"].append(video_path)
        results["label"].append(label)

    data = pd.DataFrame(results)
    vid_ds = VideoDataset(data)
    vid_ds.to_folder(out_videos_dir, set_videos_dir=True)
    vid_ds.to_csv(out_csv_path)


def video_dataset_sampling(videos_ds_csv, videos_folder_1, videos_folder_2):
    vid_ds_orig = VideoDataset.from_csv(videos_ds_csv)
    vid_ds_1 = vid_ds_orig.sample_dataset_elements(0.5, inplace=False)
    vid_df_1 = vid_ds_1.as_dataframe()
    vid_ds_2 = vid_ds_orig.filter_by_column(
        'video_id', values=vid_df_1.video_id.values, mode='exclude', inplace=False)
    vid_ds_1.to_folder(videos_folder_1)
    vid_ds_2.to_folder(videos_folder_2)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("--create_videos_dataset", default=False, action="store_true")
    parser.add_argument("--video_dataset_sampling", default=False, action="store_true")

    parser.add_argument('--snmb_json', default=None, type=str)
    parser.add_argument('--n_secs', default=20, type=int)
    parser.add_argument("--parallel_proc", type=str2bool, nargs='?', const=True, default=True)

    args = parser.parse_args()

    results_path = os.path.join('results', 'images_seq_and_videos')
    videos_ds_csv = os.path.join(results_path, f"videos_ds.csv")
    out_videos_dir = os.path.join(results_path, 'videos_out')
    videos_folder_1 = os.path.join(results_path, 'videos_snmb_1')
    videos_folder_2 = os.path.join(results_path, 'videos_snmb_2')

    if args.create_videos_dataset:
        create_videos_dataset(snmb_json=args.snmb_json,
                              out_csv_path=videos_ds_csv,
                              n_secs=args.n_secs,
                              parallel_proc=args.parallel_proc)
    if args.video_dataset_sampling:
        video_dataset_sampling(videos_ds_csv,
                               videos_folder_1,
                               videos_folder_2)
