import os
import multiprocessing
from PIL import Image
from PIL import ImageFile
import cv2

from conabio_ml.utils.logger import get_logger

logger = get_logger(__name__)
ImageFile.LOAD_TRUNCATED_IMAGES = True


def parallel_process_images(images_processes):
    """Perform parallel processing on a series of images from a list of tuples, where each
    tuple contains the parameters that indicate the image to be processed and the operations to be
    performed on it. If in a tuple one of the parameters is not in the indicated order or its value
    is `None`, that operation will not be applied to that image. The only mandatory element in each
    tuple is `source_path`
    
    Parameters
    ----------
    images_processes : list of tuples
        Each tuple must contain the positional parameters as follows:
        - source_path (str) : Path of input image.
        - dest_path (str): Path of output image. If None, use the same value of `source_path`.
        - size (2-tuple) : The dimensions (width, height) to which the image will be resized.
        - transpose_oper (int) : Transpose image (flip or rotate in 90 degree steps). One of:
        PIL.Image.FLIP_LEFT_RIGHT, PIL.Image.FLIP_TOP_BOTTOM, PIL.Image.ROTATE_90,
        PIL.Image.ROTATE_180, PIL.Image.ROTATE_270 and PIL.Image.TRANSPOSE.
        - crop (4-tuple): Crop a rectangular portion of the image defined by the 4-tuple in the
        form (left, upper, right, lower) in absolute pixels.
        - square_crop (4-tuple) : Create the square with sides equal to the largest side of the
        rectangle provided in the coordinates and cut it out on the image. The coordinates are
        given in the form (left, upper, right, lower) in absolute pixels.
        - resize_largest_side (int) : Resize an image by scaling from the largest side it should be
        - resize_smallest_side (int) : Resize an image by scaling from the smallest side it should
        be
    """
    with multiprocessing.Pool(processes=multiprocessing.cpu_count()) as pool:
        pool.starmap(process_image, images_processes)


def process_image(source_path,
                  dest_path=None,
                  size=None,
                  transpose_oper=None,
                  crop=None,
                  square_crop=None,
                  resize_largest_side=None,
                  resize_smallest_side=None):
    """Process a single image by applying a series of operations according to the positional
    parameters provided. If a parameter is None, this operation will not be performed
    
    Parameters
    ----------
    source_path : str
        Path of input image
    dest_path : str, optional
        Path of output image. If None, use the same value of `source_path`. By default None
    size : None or tuple of ints, optional
        The dimensions (width, height) to which all images found will be resized. By default None
    transpose_oper : int, optional
        Transpose image (flip or rotate in 90 degree steps)
        One of: PIL.Image.FLIP_LEFT_RIGHT, PIL.Image.FLIP_TOP_BOTTOM, PIL.Image.ROTATE_90,
        PIL.Image.ROTATE_180, PIL.Image.ROTATE_270 and PIL.Image.TRANSPOSE. By default None
    crop : 4-tuple, optional
        Crop a rectangular portion of the image defined by the 4-tuple in the form (left, upper,
        right, lower) in absolute pixels. By default None
    square_crop : 4-tuple, optional
        Create the square with sides equal to the largest side of the rectangle provided in the
        coordinates and cut it out on the image. The coordinates are given in the form
        (left, upper, right, lower) in absolute pixels. By default None
    resize_largest_side : int, optional
        Resize an image by scaling from the largest side it should be, by default None
    resize_smallest_side : int, optional
        Resize an image by scaling from the smallest side it should be, by default None
    """
    try:
        img = Image.open(source_path).convert('RGB')
        dest_path = dest_path if dest_path is not None else source_path
        change = False
        if size is not None and (img.size[0] != size[0] or img.size[1] != size[1]):
            img = img.resize(size)
            change = True
        if (crop is not None or square_crop is not None) and not os.path.isfile(dest_path):
            if square_crop is not None:
                im_width, im_height = img.size
                box = square_crop
                box_width = box[2] - box[0]
                box_height = box[3] - box[1]
                longest = max(box_width, box_height)
                center = (box[0] + box_width/2, box[1] + box_height/2)
                (x1, y1, x2, y2) = (
                    center[0] - longest/2, center[1] - longest/2,
                    center[0] + longest/2, center[1] + longest/2)
                if x1 < 0:
                    x2 -= x1
                    x1 = 0
                if y1 < 0:
                    y2 -= y1
                    y1 = 0
                if x2 > im_width:
                    x1 -= (x2-im_width)
                    x2 = im_width
                if y2 > im_height:
                    y1 -= (y2-im_height)
                    y2 = im_height
                box = (max(x1, 0), max(y1, 0), min(x2, im_width), min(y2, im_height))
            else:
                box = crop
            img = img.crop(box)
            change = True
        if transpose_oper is not None:
            img = img.transpose(transpose_oper)
            change = True
        if resize_largest_side is not None:
            new_w, new_h = largest_size_at_most(img.size[0], img.size[1], resize_largest_side)
            img = img.resize((new_w, new_h))
            change = True
        if resize_smallest_side is not None:
            new_w, new_h = smallest_size_at_least(img.size[0], img.size[1], resize_smallest_side)
            img = img.resize((new_w, new_h))
            change = True
        if change:
            img.save(dest_path)
    except Exception as e:
        logger.exception(f"Exception while processing image: {source_path}")


def largest_size_at_most(width, height, largest_side):
    """Calculate the new values of the size of an image (width, height) according to the scale
    obtained with the largest side that it should have

    Parameters
    ----------
    width : int
        Actual width of the image
    height : int
        Actual height of the image
    largest_side : int
        Size of the largest side the image will have

    Returns
    -------
    2-tuple of int
        New values of (width, height) of the scaled image
    """
    scale = min(largest_side / width, largest_side / height)
    return (int(width * scale), int(height * scale))


def smallest_size_at_least(width, height, smallest_side):
    """Calculate the new values of the size of an image (width, height) according to the scale
    obtained with the smallest side that it should have

    Parameters
    ----------
    width : int
        Actual width of the image
    height : int
        Actual height of the image
    largest_side : int
        Size of the smallest side the image will have

    Returns
    -------
    2-tuple of int
        New values of (width, height) of the scaled image
    """
    scale = max(smallest_side / width, smallest_side / height)
    return (int(width * scale), int(height * scale))


def aspect_preserving_resize(source_paths, dest_paths, largest_side=None, smallest_side=None):
    """Resize a set of images by scaling them from either the largest or the smallest side they
    should have

    Parameters
    ----------
    source_paths : list of str
        List of the paths of images to be resized
    dest_paths : list of str
        List of the paths in which resized images will be stored. It must be of the same lenght of
        `source_paths`
    largest_side : int, optional
        Size of the largest side the images will have, by default None
    smallest_side : int, optional
        Size of the smallest side the images will have, by default None
    """
    if bool(largest_side) == bool(smallest_side):
        raise ValueError("You need to specify either the largest or smallest side")
    if len(source_paths) != len(dest_paths):
        raise ValueError("Lenghts of source_paths and dest_paths does not match")
    if largest_side is not None:
        tuples = [(source, dest, None, None, None, None, largest_side)
                  for source, dest in zip(source_paths, dest_paths)]
    else:
        tuples = [(source, dest, None, None, None, None, None, smallest_side)
                  for source, dest in zip(source_paths, dest_paths)]
    parallel_process_images(tuples)


def apply_clahe_wb_to_image(image_origin, image_dest=None, discard_exist=True):
    """Apply the CLAHE and SimpleWB pre-processing operations to the image found
    in the path `image_origin` and saves the resulting image in the path `image_dest`

    Parameters
    ----------
    image_origin : str
        Path of the image to which processing is applied
    image_dest : str, optional
        Path of the resulting image. If None, it will be the same as `image_origin`.
        Default is None
    discard_exist : bool, optional
        Whether to discard images that already exists in `image_dest` or not, by default True
    """
    discard_exist = discard_exist and image_dest is not None
    image_dest = image_dest or image_origin
    try:
        if not os.path.isfile(image_dest) or discard_exist == False:
            clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(16, 16))
            wb = cv2.xphoto.createSimpleWB()
            wb.setP(0.4)
            temp_img = cv2.imread(image_origin, cv2.IMREAD_COLOR)
            img_wb = wb.balanceWhite(temp_img)
            img_lab = cv2.cvtColor(img_wb, cv2.COLOR_BGR2Lab)
            l, a, b = cv2.split(img_lab)
            res_l = clahe.apply(l)
            res = cv2.merge((res_l, a, b))
            os.makedirs(os.path.dirname(image_dest), exist_ok=True)
            cv2.imwrite(image_dest, cv2.cvtColor(res, cv2.COLOR_Lab2BGR))
    except Exception as ex:
        logger.exception(f"Image '{image_origin}' cannot be converted: {str(ex)}")
