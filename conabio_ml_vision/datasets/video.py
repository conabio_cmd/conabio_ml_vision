#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import uuid
from pathlib import Path
from multiprocessing import Manager
from collections import defaultdict
from typing import List
import time
import pandas as pd
import uuid

from conabio_ml.datasets.dataset import Dataset
from conabio_ml.datasets.dataset import PredictionDataset
from conabio_ml_vision.datasets.images import ImageDataset
from conabio_ml_vision.datasets.media import MediaDataset
from conabio_ml.utils.logger import get_logger
from conabio_ml.utils.utils import get_temp_folder
from conabio_ml.utils.utils import parallel_exec
from conabio_ml_vision.utils.videos_utils import video_to_frames, frames_to_video

logger = get_logger(__name__)


class VideoDataset(MediaDataset):
    """Represent a VideoDataset specification."""

    class MEDIA_FIELDS():
        ID = "id"
        FILENAME = "file_name"
        WIDTH = "width"
        HEIGHT = "height"
        DATE_CAPTURED = "date_captured"
        LOCATION = "location"

        SEQ_ID = 'seq_id'
        SEQ_NUM_FRAMES = 'seq_num_frames'
        SEQ_FRAME_NUM = 'seq_frame_num'

        VID_NUM_FRAMES = 'video_num_frames'
        VID_FRAME_NUM = 'video_frame_num'

        NAMES = [ID, FILENAME, WIDTH, HEIGHT, DATE_CAPTURED, LOCATION, SEQ_ID,
                 SEQ_NUM_FRAMES, SEQ_FRAME_NUM, VID_NUM_FRAMES, VID_FRAME_NUM]

        TYPES = {
            ID: str,
            LOCATION: str,
            # TODO: Think about what to do when these are incorrect
            # WIDTH: int,
            # HEIGHT: int,
            SEQ_ID: str
        }

    class ANNOTATIONS_FIELDS():
        ITEM = "item"
        LABEL = "label"
        ID = "id"
        MEDIA_ID = "video_id"
        PARTITION = "partition"

        NAMES = [ITEM, LABEL, ID,
                 MEDIA_ID, PARTITION
                 ]

        TYPES = {
            ID: str,
            MEDIA_ID: str
        }

    """
    MEDIA FIELD NAMES FOR VIDEOS
    """
    MEDIA_FIELD_INFO = 'videos_info'
    MEDIA_FIELD_DIR = 'videos_dir'

    """
    DATASET CALLBACKS
    """

    def set_videos_info(self, videos_info):
        self._set_media_info(videos_info)

    def set_videos_dir(self, videos_dir, **kwargs):
        kwargs['actual_media_dir'] = kwargs.get("actual_videos_dir", None)
        self.set_media_dir(media_dir=videos_dir, **kwargs)

    def get_videos_info(self, as_dataframe=True, rename_video_id=False):
        return self.get_media_info(as_dataframe=as_dataframe, rename_media_id=rename_video_id)

    def get_videos_dir(self):
        """Gets the directory where the videos are stored.

        Returns
        -------
        str
            Base path in which videos are stored. In case this variable has not been assigned,
            the empty string will be returned
        """
        return self.get_media_dir()

    @classmethod
    def from_csv(cls, source_path: str, **kwargs) -> Dataset.DatasetType:
        kwargs['include_id'] = True
        return super().from_csv(source_path=source_path, **kwargs)

    @classmethod
    def from_folder(cls,
                    source_path: str,
                    extensions: List[str] = [".AVI", ".avi", ".mp4", ".MP4"],
                    label_by_folder_name: bool = True,
                    split_by_folder: bool = True,
                    **kwargs) -> Dataset.DatasetType:
        return super().from_folder(source_path=source_path,
                                   extensions=extensions,
                                   label_by_folder_name=label_by_folder_name,
                                   split_by_folder=split_by_folder,
                                   **kwargs)

    def to_folder(self: Dataset.DatasetType,
                  dest_path: str,
                  split_in_partitions: bool = True,
                  split_in_labels: bool = True,
                  keep_originals: bool = True,
                  set_videos_dir: bool = False,
                  **kwargs) -> str:
        kwargs['parallel_processing'] = True
        return super().to_folder(dest_path=dest_path,
                                 split_in_partitions=split_in_partitions,
                                 split_in_labels=split_in_labels,
                                 keep_originals=keep_originals,
                                 set_videos_dir=set_videos_dir,
                                 **kwargs)

    def _callback_to_folder(self,
                            dest_path,
                            split_in_partitions,
                            split_in_labels,
                            **kwargs):
        set_videos_dir = kwargs.get("set_videos_dir", False)
        if set_videos_dir:
            kwargs['parallel_processing'] = kwargs.get('parallel_set_videos_dir', False)
            self.set_media_dir(media_dir=dest_path,
                               append_partition_to_item_path=split_in_partitions,
                               append_label_to_item_path=split_in_labels,
                               from_basename=True,
                               actual_media_dir='',
                               **kwargs)
            del kwargs['parallel_processing']

    def create_image_dataset(self,
                             frames_folder: str = None,
                             freq_sampling: int = None,
                             frame_numbers: List[int] = None,
                             time_positions: List[float] = None,
                             **kwargs) -> ImageDataset:
        """Create an image dataset from the current video dataset, taking either `freq_sampling`
        frames every second, or the frames whose positions (1-based) are given in the list
        `frame_numbers`, and then store the resulting frame images in the path `frames_folder`

        Parameters
        ----------
        frames_folder : str, optional
            Path to the directory where the resulting frames will be stored.
            For each video a folder will be created inside `frames_folder` with the base name of
            the video without extension, and there the frames will be saved in the form
            `frame0000i.jpg`, where `i` is the number of the frame inside the video,
            i.e. `[1, cv2.CAP_PROP_FRAME_COUNT]`
            If `None` a temporary folder will be used. By default None
        freq_sampling : int, optional
            If provided, it is the number of frames per second to be taken from each video.
            This parameter is mutually exclusive with `time_positions` and `frame_numbers`.
        frame_numbers : dict, optional
            If provided, it is a dict in which each key is an item of the current video dataset,
            and its value is a list containing the 1-based positions of the frames to be taken from
            that video.
            For example, let's assume a dataset of videos that have 30 frames per second and a
            duration of 60 seconds (1,800 frames in total).
            ```
            frame_numbers = {
                'path/to/videos/video01.mp4': [13, 39, 119, 235, ..., 1642, 1701, 1800],
                'path/to/videos/video02.mp4': [1, 9, 93, 192, 345, 355, 432, 546, 899, ..., 1789],
                ...
            }
            ````
            This parameter is mutually exclusive with `time_positions` and `freq_sampling`.
            By default None
        time_positions : dict, optional
            If provided, it is a dict in which each key is an item of the current video dataset,
            and its value is a list containing the time positions in seconds to be taken from that
            video.
            For example, let's assume we have a dataset of videos that are of varying length but
            less than 60 seconds.
            ```
            time_positions = {
                'path/to/videos/video01.mp4': [0.04, 6.56, 10.12, 21.88, 32.52, 48.98, 56.2],
                'path/to/videos/video02.mp4': [1.1, 9.98, 13.0, 19.2, 35.5, 45.3],
                ...
            }
            ````
            This parameter is mutually exclusive with `frame_numbers` and `freq_sampling`.
            By default None

        Returns
        -------
        ImageDataset
            Instance of the created image dataset
        """
        assert_cond = (bool(freq_sampling is not None) +
                       bool(frame_numbers is not None) +
                       bool(time_positions is not None)) == 1
        assert_msg = (f"You must specify ONLY one of the parameters (freq_sampling, "
                      f"frame_numbers, time_positions)")
        assert assert_cond, assert_msg
        if frames_folder is None:
            frames_folder = os.path.join(get_temp_folder(), f'frames_from_videos-{uuid.uuid4()}')

        videos_data = Manager().dict()
        elems = self.get_unique_items()

        frame_numbers_fn = None
        time_positions_fn = None
        if frame_numbers is not None:
            msg = f'Items in frame_numbers are not present in the dataset'
            assert len(set(frame_numbers.keys()) & set(elems)) == len(frame_numbers), msg
            def frame_numbers_fn(elem): return frame_numbers[elem]
            logger.info(f"Converting {self.get_num_rows()} videos to frames, "
                        f"given frame numbers for each video.")
        elif time_positions is not None:
            msg = f'Items in time_positions are not present in the dataset'
            assert len(set(time_positions.keys()) & set(elems)) == len(time_positions), msg
            def time_positions_fn(elem): return time_positions[elem]
            logger.info(f"Converting {self.get_num_rows()} videos to frames, "
                        f"given time positions for each video.")
        else:
            logger.info(f"Converting {self.get_num_rows()} videos to frames, "
                        f"sampling {freq_sampling} frames per second.")

        n_stems = len(set([Path(elem).stem for elem in elems]))

        def get_output_folder(elem):
            if n_stems == len(elems):
                return os.path.join(frames_folder, Path(elem).stem)
            else:
                return os.path.join(frames_folder, f'{uuid.uuid4()}')

        tic = time.time()

        parallel_exec(func=video_to_frames,
                      elements=elems,
                      input_video_file=lambda elem: elem,
                      output_folder=get_output_folder,
                      freq_sampling=freq_sampling,
                      frame_numbers=frame_numbers_fn,
                      time_positions=time_positions_fn,
                      videos_data=videos_data,
                      overwrite=False)

        logger.debug(f'Conversion of videos into frames took {time.time()- tic:.2f} seconds.')

        df = self.as_dataframe().set_index('item')
        imgs_data = defaultdict(list)
        # TODO: implement multilabel case
        for video_item, video_data in videos_data.items():
            vid_row = df.loc[video_item]
            frames_list = video_data['frames_filenames']
            frames_num_video = video_data['frames_num_video']
            n_vid_frames = video_data['frame_count']
            n_seq_frams = len(frames_list)

            ids = [str(uuid.uuid4()) for i in range(n_seq_frams)]
            image_ids = [
                f"{vid_row['video_id']}-{Path(frames_list[i]).stem}" for i in range(n_seq_frams)
            ]
            video_ids = [vid_row['video_id']] * n_seq_frams
            seq_frame_nums = [i+1 for i in range(n_seq_frams)]
            seq_num_frames = [n_seq_frams] * n_seq_frams
            vid_num_frames = [n_vid_frames] * n_seq_frams

            if 'label' in vid_row:
                labels = [vid_row['label']] * n_seq_frams
                imgs_data['label'].extend(labels)
            imgs_data['item'].extend(frames_list)
            imgs_data['id'].extend(ids)
            imgs_data['image_id'].extend(image_ids)
            imgs_data['video_id'].extend(video_ids)
            imgs_data[self.MEDIA_FIELDS.SEQ_FRAME_NUM].extend(seq_frame_nums)
            imgs_data[self.MEDIA_FIELDS.SEQ_NUM_FRAMES].extend(seq_num_frames)
            imgs_data[self.MEDIA_FIELDS.VID_FRAME_NUM].extend(frames_num_video)
            imgs_data[self.MEDIA_FIELDS.VID_NUM_FRAMES].extend(vid_num_frames)

        data = pd.DataFrame(imgs_data)
        imgs_ds = ImageDataset(data, info=self.info, images_dir=frames_folder)
        imgs_ds.get_images_sizes()
        return imgs_ds

    @classmethod
    def create_videos_from_images_ds(cls,
                                     image_ds: ImageDataset,
                                     dest_folder: str,
                                     freq_sampling: int = 1,
                                     split_in_labels: bool = True,
                                     original_vids_ds=None,
                                     **kwargs):
        """Create videos from the ImageDataset `image_ds`, assuming that the videos was previously
        sampled with a sampling frequency `freq_sampling`. The created videos will be stored in
        `dest_folder`

        Parameters
        ----------
        image_ds : ImageDataset
            Image dataset with the information of the video frames to be created. Ideally it was
            previously created with the `create_image_dataset` method
        dest_folder : str
            Path to the directory where the created videos will be saved
        freq_sampling : int, optional
            Number of frames per second taken from the videos when divided into frames,
            by default 1
        split_in_labels : bool, optional
            Whether or not to split the created videos into folders according to the label in the
            'label' column, by default True

        """
        freq_sampling = kwargs.get('Fs', freq_sampling)
        force_videos_creation = kwargs.get('force_videos_creation', False)
        df = image_ds.as_dataframe()

        assert_cond = cls.MEDIA_FIELDS.VID_FRAME_NUM in df.columns and 'video_id' in df.columns
        assert_str = (f"The dataset must contain {cls.MEDIA_FIELDS.VID_FRAME_NUM} "
                      f"and video_id columns")
        assert assert_cond, assert_str
        os.makedirs(dest_folder, exist_ok=True)

        logger.info(f"Creating videos from frames and storing them in the path: {dest_folder}")

        frm_num_fl = cls.MEDIA_FIELDS.VID_FRAME_NUM

        if original_vids_ds is not None:
            original_vids_df = original_vids_ds.as_dataframe()
            original_vids_dir = original_vids_ds.get_media_dir()

        def get_vid_file(vid_id):
            imgs_ds_rows = df[df.video_id == vid_id].sort_values(frm_num_fl)
            if original_vids_ds is not None:
                vid_row = original_vids_df[original_vids_df.video_id == vid_id].iloc[0]
                if original_vids_dir is not None:
                    fname = os.path.relpath(vid_row['item'], original_vids_dir)
                else:
                    fname = vid_row['item']
            elif not str(vid_id).lower().endswith('.mp4'):
                fname = f"{vid_id}.mp4"
            else:
                # fname = os.path.basename(vid_id)
                fname = f"{str(uuid.uuid4())}.mp4"
            if split_in_labels and 'label' in df:
                return os.path.join(dest_folder, imgs_ds_rows.iloc[0]['label'], fname)
            else:
                return os.path.join(dest_folder, fname)

        vids_ids = df['video_id'].unique()
        parallel_exec(
            func=frames_to_video,
            elements=vids_ids,
            images=lambda vid_id: df[df.video_id == vid_id].sort_values(frm_num_fl)['item'].values,
            freq_sampling=freq_sampling,
            output_file_name=get_vid_file,
            force=force_videos_creation)

    @classmethod
    def _download(cls, dest_path, media_base_url, media_dict, set_filename_with_id_and_ext):
        raise ValueError('Not implemented method')


class VideoPredictionDataset(VideoDataset, PredictionDataset):
    """Represent a VideoDataset specification for predictions."""

    class ANNOTATIONS_FIELDS():
        ITEM = "item"
        LABEL = "label"
        # TODO: check for bbox annotations in videos
        # BBOX = "bbox"
        ID = "id"
        SCORE = "score"
        MEDIA_ID = "video_id"
        PARTITION = "partition"

        NAMES = [ITEM, LABEL,
                 #  BBOX,
                 ID, SCORE,
                 MEDIA_ID, PARTITION
                 ]

        TYPES = {
            ID: str,
            MEDIA_ID: str
        }

    def _callback_dataset(self, **kwargs):
        """Method that is called each time a dataset instance is created and that initializes the
        required variables and properties of the dataset.

        Parameters
        ----------
        **kwargs
            Extra named arguments passed to initialization methods and may contains the
            following parameters:
            * round_score_digits: int
                Number of digits of which the field `score` will be rounded
        """
        round_score_digits = kwargs.get("round_score_digits", None)

        super()._callback_dataset(**kwargs)

        if round_score_digits is not None:
            self.set_anns_info_field_by_expr(
                'score', lambda x: round(x.score, round_score_digits), inplace=True)

    """
    FROM METHODS
    """
    # TODO: create MediaPredictionDataset and include these methods:
    @classmethod
    def from_csv(cls, source_path: str, **kwargs):
        """Create a VideoPredictionDataset from a csv file.

        Parameters
        ----------
        source_path : str
            Path of a csv file or a folder containing csv files that will be
            converted into a Dataset.
            The csv file(s) must have at least two columns, which represents
            `item` and `label` data.
        columns : list of str, list of int, or None, optional
            List of column names, or list of column indexes, to load from the
            csv file(s). If None, load all columns. (default is None)
        header : bool, optional
            Whether or not csv contains header at row 0 (default is True).
            If False, you need to map at least `item` and `label` columns by
            position in column_mapping parameter.
        recursive : bool
            Whether or not to seek files also in subdirectories
            (default is True)
        column_mapping : dict, optional
            Dictionary to map column names in dataset.
            If header=True mapping must be done by the column names,
            otherwise must be done by column positions.
            E.g.::
                header=True: column_mapping={"c1": "item", "c2": "label"}
                header=False: column_mapping={0: "item", 1: "label"}
            (default is None)
        include_id : bool, optional
            Wheter or not to include an id for each register (default is False)
        info : dict, optional
            Information of the csv file (default is {})
        **kwargs
            Extra named arguments passed to the `from_csv` method of `Dataset`

        Returns
        -------
        VideoPredictionDataset
            Instance of the created Dataset
        """
        kwargs['split_by_column'] = None
        kwargs['split_by_filenames'] = False
        return super().from_csv(source_path=source_path, **kwargs)

    def to_folder(self: Dataset.DatasetType,
                  dest_path: str,
                  split_in_labels: bool = True,
                  keep_originals: bool = True,
                  set_videos_dir: bool = False,
                  **kwargs) -> str:
        """Create a filesystem representation of the dataset.

        Parameters
        ----------
        dest_path : str
            Path where the dataset files will be stored
        split_in_partitions : bool, optional
            Whether or not to split items in folders with partition names.
            Only works if the dataset has been partitioned previously.
            (default is True)
        split_in_labels : bool, optional
            Whether or not to split items in folders with label names.
            These folders will be below the partition folders (default is True)
        keep_originals : bool, optional
            Whether to keep original files or delete them (default is True)
        set_videos_dir : bool, optional
            Whether to set `videos_dir` to `dest_path` at the end of the processing or not
            (default is False)
        **kwargs
            Extra named arguments passed to `to_folder` method of `Dataset`

        Returns
        -------
        str
            Path of folder created
        """
        return super().to_folder(dest_path=dest_path,
                                 split_in_labels=split_in_labels,
                                 keep_originals=keep_originals,
                                 set_videos_dir=set_videos_dir,
                                 **kwargs)

    """
    FILTER METHODS
    """

    def filter_by_score(self,
                        max_score=None,
                        min_score=None,
                        score=None,
                        column_name="score",
                        inplace=True):
        """Method that filters the predictions by the `score` column

        Parameters
        ----------
        max_score : float, optional
            Float number in [0.,1.] that indicates the maximum value of the `score` column,
            by default None
        min_score : float, optional
            Float number in [0.,1.] that indicates the minimum value of the `score` column,
            by default None
        score : float, optional
            Float number in [0.,1.] that indicates the exact value of the `score` column,
            by default None
        column_name : str, optional
            Column name of the score field in `self.data`, by default "score"
        inplace : bool, optional
            If True, perform operation in-place, by default True

        Returns
        -------
        VideoPredictionDataset or None
            Instance of the resulting dataset or None if `inplace=True`
        """
        instance = self if inplace else self.copy()
        if max_score is not None:
            instance.data = instance.data.loc[instance.data[column_name] <= float(max_score)]
        if min_score is not None:
            instance.data = instance.data.loc[instance.data[column_name] >= float(min_score)]
        if score is not None:
            instance.data = instance.data.loc[instance.data[column_name] == float(score)]
        instance.data = instance.data.reset_index(drop=True)
        instance._update_dataset_info()
        if not inplace:
            return instance

    def only_first_prediction(self):
        """Filters out all predictions in the dataset except the one with the highest score

        Returns
        -------
        VideoPredictionDataset
            Instance of the Dataset
        """
        self.data.sort_values(by='score', axis=0, ascending=False, inplace=True)
        self.data.drop_duplicates(subset='item', keep='first', inplace=True)
        self.data = self.data.reset_index(drop=True)
        self._update_dataset_info()

        return self
