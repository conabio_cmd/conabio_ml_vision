from conabio_ml.trainer.trainer_config import TrainerConfig
from typing import TypeVar


class TFSlimTrainConfig(TrainerConfig):
    """Class that allows setting up a training environment for the 
    Tensorflow Slim library.
    """
    TFSlimTrainConfigType = TypeVar('TFSlimTrainConfigType', bound='TFSlimTrainConfig')

    def __init__(self: TrainerConfig.TrainerConfigType,
                 master,
                 num_clones,
                 total_clones_in_cluster,
                 clone_on_cpu,
                 worker_hosts,
                 ps_hosts,
                 job_name,
                 worker_replicas,
                 num_ps_tasks,
                 num_readers,
                 num_preprocessing_threads,
                 sync_replicas,
                 replicas_to_aggregate,
                 task,
                 log_every_n_steps,
                 save_summaries_secs,
                 save_interval_secs):
        self.master = master                    # Unused: Default value = '' in local training
        self.num_clones = num_clones
        self.total_clones_in_cluster = total_clones_in_cluster
        self.clone_on_cpu = clone_on_cpu
        self.worker_hosts = worker_hosts
        self.ps_hosts = ps_hosts
        self.job_name = job_name
        self.worker_replicas = worker_replicas  # Unused: Default value = 1 in local training
        self.num_ps_tasks = num_ps_tasks        # Unused: Default value = 0 in local training
        self.num_readers = num_readers
        self.num_preprocessing_threads = num_preprocessing_threads
        self.sync_replicas = sync_replicas
        self.replicas_to_aggregate = replicas_to_aggregate  # Used only if sync_replicas is True
        self.task = task
        self.log_every_n_steps = log_every_n_steps
        self.save_summaries_secs = save_summaries_secs
        self.save_interval_secs = save_interval_secs

    @classmethod
    def create(cls,
               master: str = '',
               num_clones: int = 1,
               total_clones_in_cluster: int = None,
               clone_on_cpu: bool = False,
               worker_hosts: str = '',
               ps_hosts: str = '',
               job_name: str = '',
               worker_replicas: int = 1,
               num_ps_tasks: int = 0,
               num_readers: int = 4,
               num_preprocessing_threads: int = 4,
               sync_replicas: bool = False,
               replicas_to_aggregate: int = 1,
               task: int = 0,
               log_every_n_steps: int = 10,
               save_summaries_secs: int = 600,
               save_interval_secs: int = 600) -> TFSlimTrainConfigType:
        """Method that allows to create a configuration of a training 
        environment for the Tensorflow Slim library.

        Parameters
        ----------
        master : str, optional
            The address of the TensorFlow master to use. (default is '')
        num_clones : int, optional
            Number of model clones to deploy. (default is 1)
        total_clones_in_cluster : int or None, optional
            Total number of model clones to deploy in the cluster.
            This value must be the total number of clones in all replicas of 
            the cluster.
            (default is None)
        clone_on_cpu : bool, optional
            Use CPUs to deploy clones. (default is False)
        worker_hosts : str, optional
            Comma-separated list of hostname:port for the worker jobs. 
            E.g. 'machine1:2222,machine2:1111,machine2:2222'
            (default is '')
        ps_hosts : str, optional
            Comma-separated list of hostname:port for the parameter server 
            jobs. 
            E.g. 'machine1:2222,machine2:1111,machine2:2222'
            (default is '')
        job_name : str, optional
            In a distributed trainig session, whether is a worker or ps job. 
            Allowed values are "ps" and "worker" (default is '')
        worker_replicas : int, optional
            Number of worker replicas. (default is 1)
        num_ps_tasks : int, optional
            The number of parameter servers. If the value is 0, then the 
            parameters are handled locally by the worker. 
            (default is 0)
        num_readers : int, optional
            The number of parallel readers that read data from the dataset.
            (default is 4)
        num_preprocessing_threads : int, optional
            The number of threads used to create the batches. 
            This option is only available if `fine_tune_checkpoint_type` is 
            'classification'.
            (default is 4)
        sync_replicas : bool, optional
            Whether or not to synchronize the replicas during training. 
            (default is False)
        replicas_to_aggregate : int, optional
            Number of replicas to aggregate before making parameter updates. 
            (default is 1)
        task : int, optional
            Task id of the replica running the training. (default is 0)
        log_every_n_steps : int, optional
            The frequency with which logs are print. 
            This option is only available if `fine_tune_checkpoint_type` is 
            'classification'. (default is 10)
        save_summaries_secs : int, optional
            The frequency with which summaries are saved, in seconds. 
            (default is 600)
        save_interval_secs : int, optional
            The frequency with which the model is saved, in seconds. 
            (default is 600)

        Returns
        -------
        TFSlimTrainConfig
            A training environment instance for the Tensorflow Slim library.
        """
        return cls(master,
                   num_clones,
                   total_clones_in_cluster,
                   clone_on_cpu,
                   worker_hosts,
                   ps_hosts,
                   job_name,
                   worker_replicas,
                   num_ps_tasks,
                   num_readers,
                   num_preprocessing_threads,
                   sync_replicas,
                   replicas_to_aggregate,
                   task,
                   log_every_n_steps,
                   save_summaries_secs,
                   save_interval_secs)


class TFObjectDetectionAPITrainConfig(TrainerConfig):
    """Class that allows setting up a training environment for the 
    Tensorflow Object Detection API.
    """
    TFObjectDetectionAPITrainConfigType = TypeVar(
        'TFObjectDetectionAPITrainConfigType', bound='TFObjectDetectionAPITrainConfig')

    def __init__(self: TrainerConfig.TrainerConfigType,
                 master,
                 num_clones,
                 clone_on_cpu,
                 worker_hosts,
                 ps_hosts,
                 master_host,
                 job_name,
                 worker_replicas,
                 ps_tasks,
                 num_readers,
                 shuffle,
                 shuffle_buffer_size,
                 filenames_shuffle_buffer_size,
                 num_parallel_batches,
                 num_prefetch_batches,
                 read_block_length,
                 max_number_of_boxes,
                 sync_replicas,
                 replicas_to_aggregate,
                 task,
                 keep_checkpoint_every_n_hours):
        self.master = master                    # Unused: Default value = '' in local training
        self.num_clones = num_clones
        self.clone_on_cpu = clone_on_cpu
        self.worker_hosts = worker_hosts
        self.ps_hosts = ps_hosts
        self.master_host = master_host          # Used only in detection models
        self.job_name = job_name
        self.worker_replicas = worker_replicas  # Unused: Default value = 1 in local training
        self.ps_tasks = ps_tasks                # Unused: Default value = 0 in local training
        self.num_readers = num_readers          # Unused: Part of input_reader
        self.shuffle = shuffle
        self.shuffle_buffer_size = shuffle_buffer_size
        self.filenames_shuffle_buffer_size = filenames_shuffle_buffer_size
        self.num_parallel_batches = num_parallel_batches
        self.num_prefetch_batches = num_prefetch_batches
        self.read_block_length = read_block_length
        self.max_number_of_boxes = max_number_of_boxes
        self.sync_replicas = sync_replicas
        self.replicas_to_aggregate = replicas_to_aggregate  # Used only if sync_replicas is True
        self.task = task
        self.keep_checkpoint_every_n_hours = keep_checkpoint_every_n_hours

    @classmethod
    def create(cls,
               master: str = '',
               num_clones: int = 1,
               clone_on_cpu: bool = False,
               worker_hosts: str = '',
               ps_hosts: str = '',
               master_host: str = '',
               job_name: str = '',
               worker_replicas: int = 1,
               ps_tasks: int = 0,
               num_readers: int = 4,
               shuffle: bool = True,
               shuffle_buffer_size: int = 2048,
               filenames_shuffle_buffer_size: int = 100,
               num_parallel_batches: int = 8,
               num_prefetch_batches: int = 2,
               read_block_length: int = 32,
               max_number_of_boxes: int = 100,
               sync_replicas: bool = False,
               replicas_to_aggregate: int = 1,
               task: int = 0,
               keep_checkpoint_every_n_hours: float = 1.0) \
            -> TFObjectDetectionAPITrainConfigType:
        """Method that allows to create a configuration of a training 
        environment for the Tensorflow Object Detection API.

        Parameters
        ----------
        master : str, optional
            The address of the TensorFlow master to use. (default is '')
        num_clones : int, optional
            Number of model clones to deploy. (default is 1)
        clone_on_cpu : bool, optional
            Use CPUs to deploy clones. (default is False)
        worker_hosts : str, optional
            Comma-separated list of hostname:port for the worker jobs. 
            E.g. 'machine1:2222,machine2:1111,machine2:2222'
            (default is '')
        ps_hosts : str, optional
            Comma-separated list of hostname:port for the parameter server 
            jobs. 
            E.g. 'machine1:2222,machine2:1111,machine2:2222'
            (default is '')
        master_host : str, optional
            String that respresents a hostname:port for the master server job. 
            E.g. 'machine1:2222'
            (default is '')
        job_name : str, optional
            In a distributed trainig session, whether is a worker or ps job. 
            Allowed values are "ps" and "worker" (default is '')
        worker_replicas : int, optional
            Number of worker replicas. (default is 1)
        ps_tasks : int, optional
            The number of parameter servers. If the value is 0, then the 
            parameters are handled locally by the worker. 
            In image classification is also known as `num_ps_tasks`.
            (default is 0)
        num_readers : int, optional
            The number of parallel readers that read data from the dataset.
            (default is 4)
        shuffle : bool, optional
            Whether data should be processed in the order they are read in, 
            or shuffled randomly. (default is True)
        shuffle_buffer_size : int, optional
            Buffer size to be used when shuffling. (default is 2048)
        filenames_shuffle_buffer_size : int, optional
            Buffer size to be used when shuffling file names. (default is 100)
        num_parallel_batches : int, optional
            Number of batches to produce in parallel. If this is run on a 2x2 
            TPU set this to 8. (default is 8)
        num_prefetch_batches : int, optional
            Number of batches to prefetch. Prefetch decouples input pipeline and 
            model so they can be pipelined resulting in higher throughput. Set this 
            to a small constant and increment linearly until the improvements become 
            marginal or you exceed your cpu memory budget. Setting this to -1, 
            automatically tunes this value for you. (default is 2)
        read_block_length : int, optional
            Number of records to read from each reader at once.
            (default is 32)
        max_number_of_boxes : int, optional
            Maximum number of boxes to pad to during training / evaluation. 
            Set this to at least the maximum amount of boxes in the input data, 
            otherwise some groundtruth boxes may be clipped. (default is 100)
        sync_replicas : bool, optional
            Whether or not to synchronize the replicas during training. 
            (default is False)
        replicas_to_aggregate : int, optional
            Number of replicas to aggregate before making parameter updates. 
            (default is 1)
        task : int, optional
            Task id of the replica running the training. (default is 0)
        keep_checkpoint_every_n_hours : float, optional
            How frequently to keep checkpoints. 
            This option is only available if `fine_tune_checkpoint_type` is 
            'detection'. (default is 1.0)

        Returns
        -------
        TFObjectDetectionAPITrainConfig
            A training environment instance for the Tensorflow Object
            Detection API.
        """
        return cls(master,
                   num_clones,
                   clone_on_cpu,
                   worker_hosts,
                   ps_hosts,
                   master_host,
                   job_name,
                   worker_replicas,
                   ps_tasks,
                   num_readers,
                   shuffle,
                   shuffle_buffer_size,
                   filenames_shuffle_buffer_size,
                   num_parallel_batches,
                   num_prefetch_batches,
                   read_block_length,
                   max_number_of_boxes,
                   sync_replicas,
                   replicas_to_aggregate,
                   task,
                   keep_checkpoint_every_n_hours)
