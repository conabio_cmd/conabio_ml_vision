import argparse
import os
from shutil import move

from conabio_ml.utils.utils import get_temp_folder, download_file
from conabio_ml_vision.datasets import LILADataset
from conabio_ml_vision.utils import lila_utils
from conabio_ml_vision.utils.aux_utils import get_species_of_interest
from conabio_ml_vision.datasets import ImageDataset


def main(results_dir, metadata_dir, filter_specs_of_int, mapping_spcs):

    metadata_table = lila_utils.read_lila_metadata(metadata_dir)


    cats = (
        get_species_of_interest(birds_csv=metadata_dir, mammals_csv=metadata_dir)
        if filter_specs_of_int else None
    )

    datasets = []
    for ds_name in metadata_table.keys():
        metadata_table[ds_name]['json_filename'] = lila_utils.get_json_file_for_dataset(
            ds_name=ds_name,
            metadata_dir=metadata_dir,
            metadata_table=metadata_table)

        ds_name_col = ds_name.split('_bbox')[0] if ds_name.endswith('_bbox') else ds_name

        mapping_classes = './files/lila-taxonomy-mapping_release.csv'
        if not os.path.isfile(mapping_classes):
            mapping_url = os.environ.get('LILA_TAXONOMY_MAPPING_URL',
                                         LILADataset.DEFAULTS['lila-taxonomy-mapping-url'])
            model_path = download_file(mapping_url, get_temp_folder())
            move(model_path, mapping_classes)

        ds = LILADataset.from_json(
            source_path=metadata_table[ds_name]['json_filename'],
            categories=cats,
            # media_base_url=metadata_table[ds_name]['sas_url'],
            mapping_classes='./files/lila-taxonomy-mapping_release.csv',
            mapping_classes_from_col='query',
            mapping_classes_to_col='scientific_name',
            mapping_classes_filter_expr=lambda row: row['dataset_name'] == ds_name_col,
            collection=ds_name)
        datasets.append(ds)

    lila_ds = ImageDataset.from_datasets(*datasets)
    if mapping_spcs:
        lila_ds.map_categories(mapping_classes={
            "canis familiaris": "canis lupus familiaris",
            "pecari tajacu": "dicotyles tajacu",
            "tapirus bairdii": "tapirella bairdii",
            "gallus gallus domesticus": "gallus gallus"
        })
    lila_ds.label_counts().to_csv(os.path.join(results_dir, 'lila_label_counts.csv'))


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("--filter_specs_of_int", default=False, action="store_true")
    parser.add_argument("--mapping_spcs", default=False, action="store_true")
    
    args = parser.parse_args()
    
    metadata_dir = './metadatadir'
    results_dir = './results'

    os.makedirs(results_dir, exist_ok=True)

    main(results_dir, metadata_dir, args.filter_specs_of_int, args.mapping_spcs)
    