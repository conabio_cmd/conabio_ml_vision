import os
import math
import re
from operator import itemgetter
from typing import TypeVar

import tensorflow as tf

from conabio_ml.datasets.dataset import Dataset, Partitions
from conabio_ml.trainer.trainer import Trainer
from conabio_ml.trainer.builders import optimizer_builder

from conabio_ml_vision.trainer.model import ObjectDetectionModel, ClassificationModel
from conabio_ml_vision.trainer.trainer_config import TFSlimTrainConfig,\
    TFObjectDetectionAPITrainConfig

from conabio_ml.utils.logger import get_logger
from conabio_ml.utils.utils import get_and_validate_args

logger = get_logger(__name__)


class ObjectDetectionTrainer(Trainer):
    """Object Detection model trainer.
    This class provides a generic training method that can be used to train an 
    ObjectDetectionModel.

    Attributes
    ----------
    ObjectDetectionTrainerType : ObjectDetectionTrainer
        Defined type to chain training processes in a Pipeline
    args_train_def : dict
        Dictionary that contains the arguments definition with the argument 
        type and default value for each train configuration entry.
    dataset : Dataset.DatasetType
        The dataset with which the model will be fitted.
    model : ObjectDetectionModel.ObjectDetectionModelType
        The object detection model instance that will be fitted.
    execution_config : 
            TFObjectDetectionAPITrainConfig.TFObjectDetectionAPITrainConfigType
        Configuration of a training environment for the Tensorflow Object 
        Detection API.
    train_config : dict
        A dictionary that contains a train configuration. 
    optimizer : dict
        Dictionary that contains optimizer configuration for training.
    train_dir : str
        Path directory to store result training files.
    """

    ObjectDetectionTrainerType = TypeVar(
        'ObjectDetectionTrainerType', bound='ObjectDetectionTrainer')

    args_train_def = {
        'train_dir': {
            'type': str,
            'default': '.'
        },
        'batch_size': {
            'type': int,
            'default': 1
        },
        'load_all_detection_checkpoint_vars': {
            'type': bool,
            'default': False
        },
        'num_steps': {
            'type': int,
            'default': 0
        },
        'num_epochs': {
            'type': int
        },
        'partition': {
            'type': str,
            'default': Partitions.TRAIN
        },
        'data_augmentation_options': {
            'type': list
        },
        'gradient_clipping_by_norm': {
            'type': float,
            'default': 0.0
        },
        'dropout_keep_probability': {
            'type': float,
            'default': 1.0
        },
        'optimizer': {
            'type': dict,
            'default': {}
        },
        'startup_delay_steps': {
            'type': int,
            'default': 15
        },
        'bias_grad_multiplier': {
            'type': float,
            'default': 0.0
        },
        'update_trainable_variables': {
            'type': list
        },
        'freeze_variables': {
            'type': list
        },
        'merge_multiple_label_boxes': {
            'type': bool,
            'default': False
        },
        'use_multiclass_scores': {
            'type': bool,
            'default': False
        },
        'add_regularization_loss': {
            'type': bool,
            'default': True
        },
        'unpad_groundtruth_tensors': {
            'type': bool,
            'default': True
        },
        'retain_original_images': {
            'type': bool,
            'default': False
        },
        'use_bfloat16': {
            'type': bool,
            'default': False
        },
        'summarize_gradients': {
            'type': bool,
            'default': False
        },
        'tf_config': {
            'type': str
        }
    }

    RMSPROP_OPT = 'rms_prop_optimizer'
    MOMENTUM_OPT = 'momentum_optimizer'
    ADAM_OPT = 'adam_optimizer'
    ADADELTA_OPT = 'adadelta_optimizer'
    ADAGRAD_OPT = 'adagrad_optimizer'
    FTRL_OPT = 'ftrl_optimizer'
    SGD_OPT = 'sgd_optimizer'

    CONSTANT_LR = 'constant_learning_rate'
    EXPONENTIAL_LR = 'exponential_decay_learning_rate'
    MANUAL_LR = 'manual_step_learning_rate'
    COSINE_LR = 'cosine_decay_learning_rate'
    POLYNOMIAL_LR = 'polynomial_decay_learning_rate'
    VALID_LEARNING_RATES = [
        CONSTANT_LR, EXPONENTIAL_LR, MANUAL_LR, COSINE_LR, POLYNOMIAL_LR]
    VALID_OPTIMIZERS = [
        RMSPROP_OPT, MOMENTUM_OPT, ADAM_OPT, ADADELTA_OPT,
        ADAGRAD_OPT, FTRL_OPT, SGD_OPT]

    optimizers_mapping = {
        RMSPROP_OPT: optimizer_builder.RMSPROP_OPT,
        MOMENTUM_OPT: optimizer_builder.MOMENTUM_OPT,
        ADAM_OPT: optimizer_builder.ADAM_OPT,
        ADADELTA_OPT: optimizer_builder.ADADELTA_OPT,
        ADAGRAD_OPT: optimizer_builder.ADAGRAD_OPT,
        FTRL_OPT: optimizer_builder.FTRL_OPT,
        SGD_OPT: optimizer_builder.SGD_OPT
    }

    learning_rates_mapping = {
        CONSTANT_LR: optimizer_builder.CONSTANT_LR,
        EXPONENTIAL_LR: optimizer_builder.EXPONENTIAL_LR,
        MANUAL_LR: optimizer_builder.MANUAL_LR,
        COSINE_LR: optimizer_builder.COSINE_LR,
        POLYNOMIAL_LR: optimizer_builder.POLYNOMIAL_LR
    }

    def __init__(self,
                 dataset: Dataset.DatasetType,
                 model: ObjectDetectionModel.ObjectDetectionModelType,
                 execution_config:
                 TFObjectDetectionAPITrainConfig.TFObjectDetectionAPITrainConfigType,
                 train_config: dict = {}):
        self.dataset = dataset
        self.model = model
        self.train_config = get_and_validate_args(train_config, self.args_train_def)
        self.execution_config = vars(execution_config)
        self.optimizer = self.train_config['optimizer']
        self.train_dir = self.train_config['train_dir']
        del self.train_config['train_dir']

    @classmethod
    def train(cls: ObjectDetectionTrainerType,
              dataset: Dataset.DatasetType,
              model: ObjectDetectionModel.ObjectDetectionModelType,
              execution_config:
              TFObjectDetectionAPITrainConfig.TFObjectDetectionAPITrainConfigType,
              train_config: dict = {}) -> ObjectDetectionModel.ObjectDetectionModelType:
        """Training function for detection models.
        
        Parameters
        ----------
        dataset : Dataset.DatasetType
            The dataset with which the model will be fitted.
        model : Model.ModelType
            The object detection model instance that will be fitted.
        execution_config : TFObjectDetectionAPITrainConfig.
                TFObjectDetectionAPITrainConfigType
            Configuration of a training environment for the Tensorflow Object 
            Detection API.
        train_config : dict, optional
            A dictionary that contains a train configuration. (default is {})

        Returns
        -------
        Model.ModelType
            Instance of the Model that was adjusted by the trainer
        """
        raise Exception(
            f"This method has been removed due to deprecation. "
            f"If you need to use this type of training, browse "
            f"for the implementation in the repository history, and include again the "
            f"conabio_ml_vision/trainer/models module that was removed.")

    """
    GET METHODS
    """

    def get_model_config(self):
        """Get a detection model configuration to be passed to a training 
        function. Set num_classes and dropout_keep_probability in model_config.

        Returns
        -------
        model_pb2.DetectionModel
            A protocol buffer message for a model config.
        """
        raise Exception(
            f"This method has been removed due to deprecation. "
            f"If you need to use this type of functionality, browse "
            f"for the implementation in the repository history, and include again the "
            f"conabio_ml_vision/trainer/models module that was removed.")

    def get_input_config(self):
        """Get input config to be passed to a training function.
        Set tf_record_input_reader and label_map_path in train_input_config.

        Returns
        -------
        input_reader_pb2.InputReader
            A protocol buffer message for a input reader.
        """
        if not self.dataset.is_detection_dataset():
            raise ValueError(
                f"You must to create a dataset of type detection in "
                f"order to use ObjectDetectionTrainer class.")
        if 'partition' in self.train_config:
            Partitions.check_partition(self.train_config['partition'])
            partition = self.train_config['partition']
            del self.train_config['partition']
        else:
            partition = Partitions.TRAIN
        self.partition = partition
        return self.get_input_reader(partition)

    def get_train_config(self):
        """Get detection config object to be passed to a training function.
        Set sync_replicas, startup_delay_steps, replicas_to_aggregate, 
        keep_checkpoint_every_n_hours, num_steps in train_config.

        Returns
        -------
        train_pb2.TrainConfig
            A protocol buffer message for a train config.
        """
        raise Exception(
            f"This method has been removed due to deprecation. "
            f"If you need to use this type of functionality, browse "
            f"for the implementation in the repository history, and include again the "
            f"conabio_ml_vision/trainer/models module that was removed.")

    def get_input_reader(self, partition):
        """Get input reader for a partition.
        
        Parameters
        ----------
        partition : str
            Partition name for the input reader.

        Returns
        -------
        input_reader_pb2.InputReader
            A protocol buffer message for a input reader config.
        """
        raise Exception(
            f"This method has been removed due to deprecation. "
            f"If you need to use this type of functionality, browse "
            f"for the implementation in the repository history, and include again the "
            f"conabio_ml_vision/trainer/models module that was removed.")

    def get_optimizer_name(self):
        """Get optimizer name in train configuration.

        Returns
        -------
        str
            A valid optimizer name
        """
        optimizers_names = [x for x in self.optimizer.keys() if x in self.VALID_OPTIMIZERS]
        if not len(optimizers_names) == 1:
            raise ValueError(f"You must specify one optimizer configuration.")
        return optimizers_names[0]

    def get_optimizer_config(self):
        """Get optimizer configuration.

        Returns
        -------
        dict
            A dictionary with optimizer configuration
        """
        optimizer_name = self.get_optimizer_name()
        return self.optimizer[optimizer_name]

    def get_learning_rate_name(self):
        """Get learning rate name in optimizer configuration.

        Returns
        -------
        str
            A valid learning rate name
        """
        optimizer_config = self.get_optimizer_config()
        if 'learning_rate' not in optimizer_config:
            raise ValueError("You must especify a learning rate configuration")
        learning_rate_config = optimizer_config['learning_rate']
        lr_names = [x for x in learning_rate_config.keys() if x in self.VALID_LEARNING_RATES]
        if not len(lr_names) == 1:
            raise ValueError(f"You must specify one learning rate configuration")
        return lr_names[0]

    """
    SET METHODS
    """

    def set_optimizer_config(self):
        """Set configuration parameters for optimizer in terms of steps instead 
        of a given epochs configuration.
        """
        if not self.optimizer:
            return
        steps_per_epoch = math.ceil(
            self.dataset.get_num_rows(self.partition) / self.train_config['batch_size'])
        optimizer_config = self.get_optimizer_config()
        lr_name = self.get_learning_rate_name()
        lr_config = optimizer_config['learning_rate'][lr_name]
        if lr_name == self.EXPONENTIAL_LR:
            if 'decay_epochs' in lr_config:
                lr_config['decay_steps'] = steps_per_epoch * lr_config['decay_epochs']
                del lr_config['decay_epochs']
            elif 'decay_steps' not in lr_config:
                raise ValueError(
                    "You must specify either decay_epochs or decay_steps "
                    "for exponential learning rate configuration.")
            if 'burnin_epochs' in lr_config:
                lr_config['burnin_steps'] = steps_per_epoch * lr_config['burnin_epochs']
                del lr_config['burnin_epochs']
        elif lr_name == self.COSINE_LR:
            if 'total_epochs' in lr_config:
                lr_config['total_steps'] = steps_per_epoch * lr_config['total_epochs']
                del lr_config['total_epochs']
            if 'warmup_epochs' in lr_config:
                lr_config['warmup_steps'] = steps_per_epoch * lr_config['warmup_epochs']
                del lr_config['warmup_epochs']
            if 'hold_base_rate_steps' in lr_config:
                lr_config['hold_base_rate_steps'] = \
                    steps_per_epoch * lr_config['hold_base_rate_steps']
                del lr_config['hold_base_rate_steps']
        elif lr_name == self.MANUAL_LR:
            if 'schedule' not in lr_config:
                raise ValueError("You must specify a schedule for manual_step_learning_rate")
            if type(lr_config['schedule']) is not list:
                raise ValueError("Schedule for manual_step_learning_rate must be a list.")
            for schedule in lr_config['schedule']:
                if 'epoch' in schedule:
                    schedule['step'] = steps_per_epoch * schedule['epoch']
                    del schedule['epoch']

    def set_optimizer_and_lr_mappings(self):
        """Map optimizer and learning rate names from Tensorflow Object 
        Detection API to standard conabio_ml API names.
        """
        if not self.optimizer:
            return
        optimizer_name = self.get_optimizer_name()
        lr_name = self.get_learning_rate_name()
        optimizer_new_name = self.optimizers_mapping[optimizer_name]
        lr_new_name = self.learning_rates_mapping[lr_name]
        self.optimizer[optimizer_new_name] = self.optimizer.pop(optimizer_name)
        self.optimizer[optimizer_new_name]['learning_rate'][lr_new_name] = \
            self.optimizer[optimizer_new_name]['learning_rate'].pop(lr_name)

    """
    AUX METHODS
    """

    def export_inference_graph(self,
                               model_config,
                               train_config,
                               input_config,
                               dest_path):
        """Exports inference graph for the model specified in the model, train
        and input config.

        Parameters
        ----------
        model_config : model_pb2.DetectionModel
            A protocol buffer message for a model config.
        train_config : train_pb2.TrainConfig
            A protocol buffer message for a train config.
        input_config : input_reader_pb2.InputReader
            A protocol buffer message for a input reader.
        """
        raise Exception(
            f"This method has been removed due to deprecation. "
            f"If you need to use this type of functionality, browse "
            f"for the implementation in the repository history, and include again the "
            f"conabio_ml_vision/trainer/models module that was removed.")

    @classmethod
    def create_inference_graph(cls: ObjectDetectionTrainerType,
                               dataset: Dataset.DatasetType,
                               model: ObjectDetectionModel.ObjectDetectionModelType,
                               execution_config: TFObjectDetectionAPITrainConfig.
                               TFObjectDetectionAPITrainConfigType,
                               train_config: dict = None,
                               dest_path: str = None) -> \
            ObjectDetectionModel.ObjectDetectionModelType:
        """Training function for detection models.
        
        Parameters
        ----------
        dataset : Dataset.DatasetType
            The dataset with which the model will be fitted.
        model : Model.ModelType
            The object detection model instance that will be fitted.
        execution_config : TFObjectDetectionAPITrainConfig
                .TFObjectDetectionAPITrainConfigType
            Configuration of a training environment for the Tensorflow Object 
            Detection API.
        train_config : dict, optional
            A dictionary that contains a train configuration. (default is {})

        Returns
        -------
        Model.ModelType
            Instance of the Model that was adjusted by the trainer
        """
        train_config = {} if train_config is None else train_config
        instance = cls(dataset, model, execution_config, train_config)
        model_config = instance.get_model_config()
        input_config = instance.get_input_config()
        train_config = instance.get_train_config()
        instance.export_inference_graph(model_config, train_config, input_config, dest_path)


class ClassificationTrainer(Trainer):
    """Classification model trainer.
    This class provides a generic training method that can be used to train a 
    ClassificationModel.

    Attributes
    ----------
    ClassificationTrainerType : ClassificationTrainer
        Defined type to chain training processes in a Pipeline.
    args_train_def : dict
        Dictionary that contains the arguments definition with the argument 
        type and default value for each train config entry.
    dataset : Dataset.DatasetType
        The dataset with which the model will be fitted.
    model : ClassificationModel.ClassificationModelType
        The classification model instance that will be fitted.
    execution_config : TFSlimTrainConfig.TFSlimTrainConfigType
        Configuration of a training environment for the Tensorflow Slim 
        Librery.
    train_config : dict
        A dictionary that contains a train configuration. 
    optimizer : dict
        Dictionary that contains optimizer configuration for training.
    train_dir : str
        Path directory for store training checkpoint files.
    """

    ClassificationTrainerType = TypeVar('ClassificationTrainerType', bound='ClassificationTrainer')

    args_train_def = {
        'train_dir': {
            'type': str,
            'default': '/tmp/tfmodel/'
        },
        'batch_size': {
            'type': int,
            'default': 32
        },
        'checkpoint_exclude_scopes': {
            'type': str,
            'default': None
        },
        'trainable_scopes': {
            'type': str,
            'default': None
        },
        'num_steps': {
            'type': int
        },
        'num_epochs': {
            'type': int
        },
        'partition': {
            'type': str,
            'default': Partitions.TRAIN
        },
        'dropout_keep_probability': {
            'type': float
        },
        'optimizer': {
            'type': dict,
            'default': {}
        }
    }

    ADADELTA_OPT = 'adadelta'
    ADAGRAD_OPT = 'adagrad'
    ADAM_OPT = 'adam'
    FTRL_OPT = 'ftrl'
    MOMENTUM_OPT = 'momentum'
    RMSPROP_OPT = 'rmsprop'
    SGD_OPT = 'sgd'
    EXPONENTIAL_LR = 'exponential'
    FIXED = 'fixed'
    POLYNOMIAL_LR = 'polynomial'
    VALID_LEARNING_RATES = [EXPONENTIAL_LR, FIXED, POLYNOMIAL_LR]
    VALID_OPTIMIZERS = [
        ADADELTA_OPT, ADAGRAD_OPT, ADAM_OPT, FTRL_OPT, MOMENTUM_OPT, RMSPROP_OPT, SGD_OPT]

    CHECKPOINTS_GLOB = "model.ckpt-*"
    CHECKPOINTS_REGEX = "model\.ckpt-(?P<_0>[0-9]+)"

    def __init__(self,
                 dataset: Dataset.DatasetType,
                 model: ClassificationModel.ClassificationModelType,
                 execution_config: TFSlimTrainConfig.TFSlimTrainConfigType,
                 train_config: dict = {}):
        self.dataset = dataset
        self.model = model
        self.execution_config = vars(execution_config)
        self.train_config = get_and_validate_args(train_config, self.args_train_def)
        self.optimizer = self.train_config['optimizer']
        self.train_dir = self.train_config['train_dir']

    @classmethod
    def train(cls: ClassificationTrainerType,
              dataset: Dataset.DatasetType,
              model: ClassificationModel.ClassificationModelType,
              execution_config: TFSlimTrainConfig.TFSlimTrainConfigType,
              train_config: dict = {}) -> ClassificationModel.ClassificationModelType:
        """Training function for classification models.

        Parameters
        ----------
        dataset : Dataset.DatasetType
            The dataset with which the model will be fitted.
        model : ClassificationModel.ClassificationModelType
            The classification model instance that will be fitted.
        execution_config : TFSlimTrainConfig.TFSlimTrainConfigType
            Configuration of a training environment for the Tensorflow 
            Slim library.
        train_config : dict
            A dictionary that contains a train configuration. 
        """
        raise Exception(
            f"This method has been removed due to deprecation. "
            f"If you need to perform this type of training, browse "
            f"for the implementation in the repository history, and include again the "
            f"conabio_ml_vision/trainer/models module that was removed.")

    def do_train(self):
        latest_checkpoint = tf.train.latest_checkpoint(self.train_dir)
        if latest_checkpoint is not None:
            checkpoint_name = os.path.basename(latest_checkpoint)
            m = re.match(self.CHECKPOINTS_REGEX, checkpoint_name)
            res = [x for x in map(itemgetter(1), sorted(m.groupdict().items()))]
            current_step = int(res[0])
            logger.info(f"Current step: {current_step}. Number of training steps: "
                        f"{self.train_config['max_number_of_steps']}")
            return current_step < self.train_config['max_number_of_steps']
        return True
    """
    GET METHODS
    """

    def get_model_config(self):
        """Get a classification model configuration to be passed to a training 
        function. 
        Set train_image_size, weight_decay and label_smoothing in model_config.

        Returns
        -------
        dict
            Dictionary that contains model configuration arguments.
        """
        model_args = {
            'model_name': self.model.name
        }
        if self.model.model_config["train_image_size"] is not None:
            model_args['train_image_size'] = self.model.model_config["train_image_size"]
        if self.model.model_config["weight_decay"] is not None:
            model_args['weight_decay'] = self.model.model_config["weight_decay"]
        if self.model.model_config["label_smoothing"] is not None:
            model_args['label_smoothing'] = self.model.model_config["label_smoothing"]
        if self.model.saved_model is not None:
            model_args['checkpoint_path'] = self.model.saved_model['checkpoint_path']
        return model_args

    def get_input_config(self):
        """Get input config to be passed to a training function.
        Set dataset_name, dataset_dir, dataset_split_name
        and max_number_of_steps.

        Returns
        -------
        dict
            Dictionary that contains input configuration arguments.
        """
        raise Exception(
            f"This method has been removed due to deprecation. "
            f"If you need to perform this type of training, browse "
            f"for the implementation in the repository history, and include again the "
            f"conabio_ml_vision/trainer/models module that was removed.")
