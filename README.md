# README #

This repository provides a set of modules for creating datasets and training / evaluating computer vision models with them.

### Set up ###

It is recommended to use one of the Docker images to create the environment that allows using the API.
For this, change to one of the `docker_image` or `docker_image_tf2` directories of this repository and follow the following steps:
```
docker image build -t conabio_ml_vision:tf_x .
docker run -it --gpus 2 --name API_VISION_TF_x conabio_ml_vision:tf_x bash
```

If you want to change the version of Tensorflow that is installed by default, this can be passed as an argument `TF_VERSION` with the `build-arg` flag of the `docker image build command`. For example:
```
docker image build -t conabio_ml_vision:tf_1 . --build-arg TF_VERSION=1.15.0-py3-jupyter
```

Also, if you want to change the path of the folder where the repository will be downloaded (or it is already installed), you can pass it as the `BASE_DIR` argument:
```
docker image build -t conabio_ml_vision:tf_2 . --build-arg BASE_DIR=/LUSTRE/users/ecoinf_admin
```

Other arguments that can be passed in the `build-arg` flag are:

* `GIT_BRANCH`: Name of the branch that you want to clone of the repository. By default `dev` is passed.
* `BACKEND`: Name of the backend you want to use in your experiments. By default either `TF1` or `TF2` is passed.