#!/usr/bin/env python
# -*- coding: utf-8 -*-
from collections import defaultdict
from numpy import linspace
import os
import pandas as pd
from typing import Union, List

from conabio_ml.utils.utils import get_temp_folder
from conabio_ml.datasets import Partitions
from conabio_ml.utils.utils import download_file
from conabio_ml_vision.datasets import ImageDataset
from conabio_ml_vision.datasets import ImagePredictionDataset
from conabio_ml_vision.datasets import VideoPredictionDataset
from conabio_ml_vision.datasets import VideoDataset
from conabio_ml_vision.evaluator.evaluator import ImageClassificationMetrics
from conabio_ml_vision.evaluator.evaluator import ImageClassificationEvaluator
from conabio_ml_vision.utils.evaluator_utils import get_media_level_binary_pred_dataset
from conabio_ml_vision.utils.evaluator_utils import precision_recall_curve


def get_species_of_interest(birds_csv: str = None,
                            mammals_csv: str = None,
                            include_exotics: str = True) -> List:
    """Obtain a list of the scientific names of Mexican bird and mammals species considered suitable
    for camera traps

    Parameters
    ----------
    birds_csv : str, optional
        Path of the CSV file containing the list of birds considered suitable for camera traps.
        If None or the file does not exist, it will be downloaded from the path stored in the
        environment variable `BIRDS_OF_INT_URL`. By default None.
    mammals_csv : str, optional
        Path of the CSV file containing the list of mammals considered suitable for camera traps.
        If None or the file does not exist, it will be downloaded from the path stored in the
        environment variable `MAMMALS_OF_INT_URL`. By default None.
    include_exotics : str, optional
        Whether to include or not exotic species, by default True

    Returns
    -------
    List
        Resulting species list
    """
    birds = get_birds_of_interest(birds_csv, include_exotics=include_exotics)
    mammals = get_mammals_of_interest(mammals_csv, include_exotics=include_exotics)
    return birds + mammals


def get_native_species(birds_csv: str = None,
                       mammals_csv: str = None) -> List:
    """Obtain a list of the scientific names of native Mexican bird and mammals species considered
    suitable for camera traps

    Parameters
    ----------
    birds_csv : str, optional
        Path of the CSV file containing the list of birds considered suitable for camera traps.
        If None or the file does not exist, it will be downloaded from the path stored in the
        environment variable `BIRDS_OF_INT_URL`. By default None.
    mammals_csv : str, optional
        Path of the CSV file containing the list of mammals considered suitable for camera traps.
        If None or the file does not exist, it will be downloaded from the path stored in the
        environment variable `MAMMALS_OF_INT_URL`. By default None.

    Returns
    -------
    List
        Resulting species list
    """
    return get_species_of_interest(birds_csv, mammals_csv, include_exotics=False)


def get_exotic_species(birds_csv: str = None,
                       mammals_csv: str = None) -> List:
    """Obtain a list of the scientific names of exotic Mexican bird and mammals species considered
    suitable for camera trap

    Parameters
    ----------
    birds_csv : str, optional
        Path of the CSV file containing the list of birds considered suitable for camera traps.
        If None or the file does not exist, it will be downloaded from the path stored in the
        environment variable `BIRDS_OF_INT_URL`. By default None.
    mammals_csv : str, optional
        Path of the CSV file containing the list of mammals considered suitable for camera traps.
        If None or the file does not exist, it will be downloaded from the path stored in the
        environment variable `MAMMALS_OF_INT_URL`. By default None.

    Returns
    -------
    List
        Resulting species list
    """
    return list(set(get_species_of_interest(birds_csv, mammals_csv)) -
                set(get_species_of_interest(birds_csv, mammals_csv, include_exotics=False)))


def get_birds_of_interest(birds_csv: str = None,
                          include_exotics: bool = True) -> List:
    """Obtain a list of the scientific names of Mexican bird species considered suitable for
    camera traps

    Parameters
    ----------
    birds_csv : str, optional
        Path of the CSV file containing the list of birds considered suitable for camera traps.
        If None or the file does not exist, it will be downloaded from the path stored in the
        environment variable `BIRDS_OF_INT_URL`. By default None.
    include_exotics : bool, optional
        Whether to include or not exotic species, by default True

    Returns
    -------
    List
        Resulting species list
    """
    if birds_csv is None or os.path.isdir(birds_csv) or not os.path.isfile(birds_csv):
        if birds_csv is None:
            birds_csv = os.path.join(get_temp_folder(), 'AvesFT.csv')
        elif os.path.isdir(birds_csv):
            birds_csv = os.path.join(birds_csv, 'AvesFT.csv')
        if not os.path.isfile(birds_csv):
            birds_csv_url = os.environ.get('BIRDS_OF_INT_URL')
            if birds_csv_url is None:
                raise Exception(
                    f"In order to download the CSV file of birds of interest, you must assign "
                    f"the environment variable 'BIRDS_OF_INT_URL'")
            download_file(birds_csv_url, birds_csv)

    aves_set = set()
    aves_df = pd.read_csv(birds_csv)
    if not include_exotics:
        aves_df = aves_df[aves_df["Nativa"] == "nativa"]
    aves_set = set(aves_df["Binomio Especie"])
    return [x.lower() for x in aves_set]


def get_mammals_of_interest(mammals_csv: str = None,
                            include_exotics: bool=True) -> List:
    """Obtain a list of the scientific names of Mexican mammals species considered suitable for
    camera traps

    Parameters
    ----------
    mammals_csv : str, optional
        Path of the CSV file containing the list of mammals considered suitable for camera traps.
        If None or the file does not exist, it will be downloaded from the path stored in the
        environment variable `MAMMALS_OF_INT_URL`. By default None.
    include_exotics : bool, optional
        Whether to include or not exotic species, by default True

    Returns
    -------
    List
        Resulting species list
    """
    if mammals_csv is None or os.path.isdir(mammals_csv) or not os.path.isfile(mammals_csv):
        if mammals_csv is None:
            mammals_csv = os.path.join(get_temp_folder(), 'MamiferosFT.csv')
        elif os.path.isdir(mammals_csv):
            mammals_csv = os.path.join(mammals_csv, 'MamiferosFT.csv')
        if not os.path.isfile(mammals_csv):
            mammals_csv_url = os.environ.get('MAMMALS_OF_INT_URL')
            if mammals_csv_url is None:
                raise Exception(
                    f"In order to download the CSV file of mammals of interest, you must assign "
                    f"the environment variable 'MAMMALS_OF_INT_URL'")
            download_file(mammals_csv_url, mammals_csv)

    mammals_set = set()
    if mammals_csv is not None:
        mammals_df = pd.read_csv(mammals_csv)
        if not include_exotics:
            mammals_df = mammals_df[mammals_df["NatExo"] == "Nativa"]
        mammals_set = set(mammals_df["Binomio"])
    return [x.lower() for x in mammals_set]


def eval_binary(dataset_true: Union[ImageDataset, VideoDataset],
                dataset_pred: Union[ImagePredictionDataset, VideoPredictionDataset],
                eval_dir: str = None,
                partition: str = Partitions.TEST,
                pos_label: str = 'animal',
                **kwargs) -> ImageClassificationMetrics:
    """Perform binary evaluation of a model from the `dataset_pred` which is the result of
    the model's inference on the original dataset `dataset_true`.
    Since this is a binary evaluation, the total of the union of distinct labels in `dataset_true`
    and `dataset_pred` must be two, with `pos_label` being considered as the positive class,
    and the other one being the negative class.
    Optionally the plots of the metrics resulting from the evaluation will be stored in `eval_dir`.

    Parameters
    ----------
    dataset_true : Union[ImageDataset, VideoDataset]
        Dataset on which the model's inference was applied
    dataset_pred : Union[ImagePredictionDataset, VideoPredictionDataset]
        Dataset with the predictions made by the model on `dataset_true`
    eval_dir : str, optional
        Folder in which the plots of the metrics resulting from the evaluation will be stored,
        by default None
    partition : str, optional
        Partition of `dataset_true` on which the evaluation is to be performed.
        If None, the evaluation will be performed on the entire dataset. By default Partitions.TEST
    pos_label : str, optional
        Label that will be considered the positive class and for which metrics results will be
        reported, by default 'animal'
    **kwargs
        Additional parameters used to configure the evaluation and plotting of results

    Returns
    -------
    ImageClassificationMetrics
        Object containing the evaluation results that can be accessed through the `results`
        property, which is a dictionary of the form:
        {'per_class': {`category`: {`metric_name`: `metric_value`}}
         'one_class': {`metric_name`: `metric_value`}
        }
    """
    zero_division = kwargs.get('zero_division', 1)
    res_eval = ImageClassificationEvaluator.eval(
        dataset_true=dataset_true,
        dataset_pred=dataset_pred,
        eval_config={
            'metrics_set': {
                ImageClassificationMetrics.Sets.BINARY: {
                    "pos_label": pos_label,
                    "zero_division": zero_division
                }
            },
            'partition': partition,
        })
    if eval_dir is not None:
        os.makedirs(eval_dir, exist_ok=True)
        res_eval.result_plots(dest_path=eval_dir, report=True, **kwargs)
    return res_eval


def eval_multiclass(dataset_true: Union[ImageDataset, VideoDataset],
                    dataset_pred: Union[ImagePredictionDataset, VideoPredictionDataset],
                    eval_dir: str = None,
                    partition: str = Partitions.TEST,
                    labels: List = None,
                    **kwargs) -> ImageClassificationMetrics:
    """Perform multiclass evaluation of a model from the `dataset_pred` which is the result of
    the model's inference on the original dataset `dataset_true`.
    Optionally the plots of the metrics resulting from the evaluation will be stored in `eval_dir`.

    Parameters
    ----------
    dataset_true : Union[ImageDataset, VideoDataset]
        Dataset on which the model's inference was applied
    dataset_pred : Union[ImagePredictionDataset, VideoPredictionDataset]
        Dataset with the predictions made by the model on `dataset_true`
    eval_dir : str, optional
        Folder in which the plots of the metrics resulting from the evaluation will be stored,
        by default None
    partition : str, optional
        Partition of `dataset_true` on which the evaluation is to be performed.
        If None, the evaluation will be performed on the entire dataset. By default Partitions.TEST
    labels : List, optional
        List of categories on which the evaluation will be performed.
        If None, all categories contained in `dataset_true` will be taken.
        By default None

    Returns
    -------
    ImageClassificationMetrics
        Object containing the evaluation results that can be accessed through the `results`
        property, which is a dictionary of the form:
        {'per_class': {`category`: {`metric_name`: `metric_value`}}
         'one_class': {`metric_name`: `metric_value`}
        }
    """
    zero_division = kwargs.get('zero_division', 1)
    average = kwargs.get('average', 'macro')
    normalize = kwargs.get('normalize', 'true')

    labels = labels or dataset_true.get_categories()
    res_eval = ImageClassificationEvaluator.eval(
        dataset_true=dataset_true,
        dataset_pred=dataset_pred,
        eval_config={
            'metrics_set': {
                ImageClassificationMetrics.Sets.MULTICLASS: {
                    "average": average,
                    "normalize": normalize,
                    "zero_division": zero_division
                }
            },
            "labels": labels,
            'partition': partition,
        })
    if eval_dir is not None:
        os.makedirs(eval_dir, exist_ok=True)
        res_eval.result_plots(dest_path=eval_dir, report=True, **kwargs)
    return res_eval


def eval_binary_from_detections(dets_ds: ImagePredictionDataset,
                                true_ds: Union[ImageDataset, VideoDataset],
                                eval_dir: str = None,
                                pos_label: str = 'animal',
                                pred_method: str = 'highest_pred_is_pos_label',
                                title_pr_curve_plot: str = None,
                                init_thres: float = 0.,
                                end_thres: float = 1.,
                                num_steps: float = 20,
                                step_size: float = None) -> List:
    """Perform binary evaluation of the classifier based on `dets_ds` detections on the `true_ds`
    dataset for various threshold levels, and optionally saves the resulting metric plots for each
    threshold and the total precision-recall curve.
    The classification is performed using the method specified in `pred_method`.

    Parameters
    ----------
    dets_ds : ImagePredictionDataset
        Dataset with the detections from the Megadetector for the video frames in `true_ds`
    true_ds : Union[ImageDataset, VideoDataset]
        Dataset with the images or videos to be analyzed
    eval_dir : str, optional
        Folder in which the plots of the metrics resulting from the evaluation and the
        precision-recall curve will be stored.
        By default None
    pos_label : str, optional
        Label that will be considered the positive class and for which metrics results will be
        reported, by default 'animal'
    pred_method : str, optional
        Method used in the calculation of labels and score in predictions.
        The prediction label will be of category `pos_label` if the criteria imposed by
        `pred_method` is satisfied; otherwise, it will be `neg_label`.
        The `pred_method` criteria are as follows:
        - 'any_pred': At least one detection of any category in the image with id `image_id` or in
        any of the frames of the video with id `video_id`.
        - 'any_pos_label_pred': At least one detection of category `pos_label` in the image with id
        `image_id` or in any of the frames of the video with id `video_id`.
        - 'highest_pred_is_pos_label': The detection with the highest score in the image with id
        `image_id` or in the frames of the video with id `video_id` is of the `pos_label` category.
        - 'pos_label_preds_mode': Most of the detections of the image or video frames are of the
        `pos_label` category.
        In case of a tie in the mode calculation, the prediction label and score are determined
        according to the highest score that is of one of the mode classes.
        - 'n_consec_frames_pos_label_preds': There must be at least `n` consecutive frames with
        some detection of the `pos_label` category. Only in the case of `dataset_true` is a
        `VideoDataset` instance.
        By default 'highest_pos_label_pred'

    title_pr_curve_plot : str, optional
        Title of the precision-recall curve plot, by default None
    init_thres : float, optional
        Initial threshold to be analized, by default 0.
    end_thres : float, optional
        Final threshold to be analized, by default 1.
    num_steps : float, optional
        Number of steps by which the threshold will be increased from `init_thres`to `end_thres`,
        by default 20
    step_size : float, optional
        Size of the threshold increment at each step of the evaluation.
        This parameter is only used in case `num_steps = None`, by default None

    Returns
    -------
    List
        List with the results of the binary evaluation for each threshold value,
        where each element is a dictionary of the form:
        {'thres_str': `thres_str`, 'res_eval': `res_eval`}
    """
    filetype = 'images' if isinstance(true_ds, ImageDataset) else 'videos'
    dets_ds_thres = dets_ds.copy()
    prec_rec_plot = os.path.join(eval_dir, 'precision_recall_curve.png')

    evals_results = []
    for thres in linspace(init_thres, end_thres, num=num_steps):
        thres_str = f'{int(round(thres, 2)*100)}'
        eval_dir_thres = os.path.join(eval_dir, f"thres_{thres_str}")
        title_eval = (f'Binary {filetype}-level evaluation for the class '
                      f'"{pos_label.capitalize()}" and threshold {thres:.2f}')

        dets_ds_thres.filter_by_score(min_score=thres, inplace=True)

        pred_ds = get_media_level_binary_pred_dataset(
            dataset_true=true_ds,
            dataset_pred=dets_ds_thres,
            pred_method=pred_method,
            pos_label=pos_label
        )

        res_eval = eval_binary(
            dataset_true=true_ds,
            dataset_pred=pred_ds,
            eval_dir=eval_dir_thres,
            partition=None,
            pos_label=pos_label,
            title=title_eval)
        evals_results.append({'thres_str': thres_str, 'res_eval': res_eval})

    title_prc = (f'Precision-Recall Curve for {filetype}-level binary evaluation for the '
                 f'class "{pos_label}"\n and the prediction method {pred_method}')
    precision_recall_curve(evals_results, plot_path=prec_rec_plot, title=title_prc)

    return evals_results


def get_success_vs_errors_ds(dataset_true: Union[ImageDataset, VideoDataset],
                             dataset_pred: ImagePredictionDataset,
                             pos_label: str) -> Union[ImageDataset, VideoDataset]:
    """Generates the labels 'TP', 'FP', 'TN' and 'FN' for `dataset_pred`, according to its
    detections and the correct labels in `dataset_true`

    Parameters
    ----------
    dataset_true : Union[ImageDataset, VideoDataset]
        Dataset with the correct labels
    dataset_pred : ImagePredictionDataset
        Dataset with the predictions
    pos_label : str
        Positive reference label

    Returns
    -------
    ImageDataset or VideoDataset
        Dataset with labels 'TP', 'FP', 'TN' and 'FN' according to predictions and true labels
    """
    filetype = 'images' if isinstance(dataset_true, ImageDataset) else 'videos'
    media_id_field = 'video_id' if filetype == 'videos' else "image_id"
    dataset_pred_df = dataset_pred.as_dataframe(sort_by="item")
    dataset_true_df = dataset_true.as_dataframe(sort_by="item")

    items = dataset_true_df['item'].values
    ids = dataset_true_df[media_id_field].values
    true_lbls = dataset_true_df['label'].values
    pred_lbls = dataset_pred_df['label'].values

    res = defaultdict(list)
    for media_item, media_id, true_label, pred_label in zip(items, ids, true_lbls, pred_lbls):
        if true_label == pred_label:
            if true_label == pos_label:
                label = 'TP'
            else:
                label = 'TN'
        elif true_label == pos_label:
            label = 'FN'
        else:
            label = 'FP'
        res['item'].append(media_item)
        res[media_id_field].append(media_id)
        res['label'].append(label)
        res['id'].append(media_item)
    data = pd.DataFrame(res)

    if filetype == 'videos':
        ds_eval = VideoDataset(data, clean_cat_names=False)
    else:
        ds_eval = ImageDataset(data, clean_cat_names=False)

    return ds_eval


def balance_binary_dataset(dataset,
                           perc_pos_label,
                           by_partition=False,
                           random_state=None,
                           neg_label='empty'):
    """Function that samples the elements of the `neg_label` class from `dataset` so that the
    positive class has the `perc_pos_label` fraction

    Parameters
    ----------
    dataset : Dataset
        Dataset to be sampled
    perc_pos_label : float
        Percentage for the positive class in the interval [0, 1]
    by_partition : bool, optional
        Whether to permorm the sampling taking the elements of each partition or not,
        by default False
    random_state : _type_, optional
        Seed for random number generator, by default None
    neg_label : str, optional
        Value of the negative class, which will be sampled, by default 'empty'
    """
    if perc_pos_label is None:
        return

    df = dataset.as_dataframe()
    n_pos_label_act = len(df[df.label != neg_label])
    n_neg_label_final = int(((1 / perc_pos_label) - 1) * n_pos_label_act)
    perc_neg_label = n_neg_label_final / len(df[df.label == neg_label])

    dataset.sample_dataset_elements(
        {neg_label: perc_neg_label},
        by_partition=by_partition, random_state=random_state,
        inplace=True
    )
