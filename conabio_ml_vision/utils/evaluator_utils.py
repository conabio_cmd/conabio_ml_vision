import os
from collections import defaultdict
from itertools import groupby
from multiprocessing import Manager
from typing import Union

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

from conabio_ml.utils.logger import get_logger
from conabio_ml.utils.utils import parallel_exec
from conabio_ml_vision.datasets import ImageDataset, ImagePredictionDataset
from conabio_ml_vision.datasets import VideoDataset, VideoPredictionDataset

logger = get_logger(__name__)
"""
    UTILS FOR IMAGES
"""

bin_preds_methods_map = {
    'any_animal_pred': 'any_pos_label_pred',
    'any_person_pred': 'any_pos_label_pred',
    'highest_pred_is_animal': 'highest_pred_is_pos_label',
    'highest_pred_is_person': 'highest_pred_is_pos_label',
    'animal_preds_mode': 'pos_label_preds_mode',
    'person_preds_mode': 'pos_label_preds_mode'
}

def get_min_diffs(item, df_pred, smallest_diffs):
    """Gets detections of the `item` and calculates the smallest difference in the 'score' field
    between them.
    It can be used to obtain the examples of predictions in which the model is least confident

    Parameters
    ----------
    item : str
        Item by which detections will be filtered
    df_pred : pandas.DataFrame
        DataFrame in which to search for detections
    smallest_diffs : dict
        Dictionary to which the results are added in the format {`item`: `smallest_diff`}
    """
    items = df_pred.loc[df_pred["item"] == item]
    if items["label"].nunique() < 2:
        return
    xx = items.groupby(['label']).mean()
    xx.sort_values(by=["score"], ascending=False, inplace=True)
    smallest_diffs[item] = xx["score"][0] - xx["score"][1]


def label2binary(row, pos_cat, neg_cat, thres):
    """Function that determines the label of an element in a binary dataset according to the
    probability value of `pos_cat` label with respect to a threshold.

    Parameters
    ----------
    row : row of pd.DataFrame
        Element of a binary dataset
    pos_cat : str
        Value of the resulting label in case `row['score'] >= thres`
    neg_cat : str
        Value of the resulting label in case `row['score'] < thres`
    thres : float
        Reference value against which `row['score']` is compared

    Returns
    -------
    str
        Label corresponding to the dataset element
    """
    score_pos_cat = score2binary(row, pos_cat)
    return pos_cat if score_pos_cat >= thres else neg_cat


def score2binary(row, pos_cat) -> float:
    """Function that returns the score value corresponding to an element of a binary dataset
    according to the label of interest `pos_cat`.

    Parameters
    ----------
    row : row of pd.DataFrame
        Element of a binary dataset
    pos_cat : str
        Value of the label of interest

    Returns
    -------
    float
        The probability value of the dataset element in case `row['label'] == pos_cat`,
        otherwise its complement.
    """
    return row['score'] if row['label'] == pos_cat else 1 - row['score']


def plot_precision_recall_curve(data, fig_path, title='', xlabel='Recall', ylabel='Precision'):
    """Plots the precision-recall curve from the information in `data` and stores it in `fig_path`
    with the title `title` and the labels on the x-axis `xlabel` and on the y-axis `ylabel`.

    Parameters
    ----------
    data : pd.DataFrame
        Dataframe containing the data used to create the precision-recall curve.
        It must contain the columns 'precision', 'recall' and 'threshold'.
    fig_path : str
        Path to the image file where the resulting figure will be stored
    title : str, optional
        Title of the graph, by default ''
    xlabel : str, optional
        Label on x-axis, by default 'Recall'
    ylabel : str, optional
        Label on y-axis, by default 'Precision'
    """
    # TODO: Check this
    data = data.sort_values(by='threshold', ascending=False, key=lambda col: col.astype(int))
    f, ax = plt.subplots(1, 1, figsize=(10, 10))
    g = sns.lineplot(
        x='recall', y='precision', data=data, ax=ax, errorbar=None, estimator=None, sort=False)
    g = sns.scatterplot(x='recall', y='precision', data=data, size='threshold', hue='threshold',
                        size_order=data['threshold'].values, ax=ax)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.title(title)
    plt.savefig(fig_path, dpi=100)
    plt.close()


def precision_recall_curve(results_list, plot_path, title, multiclass_eval=False, csv_path=None):
    """Create the precision-recall curve from a list containing the results of the binary
    (or optionally, multiclass) evaluation for different threshold levels, and stores it in
    `plot_path`.

    Parameters
    ----------
    results_list : list of dict
        List containing the results of the binary evaluation for each threshold level, as follows:
        {'thres_str': thres_str, 'res_eval': res_eval}, where `res_eval` comes from a call to the
        method `ImageClassificationEvaluator.eval`.
    plot_path : str
        Path to the image file where the resulting figure will be stored
    title : str
        Title of the graph
    multiclass_eval : bool, optional
        Whether or not the entries `res_eval` comes from a multiclass evaluation, by default False
    csv_path : str, optional
        Path to a CSV file where the results will be stored, by default None
    """
    eval_type = 'BINARY' if multiclass_eval == False else 'MULTICLASS'
    if not plot_path.endswith('.png'):
        plot_path = os.path.join(plot_path, 'precision_recall_curve.png')

    if csv_path is None or not csv_path.endswith('.csv'):
        csv_base = os.path.dirname(plot_path) if csv_path is None else csv_path
        csv_path = os.path.join(csv_base, 'precision_recall_curve.csv')

    evals_results = defaultdict(list)
    for res_elem in results_list:
        for m_name, m_val in res_elem['res_eval'].results[eval_type]['one_class'].items():
            if m_name not in ['precision', 'recall', 'f1_score']:
                continue
            evals_results['threshold'].append(res_elem['thres_str'])
            evals_results['metric_name'].append(m_name)
            evals_results['metric_val'].append(m_val)
    prc_df = pd.DataFrame(evals_results).set_index(['threshold', 'metric_name']).unstack()
    prc_df.columns = prc_df.columns.droplevel(0).rename(None)
    prc_df = prc_df.reset_index()
    prc_df = prc_df.sort_values(by='threshold', key=lambda x: x.astype(int)).reset_index(drop=True)

    prc_df[['threshold', 'precision', 'recall']].to_csv(csv_path, index=False)

    plot_precision_recall_curve(data=prc_df, fig_path=plot_path, title=title)

# region Image level classification


def _get_image_level_binary_label_and_score(dets_df: pd.DataFrame,
                                            image_id: str,
                                            img_id_to_label_and_score: dict,
                                            pos_label: str,
                                            neg_label: str,
                                            pred_method: str,
                                            score_neg_label: float):
    """Calculates the label and prediction score for the image with id `image_id` according to the
    `dets_df` detections, and store the result in the `img_id_to_label_and_score` dictionary.
    The label will be `pos_label` if the `pred_method` criterion is satisfied, and `neg_label`
    otherwise.

    Parameters
    ----------
    dets_df : pd.DataFrame
        DataFrame containing the detections to be analyzed
    image_id : str
        Value of the field `image_id` of the image to be analyzed
    img_id_to_label_and_score : dict
        Dictionary where the result will be stored, in the form:
        {`image_id`: {'label': `label`, 'score': `score`}}
    pos_label : str
        Label that is considered positive, and that will be assigned as 'label' if the
        `pred_method` criterion is satisfied.
    neg_label : str
        Label that will be assigned as 'label' if the `pred_method` criterion isn't satisfied.
    pred_method : str
        Method used in the calculation of labels and score in predictions.
        The prediction label will be of category `pos_label` if the criteria imposed by
        `pred_method` is satisfied; otherwise, it will be `neg_label`.
        The `pred_method` criteria are as follows:
        - 'any_pred': At least one detection of any category.
        - 'any_pos_label_pred': At least one detection of category `pos_label`.
        - 'highest_pred_is_pos_label': The detection with the highest score is of the `pos_label`
        category.
        - 'pos_label_preds_mode': Most of the detections of an image are of the `pos_label`
        category.
        In case of a tie in the mode calculation, the prediction label and score are determined
        according to the highest score that is of one of the mode classes.

    score_neg_label : float
        Score given to the resulting predictions with the label `neg_label`.
    """
    preds_of_img = dets_df[dets_df.image_id == image_id]

    max_score = 0.
    if len(preds_of_img) == 0:
        label = neg_label
        max_score = score_neg_label
    elif pred_method == 'any_pred':
        label = pos_label
        max_score = preds_of_img.score.max()
    elif pred_method == 'any_pos_label_pred':
        preds_of_pos_label = preds_of_img[preds_of_img.label == pos_label]
        if len(preds_of_pos_label) > 0:
            label = pos_label
            max_score = preds_of_pos_label.score.max()
        else:
            label = neg_label
            max_score = score_neg_label
    elif pred_method == 'highest_pred_is_pos_label':
        preds_of_img_sorted = preds_of_img.sort_values(by='score', ascending=False)
        if preds_of_img_sorted.iloc[0]['label'] == pos_label:
            label = pos_label
            max_score = (
                preds_of_img_sorted[preds_of_img_sorted.label == pos_label]
                .score.max()
            )
        else:
            label = neg_label
            max_score = score_neg_label
    elif pred_method == 'pos_label_preds_mode':
        preds_of_img_sorted = preds_of_img.sort_values(by='score', ascending=False)
        label_modes = preds_of_img['label'].mode()
        if len(label_modes) > 1:
            # If modes > 1 then select the one with the highest score
            modes = label_modes.values
            preds_sorted_modes = preds_of_img_sorted[preds_of_img_sorted["label"].isin(modes)]
            if preds_sorted_modes.iloc[0]['label'] == pos_label:
                label = pos_label
                max_score = preds_sorted_modes.iloc[0]['score']
            else:
                label = neg_label
                max_score = score_neg_label
        else:
            if label_modes.iloc[0] == pos_label:
                label = pos_label
                max_score = (
                    preds_of_img_sorted[preds_of_img_sorted.label == pos_label]
                    .score.max()
                )
            else:
                label = neg_label
                max_score = score_neg_label

    img_id_to_label_and_score[image_id] = {
        'label': label,
        'score': max_score
    }


def get_image_level_binary_pred_dataset(dataset_true: ImageDataset,
                                        dataset_pred: ImagePredictionDataset,
                                        pred_method: str = 'any_pred',
                                        pos_label: str = 'animal',
                                        neg_label: str = 'empty',
                                        score_neg_label: float = 0.,
                                        **kwargs):
    """Get the binary image-level labels from the detections generated by the Megadetector in
    `dataset_pred` for each image in `dataset_true`, labeling `pos_label` on the images that
    satisfy the requirements imposed by the method chosen in `pred_method` and `neg_label`
    on the other ones.

    Parameters
    ----------
    dataset_true : ImageDataset
        Dataset with the images to be analyzed
    dataset_pred : ImagePredictionDataset
        Dataset with the detections from the Megadetector for the images in `dataset_true`
    pred_method : str, optional
        Method used in the calculation of labels and score in predictions.
        The prediction label will be of category `pos_label` if the criteria imposed by
        `pred_method` is satisfied; otherwise, it will be `neg_label`.
        The `pred_method` criteria are as follows:
        - 'any_pred': At least one detection of any category.
        - 'any_pos_label_pred': At least one detection of category `pos_label`.
        - 'highest_pred_is_pos_label': The detection with the highest score is of the `pos_label`
        category.
        - 'pos_label_preds_mode': Most of the detections of an image are of the `pos_label`
        category.
        In case of a tie in the mode calculation, the prediction label is determined according to
        the highest score that is of one of the mode classes.
        By default 'any_pred'
    pos_label : str, optional
        Label that is considered positive, and that will be assigned as 'label' if the
        `pred_method` criterion is satisfied, by default 'animal'
    neg_label : str, optional
        Label that will be assigned as 'label' if the `pred_method` criterion isn't satisfied,
        by default 'empty'
    score_neg_label : float, optional
        Score given to the resulting predictions with the label `neg_label`, by default 0.

    Returns
    -------
    ImagePredictionDataset
        Dataset with the image-level binary predictions of the `dataset_true` dataset using the
        `dataset_pred` detections
    """
    dataset_pred = kwargs.get('dataset_dets', dataset_pred)
    true_df = dataset_true.as_dataframe()
    dets_df = dataset_pred.as_dataframe()
    img_id_to_label_and_score = Manager().dict()

    logger.info(f"Generating image-level binary prediction dataset for positive label {pos_label} "
                f"with {len(true_df)} images and {len(dets_df)} detections")

    pred_method = bin_preds_methods_map.get(pred_method, pred_method)

    parallel_exec(
        func=_get_image_level_binary_label_and_score,
        elements=true_df.image_id.unique(),
        dets_df=dets_df,
        image_id=lambda img_id: img_id,
        img_id_to_label_and_score=img_id_to_label_and_score,
        pos_label=pos_label,
        neg_label=neg_label,
        pred_method=pred_method,
        score_neg_label=score_neg_label)

    anns_data = defaultdict(list)
    for row in true_df.to_dict('records'):
        image_id = row['image_id']
        anns_data['label'].append(img_id_to_label_and_score[image_id]['label'])
        anns_data['item'].append(row['item'])
        anns_data['image_id'].append(image_id)
        anns_data['score'].append(img_id_to_label_and_score[image_id]['score'])
    anns_info = pd.DataFrame(anns_data)

    preds_ds = ImagePredictionDataset(anns_info,
                                      images_dir=dataset_true.get_images_dir(),
                                      images_info=dataset_true.get_images_info())
    return preds_ds


def _get_image_level_label_and_score(dets_df: pd.DataFrame,
                                     image_id: str,
                                     pred_method: str,
                                     img_id_to_label_and_score: dict,
                                     empty_label: str = 'empty',
                                     score_empty_label: float = 0.):
    """Calculate the label and prediction score for the image with id `image_id` according to the
    `dets_df` detections, and store the result in the `img_id_to_label_and_score` dictionary.
    The label will be `empty_label` in case no detections are found in `dets_df` for `image_id`.

    Parameters
    ----------
    dets_df : pd.DataFrame
        DataFrame containing the detections to be analyzed
    image_id : str
        Value of the field `image_id` of the image to be analyzed
    pred_method : str
        Method used in the calculation of labels and score in predictions.
        The `pred_method` criteria are as follows:
        - 'highest_pred': The label and score of the detection with the highest score will be
        assigned.
        - 'pos_label_preds_mode': The tag and the score of the most frequent detection of
        `image_id` will be assigned.
        In case of a tie in the mode calculation, the prediction label and score are determined
        according to the highest score that is of one of the mode classes.

    img_id_to_label_and_score : dict
        Dictionary where the result will be stored, in the form:
        {`image_id`: {'label': `label`, 'score': `score`}}
    empty_label : str, optional
        Label that will be assigned as 'label' in case no detections were found for the image with
        id `image_id`. By default 'empty'
    score_empty_label : float, optional
        Score given to the resulting predictions with the label `empty_label`, by default 0.

    """
    preds_img = dets_df[dets_df.image_id == image_id]
    if len(preds_img) == 0:
        label = empty_label
        max_score = score_empty_label
    elif pred_method == 'highest_pred':
        highest_pred = (
            preds_img.sort_values(by='score', ascending=False)
            .iloc[0]
        )
        label = highest_pred['label']
        max_score = highest_pred['score']
    elif pred_method == 'preds_mode':
        label_modes = preds_img['label'].mode()
        if len(label_modes) > 1:
            highest_mode = (
                preds_img[preds_img["label"].isin(label_modes.values)]
                .sort_values(by='score', ascending=False)
                .iloc[0]
            )
            label = highest_mode['label']
            max_score = highest_mode['score']
        else:
            label = label_modes.iloc[0]
            max_score = preds_img[preds_img.label == label].score.max()
    else:
        raise ValueError(f"Invalid pred_method: {pred_method}")

    img_id_to_label_and_score[image_id] = {
        'label': label,
        'score': max_score
    }


def get_image_level_pred_dataset(dataset_true: ImageDataset,
                                 dataset_pred: ImagePredictionDataset,
                                 pred_method: str = 'highest_pred',
                                 empty_label: str = 'empty',
                                 det_labels_map: dict = None,
                                 score_empty_label: float = 0.,
                                 **kwargs) -> ImagePredictionDataset:
    """Get the multiclass image-level labels from the detections generated by the Megadetector in
    `dataset_pred` for each image in `dataset_true`, taking the label of each image from the
    detections found for it, according to the criteria specified in `pred_method`.

    Parameters
    ----------
    dataset_true : ImageDataset
        Dataset with the images to be analyzed
    dataset_pred : ImagePredictionDataset
        Dataset with the detections from the Megadetector for the images in `dataset_true`
    pred_method : str, optional
        Method used in the calculation of labels and score in predictions.
        The `pred_method` criteria are as follows:
        - 'highest_pred': The label and score of the detection with the highest score will be
        assigned.
        - 'pos_label_preds_mode': The tag and the score of the most frequent detection of
        `image_id` will be assigned.
        In case of a tie in the mode calculation, the prediction label and score are determined
        according to the highest score that is of one of the mode classes.
        By default 'highest_pred'.

    empty_label : str, optional
        Label that will be assigned as 'label' in case no detections were found for `image_id`.
        By default 'empty'
    det_labels_map : dict, optional
        Mapping to be applied to `dataset_pred` using the `map_categories` method before analysis,
        by default None
    score_empty_label : float, optional
        Score given to the resulting predictions with the label `empty_label`, by default 0.

    Returns
    -------
    ImagePredictionDataset
        Dataset with the image-level multiclass predictions of the `dataset_true` dataset using the
        `dataset_pred` detections
    """
    dataset_pred = kwargs.get('dataset_dets', dataset_pred)
    det_labels_map = det_labels_map or {'animal': 'animal',
                                        'person': 'person',
                                        'vehicle': 'person',
                                        '*': '*'}
    dataset_pred.map_categories(mapping_classes=det_labels_map)

    true_df = dataset_true.as_dataframe()
    dets_df = dataset_pred.as_dataframe()

    img_id_to_label_and_score = Manager().dict()
    imgs_ids = true_df.image_id.unique()
    parallel_exec(
        func=_get_image_level_label_and_score,
        elements=imgs_ids,
        dets_df=dets_df,
        image_id=lambda img_id: img_id,
        pred_method=pred_method,
        img_id_to_label_and_score=img_id_to_label_and_score,
        empty_label=empty_label,
        score_empty_label=score_empty_label)

    anns_data = defaultdict(list)
    for row in true_df.to_dict('records'):
        image_id = row['image_id']
        anns_data['label'].append(img_id_to_label_and_score[image_id]['label'])
        anns_data['item'].append(row['item'])
        anns_data['image_id'].append(image_id)
        anns_data['score'].append(img_id_to_label_and_score[image_id]['score'])
    anns_info = pd.DataFrame(anns_data)

    preds_ds = ImagePredictionDataset(anns_info,
                                      images_dir=dataset_true.get_images_dir(),
                                      images_info=dataset_true.get_images_info())
    return preds_ds


# endregion

# region Video level classification

def _get_video_level_binary_label_and_score(frames_dets_df: pd.DataFrame,
                                            video_id: str,
                                            pred_method: str,
                                            vid_id_to_label_and_score: dict,
                                            pos_label: str,
                                            neg_label: str,
                                            sort_seq_field: str,
                                            score_neg_label: float):
    """Calculates the label and prediction score for the video with id `video_id` according to the
    `frames_dets_df` detections, and store the result in the `vid_id_to_label_and_score`
    dictionary.
    The label will be `pos_label` if the `pred_method` criterion is satisfied, and `neg_label`
    otherwise.

    Parameters
    ----------
    frames_dets_df : pd.DataFrame
        DataFrame containing the detections on the frames to be analyzed
    video_id : str
        Value of the field `video_id` of the video to be analyzed
    pred_method : str
        Method used in the calculation of labels and score in predictions.
        The prediction label will be of category `pos_label` if the criteria imposed by
        `pred_method` is satisfied; otherwise, it will be `neg_label`.
        The `pred_method` criteria are as follows:
        - 'any_pred': At least one detection of any category in any of the frames of the video
        with id `video_id`.
        - 'any_pos_label_pred': At least one detection of category `pos_label` in any of the frames
        of the video with id `video_id`.
        - 'highest_pred_is_pos_label': The detection with the highest score in all the frames of
        the video with id `video_id` is of the `pos_label` category.
        - 'pos_label_preds_mode': Most of the detections of the video frames are of the `pos_label`
        category.
        In case of a tie in the mode calculation, the prediction label and score are determined
        according to the highest score that is of one of the mode classes.
        - 'n_consec_frames_pos_label_preds': There must be at least `n` consecutive frames with
        some detection of the `pos_label` category.
    vid_id_to_label_and_score : dict
        Dictionary where the result will be stored, in the form:
        {`video_id`: {'label': `label`, 'score': `score`}}
    pos_label : str
        Label that is considered positive, and that will be assigned as 'label' if the
        `pred_method` criterion is satisfied.
    neg_label : str
        Label that will be assigned as 'label' if the `pred_method` criterion isn't satisfied.
    sort_seq_field : str
        Field by which the video frames will be ordered in case
        `pred_method == n_consec_frames_pos_label_preds`.
    score_neg_label : float
        Score given to the resulting predictions with the label `neg_label`.
    """
    preds_video = frames_dets_df[frames_dets_df.video_id == video_id]
    pos_label_preds = preds_video[preds_video.label == pos_label]
    if len(preds_video) == 0:
        label = neg_label
        max_score = score_neg_label
    elif pred_method == 'any_pred':
        label = (
            pos_label if len(preds_video) > 0
            else neg_label
        )
        max_score = (
            preds_video['score'].max() if len(preds_video) > 0
            else score_neg_label
        )
    elif pred_method == 'any_pos_label_pred':
        label = (
            pos_label if len(pos_label_preds) > 0
            else neg_label
        )
        max_score = (
            pos_label_preds['score'].max() if len(pos_label_preds) > 0
            else score_neg_label
        )
    elif pred_method == 'highest_pred_is_pos_label':
        preds_vid_sorted = preds_video.sort_values(by='score', ascending=False)
        highest_pred = preds_vid_sorted.iloc[0]
        label = (
            pos_label if highest_pred['label'] == pos_label
            else neg_label
        )
        max_score = (
            highest_pred['score'] if highest_pred['label'] == pos_label
            else score_neg_label
        )
    elif pred_method == 'pos_label_preds_mode':
        label_mode = preds_video['label'].mode()

        label = (
            pos_label if label_mode.iloc[0] == pos_label
            else neg_label
        )
        max_score = (
            pos_label_preds['score'].max() if label_mode.iloc[0] == pos_label
            else score_neg_label
        )
    else:
        valids = ('consec_frames_animal_preds', 'consec_frames_person_preds',
                  'consec_frames_pos_label_preds')
        assert_cond = (
            len(pred_method.split('_')) > 1
            and '_'.join(pred_method.split('_')[1:]) in valids
        )
        assert assert_cond, f"Invalid pred_method: {pred_method}"

        n_consec = int(pred_method.split('_')[0])
        preds_vid_sorted = (
            pos_label_preds.sort_values(by=sort_seq_field, ascending=True)
            .drop_duplicates(subset='image_id', keep='first')
        )
        # Array con las distancias entre cada par de `sort_seq_field` consecutivos
        # de las detecciones de `pos_label`
        diffs = np.diff(preds_vid_sorted[sort_seq_field].values)
        try:
            max_conseq = max(len(list(v)) for g, v in groupby(diffs, lambda x: x == 1) if g)
            label = (
                pos_label if max_conseq >= n_consec - 1
                else neg_label
            )
            max_score = (
                pos_label_preds['score'].max() if max_conseq >= n_consec - 1
                else score_neg_label
            )
        except Exception:
            label = neg_label
            max_score = score_neg_label

    vid_id_to_label_and_score[video_id] = {
        'label': label,
        'score':  max_score
    }


def get_video_level_binary_pred_dataset(dataset_true: VideoDataset,
                                        dataset_pred: ImagePredictionDataset,
                                        pred_method: str = 'highest_pos_label_pred',
                                        pos_label: str = 'animal',
                                        neg_label: str = 'empty',
                                        score_neg_label: float = 0.,
                                        **kwargs) -> VideoPredictionDataset:
    """Get the binary video-level labels from the detections generated by the Megadetector in
    `dataset_pred` for each video frame in `dataset_true`, labeling `pos_label` on the videos that
    satisfy the requirements imposed by the method chosen in `pred_method` and `neg_label`
    on the other ones.

    Parameters
    ----------
    dataset_true : VideoDataset
        Dataset with the videos to be analyzed
    dataset_pred : ImagePredictionDataset
        Dataset with the detections from the Megadetector for the video frames in `dataset_true`
    pred_method : str, optional
        Method used in the calculation of labels and score in predictions.
        The prediction label will be of category `pos_label` if the criteria imposed by
        `pred_method` is satisfied; otherwise, it will be `neg_label`.
        The `pred_method` criteria are as follows:
        - 'any_pred': At least one detection of any category in any of the frames of the video
        with id `video_id`.
        - 'any_pos_label_pred': At least one detection of category `pos_label` in any of the frames
        of the video with id `video_id`.
        - 'highest_pred_is_pos_label': The detection with the highest score in all the frames of
        the video with id `video_id` is of the `pos_label` category.
        - 'pos_label_preds_mode': Most of the detections of the video frames are of the `pos_label`
        category.
        In case of a tie in the mode calculation, the prediction label and score are determined
        according to the highest score that is of one of the mode classes.
        - 'n_consec_frames_pos_label_preds': There must be at least `n` consecutive frames with
        some detection of the `pos_label` category.
        By default 'highest_pos_label_pred'
    pos_label : str, optional
        Label that is considered positive, and that will be assigned as 'label' if the
        `pred_method` criterion is satisfied., by default 'animal'
    neg_label : str, optional
        Label that will be assigned as 'label' if the `pred_method` criterion isn't satisfied,
        by default 'empty'
    score_neg_label : float, optional
        Score given to the resulting predictions with the label `neg_label`, by default 0.

    Returns
    -------
    VideoPredictionDataset
        Dataset with the video-level binary predictions of the `dataset_true` dataset using the
        `dataset_pred` detections
    """
    dataset_pred = kwargs.get('dataset_dets', dataset_pred)
    video_true_df = dataset_true.as_dataframe()
    frames_dets_df = dataset_pred.as_dataframe()

    vid_id_to_label_and_score = Manager().dict()
    vids_ids = video_true_df.video_id.unique()

    pred_method = bin_preds_methods_map.get(pred_method, pred_method)

    parallel_exec(func=_get_video_level_binary_label_and_score,
                  elements=vids_ids,
                  frames_dets_df=frames_dets_df,
                  video_id=lambda elem: elem,
                  pred_method=pred_method,
                  vid_id_to_label_and_score=vid_id_to_label_and_score,
                  pos_label=pos_label,
                  neg_label=neg_label,
                  sort_seq_field='seq_frame_num',
                  score_neg_label=score_neg_label)

    anns_data = defaultdict(list)
    for row in video_true_df.to_dict('records'):
        video_id = row['video_id']
        anns_data['item'].append(row['item'])
        anns_data['label'].append(vid_id_to_label_and_score[video_id]['label'])
        anns_data['video_id'].append(video_id)
        anns_data['score'].append(vid_id_to_label_and_score[video_id]['score'])
    anns_info = pd.DataFrame(anns_data)

    preds_ds = VideoPredictionDataset(anns_info,
                                      videos_dir=dataset_true.get_videos_dir(),
                                      videos_info=dataset_true.get_videos_info())
    return preds_ds


def _get_video_level_label_and_score(frames_dets_df: pd.DataFrame,
                                     video_id: str,
                                     pred_method: str,
                                     vid_id_to_label_and_score: dict,
                                     empty_label: str = 'empty',
                                     score_empty_label: float = 0.):
    """Calculate the label and prediction score for the video with id `video_id` according to the
    `frames_dets_df` detections, and store the result in the `vid_id_to_label_and_score`
    dictionary.
    The label will be `empty_label` in case no detections are found in `frames_dets_df`
    for `video_id`.

    Parameters
    ----------
    frames_dets_df : pd.DataFrame
        DataFrame containing the detections to be analyzed
    video_id : str
        Value of the field `video_id` of the video to be analyzed
    pred_method : str
        Method used in the calculation of labels and score in predictions.
        The `pred_method` criteria are as follows:
        - 'highest_pred': The label and the score of the detection with the highest score in all
        frames of the video will be assigned.
        - 'pos_label_preds_mode': The label and the score of the most frequent detection label in
        all the frames of the video with id `video_id` will be assigned.
        In case of a tie in the mode calculation, the prediction label and score are determined
        according to the highest score that is of one of the mode classes.

    vid_id_to_label_and_score : dict
        Dictionary where the result will be stored, in the form:
        {`video_id`: {'label': `label`, 'score': `score`}}
    empty_label : str, optional
        Label that will be assigned as 'label' in case no detections were found for the video with
        id `video_id`. By default 'empty'
    score_empty_label : float, optional
        Score given to the resulting predictions with the label `empty_label`, by default 0.

    """
    preds_video = frames_dets_df[frames_dets_df.video_id == video_id]
    if len(preds_video) == 0:
        label = empty_label
        max_score = score_empty_label
    elif pred_method == 'highest_pred':
        preds_vid_sorted = preds_video.sort_values(by='score', ascending=False)
        highest_pred = preds_vid_sorted.iloc[0]
        label = highest_pred['label']
        max_score = highest_pred['score']
    elif pred_method == 'preds_mode':
        label_modes = preds_video['label'].mode()
        if len(label_modes) > 1:
            highest_mode = (
                preds_video[preds_video["label"].isin(label_modes.values)]
                .sort_values(by='score', ascending=False)
                .iloc[0]
            )
            label = highest_mode['label']
            max_score = highest_mode['score']
        else:
            label = label_modes.iloc[0]
            max_score = preds_video[preds_video.label == label].score.max()
    # TODO: Refactor this
    elif pred_method == 'preds_mean':
        counts = preds_video.groupby('label')['item'].count().sort_values()
        if len(counts) > 1:
            if counts[-1] / counts[-2] > 3:
                label = counts.index[-1]
                max_score = preds_video[preds_video.label.isin(counts.index[-2:])].score.max()
            else:
                x = preds_video[preds_video.label.isin(counts.index[-2:])]
                x = x.groupby('label')['score'].mean().sort_values()
                label = x.index[-1]
                max_score = x[-1]
        else:
            label = counts.index[-1]
            max_score = preds_video[preds_video.label.isin(counts.index[-2:])].score.max()

    else:
        raise ValueError(f"Invalid pred_method: {pred_method}")

    vid_id_to_label_and_score[video_id] = {
        'label': label,
        'score':  max_score
    }


def get_video_level_pred_dataset(dataset_true: VideoDataset,
                                 dataset_pred: ImagePredictionDataset = None,
                                 pred_method: str = 'highest_pred',
                                 empty_label: str = 'empty',
                                 det_labels_map: dict = None,
                                 score_empty_label: float = 0.,
                                 **kwargs) -> VideoPredictionDataset:
    """Get the multiclass video-level labels from the predictions generated by a model (e.g.,
    the Megadetector) in `dataset_pred` for each video in `dataset_true`, taking the label of each video from the
    detections found in all of its frames, according to the criteria specified in `pred_method`.

    Parameters
    ----------
    dataset_true : VideoDataset
        Dataset with the videos to be analyzed
    dataset_pred : ImagePredictionDataset
        Dataset with the detections from the Megadetector for all the frames in `dataset_true`
    pred_method : str, optional
        Method used in the calculation of labels and score in predictions.
        The `pred_method` criteria are as follows:
        - 'highest_pred': The label and the score of the detection with the highest score in all
        frames of the video will be assigned.
        - 'pos_label_preds_mode': The label and the score of the most frequent detection label in
        all the frames of the video with id `video_id` will be assigned.
        In case of a tie in the mode calculation, the prediction label and score are determined
        according to the highest score that is of one of the mode classes.
        By default 'highest_pred'

    empty_label : str, optional
        Label that will be assigned as 'label' in case no detections were found for `image_id`.
        By default 'empty'
    det_labels_map : dict, optional
        Mapping to be applied to `dataset_pred` using the `map_categories` method before analysis,
        by default None
    score_empty_label : float, optional
        Score given to the resulting predictions with the label `empty_label`, by default 0.

    Returns
    -------
    VideoPredictionDataset
        Dataset with the video-level multiclass predictions of the `dataset_true` dataset using the
        `dataset_pred` detections
    """
    dataset_pred = kwargs.get('dataset_dets', dataset_pred)
    det_labels_map = det_labels_map or {'animal': 'animal',
                                        'person': 'person',
                                        'vehicle': 'person',
                                        '*': '*'}
    dataset_pred.map_categories(mapping_classes=det_labels_map)

    video_true_df = dataset_true.as_dataframe()
    frames_dets_df = dataset_pred.as_dataframe()

    vid_id_to_label_and_score = Manager().dict()
    vids_ids = video_true_df.video_id.unique()
    parallel_exec(func=_get_video_level_label_and_score,
                  elements=vids_ids,
                  frames_dets_df=frames_dets_df,
                  video_id=lambda vid_id: vid_id,
                  pred_method=pred_method,
                  vid_id_to_label_and_score=vid_id_to_label_and_score,
                  empty_label=empty_label,
                  score_empty_label=score_empty_label)

    anns_data = defaultdict(list)
    for row in video_true_df.to_dict('records'):
        video_id = row['video_id']
        anns_data['item'].append(row['item'])
        anns_data['label'].append(vid_id_to_label_and_score[video_id]['label'])
        anns_data['video_id'].append(video_id)
        anns_data['score'].append(vid_id_to_label_and_score[video_id]['score'])
    anns_info = pd.DataFrame(anns_data)

    preds_ds = VideoPredictionDataset(anns_info,
                                      videos_dir=dataset_true.get_videos_dir(),
                                      videos_info=dataset_true.get_videos_info())
    return preds_ds

# endregion


def get_media_level_binary_pred_dataset(dataset_true: Union[ImageDataset, VideoDataset],
                                        dataset_pred: ImagePredictionDataset,
                                        pred_method: str = 'highest_pos_label_pred',
                                        pos_label: str = 'animal',
                                        neg_label: str = 'empty',
                                        score_neg_label: float = 0.,
                                        **kwargs)\
                                            -> Union[ImagePredictionDataset,
                                                     VideoPredictionDataset]:
    """Get the binary image-level or video-level labels from the detections generated by the
    Megadetector in `dataset_pred` for each image or video in `dataset_true`, labeling `pos_label`
    on the images that satisfy the requirements imposed by the method chosen in `pred_method` and
    `neg_label` on the other ones.

    Parameters
    ----------
    dataset_true : Union[ImageDataset, VideoDataset]
        Dataset with the images or videos to be analyzed
    dataset_pred : ImagePredictionDataset
        Dataset with the detections from the Megadetector for the video frames in `dataset_true`
    pred_method : str, optional
        Method used in the calculation of labels and score in predictions.
        The prediction label will be of category `pos_label` if the criteria imposed by
        `pred_method` is satisfied; otherwise, it will be `neg_label`.
        The `pred_method` criteria are as follows:
        - 'any_pred': At least one detection of any category in the image with id `image_id` or in
        any of the frames of the video with id `video_id`.
        - 'any_pos_label_pred': At least one detection of category `pos_label` in the image with id
        `image_id` or in any of the frames of the video with id `video_id`.
        - 'highest_pred_is_pos_label': The detection with the highest score in the image with id
        `image_id` or in the frames of the video with id `video_id` is of the `pos_label` category.
        - 'pos_label_preds_mode': Most of the detections of the image or video frames are of the
        `pos_label` category.
        In case of a tie in the mode calculation, the prediction label and score are determined
        according to the highest score that is of one of the mode classes.
        - 'n_consec_frames_pos_label_preds': There must be at least `n` consecutive frames with
        some detection of the `pos_label` category. Only in the case of `dataset_true` is a
        `VideoDataset` instance.
        By default 'highest_pos_label_pred'

    pos_label : str, optional
        Label that is considered positive, and that will be assigned as 'label' if the
        `pred_method` criterion is satisfied., by default 'animal'.
    neg_label : str, optional
        Label that will be assigned as 'label' if the `pred_method` criterion isn't satisfied,
        by default 'empty'
    score_neg_label : float, optional
        Score given to the resulting predictions with the label `neg_label`, by default 0.

    Returns
    -------
    Union[ImagePredictionDataset, VideoPredictionDataset]
        Dataset with the image-level or video-level binary predictions of the `dataset_true`
        dataset using the `dataset_pred` detections
    """
    dataset_pred = kwargs.get('dataset_dets', dataset_pred)
    if isinstance(dataset_true, ImageDataset):
        dataset_pred = get_image_level_binary_pred_dataset(
            dataset_true=dataset_true, dataset_pred=dataset_pred, pred_method=pred_method,
            pos_label=pos_label, neg_label=neg_label, score_neg_label=score_neg_label)
    elif isinstance(dataset_true, VideoDataset):
        dataset_pred = get_video_level_binary_pred_dataset(
            dataset_true=dataset_true, dataset_pred=dataset_pred, pred_method=pred_method,
            pos_label=pos_label, neg_label=neg_label, score_neg_label=score_neg_label)
    else:
        raise Exception('Invalid type of dataset_true')

    return dataset_pred
