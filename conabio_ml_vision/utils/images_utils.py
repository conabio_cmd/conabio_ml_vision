#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
from multiprocessing import Manager
import math

from PIL import Image
import cv2
import numpy as np
from shutil import copy2

from conabio_ml.utils.utils import parallel_exec
from conabio_ml.utils.logger import get_logger

logger = get_logger(__name__)

BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
GREEN = (0, 255, 0)
RED = (51, 51, 255)
ORANGE = (51, 153, 255)
YELLOW = (0, 255, 255)
BLUE = (255, 128, 0)
PURPLE = (255, 102, 178)
COLORS_MAP = {
    'GREEN': GREEN,
    'ORANGE': ORANGE,
    'BLUE': BLUE,
    'YELLOW': YELLOW,
    'PURPLE': PURPLE,
    'WHITE': WHITE,
    'RED': RED,
    'BLACK': BLACK
}


def get_image_size(image):
    """Open an image, get its size and return it in format (width, height).
    In case an exception occurs when trying to open the image, None will be returned for both width
    and height

    Parameters
    ----------
    image : str or a file object
        Path where the image is stored, or file object containing the image data

    Returns
    -------
    tuple of int
        The size of the image in format (width, height)
    """
    try:
        with Image.open(image) as img:
            return img.size
    except:
        logger.exception(f"Exception occurred while opening image {image}")
        return None, None


def get_batches(elems, batch_size):
    """Divides a list of elements into batches of size `batch_size` and returns them in a list of
    lists

    Parameters
    ----------
    elems : list
        List of elements to be divided into batches
    batch_size : int
        Size of batches that are returned.

    Returns
    -------
    list of lists
        List with the lists of elements of size `batch_size`. If the last batch is not large
        enough, it will be returned smaller
    """
    return [elems[i:i + batch_size] for i in range(0, len(elems), batch_size)]


def load_image_into_numpy_array_batch(image_batch):
    """It takes a batch of image paths and in parallel loads them into numpy arrays and fits them
    in a dictionary

    Parameters
    ----------
    image_batch : list
        Batch of image paths

    Returns
    -------
    dict
        Dictionary of the form {`image_path`: `image_np_array`}
    """
    images_np_dict = Manager().dict()
    parallel_exec(
        func=load_image_into_numpy_array_image,
        elements=image_batch,
        image_path=lambda image_path: image_path,
        images_np_dict=images_np_dict)

    return {k: v for k, v in images_np_dict.items()}


def load_image_into_numpy_array_image(image_path, images_np_dict):
    """It loads an image into a numpy array from its file path and stores it in the
    `images_np_dict` dictionary in the form {`image_path`: `image_np_array`}

    Parameters
    ----------
    image_path : str
        Path of the image to load
    images_np_dict : dict
        Dictionary where the loaded image is stored
    """
    try:
        image = Image.open(os.path.abspath(image_path))
    except Exception as e:
        logger.exception(f"Exception while opening image {image_path}")
        image = None
    if image is not None:
        if len(image.getbands()) != 3:
            image = image.convert(mode='RGB')
        (im_width, im_height) = image.size
        image = np.array(image.getdata()).reshape((im_height, im_width, 3)).astype(np.uint8)
    images_np_dict[image_path] = image


def load_image_into_numpy_array_batch_keras(image_batch, target_size=None):
    """It takes a batch of image paths and in parallel loads them into numpy arrays, normalizes
    them and fits them in a dictionary

    Parameters
    ----------
    image_batch : list
        Batch of image paths
    target_size : tuple of ints, optional
        Target size of the image in the form (img_height, img_width). If None default to original
        size. By default None
    Returns
    -------
    dict
        Dictionary of the form {`image_path`: `image_np_array`}
    """
    images_np_dict = Manager().dict()
    parallel_exec(
        func=load_image_into_numpy_array_image_keras,
        elements=image_batch,
        image_path=lambda img: img,
        target_size=target_size,
        images_np_dict=images_np_dict)

    return {k: v for k, v in images_np_dict.items()}


def load_image_into_numpy_array_image_keras(image_path, target_size, images_np_dict):
    """Loads an image into PIL format, normalizes it and fits it in a dictionary

    Parameters
    ----------
    image_path : str
        Path of the image to load
    target_size : tuple of ints
        Target size of the image in the form (img_height, img_width). If None default to original
        size. By default None
    images_np_dict : dict
        Dictionary where the loaded image is stored
    """
    from tensorflow.keras.preprocessing import image as image_keras

    img = image_keras.load_img(image_path, target_size=target_size)
    img = image_keras.img_to_array(img) / 255
    images_np_dict[image_path] = np.expand_dims(img, axis=0)


def load_image_into_numpy_array(image_path):
    """Open image stored in `image_path`, convert it in RGB mode and load
    it into a np.uint8 array

    Parameters
    ----------
    image_path : str
        The path to a saved image.

    Returns
    -------
    np.array
        uint8 numpy array with shape (img_height, img_width, 3)
    """
    try:
        image = Image.open(os.path.abspath(image_path))
    except Exception as e:
        logger.exception(f"Exception while opening image {image_path}")
        return None
    if len(image.getbands()) != 3:
        image = image.convert(mode='RGB')
    (im_width, im_height) = image.size
    return np.array(image.getdata()).reshape(
        (im_height, im_width, 3)).astype(np.uint8)


def save_image(image_np, output_file):
    """Saves the image given in `image_np` under the given `output_file` filename

    Parameters
    ----------
    image_np : numpy.array
        uint8 numpy array with shape (img_height, img_width, 3)
    output_file : str
        Filename of the stored image
    """
    os.makedirs(os.path.dirname(output_file), exist_ok=True)
    image = Image.fromarray(image_np.astype(np.uint8))
    image.save(output_file)


def filter_corrupted_images(images):
    """Take a list of image paths and add only those that can be opened without raising an
    exception to the resulting list

    Parameters
    ----------
    images : list
        List of image paths

    Returns
    -------
    list
        List with the paths of the images that could be opened without raising an exception
    """
    new_images = []
    for image in images:
        try:
            img = Image.open(image)
            new_images.append(image)
        except:
            continue
    return new_images


def get_corrupted_images(images):
    """Take a list of image paths and add only those that can not be opened without raising an
    exception to the resulting list

    Parameters
    ----------
    images : list
        List of image paths

    Returns
    -------
    list
        List with the paths of the images that generate an exception when trying to be loaded
    """
    corrupted_images = Manager().list()
    parallel_exec(
        func=get_corrupted_image,
        elements=images,
        image=lambda img: img,
        corrupted_images=corrupted_images)
    return [x for x in corrupted_images]


def get_corrupted_image(image, corrupted_images):
    """Try to open an image and if an exception is thrown, add the image path to the list
    `corrupted_images`

    Parameters
    ----------
    image : str
        Path of the image
    corrupted_images : list
        List to which the image path is added in case of an exception thrown when trying to open it
    """
    try:
        Image.open(image)
    except:
        corrupted_images.append(image)


def draw_detections_of_image(item,
                             dets_df,
                             dest_dir,
                             score_thres_colors=None,
                             copy_img_if_has_no_dets=True,
                             include_score=False,
                             blur_people=False):
    """Function to draw bounding boxes of `dets_df` detections in `item`, and store the resulting
    image in `dest_dir`. Optionally applies a Gaussian filter to blur the region of person
    detections and preserve anonymity; in such cases, neither the bounding boxes nor the labels
    will be drawn on the image.

    Parameters
    ----------
    item : str
        Element to which the detection bounding boxes will be drawn
    dets_df : pd.DataFrame
        DataFrame containing the detections for `item`
    dest_dir : str
        Folder in which the resulting image will be stored
    score_thres_colors : float
        If provided, detections with `score ≥ score_thres_colors` will be drawn in green,
        and the others in orange. By default None
    copy_img_if_has_no_dets : bool, optional
        Whether or not to make a copy of those images without detections in `dets_df`,
        by default True
    include_score : bool, optional
        Whether or not to include the score in % in the label of the detection, by default False
    blur_people : bool, optional
        Whether or not to blur the detections of persons in order to preserve anonymity,
        by default False
    """
    if dest_dir is not None:
        os.makedirs(dest_dir, exist_ok=True)
        img_dest_path = os.path.join(dest_dir, os.path.basename(item))
    else:
        img_dest_path = item

    dets_item = dets_df[dets_df["item"] == item]

    if len(dets_item) > 0:
        bboxes = []
        labels = []
        scores = []
        for row in dets_item.to_dict('records'):
            bboxes.append([int(x) for x in row["bbox"].split(',')])
            labels.append(row["label"])
            if include_score:
                scores.append(row["score"])
        if 'color' in dets_df.columns:
            colors = [COLORS_MAP[color] for color in dets_item.color.values]
        elif score_thres_colors is not None:
            colors = [GREEN if score >= score_thres_colors else ORANGE
                      for score in dets_item.score.values]
        else:
            colors = GREEN
        draw_bboxes_in_image(
            item, img_dest_path, bboxes, labels, scores, colors, blur_people=blur_people
        )
    elif copy_img_if_has_no_dets and item != img_dest_path:
        copy2(item, img_dest_path)


def draw_bboxes_in_image(img_orig_path,
                         img_dest_path,
                         bboxes,
                         labels,
                         scores,
                         colors=GREEN,
                         thickness=None,
                         blur_people=False,
                         blur_people_factor=7.0,
                         person_label='person'):
    """Function that draws bounding boxes in the image `img_orig_path` and saves the resulting
    image in `img_dest_path`.

    Parameters
    ----------
    img_orig_path : str
        Path of the original image
    img_dest_path : str
        Path of the resulting image
    bboxes : list
        List with the coordinates of the bounding boxes in format [x, y, width, height]
    labels : list
        List with the labels for the bounding boxes
    scores : list
        List with the scores of the detections. It must have the same lenght of bboxes
        If empty list, scores won't be showed.
    colors : list of tuples or tuple, optional
        Colors of the bounding boxes lines, by default GREEN
    thickness : int, optional
        Width of the bounding box lines. If None, it will be determined by the size of the image.
        By default None
    blur_people : bool, optional
        Whether or not to blur the detections of persons in order to preserve anonymity,
        by default False
    blur_people_factor : float, optional
        The 'blurriness' applied to each human. Lower values are more obscured.
        Recommended values are between 3 and 7, by default 7.0
    person_label : str, optional
        Label that people detections have, by default 'person'
    """
    colors = colors if type(colors) is list else [colors] * len(bboxes)
    assert len(colors) == len(bboxes), "Parameters colors and bboxes must be of the same lenght"

    image = cv2.imread(img_orig_path)

    height = image.shape[0]
    width = image.shape[1]

    FONT = cv2.FONT_HERSHEY_SIMPLEX
    LINE_TYPE = 2

    if thickness is None:
        thickness = math.ceil(width / 400)
    THICKNESS = math.ceil(width / 400)

    if width <= 800:
        FONT_SCALE = 0.5
    elif width <= 1200:
        FONT_SCALE = 0.8
    else:
        FONT_SCALE = 1.

    for i, bbox in enumerate(bboxes):
        label = labels[i]
        [x, y, width, height] = bbox

        if label == person_label and blur_people:
            person_data = image[y:y+height, x:x+width]
            person_data = anonymize_image(person_data, factor=blur_people_factor)
            image[y:y+height, x:x+width] = person_data
            continue

        cv2.line(image, (x, y), (x+width, y), color=colors[i], thickness=thickness)
        cv2.line(image, (x+width, y), (x+width, y+height), color=colors[i], thickness=thickness)
        cv2.line(image, (x+width, y+height), (x, y+height), color=colors[i], thickness=thickness)
        cv2.line(image, (x, y+height), (x, y), color=colors[i], thickness=thickness)

        label = label.capitalize()
        if scores:
            label += f' {int(scores[i] * 100)} %'

        (lbl_width, lbl_height), _ = cv2.getTextSize(label, FONT, FONT_SCALE, THICKNESS)
        cv2.rectangle(image, (x, y),  (x + lbl_width, y - lbl_height), colors[i], -1)
        cv2.putText(image, label, (x, y), FONT,  FONT_SCALE, BLACK, LINE_TYPE)

    cv2.imwrite(img_dest_path, image)


def anonymize_image(image, factor=7.0):
    """Blurs an image using a Gaussian filter.

    Parameters
    ----------
    image : np.array
        Input image; the image can have any number of channels, which are processed independently.
    factor : float, optional
        The 'blurriness' applied to each human. Lower values are more obscured.
        Recommended values are between 3 and 7, by default 7.0

    Returns
    -------
    np.array
        Output blurred image of n-dimensional array.
    """
    # automatically determine the size of the blurring kernel based
    # on the spatial dimensions of the input image
    (h, w) = image.shape[:2]
    kW = int(w / factor)
    kH = int(h / factor)
    # ensure the width of the kernel is odd
    if kW % 2 == 0:
        kW += 1
    # ensure the height of the kernel is odd
    if kH % 2 == 0:
        kH += 1

    # apply a Gaussian blur to the input image using our computed
    # kernel size
    return cv2.GaussianBlur(image, (kW, kH), 0)


def movement_detection_in_seqs(images_df,
                               dets_df,
                               mov_detection_method,
                               seq_id_to_valid_dets_ids,
                               seq_id,
                               seqs_field='seq_id',
                               frames_field='frame_num',
                               window_size=None,
                               accAvg=0.35,
                               threshT=40,
                               kernel_size=(9, 9)):
    """Function that performs motion detection on the image sequence `seq_id`, whose images can be
    obtained by filtering the image DataFrame `images_df` by the `seqs_field` field, and whose
    detections can be obtained by filtering the DataFrame `dets_df` by the `seqs_field` field.
    Motion detection is performed by taking each of the sequence detections, and cropping the
    region of its bounding box across the frames of the sequence (or optionally, just the frames
    of a window of size `window_size` around the frame to which the detection belongs),
    and applying the `mov_detection_method` method on those 'crops across the sequence'.
    The detections of the `seq_id` sequence that happen to have movement will be added to the
    `seq_id_to_valid_dets_ids` dictionary of the form:
    {`seq_id`: [`valid_det_id_1`, `valid_det_id_2`, ...]}

    Parameters
    ----------
    images_df : pd.DataFrame
        DataFrame containing all the images of the dataset being analyzed, and must contain the
        `seqs_field` column to perform the filter by the sequence `seq_id`.
    dets_df : pd.DataFrame
        DataFrame containing all the detections of the images of the dataset being analyzed,
        and must contain the `seqs_field` column to perform the filter by the sequence `seq_id`.
    mov_detection_method : str
        Method applied to perform motion detection; it can be: "Acc" (Background Subtraction)
        or "MoG" (Mixture of Gaussian).
    seq_id_to_valid_dets_ids : dict
        Dictionary in which a list will be stored with the ids of the detections that have movement
        in the sequence `seq_id`.
    seq_id : str
        Identifier of the sequence to be analyzed
    seqs_field : str, optional
        Name of the column in `images_df` and `dets_df` by which the `seq_id` sequence will be
        filtered, by default `seq_id`.
    frames_field : str, optional
        Name of the column in `dets_df` that contains the sequential value of the frame within
        the sequence, by default 'frame_num'.
    window_size : int, optional
        Size of the window around which the movement detection will be performed on the detections
        of each frame. If None, the analysis will be performed on all the frames of the sequence.
        By default None
    accAvg : float, optional
        Weight of the input image sent to the `cv2.accumulateWeighted` function when
        `mov_detection_method` == 'Acc', by default 0.35
    threshT : int, optional
        Threshold value to be used when `sub_method` == 'Acc', by default 40
    kernel_size : tuple, optional
        Kernel size for morphological operations, by default (9, 9)
    """
    imgs_of_seq = images_df[images_df[seqs_field] == seq_id]
    dets_of_seq = dets_df[dets_df[seqs_field] == seq_id]

    if len(imgs_of_seq) > 1:

        if window_size is not None:
            windows_seqs = []
            seq_idxs = [x for x in range(len(imgs_of_seq))]
            for i in range(len(imgs_of_seq) - window_size + 1):
                windows_seqs.append(seq_idxs[i: i + window_size])
            half_window = window_size // 2

        valid_dets_ids = []
        for det_of_seq in dets_of_seq.to_dict('records'):
            [x, y, width, height] = [int(x) for x in det_of_seq["bbox"].split(",")]
            crop_of_det = cv2.imread(det_of_seq["item"])[y: y + height, x: x + width]

            if window_size is not None and len(imgs_of_seq) > window_size:
                frame_idx = det_of_seq[frames_field] - 1
                if frame_idx - half_window <= 0:
                    win_idx = 0
                elif frame_idx - half_window >= len(windows_seqs):
                    win_idx = -1
                else:
                    win_idx = frame_idx - half_window
                try:
                    items = imgs_of_seq.iloc[windows_seqs[win_idx]]["item"].values
                except Exception:
                    logger.error(f"Error with the sequence {seq_id} and item {det_of_seq['item']}")
                    continue
            else:
                items = imgs_of_seq["item"].values

            crops_over_seq = [cv2.imread(item)[y: y + height, x: x + width] for item in items]

            if mov_detection_method == "Acc":
                try:
                    running_avg_image = np.float32(crops_over_seq[0])
                    for crop_of_img in crops_over_seq[1:]:
                        color_image = cv2.GaussianBlur(crop_of_img, (3, 3), 0)
                        cv2.accumulateWeighted(color_image, running_avg_image, accAvg)
                    running_avg_in_display_color_depth = cv2.convertScaleAbs(running_avg_image)
                    difference = cv2.absdiff(crop_of_det, running_avg_in_display_color_depth)
                    grey_image = cv2.cvtColor(difference, cv2.COLOR_BGR2GRAY)
                    _, grey_image = cv2.threshold(grey_image, threshT, 255, cv2.THRESH_BINARY)
                except Exception as ex:
                    logger.error(f"An error ocurred while processing the sequence {seq_id}: {ex}")
                    continue
            kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, kernel_size)
            new_grey_image = cv2.morphologyEx(grey_image, cv2.MORPH_OPEN, kernel)
            contours, _ = cv2.findContours(new_grey_image, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
            if len(contours) > 0:
                valid_dets_ids.append(det_of_seq["id"])
        seq_id_to_valid_dets_ids[seq_id] = valid_dets_ids
